#!/usr/bin/env python3
import argparse
import sys

parser = argparse.ArgumentParser( description = 'This script converts the output matrices of ADMIXTURE (P,Q) into input matrices of nemeco and qpas in Ohana. This is necessary because the P matrix needs to be transposed, and the Q matrix rows need to be normalized to sum to 1.' )
parser.add_argument("ped", type=str, help="ped input used for nemeco (convert ped2dgm)")
parser.add_argument('Q', type=str, help="Q outputted by ADMIXTURE after compatibilizng")
parser.add_argument('q_with_ids_file', type=str, help="Output file containing individual ids, population ids, and cluster membership proportions")

args = parser.parse_args()

# Parse ped
individuals=[]
with open(args.ped, 'r') as handle:
    for line in handle:
        fid = line.rstrip().split()[0]
        iid = line.rstrip().split()[1]
        individuals.append([fid,iid])
# Parse Q
q_rows=[]
with open(args.Q , 'r' ) as handle:
    next(handle)
    for line in handle:
        q_rows.append(line.rstrip().split())

# Check if same length
if len(individuals) != len(q_rows):
    sys.exit("ERROR: Different number of rows, check files.")

with open(args.q_with_ids_file, 'w' ) as handle:
    for i,j in zip(individuals, q_rows):
        newline = i
        newline.extend(j)
        handle.write(   ' '.join(newline) + '\n' )
