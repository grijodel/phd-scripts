#!/usr/bin/env python3
import sys
from random import sample
from collections import defaultdict
import argparse

parser = argparse.ArgumentParser( description = '' )
parser.add_argument("fam", type=str, help="")
parser.add_argument("number_of_individuals", type=str, help="")
args = parser.parse_args()
fam=args.fam
number_of_individuals=int(args.number_of_individuals)



group_dict = defaultdict(list)
with open(fam,'r') as handle:
    for line in handle:
        fid,iid,a,b,c,d = line.rstrip().split()
        group_dict[fid].append(iid)
        

for fid in group_dict:
    # Check if number of individuals is not lower than sample
    if len(group_dict[fid]) < number_of_individuals:
        sys.exit("Number of individuals per population is higher than individuals in {0}".format(fid))
    else:
        selected_samples = sample( group_dict[fid], number_of_individuals )

        for i in selected_samples:
            print("{0} {1}".format(fid,i))


