---
title: "F-statistics notebook"
author: "Gaston Rijo De León"
format:
  html:
    embed-resources: true
    toc: true
    toc-depth: 2
execute: 
  echo: true
  warning: false
  message: false
  eval: true
date:  today
date-format: 'full'
engine: knitr
---

# Setup

```{r}
#| eval: true
library(tidyverse)
library(janitor)
library(plotly)
library(MetBrewer)
library(qqplotr)
library(here)
library(glue)
library(ggrepel)
library(admixtools)

i_am("00_SCRIPTS/01_working_notebook_repository/2023-08-08_f_statistics/01_f_statistics.qmd")
theme_set(theme_bw())
pal <- readxl::read_excel("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/population_colors.xlsx",
                          sheet = 4) 
ind_labels <- read_delim("/pasteur/helix/projects/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/04_Quality_Control/labels.tsv",
                         col_names = c("individual", "identifier", "cohort"))
'%!in%' <- function(x,y)!('%in%'(x,y))
```



```{r}
prefix = "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/HC_MAF_PRUNED"
my_f2_dir = here("01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/fstats")
```



```{r}
#| eval: false
extract_f2(prefix, my_f2_dir, overwrite=TRUE)
```


```{r}
f2_blocks = f2_from_precomp(my_f2_dir)
```


# Using $f-2$ to inspect population structure
```{r}
populations <- pal %>%
  pull(identifier)
pop1 <- populations
pop2 <- populations 
pairwise_f2 <- f2(f2_blocks, pop1, pop2) %>%
            mutate(pop1=factor(pop1,levels=populations)) %>%
            mutate(pop2=factor(pop2,levels=populations)) %>%
            mutate(est = case_when(pop1 == pop2 ~ NA,
                                 pop1 != pop2 ~ est))
```

```{r}
p <- pairwise_f2 %>%
  filter(pop1 %in% oceanian_populations,
         pop2 %in% oceanian_populations) %>%
  ggplot(aes(x=pop1,y=pop2)) + 
    geom_tile(aes(fill=est)) +
    scale_fill_viridis_c() +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
  coord_fixed()
p
```


# Using $f-3$ outgroup to check the closest reference population


Compute all pairwise $f_3$ statistics using Yoruba or Mbuti as outgroup.

```{r}
outgroups <- c("Mbuti","Yoruba") 
populations <- pal %>%
  filter(identifier %!in% outgroups) %>%
  pull(identifier)

pop1 <- outgroups[2]
pop2 <- populations
pop3 <- populations 

pairwise_f3 <- f3(f2_blocks, pop1, pop2, pop3) %>%
            mutate(pop2=factor(pop2,levels=populations)) %>%
            mutate(pop3=factor(pop3,levels=populations)) %>%
            mutate(est = case_when(pop2 == pop3 ~ NA,
                                 pop2 != pop3 ~ est))
```





Define useful vector of population names.

```{r}
### Oceanian
oceanian_group3<-c("Santa_Cruz",
                   "ni-Vanuatu",
                   "Papuan_Highlander",
                   "Bismarck_Islander",
                   "Papuan_Sepik",
                   "Bougainville",
                   "Solomon_Islander",
                   "RenBell",
                   "Tikopia",
                   "Ontong_Java",
                          "Fiji_Islander",
                          "Tonga_Islander",
                          "Samoa_Islander",
                          "Cook_Islander",
                          "Austral_Islander",
                          "Society_Islander",
                          "Tuamotu_Islander",
                          "Marquesas_Islander",
                          "Gambier_Islander"
                          )
oceanian_populations <- pal %>%
                          filter(group3 %in% oceanian_group3 ) %>%
                          pull(identifier)






### Papuan-like
papuan_group3 <- c("Santa_Cruz",
                   "ni-Vanuatu",
                   "Papuan",
                   "Papuan_Highlander",
                   "Bismarck_Islander",
                   "Papuan_Sepik",
                   "Bougainville",
                   "Solomon_Islander")
papuan_populations <- pal %>%
                          filter(group3 %in% papuan_group3 ) %>%
                          pull(identifier)


### Polynesian
polynesian_group3 <-  c("Fiji_Islander",
                          "Tonga_Islander",
                          "Samoa_Islander",
                          "Cook_Islander",
                          "Austral_Islander",
                          "Society_Islander",
                          "Tuamotu_Islander",
                          "Marquesas_Islander",
                          "Gambier_Islander")
                          
polynesian_populations <- pal %>%
                          filter(group3 %in% polynesian_group3 ) %>%
                          pull(identifier)

### Polynesian Outliers
polynesian_outlier_group3 <-  c("RenBell",
                          "Tikopia",
                          "Ontong_Java")
polynesian_outlier_populations <- pal %>%
                                  filter(group3 %in% polynesian_outlier_group3 ) %>%
                                  pull(identifier)
### All polynesians

                          
all_polynesian_populations <- pal %>%
                          filter(identifier %in% c(polynesian_populations,polynesian_outlier_populations ) ) %>%
                          pull(identifier)


### European
european_populations <- c("French","Orcadian")

### American
american_group1 <- c("South_American","North_American")
american_populations <- pal %>%
                          filter(group1 %in% american_group1 ) %>%
                          pull(identifier)


### East Asian
asian_populations <- pal %>%
  filter(region %in% c("East_Asia","Philippines", "Taiwan")) %>%
  pull(identifier)


```

Look at $f_3$ between oceanian populations.


```{r}
p <- pairwise_f3 %>%
  filter(pop2 %in% oceanian_populations,
         pop3 %in% oceanian_populations) %>%
  ggplot(aes(x=pop2,y=pop3)) + 
    geom_tile(aes(fill=est)) +
    scale_fill_viridis_c() +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
  coord_fixed()

p
ggsave("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/images/f3_oceanian.png",p,scale = 3, dpi=400)

```

Zoom in on Polynesian populations 



```{r}
p <- pairwise_f3 %>%
  filter(pop2 %in% oceanian_populations,
         pop3 %in% c(polynesian_outlier_populations,polynesian_populations),
         pop2 %!in% c(polynesian_outlier_populations,polynesian_populations),
         ) %>%
  ggplot(aes(x=pop3,y=pop2)) + 
    geom_tile(aes(fill=est)) +
    scale_fill_viridis_c() +
  scale_x_discrete(guide = guide_axis(angle = 90))  +
  coord_fixed() 
p

ggsave("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/images/f3_papuan_poly.svg",p)
```




```{r}
p <- pairwise_f3 %>%
  filter(pop2 %in% polynesian_populations,
         pop3 %in% polynesian_populations) %>%
  ggplot(aes(x=pop2,y=pop3)) + 
    geom_tile(aes(fill=est)) +
    scale_fill_viridis_c() +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
  coord_fixed()
p
ggsave("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/images/f3_matrix_poly.svg",p)
```

Look into more detail with standard errors.

```{r}
p <- pairwise_f3 %>%
  filter(pop3 %in% polynesian_populations) %>%
  filter(pop2 %in% papuan_populations) %>%
  ggplot(aes(y=pop2,x=est, fill=pop2,color=pop2) ) +
           geom_point(size=1) +
            geom_errorbar(aes(xmin=est-3*se, xmax=est+3*se), width=.1)   +
          facet_wrap(~pop3, nrow = 2) +
   theme(legend.position="none") 
p
#ggsave(filename = "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/images/papuan_f3.svg",plot = p, width = 12, height = 8)
```


Look at European ancestry.




```{r}
pairwise_f3 %>%
  filter(pop2 %in% european_populations,
         pop3 %in% polynesian_populations) %>%
  ggplot(aes(x=pop2,y=pop3)) + 
    geom_tile(aes(fill=est)) +
    scale_fill_viridis_c() +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
  coord_fixed()
```

```{r}
pairwise_f3 %>%
  filter(pop3 %in% polynesian_populations) %>%
  filter(pop2 %in% european_populations,
           pop2 %!in% polynesian_populations) %>%
  ggplot(aes(y=pop2,x=est, fill=pop2,color=pop2) ) +
           geom_point(size=1) +
            geom_errorbar(aes(xmin=est-3*se, xmax=est+3*se), width=.1)   +
          facet_wrap(~pop3) +
   theme(legend.position="none")
  
```



Look at Native American ancestry.

```{r}
pairwise_f3 %>%
  filter(pop2 %in% american_populations,
         pop3 %in% polynesian_populations) %>%
  ggplot(aes(x=pop2,y=pop3)) + 
    geom_tile(aes(fill=est)) +
    scale_fill_viridis_c() +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
  coord_fixed()
```


```{r}
pairwise_f3 %>%
  filter(pop3 %in% polynesian_populations) %>%
  filter(pop2 %in% american_populations,
           pop2 %!in% polynesian_populations) %>%
  ggplot(aes(y=pop2,x=est, fill=pop2,color=pop2) ) +
           geom_point(size=1) +
            geom_errorbar(aes(xmin=est-3*se, xmax=est+3*se), width=.1)   +
          facet_wrap(~pop3) +
   theme(legend.position="none")
  
```

Look at east asian ancestry



```{r}
pairwise_f3 %>%
  filter(pop2 %in% asian_populations,
         pop3 %in% polynesian_populations) %>%
  ggplot(aes(x=pop2,y=pop3)) + 
    geom_tile(aes(fill=est)) +
    scale_fill_viridis_c() +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
  coord_fixed()
```



```{r}
pairwise_f3 %>%
  filter(pop3 %in% polynesian_populations) %>%
  filter(pop2 %in% asian_populations,
           pop2 %!in% polynesian_populations) %>%
  ggplot(aes(y=pop2,x=est, fill=pop2,color=pop2) ) +
           geom_point(size=1) +
            geom_errorbar(aes(xmin=est-3*se, xmax=est+3*se), width=.1)   +
          facet_wrap(~pop3) +
   theme(legend.position="none")
```


# Using $f_4$ statistics to disentangle admixture




```{r}
oceanian_group3<-c("ni-Vanuatu",
                   "Papuan_Highlands",
                   "Bougainville",
                   "Vella_Lavella",
                          "RenBell",
                          "Tikopia",
                          "Fiji_Islander",
                          "Tonga_Islander",
                          "Samoa_Islander",
                          "Cook_Islander",
                          "Australes_Islander",
                          "Society_Islander",
                          "Tuamotu_Islander",
                          "Marquesas_Islander",
                          "Gambier_Islander"
                          )
oceanian_populations <- pal %>%
                          filter(group3 %in% polynesian_group3 ) %>%
                          pull(identifier)


P_1 <- "Pima"
P_3 <- "Paiwan"
P_4 <- "Yoruba"

treeness <- f4(f2_blocks,P_1,oceanian_populations,P_3,P_4) %>%
  select(pop2,est,se) %>%
  rename(treeness_est='est',treeness_se='se') %>%
  mutate(pop2=factor(pop2,levels=oceanian_populations))



selected_palettes <- pal %>%
  filter(group3 %in% oceanian_populations) %>%
  select(group3,color) %>%
  distinct() %>%
  mutate(group3=factor(group3,levels=oceanian_populations)) %>%
  arrange(group3)
  
y_lab <- paste0("f4(",P_1,",X",";",P_3,",",P_4,")")
treeness %>%
  ggplot(aes(x=pop2,y=treeness_est,color=pop2,fill=pop2)) +
  geom_point(size=3) +
  geom_errorbar(aes(ymin=treeness_est-3*treeness_se, ymax=treeness_est+3*treeness_se), width=.1) +
  geom_hline(yintercept = 0, color='red', linetype='dashed') +
  xlab("") +
  theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust=1),legend.position = "none") +
  #scale_fill_manual(values = selected_palettes$color) +
  #scale_color_manual(values = selected_palettes$color) +
  ylab(y_lab)
  
 
```


```{r}
A_1 <- "Paiwan"
A_2 <- "Paiwan"

B_2 <- "Dai"
B_1 <- "Papuan_Sepik"

C_1 <- all_polynesian_populations
C_2 <- all_polynesian_populations

D_1 <- "Yoruba"
D_2 <- "Yoruba"

f4_1 <- f4(f2_blocks,
           A_1,B_1,C_1,D_1) %>%
  select(pop3,est,se) %>%
  rename(f4_1_est='est',f4_1_se='se')

f4_2 <- f4(f2_blocks,
           A_2,B_2,C_2,D_2) %>%
  select(pop3,est,se) %>%
  rename(f4_2_est='est',f4_2_se='se')

df <- full_join(f4_1,f4_2, by='pop3') %>%
  mutate(pop3=factor(pop3,levels = all_polynesian_populations))
```


```{r}
color_palette <- c('#034732', # OJ
                   '#008148','#008148', # renbell
                   '#B5DDA4', # tikopia
                   '#6B2466', # fiji
                   '#CA61C3','#FAB3A9', # WP
                   '#2708a0', # ATIU
                   '#C7FFED', # TUAMOTU
                   '#19647E','#19647E', # SOCIETY
                   '#68C5DB','#68C5DB', # AUSTRAL
                   '#80b1d3','#80b1d3', #MARQUESAS
                   '#016FB9' # GAMBIER
                   )


x_lab <- paste0("f4(",A_1,", ",B_1,";","X",", ",D_1,")")
y_lab <- paste0("f4(",A_2,", ",B_2,";","X",", ",D_1,")")

p <- ggplot(df,
          aes(x=f4_1_est,y=f4_2_est,
              color=pop3,label=pop3)) +
          geom_point()+ 
          geom_errorbar(aes(xmin=f4_1_est-f4_1_se, xmax=f4_1_est+f4_1_se), width=.00001) +
          geom_errorbar(aes(ymin=f4_2_est-f4_2_se, ymax=f4_2_est+f4_2_se), width=.0001) +
  #geom_text_repel(force=100) +
  scale_color_manual(values = color_palette) +
      xlab(x_lab) +
      ylab(y_lab) 
      #coord_fixed() 
      #theme(legend.position = "NONE")


p

ggsave("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/images/papuan_f4_corr.svg")
#ggplotly(p)
```






```{r}
A_1 <- "Paiwan"
A_2 <- "Paiwan"

B_2 <- "Dai"
B_1 <- "Mamusi"

C_1 <- c(all_polynesian_populations,papuan_populations)
C_2 <- c(all_polynesian_populations,papuan_populations)

D_1 <- "Yoruba"
D_2 <- "Yoruba"

f4_1 <- f4(f2_blocks,
           A_1,B_1,C_1,D_1) %>%
  select(pop3,est,se) %>%
  rename(f4_1_est='est',f4_1_se='se')

f4_2 <- f4(f2_blocks,
           A_2,B_2,C_2,D_2) %>%
  select(pop3,est,se) %>%
  rename(f4_2_est='est',f4_2_se='se')

df <- full_join(f4_1,f4_2, by='pop3') %>%
  mutate(pop3=factor(pop3,levels = C_1))

write_delim(df, file = "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/fstats/f4_corr.tsv")

```


```{r}
x_lab <- paste0("f4(",A_1,", ",B_1,";","X",", ",D_1,")")
y_lab <- paste0("f4(",A_2,", ",B_2,";","X",", ",D_1,")")
p <- ggplot(df,
          aes(x=f4_1_est,y=f4_2_est,
              color=pop3,label=pop3)) +
          geom_point()+ 
          geom_errorbar(aes(xmin=f4_1_est-f4_1_se, xmax=f4_1_est+f4_1_se), width=.00001) +
          geom_errorbar(aes(ymin=f4_2_est-f4_2_se, ymax=f4_2_est+f4_2_se), width=.0001) +
          #geom_text_repel(force=100) +
          xlab(x_lab) +
          ylab(y_lab) 
          #coord_fixed() 
          #theme(legend.position = "NONE")
ggplotly(p)
```



```{r}

```
























f4 ratio for papuana admixture



```{r}
A_1 <- all_polynesian_populations
B_1 <- "Papuan_Highlands"
C_1 <- "Paiwan"
D_1 <- "Papuan_Sepik"
O_1 <- "Yoruba"

populations <- data.frame(rep(B_1, length(A_1)),
                            rep(O_1, length(A_1)),
                            A_1,
                            rep(C_1, length(A_1)),
                            rep(D_1, length(A_1))                          ) %>% as.matrix()
colnames(populations) <- c("B_1","O_1","A_1","C_1","D_1")

f4_ratio_1 <- qpf4ratio(data = f2_blocks,
          pops = populations,
          boot=FALSE,
          verbose = TRUE) %>% 
  mutate(pop3 = factor(pop3,levels=A_1))
```


```{r}
color_palette <- c('#034732', # OJ
                   '#008148','#008148', # RENBELL
                   '#B5DDA4', # TIKOPIA 
                   '#6B2466', # FIJI
                   '#CA61C3','#FAB3A9', # WP
                   '#2708a0', # ATIU
                   '#C7FFED', # TUAMOTU
                   '#19647E','#19647E', # SOCIETY
                   '#68C5DB','#68C5DB', # AUSTRAL
                   '#80b1d3','#80b1d3', #MARQUESAS
                   '#016FB9' # GAMBIER
                   )

p <- f4_ratio_1 %>%
  ggplot(aes(x=pop3,y=alpha,color=pop3)) +
  geom_point(size=3) +
  geom_errorbar(aes(ymin=alpha-3*se, ymax=alpha+3*se), width=.1) +
  geom_hline(yintercept = 0, color='red', linetype='dashed') +
  xlab("") +
  theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust=1),legend.position = "none") +
  scale_color_manual(values = color_palette) +
  ylab("alpha") +
  ylim(0,1)
p
```



```{r}
ggsave(plot = p,
       filename = "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/images/papuan_f4_ratio.svg",height = 4, width = 10)
```

```{r}
f4_ratio_1  %>%
  write_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/f4_ratio_papuan.csv")
```







## Investigating Austronesian-Papuan admixture


```{r}
outgroup <- "Yoruba"
A_1 <- "Paiwan"
A_2 <- c(polynesian_outlier_populations,polynesian_populations)
B_1 <- papuan_populations
papuan_f4 <- f4(f2_blocks,pop1 = A_1,pop2 = A_2,pop3=B_1,pop4=outgroup)  %>%
  mutate(pop2=factor(pop2,levels = A_2),
         pop3=factor(pop3,levels = B_1),)
papuan_f4 %>%
  ggplot(aes(x=pop2,y=pop3)) + 
    geom_tile(aes(fill=z)) +
    scale_fill_gradient2( midpoint = 0,low = "#b2182b", high="#2166ac") +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
  coord_fixed()

ggsave("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/images/papuan_f4_allp.svg")
```



The problem with this is that we don't know if the difference is due to recent admixture with east asians.
What if we select populations that don't show recent East Asian admixture?




```{r}
outgroup <- "Yoruba"
A_1 <- "Paiwan"
A_2 <- c(polynesian_outlier_populations,c("Fiji","Tonga","Western_Samoa","Atiu","Rangiroa","Raivavae"))
B_1 <- papuan_populations
papuan_f4 <- f4(f2_blocks,pop1 = A_1,pop2 = A_2,pop3=B_1,pop4=outgroup)  %>%
  mutate(pop2=factor(pop2,levels = A_2),
         pop3=factor(pop3,levels = B_1),)
p <- papuan_f4 %>%
  ggplot(aes(x=pop2,y=pop3)) + 
    geom_tile(aes(fill=z)) +
    scale_fill_gradient2( midpoint = 0,low = "#b2182b", high="#2166ac") +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
  coord_fixed()
p
ggsave( "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/images/papuan_f4_noea.svg",p)
```

```{r}
p <- papuan_f4 %>%
  #filter(pop2=="Atiu") %>%
    ggplot(aes(y=pop3,x=est, fill=pop3,color=pop3) ) +
           geom_point(size=1) +
            geom_errorbar(aes(xmin=est-3*se, xmax=est+3*se), width=.1) +
            facet_wrap(~pop2) +
   theme(legend.position="none")
p
ggsave( "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/images/papuan_f4_atiu.svg",p,width = 4,height = 8)
```

Let's take recent East Asian admixture into account



```{r}
outgroup <- "Yoruba"
A_1 <- "Paiwan"
A_2 <- c(polynesian_outlier_populations,polynesian_populations)
B_1 <- asian_populations
asian_f4 <- f4(f2_blocks,pop1 = A_1,pop2 = A_2,pop3=B_1,pop4=outgroup)  %>%
  mutate(pop2=factor(pop2,levels = A_2),
         pop3=factor(pop3,levels = B_1),)
asian_f4 %>%
  ggplot(aes(x=pop2,y=pop3)) + 
    geom_tile(aes(fill=z)) +
    scale_fill_gradient2( midpoint = 0,low = "#b2182b", high="#2166ac") +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
  coord_fixed()


```


```{r}
p <- asian_f4 %>%
  #filter(pop2=="Atiu") %>%
    ggplot(aes(y=pop3,x=est, fill=pop3,color=pop3) ) +
           geom_point(size=1) +
            geom_errorbar(aes(xmin=est-3*se, xmax=est+3*se), width=.1) +
            facet_wrap(~pop2) +
   theme(legend.position="none")
p

```





## Estimating the extent of Polynesian admixture in source populations



We have been using Mamusi as a source population for papuan ancestry, but Etienne says that it is admixed. To what extent are Mamusi admixed? Should we be using another source like Papuan Highlander?

```{r}

```






































