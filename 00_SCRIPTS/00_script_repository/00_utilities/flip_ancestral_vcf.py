#!/usr/bin/env python3
from cyvcf2 import VCF, Writer
import pysam
import sys
import os
import subprocess

# Dictionary to map genotype changes, including phasing
GENOTYPE_MAP = {
    (0, 0, True): (1, 1, True),   # Phased homozygous reference becomes phased homozygous alternate
    (0, 0, False): (1, 1, False), # Unphased homozygous reference becomes unphased homozygous alternate
    (0, 1, True): (1, 0, True),   # Phased heterozygous (0|1) becomes (1|0)
    (1, 0, True): (0, 1, True),   # Phased heterozygous (1|0) becomes (0|1)
    (0, 1, False): (0, 1, False), # Unphased heterozygous stays the same
    (1, 1, True): (0, 0, True),   # Phased homozygous alternate becomes phased homozygous reference
    (1, 1, False): (0, 0, False), # Unphased homozygous alternate becomes unphased homozygous reference
    (-1, -1, True): (-1, -1, True),   # Phased missing data stays missing
    (-1, -1, False): (-1, -1, False), # Unphased missing data stays missing
}

def flip_alleles(vcf_in, vcf_out):
    # Open input VCF (automatically handles bgzip)
    vcf = VCF(vcf_in)

    # Create a temporary uncompressed output file
    temp_out = vcf_out.rstrip('.gz') + '.temp'

    # Create a new VCF writer
    w = Writer(temp_out, vcf)

    for variant in vcf:
        aa = variant.INFO.get('AA')

        if aa and aa == variant.ALT[0]:
            # Flip REF and ALT
            new_ref = variant.ALT[0]
            new_alt = variant.REF

            # Update variant
            variant.REF = new_ref
            variant.ALT = [new_alt]

            # Update genotypes
            new_gts = []
            for gt in variant.genotypes:
                old_gt = (gt[0], gt[1], gt[2] == 1)  # gt[2] == 1 means phased
                new_gt = GENOTYPE_MAP.get(old_gt, old_gt)
                new_gts.append(list(new_gt))
            variant.genotypes = new_gts

        # Write the variant
        w.write_record(variant)

    # Close the writer
    w.close()

    # Compress the output file using bgzip
    try:
        subprocess.run(['bgzip', '-f', temp_out], check=True)
    except subprocess.CalledProcessError:
        print("Error: bgzip compression failed. Make sure bgzip is installed and in your PATH.")
        sys.exit(1)

    # Rename the bgzipped file
    os.rename(temp_out + '.gz', vcf_out)

    # Index the bgzipped file
    pysam.tabix_index(vcf_out, preset="vcf", force=True)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python script.py input.vcf.gz output.vcf.gz")
        sys.exit(1)

    input_vcf = sys.argv[1]
    output_vcf = sys.argv[2]

    flip_alleles(input_vcf, output_vcf)
    print(f"Processed VCF written to {output_vcf}")
    print(f"Index file created: {output_vcf}.tbi")
