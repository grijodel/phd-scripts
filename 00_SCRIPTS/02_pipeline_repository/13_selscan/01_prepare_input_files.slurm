#!/usr/bin/env bash
#SBATCH -J PREP_SELSCAN 
#SBATCH -o logs/PREP_SELSCAN_%A_%a.out -e logs/PREP_SELSCAN_%A_%a.err 
#SBATCH --mem=10000
#SBATCH --array=0-21
#SBATCH -c 10
#SBATCH --partition=geh,common
module load R/4.1.0
module load plink
module load tabix
module load vcftools
module load samtools
# Activate Anaconda work environment for OpenDrift
source /pasteur/appa/homes/grijodel/.bashrc
conda activate developer
chromosomes=(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 "X")
chromosome=${chromosomes[$SLURM_ARRAY_TASK_ID]}
POLARISED_HAPS=/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/02_relate_pipeline/02_input_prep/Chr${chromosome}_processed.haps.gz
MAPFILE=/pasteur/helix/projects/IGSR/1KG/1KG_Genetic_Maps/SHAPEIT_Format/b37/chr${chromosome}.b37.gmap.gz
VCF=/pasteur/helix/projects/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/05_Phasing/03_Phase_Scaffold/shapeit4Phased_scaffold_chr${chromosome}.vcf.gz
OUTPUT_DIR=/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/07_selscan/01_prepare_input_files/
mkdir -p ${OUTPUT_DIR}/chr${chromosome}
# Create new VCF files with derived/ancestral
zcat $POLARISED_HAPS | \
    awk '{print $1,$3,$4}' > \
    ${OUTPUT_DIR}/chr${chromosome}/ancestral_alleles_Chr${chromosome}.txt
# Generate MAP files
### generate bim file
plink2 --vcf $VCF \
    --make-bed \
    --out ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}
rm ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.bed ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.fam 
### interpolate recombination map
interpolate_recombination_map.R ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.bim \
    ${chromosome}
### generate anestral tables
generate_ancestral_table.R ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.bim \
    ${OUTPUT_DIR}/chr${chromosome}/ancestral_alleles_Chr${chromosome}.txt \
    ${OUTPUT_DIR}/chr${chromosome}/ancestral_table_Chr${chromosome}.tab
bgzip ${OUTPUT_DIR}/chr${chromosome}/ancestral_table_Chr${chromosome}.tab
tabix \
    -s 1 \
    -b 2 \
    -e 2 \
    ${OUTPUT_DIR}/chr${chromosome}/ancestral_table_Chr${chromosome}.tab.gz
### get only positions for available ancestral
zcat ${OUTPUT_DIR}/chr${chromosome}/ancestral_table_Chr${chromosome}.tab.gz | \
    awk '{print $2}' > \
    ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.ancestralinfo
### generate map file 
cat ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.bim | \
    awk '{print $1,$2,$3,$4}' | \
    grep -v -w -f ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}_snps_to_remove.txt | \
    grep -w -f ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.ancestralinfo > \
    ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.map 
### get positions from map file
cat ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.map |\
    awk '{print $1"\t"$4}' > \
    ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.positions


# Select individuals (Western Polynesia, Eastern Polynesia, Polynesian Outlier)
# Convert to VCF format, apply MAF filter at 5%
## Western Polynesia##########################################
vcftools --gzvcf ${VCF} \
         --keep ${OUTPUT_DIR}/keep_WP.txt \
         --maf 0.05 \
         --positions ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.positions \
         --recode \
         --recode-INFO-all \
         --stdout | \
    bcftools annotate -x INFO/AF,INFO/AC,INFO/CM | \
    bcftools annotate \
        --annotations ${OUTPUT_DIR}/chr${chromosome}/ancestral_table_Chr${chromosome}.tab.gz \
        -c CHROM,POS,AA \
        --header-lines <(echo '##INFO=<ID=AA,Number=1,Type=Character,Description="Ancestral Allele">') \
        --output-type z \
        --output ${OUTPUT_DIR}/chr${chromosome}/WP_Chr${chromosome}.vcf.gz
## Polarise VCF
flip_ancestral_vcf.py WP_Chr${chromosome}.vcf.gz WP_Chr${chromosome}_polarised.vcf.gz
## Get list of SNPs
bcftools query -f '%POS\n' ${OUTPUT_DIR}/chr${chromosome}/WP_Chr${chromosome}.vcf.gz > ${OUTPUT_DIR}/chr${chromosome}/WP_Chr${chromosome}.positions
cat ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.map | \
    grep -w -f ${OUTPUT_DIR}/chr${chromosome}/WP_Chr${chromosome}.positions > \
    ${OUTPUT_DIR}/chr${chromosome}/WP_Chr${chromosome}.map

## Eastern Polynesia##########################################
vcftools --gzvcf ${VCF} \
         --keep ${OUTPUT_DIR}/keep_EP.txt \
         --maf 0.05 \
         --positions ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.positions \
         --recode \
         --recode-INFO-all \
         --stdout | \
    bcftools annotate -x INFO/AF,INFO/AC,INFO/CM | \
    bcftools annotate \
        --annotations ${OUTPUT_DIR}/chr${chromosome}/ancestral_table_Chr${chromosome}.tab.gz \
        -c CHROM,POS,AA \
        --header-lines <(echo '##INFO=<ID=AA,Number=1,Type=Character,Description="Ancestral Allele">') \
        --output-type z \
        --output ${OUTPUT_DIR}/chr${chromosome}/EP_Chr${chromosome}.vcf.gz
## Polarise VCF
flip_ancestral_vcf.py EP_Chr${chromosome}.vcf.gz EP_Chr${chromosome}_polarised.vcf.gz
## Get list of SNPs
bcftools query -f '%POS\n' ${OUTPUT_DIR}/chr${chromosome}/EP_Chr${chromosome}.vcf.gz > ${OUTPUT_DIR}/chr${chromosome}/EP_Chr${chromosome}.positions
cat ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.map | \
    grep -w -f ${OUTPUT_DIR}/chr${chromosome}/EP_Chr${chromosome}.positions > \
    ${OUTPUT_DIR}/chr${chromosome}/EP_Chr${chromosome}.map


## Polynesian Outlier ##########################################
vcftools --gzvcf ${VCF} \
         --keep ${OUTPUT_DIR}/keep_PO.txt \
         --maf 0.05 \
         --positions ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.positions \
         --recode \
         --recode-INFO-all \
         --stdout | \
    bcftools annotate -x INFO/AF,INFO/AC,INFO/CM | \
    bcftools annotate \
        --annotations ${OUTPUT_DIR}/chr${chromosome}/ancestral_table_Chr${chromosome}.tab.gz \
        -c CHROM,POS,AA \
        --header-lines <(echo '##INFO=<ID=AA,Number=1,Type=Character,Description="Ancestral Allele">') \
        --output-type z \
        --output ${OUTPUT_DIR}/chr${chromosome}/PO_Chr${chromosome}.vcf.gz
## Polarise VCF
flip_ancestral_vcf.py PO_Chr${chromosome}.vcf.gz PO_Chr${chromosome}_polarised.vcf.gz
## Get list of SNPs
bcftools query -f '%POS\n' ${OUTPUT_DIR}/chr${chromosome}/PO_Chr${chromosome}.vcf.gz > ${OUTPUT_DIR}/chr${chromosome}/PO_Chr${chromosome}.positions
cat ${OUTPUT_DIR}/chr${chromosome}/Chr${chromosome}.map | \
    grep -w -f ${OUTPUT_DIR}/chr${chromosome}/PO_Chr${chromosome}.positions > \
    ${OUTPUT_DIR}/chr${chromosome}/PO_Chr${chromosome}.map



















