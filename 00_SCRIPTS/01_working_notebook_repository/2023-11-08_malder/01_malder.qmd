---
title: "Malder analysis admixture" 
author: "Gaston Rijo De León"
format:
  html:
    embed-resources: true
    toc: true
    toc-depth: 2
date:  today
date-format: 'full'
engine: knitr
---

# Setup

```{r global-options}
knitr::opts_chunk$set(engine.opts = list(bash = "-l"), eval = FALSE)
```

```{r, warning=FALSE, message=FALSE, eval=TRUE}
library(tidyverse)
library(janitor)
library(plotly)
library(svglite)
library(MetBrewer)
library(here)
library(glue)
theme_set(theme_bw())

i_am("00_SCRIPTS/01_working_notebook_repository/2023-03-24_allele_frequency_structure/01_af_structure.qmd")
color_pal <- readxl::read_excel("/pasteur/zeus/projets/p02/MATAEA/gaston/mataea/metadata/population_colors_all_sheets.xlsx", sheet = 3) 
'%!in%' <- function(x,y)!('%in%'(x,y))
ind_labels <- read_delim("/pasteur/zeus/projets/p02/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/04_Quality_Control/labels.tsv",col_names = c("individual", "identifier", "cohort"))
pal <- readxl::read_excel("/pasteur/zeus/projets/p02/MATAEA/gaston/mataea/metadata/population_colors_all_sheets.xlsx", sheet = 3) 
```


# Introduction



# Prepare input files

```{bash}
sbatch /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/00_SCRIPTS/02_pipeline_repository/04_allele_frequency_structure/01_convert_to_eigenstrat.slurm
```

```{r}
reference_populations <-  c(
                     "Atayal","Paiwan", # Taiwanese
                     "Han","Dai", # East Asian
                     "Agta","Cebuano", # Island SEA
                     "Papuan_Highlands","Papuan_Sepik","Mendi","Marawaka","Bundi","Kundiawa","Tari", # Papuan mainland
                     "Mamusi", # Bismarck
                     "Bougainville", # Solomon I.
                     "Pentecost"#,
                     #"Native American
                     # European
                     )
```

```{r}
read_delim("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/03_MALDER/HC_AF_FILTERS_RMAP.ind",
           col_names = c("individual","sex","identifier")) %>%
  left_join(pal) %>%
  select(individual,sex, identifier, group3) %>%
  mutate(population_label =case_when(
    identifier %in% reference_populations ~ identifier,
    identifier %!in% reference_populations ~ group3 )) %>%
  select(individual,sex,population_label) %>%
  write_delim("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/03_MALDER/HC_AF_FILTERS_RMAP.ind",col_names = FALSE)
```





# Run MALDER

## Eastern Polynesia

```{bash}
module load malder
cd /pasteur/appa/homes/grijodel/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/03_MALDER
run_malder.py Cook_Islander Paiwan French Mamusi HC_AF_FILTERS_RMAP
```

```{bash}
module load malder
cd /pasteur/appa/homes/grijodel/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/03_MALDER
run_malder.py Society_Islander Paiwan French Mamusi HC_AF_FILTERS_RMAP
```

```{bash}
module load malder
cd /pasteur/appa/homes/grijodel/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/03_MALDER
run_malder.py Austral_Islander Paiwan French Mamusi HC_AF_FILTERS_RMAP
```

```{bash}
module load malder
cd /pasteur/appa/homes/grijodel/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/03_MALDER
run_malder.py Tuamotu_Islander Paiwan French Mamusi HC_AF_FILTERS_RMAP
```

```{bash}
module load malder
cd /pasteur/appa/homes/grijodel/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/03_MALDER
run_malder.py Gambier_Islander Paiwan French Mamusi HC_AF_FILTERS_RMAP
```

```{bash}
module load malder
cd /pasteur/appa/homes/grijodel/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/03_MALDER
run_malder.py Marquesas_Islander Paiwan French Mamusi HC_AF_FILTERS_RMAP
```


## Western Polynesia

```{bash}
module load malder
cd /pasteur/appa/homes/grijodel/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/03_MALDER
run_malder.py Tonga_Islander Paiwan French Mamusi HC_AF_FILTERS_RMAP
```

```{bash}
module load malder
cd /pasteur/appa/homes/grijodel/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/03_MALDER
run_malder.py Samoa_Islander Paiwan French Mamusi HC_AF_FILTERS_RMAP
```




## Polynesian Outliers

```{bash}
module load malder
cd /pasteur/appa/homes/grijodel/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/03_MALDER
run_malder.py Ontong_Java Paiwan French Mamusi HC_AF_FILTERS_RMAP
```

```{bash}
module load malder
cd /pasteur/appa/homes/grijodel/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/03_MALDER
run_malder.py Tikopia Paiwan French Mamusi HC_AF_FILTERS_RMAP
```

```{bash}
module load malder
cd /pasteur/appa/homes/grijodel/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/03_MALDER
run_malder.py RenBell Paiwan French Mamusi HC_AF_FILTERS_RMAP
```

## Fiji

```{bash}
module load malder
cd /pasteur/appa/homes/grijodel/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/03_MALDER
run_malder.py Fiji_Islander Paiwan French Mamusi HC_AF_FILTERS_RMAP
```



































