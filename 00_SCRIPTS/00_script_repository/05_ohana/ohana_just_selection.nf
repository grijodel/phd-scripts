nextflow.enable.dsl=2
workflow {
    genotype=Channel.fromFilePairs(params.prefix + '{.bim,.bed,.fam}', size:-1)
    K=params.K                                                                  
    ancestral_components=Channel.from(0..K-1) 
    chromosomes=Channel.from(1..22)
    q_matrix = Channel.fromPath( params.q_matrix )
    cov_matrix = Channel.fromPath( params.cov_matrix )
    // Selection scan

    /// Convert to dgm format and compute admixture corrected f matrix
    for_compute_f = genotype.combine(q_matrix).combine(chromosomes)
    COMPUTE_F(for_compute_f)
    /// Run selscan for the different tips
    for_selection = COMPUTE_F.out.combine(cov_matrix).combine(ancestral_components)
    SELECTION(for_selection)
    /// Aggregate results per tip
    AGGREGATE(  
            SELECTION.out
                        .groupTuple(by:[0,2])  )
}
process COMPUTE_F {
    scratch false 
    module "plink/1.90b6.16:ohana/0.1.7666.41124"                               
    memory { 50.GB * task.attempt }                                             
    errorStrategy 'retry'

    input:                                                                      
        tuple val(prefix), file(genotype), file(q_matrix), val(chromosome)
    output:                                                                     
        tuple val(prefix), file("${prefix}_${chromosome}.dgm"), val(chromosome), file("${prefix}_${chromosome}_f.matrix")

    script:
        """
        # Get chromosome from plink file
        plink --bfile ${prefix} \
            --chr $chromosome \
            --recode12 tab \
            --out ${prefix}_${chromosome}
        convert ped2dgm ${prefix}_${chromosome}.ped ${prefix}_${chromosome}.dgm 
        qpas ${prefix}_${chromosome}.dgm \
                --qin $q_matrix \
                --fout ${prefix}_${chromosome}_f.matrix \
                --max-iterations 100 \
                --frequency-bounds \
                --epsilon 1e-10 \
                --fixed-q 
        """
    stub:
        """
        touch ${prefix}_${chromosome}.dgm
        touch ${prefix}_${chromosome}_f.matrix
        """
}
process SELECTION {
    scratch false
    module "plink/1.90b6.16:ohana/0.1.7666.41124"
    memory { 10.GB * task.attempt }                                             
    errorStrategy 'retry'
    input:
        tuple val(prefix), file(dgm), val(chromosome), file(f_matrix), file(cov_matrix), val(ancestral_component)
    output:
        tuple val(prefix), file("${prefix}_${chromosome}_${ancestral_component}.selscan"), val(ancestral_component)
        
    script:
        """
        # Compute scale matrix 
        compute_scale_matrix.py $cov_matrix ${ancestral_component} > scale_matrix_${chromosome}_${ancestral_component}.matrix
        # Selection scan
        selscan  ${prefix}_${chromosome}.dgm \
                    ${prefix}_${chromosome}_f.matrix \
                    $cov_matrix \
                    --c-scale scale_matrix_${chromosome}_${ancestral_component}.matrix > ${prefix}_${chromosome}_${ancestral_component}.selscan
        """

    stub:
        """
        touch ${prefix}_${chromosome}_${ancestral_component}.selscan 
        """
}
process AGGREGATE {
    publishDir "${params.outdir}", mode: 'copy'
    memory { 500.MB * task.attempt }                                             
    errorStrategy 'retry'
    input:
        tuple val(prefix), file(selscan) ,val(ancestral_component)
        
    output:
        file("*.selscan")

    script:
        """
        cat ${prefix}_{1..22}_${ancestral_component}.selscan | grep -v "step" > ${prefix}_component_${ancestral_component}.selscan
        """
    stub:
        """
        touch ${prefix}_component_${ancestral_component}.selscan
        """
}

















