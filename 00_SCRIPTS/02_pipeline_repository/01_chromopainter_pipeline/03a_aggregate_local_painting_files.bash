#!/bin/sh
#SBATCH -J CP_aggregate
#SBATCH -o 01_CP_aggregate_%A_%a.out -e 01_CP_aggregate_%A_%a.err
#SBATCH --mem=25000
#SBATCH -c 1
#SBATCH --array=1-22
#SBATCH --qos=geh
#SBATCH --partition=geh
chromosome=$SLURM_ARRAY_TASK_ID

INDIR=$1
PREFIX=$2
OUTDIR=$3



cat $INDIR/chr${chromosome}/${PREFIX}{1..1904}_chr${chromosome}.samples.out | head -n 1 > $OUTDIR/all_samples_chr${chromosome}.samples.out
cat $INDIR/chr${chromosome}/${PREFIX}{1..1904}_chr${chromosome}.samples.out | grep -v EM_iter >> $OUTDIR/all_samples_chr${chromosome}.samples.out

