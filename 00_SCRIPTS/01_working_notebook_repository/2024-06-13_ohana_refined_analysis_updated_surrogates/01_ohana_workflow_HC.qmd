---
title: "Ohana refined, HC dataset"
author: "Gaston Rijo De León"
format:
  html:
    embed-resources: true
    toc: true
    toc-depth: 2
date:  today
execute: 
  echo: true
  warning: false
  message: false
  eval: false
date-format: 'full'
engine: knitr
---

# Setup

```{r}
#| eval: true
library(tidyverse)
library(janitor)
library(plotly)
library(MetBrewer)
library(ggtree)
library(treeio)
library(qqplotr)
library(here)
library(glue)

theme_set(theme_bw())
pal <- readxl::read_excel("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/population_colors.xlsx", sheet = 4) 
ind_labels <- read_delim("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/labels.tsv",col_names = c("individual", "identifier", "cohort"))


'%!in%' <- function(x,y)!('%in%'(x,y))

admixture_plot <- function(Q_matrix, K=5) {
  p <- Q_matrix %>%
  ggplot() +
    geom_bar(aes(x=individual,y=proportion,fill=cluster),stat="identity") +
    scale_fill_manual(values = met.brewer("Isfahan2", n=K)) +
    theme(axis.text.x=element_blank())
  return(p)
}
```

# Introduction

Let's keep things consistent with local ancestry and globetrotter/chromopainter pipelines for the merged dataset.

# Preparing files

Read list of individuals to include, filtered considering locality of family.

```{r}
#| eval: true
targets_to_exclude <- read_delim("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/00_inclusion_tables/hc_exclude_ecrf_family_filter_oceanians.txt")
```

Subset individuals and non-singleton variants from the MC files.

```{r}
#| eval: true
reference_group3 <- c("Yoruba","Mbuti", # African
                      "Papuan_Highlander","Papuan_Sepik","Papuan", # Papuan
                      "French","Orcadian", # European
                      "Han","Dai", # East Asian 
                      "Cebuano","Paiwan","Atayal") # AN_PROXY

keep_groups <- c("Polynesian_Outlier","Fiji","Eastern_Polynesian","Western_Polynesian")
target_names <- pal %>%
                  filter(group0 %in% keep_groups) %>%
                  select(identifier) %>% 
                  distinct() %>%
                  pull(identifier) 
```

Check admixture proportions to exclude surrogate individuals that are admixed.

```{r}
#| eval: true
# remove admixed individuals
# use admixture estimates from the high coverage dataset
ind_labels <- read_delim("/pasteur/helix/projects/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/04_Quality_Control/labels.tsv",col_names = c("individual", "identifier", "cohort"))
fam <- read_table("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/HC_MAF_PRUNED_THINNED.fam",
                  col_names = c("identifier","individual","a","b","c","d")) %>%
  select(individual) %>% 
  left_join(ind_labels) %>% 
  select(-cohort)

cluster <- c("c0","c1","c2","c3","c4","c5","c6")
Q <- read_table("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/admixture/all_samples/K_7_R_6414189/HC_MAF_PRUNED_THINNED.7.Q",
                col_names = cluster)


Q <- cbind(fam,Q) %>%
  as_tibble() %>%
  pivot_longer(cols = starts_with("c"),names_to = "cluster", values_to = "proportion") %>%
  arrange(identifier,proportion)

reference_populations <- pal %>%
  filter(group3 %in% reference_group3) %>%
  select(identifier,group3) %>%
  left_join(fam) %>%
  select(individual,identifier)



ind_levels <- Q %>%
  select(individual,identifier) %>%
  distinct() %>%
  mutate(identifier=factor(identifier, levels=pal$identifier)) %>%
  arrange(identifier) 
Q$individual <- factor(Q$individual,levels=ind_levels$individual)

ancestry <- c("East_Asian",
              "Polynesian",
              "Archipelagic_Papuan",
              "European",
              "African",
              "American",
              "Mainland_Papuan")
ancestries <- tibble(cluster,ancestry)
Q <- Q %>% left_join(ancestries)

ancestry_filter <- Q %>%
                      right_join(reference_populations) %>%
                      select(-cluster) %>%
                      drop_na() %>%
                      pivot_wider(names_from = ancestry, values_from = proportion ) %>%
                      filter( case_when(TRUE~East_Asian > 0.95 |
                                          Polynesian > 0.99 |
                                          Archipelagic_Papuan > 0.99 |
                                          European > 0.99 |
                                          Mainland_Papuan > 0.99 |
                                          African > 0.99 |
                                          American > 0.999) ) %>%
                      select(individual, identifier)
                      



Q %>%
  filter(individual %in% ancestry_filter$individual ) %>%
  mutate(identifier = factor(identifier, levels=pal$identifier)) %>%
  ggplot() +
      geom_bar(aes(x=individual,y=proportion,fill=cluster, label=identifier, label=ancestry),stat="identity") +
      scale_fill_manual(values = met.brewer("Isfahan2", n=7)) +
      theme(axis.text.x=element_blank())
ggplotly()

surrogate_names <- ancestry_filter %>% 
  left_join(pal) %>% 
  filter(group3 %in%  reference_group3 ) %>%
  pull(identifier) %>%
  unique()
```

```{r}
#| eval: true
keep_identifiers_file <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/03_refined_analysis_updated_surrogates/keep_identifiers.csv"

mataea_group3 <- c("Tuamotu_Islander",
               "Society_Islander",
               "Austral_Islander",
               'Marquesas_Islander',
               "Gambier_Islander")

read_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/HC_MAF_PRUNED_THINNED.fam",
           col_names = c("identifier","individual","a","b","c","d")) %>%
  select(identifier,individual) %>%
  filter(identifier %in% c(surrogate_names, target_names)) %>%
  filter(individual %!in% targets_to_exclude$individual) %>% 
  select(identifier,individual) %>%
  write_delim(file = keep_identifiers_file,delim = "\t",col_names = FALSE)

input_plink_prefix <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/HC"
output_dir <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/03_refined_analysis_updated_surrogates"

subset_plink_command <- glue("plink --bfile {input_plink_prefix} --mac 2 --keep {keep_identifiers_file} --indep-pairwise 50 5 0.5 --make-bed --out {output_dir}/HC_for_ohana ")
```

```{r}
systemPipeR::moduleload("plink/1.90b6.16")
system(subset_plink_command)
```

LD prune and thinning for computing ADMIXTURE components:

```{r}
#| eval: true
prune_plink_command <- glue("plink --bfile {output_dir}/HC_for_ohana --extract {output_dir}/HC_for_ohana.prune.in --thin-count 1000000 --make-bed --out {output_dir}/HC_for_ohana_pruned")
```

Interpolate recombination map for smoothing scores over the genome

```{bash}
cd /pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/03_refined_analysis_updated_surrogates/01_q_matrix
module load R/4.1.0
interpolate_recombination_map_allchr.R HC_for_ohana.bim HC_for_ohana_rmap.bim
```

```{r}
systemPipeR::moduleload("plink/1.90b6.16")
system(prune_plink_command)
```

Run the ADMIXTURE pipeline on the working directory.

```{bash}
cd /pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/03_refined_analysis_updated_surrogates/01_q_matrix
module load java
nextflow run admixture.nf -c admixture.config -profile maestro
```

# Choose best K for admixture

Proceed to visually inspect the results using `pong`. Select best K using cross-validation and pong alignment.

ADMIXTURE cross-validation

```{r}
#| eval: true
cv_err <- read_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/03_refined_analysis_updated_surrogates/01_q_matrix/CV_Error_admixture.tsv") %>%
  mutate(K=factor(K),
         seed=factor(seed))


p1 <- ggplot(cv_err,aes(x=K,y=CV_Error, color=K,label=seed)) +
          geom_boxplot(outlier.shape = NA) +
          geom_jitter(height=0, size=0.3) +
  scale_color_manual(values=met.brewer("Redon",n = 14))

p2 <- ggplot(cv_err,aes(x=K,y=LogL, color=K,label=seed)) +
          geom_boxplot(outlier.shape = NA) +
          geom_jitter(height=0, size=0.3) +
  scale_color_manual(values=met.brewer("Redon",n = 14))
ggplotly(p1)
ggplotly(p2)
```

## Choosing best K

According to cross-validation, best K is 6, but that means splitting Polynesians into a Marquesas-Gambier and a non-Marquesas-Gambier ancestry. Let's use K=5, which clusters Polynesians in a single group, and it scores best according to pong alignment criterion.

# Start with Ohana

Let's outline the workflow for doing the Ohana selection scan.

1.  Convert ADMIXTURE matrices to Ohana format
2.  Compute admixture-awarec covariance matrices
3.  Compute tree from covariance matrix
4.  Assign individual IDs to files

--- Per chromosome ---

5.  Convert unpruned genotypes to Ohana format
6.  Produce admixture-corrected alelle frequencies
7.  Select tree branch and compute scaled matrix
8.  Perform selection scan

------------------------------------------------------------------------

## K=5

### Prepare and run Ohana

```{bash}
module load plink/1.90b6.16 ohana/0.1.7666.41124
cd /pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/03_refined_analysis_updated_surrogates/02_ohana
input_dir=/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/03_refined_analysis_updated_surrogates
prefix=HC_for_ohana_pruned
# 1 Convert matrices to Ohana format
## Convert to ped
plink --bfile  ${input_dir}/${prefix} \
                --recode12 tab \
                --out ${prefix}
## Convert to dgm
convert ped2dgm ${prefix}.ped ${prefix}.dgm
############## RERUN FROM HERE
## Make admixture matrices compatible
K_DIR=/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/03_refined_analysis_updated_surrogates/01_q_matrix/q_matrices/K_5_R_8576980
ADMIXTURE_P_MATRIX=${K_DIR}/HC_for_ohana_pruned.5.P
ADMIXTURE_Q_MATRIX=${K_DIR}/HC_for_ohana_pruned.5.Q
admixture_to_ohana.py ${ADMIXTURE_P_MATRIX} ${ADMIXTURE_Q_MATRIX} f_subset.matrix q_subset.matrix
# 2 Compute admixture-aware covariance matrices
nemeco ${prefix}.dgm \
                f_subset.matrix \
                --max-iterations 1000 \
                --epsilon 1e-10 \
                --cout cov_subset.matrix
# 3 Compute tree from covariance matrix
convert cov2nwk cov_subset.matrix cov_subset.nwk
# 4 Assign individual IDs to files
assign_iid_to_cluster.py ${prefix}.ped q_subset.matrix q_subset_iid.txt
```

Let's take a look at the admixture components barplot and the ancestral components tree.

```{r}
#| eval: true
fam <- read_table("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/03_refined_analysis_updated_surrogates/HC_for_ohana_pruned.fam",
                  col_names = c("identifier","individual","a","b","c","d")) %>%
  select(individual) %>% 
  left_join(ind_labels) %>% 
  select(-cohort)
 
Q <- read_table("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/03_refined_analysis_updated_surrogates/01_q_matrix/q_matrices/K_5_R_8576980/HC_for_ohana_pruned.5.Q",
                col_names =  c("c0","c1","c2","c3","c4"))


Q <- cbind(fam,Q) %>%
  as_tibble() %>%
  pivot_longer(cols = starts_with("c"),names_to = "cluster", values_to = "proportion") %>%
  arrange(identifier,proportion)

ind_levels <- Q %>%
  select(individual,identifier) %>%
  distinct() %>%
  mutate(identifier=factor(identifier, levels=pal$identifier)) %>%
  arrange(identifier) 
Q$individual <- factor(Q$individual,levels=ind_levels$individual)
```

```{r}
#| eval: true
p <- ggplot(Q) +
    geom_bar(aes(x=individual,y=proportion,fill=cluster, label=identifier),stat="identity") +
    scale_fill_manual(values = met.brewer("Isfahan2", n=5)) +
    theme(axis.text.x=element_blank())

ggplotly(p)

#ggsave("",width = 15, height=4)
```

```{r}
#| eval: true
tree <- read.tree("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/03_refined_analysis_updated_surrogates/02_ohana/cov_subset.nwk")
cluster_label <- c("0","1","2","3","4")
ancestry_proxy <- c("East_Asian","Papuan","African","European","Polynesian")
tip_labels <- tibble(cluster_label, ancestry_proxy) %>%
                mutate(cluster_label=factor(cluster_label,levels=tree$tip.label)) %>%
                arrange(cluster_label)
tree$tip.label <- tip_labels$ancestry_proxy
ggtree(tree,
       layout="daylight",
       branch.length = 'none') +
   geom_tiplab(color='firebrick') +
   xlim(-10, 10) +
   ylim(-10, 10)
```

If I include African populations, Polynesians and Polynesian Outliers cluster together. Otherwise, Polynesians cluster with Europeans, probably due to recent admixture. This model is more interpretable.

Let's run Ohana for all ancestral components (all tips).

```{bash}
cd /pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/03_refined_analysis_updated_surrogates/02_ohana
module load java
nextflow run ohana_just_selection.nf -c ohana_just_selection.config -profile maestro
```

# Read results

Polynesian cluster is cluster 4. Read selection scan results.

```{r}
#| eval: true
column_names <- c("step","lle-ratio",
                  "global-lle","local-lle",
                  "f-pop0","f-pop1",
                  "f-pop2","f-pop3",
                  "f-pop4")
ancestral_component <- 4
selscan <- read_delim(file = "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/03_refined_analysis_updated_surrogates/02_ohana/selscan/K5/HC_for_ohana_component_4.selscan",
                          col_names = column_names,
                          progress = TRUE) %>%
  clean_names() %>%
  select(step,lle_ratio,global_lle,local_lle)

bim <- read_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/03_refined_analysis_updated_surrogates/HC_for_ohana.bim",
                  col_names = c("chromosome","id","cM","position","ref","alt"),
                  col_types = cols(id=col_character() )
                    )
selscan <- cbind(bim,selscan) %>%
  as_tibble()
rm(bim)
```

```{r}
selscan %>%
  select(-id) %>%
  write_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/03_refined_analysis_updated_surrogates/02_ohana/selscan/K5/HC_ohana_c4_polynesian.tsv")
```

Check the distribution of the likelihood ratio values

```{r}
#| eval: true
selscan %>%
  ggplot(aes(x=lle_ratio)) +
  geom_histogram()

selscan %>% 
  filter(lle_ratio != 0 ) %>%
  ggplot(aes(x=lle_ratio)) +
  geom_histogram()



selscan %>% 
  filter(lle_ratio != 0 ) %>%
  filter(lle_ratio < 200 ) %>%
  ggplot(aes(x=lle_ratio)) +
  geom_histogram()


```

```{r}
#| eval: true
selscan %>% filter(lle_ratio == 0) %>% summarise(n=n())
selscan %>% filter(lle_ratio != 0) %>% summarise(n=n())
selscan %>% filter(lle_ratio > 100) %>% summarise(n=n())
```

```{r}
#| eval: true
#| cache: true
selscan %>%
  filter(lle_ratio != 0) %>%
  ggplot(aes(x=position,y=lle_ratio)) +
    geom_point(alpha=0.3) +
    facet_wrap(~chromosome, scales = "free_x")
```

```{r}
#| eval: true
#| cache: true
di <- "chisq"
dp <- list("df" = 2)
de <- TRUE # enabling the detrend option

selscan %>% 
filter(lle_ratio != 0,
       lle_ratio < 100) %>%
    ggplot(., mapping = aes(sample = lle_ratio)) +
      stat_qq_band(distribution=di,dparams=dp,detrend = de) +
      stat_qq_line(distribution=di,dparams=dp,detrend = de) +
      stat_qq_point(distribution=di,dparams=dp,detrend = de)+
      xlab("Theoretical Quantiles") +
      ylab("Sample Quantiles") +
      ggtitle(bquote("QQ plot "~chi^2~ ~(.(1)))) 
```

### 

```{r}
#| eval: true
#| cache: true
selscan %>%
  filter(chromosome==12) %>%
  filter(lle_ratio != 0) %>%
  ggplot(aes(x=position,y=lle_ratio)) +
    geom_point(alpha=0.3) +
    facet_wrap(~chromosome, scales = "free_x")
```
