#!/usr/bin/env python3
import argparse
import glob
import os
from datetime import datetime
parser = argparse.ArgumentParser( description = 'Aggegate Ohana selscan results' )
parser.add_argument("directory", type=str, help="Directory containing {prefix}_{block}_{K}.selscan files")
parser.add_argument('--output_prefix', type=str, help="Output prefix", default="selection_scan")

args = parser.parse_args()
directory=args.directory
output_prefix=args.output_prefix



def pdate(message):
    current_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print(f"[{current_date}] {message}")
# Read file paths
files=glob.glob( directory + "/*.selscan")
# Make file list for ordering
files_dict = {}


blocks = []
ks = []
for i in files:
    # remove absolute path and file extension
    basename=i.split('/')[-1].split('.')[0]
    # chromosome/block number
    block_number = int( basename.split("_")[-2] )
    blocks.append(block_number)
    # ancestry number
    k = int(basename.split("_")[-1])
    ks.append( k )
    files_dict[(block_number,k)] = i
# Get ordered keys
blocks.sort()
ks.sort()
counter=0


# Join all files 

for k in ks:
    pdate("Processing {0}/{1} Ks".format(str(k),str(len(ks))))
    with open( output_prefix + "_" + '.selscan' , 'w' ) as tmpfile:
        for block in blocks:
            pdate("Processing block {0}/{1} of k {2}".format(str(block),str(len(blocks)),str(k) ))
            sel_file=files_dict[(block,k)] 
            with open(sel_file,'r') as selfile:
                next(tmpfile)
                for line in selfile:
                    linelist = line.split()
                    newline = '\t'.join(linelist) + '\n'
                    tmpfile.write(newline)




