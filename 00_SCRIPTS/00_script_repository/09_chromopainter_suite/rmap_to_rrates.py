#!/usr/bin/env python3
import argparse
from pathlib import Path
import os
import pandas as pd
import glob
import sys

parser = argparse.ArgumentParser( description = 'Convert a genetic map file in cM to a per site recombination rate file in M for chromopainter')
parser.add_argument("input_map", type=str, help="Input map file (plink)")
parser.add_argument("output_recomrates", type=str, help="Output recomrates file (chromopainter)")

args = parser.parse_args()
snp_list = []

with open( args.input_map , 'r' ) as handle:
    for line in handle:
        chromosome, snpid,cM,pos = line.rstrip().split()
        chromosome = int(chromosome)
        cM = float(cM)
        pos = int(pos)
        snp_list.append( [chromosome,snpid,cM,pos] )
number_of_snps = len(snp_list)

with open( args.output_recomrates, 'w' ) as handle:
    handle.write(str(number_of_snps)+"\n")
    for i in range(0,number_of_snps):
        snp = snp_list[i]
        chromosome, snpid, cM, pos = snp
        try:
            snp_next = snp_list[i+1]
        except IndexError:
            # if last snp in list
            rate = 0
        else:
            chromosome_next, snpid_next, cM_next, pos_next = snp_next
            rate = (cM_next - cM) / (pos_next - pos ) * 1e-6
            #rate = rate / 100 # from 0 to 1 prob to 0 to 100
       
        newline = str(pos) + " " + f'{rate:.30f}' + "\n"
        handle.write(newline)



