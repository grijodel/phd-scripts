`2023-03-15` This directory is a repository that contains scripts that are standardized for general use, such as filtering sites, renaming plink files, or running admixture.
