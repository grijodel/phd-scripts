##################################################################
## Finestructure R Example
## Author: Daniel Lawson (dan.lawson@bristol.ac.uk)
## For more details see www.paintmychromosomes.com ("R Library" page)
## Date: 14/02/2012
## Notes:
##    These functions are provided for help working with fineSTRUCTURE output files
## but are not a fully fledged R package for a reason: they are not robust
## and may be expected to work only in some very specific cases! USE WITH CAUTION!
## SEE FinestrictureLibrary.R FOR DETAILS OF THE FUNCTIONS
##
## Licence: GPL V3
##
##    This program is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.

##    This program is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.

##    You should have received a copy of the GNU General Public License
##    along with this program.  If not, see <http://www.gnu.org/licenses/>.

args <- commandArgs(TRUE)
seed <- args[1]
source("/pasteur/zeus/projets/p02/LifeChange/Array_processing/6_Haplotype_Analyses/ChromoPainter/bin/FinestructurePlots/FinestructureLibrary.R") # read in the R functions, which also calls the needed packages

## make some colours
some.colors<-MakeColorYRP() # these are yellow-red-purple
some.colorsEnd<-MakeColorYRP(final=c(0.2,0.2,0.2)) # as above, but with a dark grey final for capped values

### Define our input files
for (seed in seed){

  chunkfile<- paste0("ChP/ChP_concatenate/ChPrun_allchromosomes.chunkcounts.out") ## chromopainter chunkcounts file
  mcmcfile<- paste0("ChP/FineStructure/Fs.seed", seed, ".chunkcounts.mcmc.out") ## finestructure mcmc file
  treefile<- paste0("ChP/FineStructure/Fs.seed", seed, ".chunkcounts.tree.out") ## finestructure tree file
  outputs <- paste0("ChP/Plots/Seed", seed)

  ## Additional files that you can extract from finestructure
  #mappopchunkfile<-"" # population-by-population chunkcount file for the populations used in the MAP (i.e tree)
  #system( paste("fs fs -X -Y -e X2",chunkfile,treefile,mappopchunkfile) )
  #meancoincidencefile<-"" # pairwise coincidence, .i.e. proportion of MCMC files where individuals are found in the same
  #system( paste("fs fs -X -Y -e meancoincidence",chunkfile,mcmcfile,meancoincidencefile) )
  ## there are ways of generating these within R but are either slower or more annoying - its your call how you do it

  ###### READ IN THE CHUNKCOUNT FILE
  dataraw<-as.matrix(read.table(chunkfile,row.names=1,header=T,skip=0)) # read in the pairwise coincidence

  ###### READ IN THE MCMC FILES
  mcmcxml<-xmlTreeParse(mcmcfile) ## read into xml format
  mcmcdata<-as.data.frame.myres(mcmcxml) ## convert this into a data frame

  ###### READ IN THE TREE FILES

  treexml<-xmlTreeParse(treefile) ## read the tree as xml format
  ttree<-extractTree(treexml) ## extract the tree into ape's phylo format
  ## If you dont want to plot internal node labels (i.e. MCMC posterior assignment probabilities)
  ## now is a good time to remove them via:
  #     ttree$node.label<-NULL
  ## Will will instead remove "perfect" node labels
  ttree$node.label[ttree$node.label=="1"] <-""
  ## And reduce the amount of significant digits printed:
  ttree$node.label[ttree$node.label!=""] <-format(as.numeric(ttree$node.label[ttree$node.label!=""]),digits=2)
  tdend<-myapetodend(ttree,factor=1) # convert to dendrogram format

  ####################################
  ## PLOT 1: RAW DENDROGRAM PLOT
  pdf(file=paste0(outputs, "ChPrun.chunkcounts.SimpleFullDendrogram.pdf"),height=7,width=14)
  par(mar=c(0,0,2,0),mfrow=c(1,1))
  plot.dendrogram(tdend,horiz=FALSE,nodePar=list(cex=0,lab.cex=0.6),edgePar=list(p.lwd=0,t.srt=90,t.off=-0.5),axes=F)
  dev.off()

  ## Now we work on the MAP state
  mapstate<-extractValue(treexml,"Pop") # map state as a finestructure clustering
  mapstatelist<-popAsList(mapstate) # .. and as a list of individuals in populations

  popnames<-lapply(mapstatelist,NameSummary) # population names IN A REVERSIBLE FORMAT (I.E LOSSLESS)

  ## NOTE: if your population labels don't correspond to the format we used (NAME<number>) YOU MAY HAVE TROUBLE HERE. YOU MAY NEED TO RENAME THEM INTO THIS FORM AND DEFINE YOUR POPULATION NAMES IN popnamesplot BELOW
  popnamesplot<-lapply(mapstatelist,NameMoreSummary) # a nicer summary of the populations
  names(popnames)<-popnamesplot # for nicety only
  names(popnamesplot)<-popnamesplot # for nicety only


  popdend<-makemydend(tdend,mapstatelist) # use NameSummary to make popdend
  popdend<-fixMidpointsComplete(popdend) # needed for obscure dendrogram reasons

  popdendclear<-makemydend(tdend,mapstatelist,"NameMoreSummary")# use NameMoreSummary to make popdend
  popdendclear<-fixMidpointsComplete(popdendclear) # needed for obscure dendrogram reasons

  ########################
  ## PLOT 2: population tree
  pdf(file=paste0(outputs, "ChPrun.chunkcounts.SimplePopulationDendrogram.pdf"),height=10,width=40)
  par(mar=c(20,0,2,0),oma=c(0,0,0,0),cex=1)
  plot.dendrogram(popdendclear,horiz=FALSE,nodePar=list(cex=0,lab.cex=1.2,las=2),edgePar=list(p.lwd=0,t.srt=0,t.off=0.3),yaxt="n",height=15,dLeaf=0.2)
  dev.off()

  ########################
  ## PAIRWISE COINCIDENCES

  fullorder<-labels(tdend) # the order according to the tree
  #mcmcmatrixraw<-as.matrix(read.csv(meancoincidencefile,row.names=1)) # read in the pairwise coincidence file we created earlier
  #mcmcmatrix<-mcmcmatrixraw[fullorder,fullorder]
  #mapstatematrix<-groupingAsMatrix(mapstatelist)[fullorder,fullorder] # map state for reference

  #########################
  ## PLOT 3: Pairwise coincidence, showing the MAP state

  source("bin/FinestructurePlots/FinestructureLibrary.R")
  #pdf(file=paste0(outputs, "ChPrun.chunkcounts.SimplePairwiseCoincidence.pdf"),height=12,width=12)
  #plotFinestructure(mcmcmatrix,dimnames(mcmcmatrix)[[1]],dend=tdend,optpts=mapstatematrix,cex.axis=0.6,edgePar=list(p.lwd=0,t.srt=90,t.off=-0.1,t.cex=0.8))
  #dev.off()

  ########################
  ## COANCESTRY MATRIX

  datamatrix<-dataraw[fullorder,fullorder] # reorder the data matrix

  #Scale manually or based on SD: max value=mean+2sd
  #you can also set the maximum manually
  colscale <-c(max(0,min(unlist(datamatrix)[unlist(datamatrix)>0])),median(datamatrix)+(sd(datamatrix)*2))
  tmatmax <- colscale[2] # cap the heatmap, set a number for the max here if you want to
  #                       set it manually

  #tmatmax<-500 # cap the heatmap
  tmpmat<-datamatrix
  tmpmat[tmpmat>tmatmax]<-tmatmax #
  pdf(file=paste0(outputs, "ChPrun.chunkcounts.SimpleCoancestry.pdf"),height=12,width=12)
  plotFinestructure(tmpmat,dimnames(tmpmat)[[2]],dend=tdend,cols=some.colorsEnd,cex.axis=0.6,edgePar=list(p.lwd=0,t.srt=90,t.off=-0.1,t.cex=0.8))
  dev.off()

  ## Population averages
  popmeanmatrix<-getPopMeanMatrix(datamatrix,mapstatelist)

  tmatmax<-500 # cap the heatmap
  tmpmat<-popmeanmatrix
  tmpmat[tmpmat>tmatmax]<-tmatmax #
  pdf(file=paste0(outputs, "ChPrun.chunkcounts.PopAveragedCoancestry.pdf"),height=20,width=20)
  plotFinestructure(tmpmat,dimnames(tmpmat)[[2]],dend=tdend,cols=some.colorsEnd,cex.axis=0.6,edgePar=list(p.lwd=0,t.srt=90,t.off=-0.1,t.cex=0.8))
  dev.off()


  ### Useful tricks with labels

  mappopcorrectorder<-NameExpand(labels(popdend))
  mappopsizes<-sapply(mappopcorrectorder,length)
  labellocs<-PopCenters(mappopsizes)
  labelcols<-c(2,2,3,3,4,4,1,1,1,5,6,6,6,7,8,8,2) # different label colours allow clearer identification of individuals, too
  labelcrt=45
  #Scale manually or based on SD: max value=mean+2sd
  #you can also set the maximum manually
  colscale <-c(max(0,min(unlist(datamatrix)[unlist(datamatrix)>0])),median(datamatrix)+(sd(datamatrix)*2))
  tmatmax <- colscale[2] # cap the heatmap, set a number for the max here if you want to
  #                       set it manually

  #tmatmax<-500 # cap the heatmap
  tmpmat<-datamatrix
  tmpmat[tmpmat>tmatmax]<-tmatmax #

  pdf(file=paste0(outputs, "ChPrun.chunkcounts.SimpleCoancestry2.pdf"),height=28,width=28)
  plotFinestructure(tmpmat,labelsx=labels(popdendclear),labelsatx=labellocs,crt=labelcrt,dend=tdend,text.col=labelcols,cex.axis=0.5,edgePar=list(p.lwd=0,t.srt=90,t.off=-0.1,t.cex=0.8))
  dev.off()


  ####################################
  ## PCA Principal Components Analysis
  pcares<-mypca(dataraw)
  # For figuring out how many PCs are important; see Lawson & Falush 2012
  # You need packages GPArotation and paran
  #tmap<-optimalMap(dataraw)
  #thorn<-optimalHorn(dataraw)
  #c(tmap,thorn) # 11 and 5. Horn typically underestimates, Map is usually better
  pcapops<-getPopIndices(rownames(dataraw),mapstatelist)
  pcanames<-rownames(dataraw)
  rcols<-rainbow(max(pcapops))

  pdf(paste0(outputs, "ChPrun.chunkcounts.SimpleExample_PCA.pdf"),height=16,width=12)
  par(mfrow=c(4,3))
  for(i in 1:4) for(j in (i+1):5) {
    plot(pcares$vectors[,i],pcares$vectors[,j],col=rcols[pcapops],xlab=paste("PC",i),ylab=paste("PC",j),main=paste("PC",i,"vs",j),pch=rcols)
    text(pcares$vectors[,i],pcares$vectors[,j],labels=pcanames,col=rcols[pcapops],cex=0.5,pos=1)
  }
  dev.off()

  pdf(paste0(outputs, "ChPrun.chunkcounts.SimpleExample_PCA_nice.pdf"),height=16,width=12)
par(pty="s")
for(i in 1:4) for(j in (i+1):5) {
  		pcanames -> pops
		a <- strsplit(pops,"[.]")
		sapply (a, function(ext) ext[1])-> pops
		rcols[pcapops]
		library(RColorBrewer)
		col <- c(colorRampPalette(brewer.pal(n=9, "Set3"))(length(unique(pops))))
		pch <- rep(c(15:19),length.out=length(unique(pops)))
		la <- pops[match(unique(pops),pops)]
		leyenda <- cbind(la, col, pch)
		for (pop in 1:length(unique(pops))){
 		 	pops[which(pops==la[pop])] <- col[pop]
		}
		pops -> col.pop
		pcanames -> pops
		a <- strsplit(pops,"[.]")
		sapply (a, function(ext) ext[1])-> pops
		la <- pops[match(unique(pops),pops)]
		for (pop in 1:length(unique(pops))){
 		 	pops[which(pops==la[pop])] <- pch[pop]
		}
		pops -> pch.pop
		plot (pcares$vectors[,i],pcares$vectors[,j], xlab=paste0("PC",i), ylab=paste0("PC",j)
     	 ,cex=1.2,col=as.character(col.pop),bg=col.pop,pch=as.numeric(pch.pop))
		legend("bottomleft", legend=leyenda[,1], pch=as.numeric(leyenda[,3]), col=leyenda[,2], pt.bg=leyenda[,2],
      	 bty="n", pt.cex=1, cex=0.8, ncol=3, y.intersp=0.7,x.intersp=0.5)
	}
	dev.off()

}
