## Compare clusters of FS in diferent runs
### ____________________________________________________________
source("../../bin/FinestructurePlots/FinestructureLibrary.R") # read in the R functions, which also calls the needed packages
library(dendextend)
library(ggplot2)
library(reshape2)
library(viridis)
library(plotrix)
library(maps)
library(mapplots)
library(RColorBrewer)
library(dendextend)
library(factoextra)
## Add an alpha value to a colour
add.alpha <- function(col, alpha=1){
  if(missing(col))
    stop("Please provide a vector of colours.")
  apply(sapply(col, col2rgb)/255, 2,
        function(x)
          rgb(x[1], x[2], x[3], alpha=alpha))
}

## Name files
seed1="186"
path1="ChP_onlyVanuatu/FineStructure"
mcmcfile1 <- paste0(path1, "/Fs.seed", seed1, ".chunkcounts.mcmc.out") ## finestructure mcmc file
treefile1 <- paste0(path1, "/Fs.seed", seed1, ".chunkcounts.tree.out") ## finestructure tree file

seed2="365"
path2="ChP/FineStructure"
mcmcfile2 <- paste0(path2, "/Fs.seed", seed2, ".chunkcounts.mcmc.out") ## finestructure mcmc file
treefile2 <- paste0(path2, "/Fs.seed", seed2, ".chunkcounts.tree.out") ## finestructure tree file

###### READ IN THE MCMC FILES
mcmcxml1<-xmlTreeParse(mcmcfile1) ## read into xml format
mcmcdata1<-as.data.frame.myres(mcmcxml1) ## convert this into a data frame

mcmcxml2<-xmlTreeParse(mcmcfile2) ## read into xml format
mcmcdata2<-as.data.frame.myres(mcmcxml2) ## convert this into a data frame

###### READ IN THE TREE FILES

treexml1<-xmlTreeParse(treefile1) ## read the tree as xml format
ttree1<-extractTree(treexml1) ## extract the tree into ape's phylo format
ttree1$node.label[ttree1$node.label=="1"] <-""
## And reduce the amount of significant digits printed:
ttree1$node.label[ttree1$node.label!=""] <-format(as.numeric(ttree1$node.label[ttree1$node.label!=""]),digits=2)
tdend1<-myapetodend(ttree1,factor=1) # convert to dendrogram format

treexml2<-xmlTreeParse(treefile2) ## read the tree as xml format
ttree2<-extractTree(treexml2) ## extract the tree into ape's phylo format
ttree2$node.label[ttree2$node.label=="1"] <-""
## And reduce the amount of significant digits printed:
ttree2$node.label[ttree2$node.label!=""] <-format(as.numeric(ttree2$node.label[ttree2$node.label!=""]),digits=2)
tdend2<-myapetodend(ttree2,factor=1) # convert to dendrogram format


get.compare <- function(ind,clusters1, clusters2){
  clust1 <- unlist(clusters1[which(unlist(lapply(clusters1, function(y) any(y==ind))))])
  clust2 <- unlist(clusters2[which(unlist(lapply(clusters2, function(y) any(y==ind))))])
  ratio.clust1 <- length(which(clust1 %in% clust2))/length(clust1)
  # ratio.clust2 <- length(which(clust2 %in% clust1))/length(clust2)
  return(ratio.clust1)
  # return(ratio.clust2)
}


compare.list <- NULL
K.last <- 20
for (K in 15:K.last){
  clusters1 <- lapply(get_subdendrograms(tdend1,K), function(x)x %>% labels)
    compare.df <- NULL
    for (K2 in 20){
      clusters2 <- lapply(get_subdendrograms(tdend2, K2), function(x)x %>% labels)
      individuals <- tdend1 %>% labels
      compare <- sapply(individuals, function(ind) get.compare(ind,clusters1,clusters2))
      compare.df <- cbind(compare.df, compare)
    } 
  colnames(compare.df) <- paste0("K_",20)
  compare.list[[K]] <- compare.df
}

df <- as.data.frame(compare.list)
lapply(compare.list, function(x)apply(x,2,mean))

df.ggplot <- melt(t(df)) # transform to ggplot format
colnames(df.ggplot) <- c("K", "Ind", "ratio")
ggplot(df.ggplot, aes(x = as.character(K), y = Ind, fill = ratio)) +
  geom_tile() +
  scale_fill_viridis(option="viridis", direction=-1)+
  theme(axis.text=element_text(size=1),
        axis.ticks.length=unit(.1, "cm"))

