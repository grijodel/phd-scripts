#!/usr/bin/env python3
import argparse
parser = argparse.ArgumentParser( description = 'Query variants against a reference VCF database.')
parser.add_argument("reference", type=str, help="VCF used as reference database")
parser.add_argument("query", type=str, help="VCF of variants to query against the reference")
parser.add_argument("output_file", type=str, help="Output file specifying which variants are found and not found")
args = parser.parse_args()

import cyvcf2

def check_snp_in_reference(reference_vcf_path, query_vcf_path, output_file):

    # Open the reference VCF file
    print("Reading reference VCF...")
    reference_vcf = cyvcf2.VCF(reference_vcf_path)
    # Create a dictionary to store reference SNP information
    reference_snps = {}

    # Iterate through the reference VCF and store SNP information
    for variant in reference_vcf:
        # Create a unique identifier for each SNP based on chromosome and position
        snp_id = f"{variant.CHROM} {variant.POS}"

        # If there are multiple alternate alleles, store them in a set
        alternate_alleles = set(variant.ALT)

        # Store the reference SNP information in the dictionary
        reference_snps[snp_id] = {
            "ref_allele": variant.REF,
            "alt_alleles": alternate_alleles,
        }
    print("Done.")
    print("Parsing query VCF...")
    # Open the query VCF file
    query_vcf = cyvcf2.VCF(query_vcf_path)
    with open(output_file,'w') as handle:
        # Iterate through the query VCF and check if each SNP exists in the reference VCF
        for query_variant in query_vcf:
            # Create a unique identifier for the query SNP based on chromosome and position
            query_snp_id = f"{query_variant.CHROM} {query_variant.POS}"

            # Check if the query SNP exists in the reference SNPs dictionary
            if query_snp_id in reference_snps:
                reference_snp = reference_snps[query_snp_id]

                # Check if the query SNP's reference allele matches the reference VCF
                if query_variant.REF == reference_snp["ref_allele"]:
                    # Check if any of the query SNP's alternate alleles match the reference VCF
                    if any(allele in reference_snp["alt_alleles"] for allele in query_variant.ALT):
                        print(f"SNP {query_snp_id} exists in the reference VCF.")
                        handle.write(f"{query_snp_id} {query_variant.REF} {query_variant.ALT[0]} in_reference\n")
                    else:
                        print(f"SNP {query_snp_id} exists in the reference VCF but alternate alleles don't match.")
                        handle.write(f"{query_snp_id} {query_variant.REF} {query_variant.ALT[0]} in_reference_multiallelic\n")
                # Check if the flipped allele combination exists in the reference VCF
                elif query_variant.ALT == reference_snp["ref_allele"]:
                    print(f"SNP {query_snp_id} (with flipped alleles) exists in the reference VCF.")
                    handle.write(f"{query_snp_id} {query_variant.REF} {query_variant.ALT[0]} in_reference_flipped\n")
                else:
                    print(f"SNP {query_snp_id} does not exist in the reference VCF.")
                    handle.write(f"{query_snp_id} {query_variant.REF} {query_variant.ALT[0]} not_in_reference\n")
    print("Done.")
if __name__ == "__main__":
    reference_vcf_file = args.reference
    query_vcf_file = args.query
    check_snp_in_reference(reference_vcf_file, query_vcf_file, args.output_file)
