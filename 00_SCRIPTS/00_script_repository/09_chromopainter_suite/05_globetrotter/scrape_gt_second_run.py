#!/usr/bin/env python3
import glob
import pandas as pd
import argparse
import os
import sys
### SETTINGS
parser = argparse.ArgumentParser( description = 'Aggregate globetrotter second run outputs. This script assumes a very rigid directory structure and naming convention, so read how it works before using.')
parser.add_argument("input_directory", type=str, help="Input dir with one folder per population")
parser.add_argument("output_prefix", type=str, help="Output prefix for resulting tables")
args = parser.parse_args()
input_directory=args.input_directory
output_prefix =args.output_prefix

### CHANGE WD ###
old_wd = os.getcwd()
new_wd = input_directory
os.chdir(new_wd)

output_colnames = ["target_group","conclusion","bootstrap_num","date_1_est_boot","date_2_est_boot","max_R2_fit_1_date_boot","max_score_2_events_boot"]
bootstraps = []

for i in glob.glob("*/globetrotter_output_bootstrap.txt"):
    group_name = i.split("/")[-2]
    with open(i, 'r') as handle:
        header=next(handle).rstrip().split()
        if len(header) == 4:
            # Single date
            conclusion = "one-date"
        elif len(header) == 5:
            # two date
            conclusion = "multiple-dates"
        else:
            sys.exit("Error: weird headers or something")
        for line in handle:
            if conclusion == "one-date":
                bootstrap_num,date_1,r2,score=line.rstrip().split()
                date_2="NA"
            elif conclusion == "multiple-dates":
                bootstrap_num,date_1,date_2,r2,score=line.rstrip().split()
                
            
            row = [group_name,conclusion, bootstrap_num,date_1,date_2,r2,score]    
            bootstraps.append(row)
            
bootstraps_dataframe = pd.DataFrame(bootstraps, columns=output_colnames)
bootstraps_dataframe.to_csv(f"{output_prefix}_bootstrap_replicates.csv", index=False)
os.chdir(old_wd)