#!/usr/bin/env python3
import argparse
from pathlib import Path
import os
import sys
import pandas as pd
import glob

parser = argparse.ArgumentParser(description='Generate filemap for pong + Cross-Validation error table.')
parser.add_argument("directory", type=str, help="Directory containing K_x_R_y directories containing ADMIXTURE output. Proper naming is essential.")

args = parser.parse_args()
admdir = args.directory
prefix="admixture"

def read_LogFile(path_to_file):
    logL = CV_Error = None
    with open(path_to_file,'r') as f:
        for line in f:
            if line.startswith('Loglikelihood:'):
                logL = line.split()[1]
            elif line.startswith('CV error'):
                CV_Error = line.split()[3]
        if logL is None:
            sys.exit(f'Could not find LogL in file {path_to_file}')
        if CV_Error is None:
            sys.exit(f'Could not find CV Error in file {path_to_file}')
        return float(logL), float(CV_Error)


cv_data = []


with open(prefix+'.filemap', 'w') as filemap:
    for directory in glob.glob(os.path.join(admdir, "K_*_R_*")):
        basename = os.path.basename(directory)
        rpath = Path(admdir) / basename
        Qfile = glob.glob(str(rpath / "*.Q"))
        logfile = glob.glob(str(rpath / "*.log"))
        if Qfile and logfile:
            Qfile = Path(Qfile[0]).resolve()
            logfile = logfile[0]
            K = basename.split("_")[1]
            rep = basename.split("_")[3]
            LogL, CV_Error = read_LogFile(logfile)
            filemap.write(f"{basename}\t{K}\t{Qfile}\n")
            cv_data.append({'K': int(K), 'seed': int(rep), 'CV_Error': CV_Error, 'LogL': LogL})


cv_dataframe = pd.DataFrame(cv_data)
cv_dataframe = cv_dataframe.sort_values(by=['K', 'CV_Error', 'LogL'], ascending=[True, True, False])
cv_dataframe.to_csv(f'CV_Error_{prefix}.tsv', sep='\t', index=False)

