---
title: "GlobeTrotter workflow"
author: "Gaston Rijo De León"
format:
  html:
    embed-resources: true
    toc: true
    toc-depth: 2
execute: 
  eval: false
  warning: false
  message: false
  echo: true
date:  today
date-format: 'full'
engine: knitr
---


```{r}
#| eval: true
library(tidyverse)
library(janitor)
library(plotly)
library(glue)
library(MetBrewer)
library(ggridges)
theme_set(theme_bw())
pal <- readxl::read_excel("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/population_colors.xlsx",
                          sheet = 5) 
ind_labels_all <- read_delim("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/labels.tsv",
                         col_names = c("individual", "identifier", "cohort"))

ind_labels_cp <- read_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/01_phase_format/02_full_dataset/chr1/CHP_FILTER1_chr1.labels",
                         col_names = c("individual", "identifier", "included")) %>%
  select(-included)

'%!in%' <- function(x,y)!('%in%'(x,y))
```


# Introduction


# Run globetrotter

We will run globetrotter using the "supervised" chromosome painting, where we paint all the target populations (Polynesians & Fiji) with all the surrogate populations (reference populations).


IMPORTANT: GLOBETROTTER WAS RUN INCLUDING THOSE TARGET INDIVIDUALS THAT ARE NOT PASSED THROUGH THE ECRF FAMILY FILTER. IM RUNNING THIS AGAIN REMOVING THOSE INDIVIDUALS.

## Generate universal files

We need to generate the parameter file, and for this we will need to pass it the copymatrix and a population labels file.

The original population labels file has the fine grained population labels, but we should use a coarser grain to have more samples per population. We will use the archipelago level.


### Labels

Generate labels file for globetrotter. Use SourceFind files, but regroup them in bigger groups (per archipelago).

```{r}
labels_file <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/03_supervised_sourcefind/oceanians_supervised.labels" 
input_file_ids <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/01_oceanian_populations_PA_AN/03_per_pop_locality/globetrotter_locality_filtered.labels"

target_group3 <- c("Cebuano",
                    "Agta",
                    "Bismarck_Islander",
                    "Bougainville",
                    "Solomon_Islander",
                    "Ontong_Java",
                    "RenBell",
                    "Tikopia",
                    "Santa_Cruz",
                    "ni-Vanuatu",
                    "Fiji_Islander",
                    "Tonga_Islander",
                    "Samoa_Islander",
                    "Cook_Islander",
                    "Tuamotu_Islander",
                    "Society_Islander",
                    "Austral_Islander",
                    'Marquesas_Islander',
                    "Gambier_Islander")

surrogate_popnames <- c("Yoruba","Mbuti", # African
                             "French","Orcadian", #European
                             "Han", # East Asian
                             "Atayal","Paiwan", # Taiwanese
                             "Chopccas", # Native American
                           "Papuan_Sepik", # Papuan
                          "Papuan_Highlands", # Papuan 
                          "Mendi", # Papuan
                          "Marawaka", # Papuan
                          "Bundi", # Papuan
                          "Kundiawa", # Papuan
                          "Tari") # Papuan



regrouped_labels <- read_delim(labels_file,
                col_names = c("individual","identifier","include")) %>%
                select(individual,include) %>%
                left_join(ind_labels_cp) %>% 
                left_join(pal, by="identifier") %>%
                select(individual,identifier,group0,group3,include) %>%
                mutate( pop_label = case_when(group3 %in% target_group3 ~ group3,
                                              group3 %!in% target_group3 ~ identifier) ) %>%
                select(individual, pop_label, include)
```

Some individuals come from recent migrations. Filter those out by only keeping individuals where all of their grandparents and parents come from Oceania.

```{r}
targets_to_exclude <- read_delim("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/00_inclusion_tables/mc_exclude_ecrf_family_filter_oceanians.txt")

regrouped_labels <- regrouped_labels %>%
                    mutate(include = case_when(individual %in% targets_to_exclude$individual ~ 0,
                                               pop_label %in% surrogate_popnames ~ 1,
                                               pop_label %in% target_group3 ~ 1,
                                               .default=0))
```


```{r}
regrouped_labels %>%
  write_delim(file = input_file_ids,col_names = FALSE,delim = " ")
```


### Donor populations



```{r}

copymatrix <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/03_supervised_sourcefind/01_supervised_maf5/painting_by_groups/allchr.chunklengths.out"
donor_populations <- read_delim(copymatrix) %>%
  select(-Recipient) %>%
  colnames()
donor_populations <- paste(donor_populations,
                           collapse =  " ")
```




## Test run (Gambier Islander)

Parameter file

```{r}
#| eval: false
parfile <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/01_oceanian_populations_PA_AN/03_per_pop_locality//test.params"
save_file <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/01_oceanian_populations_PA_AN/03_per_pop_locality//test"
save_file_boot <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/01_oceanian_populations_PA_AN/03_per_pop_locality//test"


# Write "homogeneous" populations

surrogate_popnames <- c("Yoruba","Mbuti", # African
                             "French","Orcadian", #European
                             "Han", # East Asian
                             "Atayal","Paiwan", # Taiwanese
                             "Chopccas", # Native American
                           "Papuan_Sepik", # Papuan
                          "Papuan_Highlands", # Papuan 
                          "Mendi", # Papuan
                          "Marawaka", # Papuan
                          "Bundi", # Papuan
                          "Kundiawa", # Papuan
                          "Tari") # Papuan
surrogate_popnames <- paste(surrogate_popnames,collapse = " ")

target_popname <- "Gambier_Islander"

# Write parameter file
params = "prop.ind: 1
bootstrap.date.ind: 0
null.ind: 1
input.file.ids: {input_file_ids}
input.file.copyvectors: {copymatrix}
save.file.main: {save_file}
save.file.bootstraps: {save_file_boot}
copyvector.popnames: {donor_populations}
surrogate.popnames: {surrogate_popnames}
target.popname: {target_popname}
num.mixing.iterations: 5
props.cutoff: 0.001
bootstrap.num: 100
num.admixdates.bootstrap: 1
num.surrogatepops.perplot: 3
curve.range: 1 30
bin.width: 0.1
xlim.plot: 0 30
prop.continue.ind: 0
haploid.ind: 0"

parameters_full <- glue(params)

write(parameters_full,file = parfile)
```

Additional files

```{r}
painting_files_list <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/03_supervised_sourcefind/01_supervised_maf5/local_painting_files/sample_files_list.txt"
recom_files_list <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/01_phase_format/02_full_dataset/recombination_files_list.txt"
```

```{r}
gt <- "fastGlobeTrotter {parfile} {painting_files_list} {recom_files_list} 1 --no-save"
(glue(gt))
```
Does it work?



## Function to facilitate running GLOBETROTTER

1. Create parameter file
  a. Parameter file for run with no bootstrap
  b. Parameter file for run with bootstrap
2. Create slurm scripts for this
3. Run slurm scripts using sbatch


```{r}
run_globetrotter_slurm <- function(output_dir,
                                   target_popname,
                                   bootstrap="0",
                                   admixdates = "1",
                                   copymatrix="",
                                   input_file_ids="",
                                   painting_files_list="/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/03_supervised_sourcefind/01_supervised_maf5/local_painting_files/sample_files_list.txt",
                                   recom_files_list="/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/01_phase_format/02_full_dataset/recombination_files_list.txt"){
  # Create parameter_files
  output_dir <- glue("{output_dir}/{target_popname}")
  dir.create(output_dir)
  parfile <- paste0(output_dir, "/","globetrotter.params")
  save_file <-paste0(output_dir, "/","globetrotter_output")
  # Write "homogeneous" populations
  surrogate_popnames <- c("Yoruba","Mbuti", # African
                               "French","Orcadian", #European
                               "Han", # East Asian
                               "Atayal","Paiwan", # Taiwanese
                               "Chopccas", # Native American
                             "Papuan_Sepik", # Papuan
                            "Papuan_Highlands", # Papuan 
                            "Mendi", # Papuan
                            "Marawaka", # Papuan
                            "Bundi", # Papuan
                            "Kundiawa", # Papuan
                            "Tari") # Papuan
  surrogate_popnames <- paste(surrogate_popnames,collapse = " ")
  # Create donor populations list
  donor_populations <- read_delim(copymatrix) %>%
  select(-Recipient) %>%
  colnames()
donor_populations <- paste(donor_populations,
                           collapse =  " ")
  #### param file
  params = "prop.ind: 1
  bootstrap.date.ind: {bootstrap}
  null.ind: 1
  input.file.ids: {input_file_ids}
  input.file.copyvectors: {copymatrix}
  save.file.main: {save_file}
  save.file.bootstraps: {save_file}_bootstrap
  copyvector.popnames: {donor_populations}
  surrogate.popnames: {surrogate_popnames}
  target.popname: {target_popname}
  num.mixing.iterations: 5
  props.cutoff: 0.001
  bootstrap.num: 100
  num.admixdates.bootstrap: {admixdates}
  num.surrogatepops.perplot: 3
  curve.range: 1 30
  bin.width: 0.1
  xlim.plot: 0 30
  prop.continue.ind: 0
  haploid.ind: 0"
  parameters_full<- glue(params)
  # Write slurm scripts
  ## --
  fileConn<-file(parfile)
  writeLines(parameters_full, fileConn)
  close(fileConn)
  gt <- "fastGlobeTrotter {parfile} {painting_files_list} {recom_files_list} 1 --no-save"
  gt <- (glue(gt))
  ## slurm
  slurm_command <- "#!/bin/bash
#SBATCH -J globetrotter
#SBATCH -o {output_dir}/gt.out -e  {output_dir}/gt.err
#SBATCH --mem=30000
#SBATCH --qos=geh
#SBATCH --partition=geh
module load  R/4.1.0
module load  fastGlobeTrotter/a1dc395
{gt}"
  slurm_script <- glue(slurm_command)
  ## Write to file
  slurm_script_path <- paste0(output_dir, "/","globetrotter.slurm")
  fileConn<-file(slurm_script_path)
  writeLines(slurm_script, fileConn)
  close(fileConn)
  ## Run using slurm
  run_slurm <- "sbatch {slurm_script_path}"
  run_slurm <- glue(run_slurm)
  system(run_slurm)
}
```


## Run on MATAEA

As the only populations that are filtered using eCRF data are from MATAEA, we only need to rerun MATAEA pops.

```{r}
mataea_pops <- c("Tuamotu_Islander",
                    "Society_Islander",
                    "Austral_Islander",
                    'Marquesas_Islander',
                    "Gambier_Islander")
```


Run once to get best conclusion

```{r}
for (pop in mataea_pops){
  run_globetrotter_slurm(output_dir = "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/01_oceanian_populations_PA_AN/03_per_pop_locality/01_first_run",
                       target_popname = pop,input_file_ids =  )
}
```





Run with bootstraps.

```{r}
#| eval: true
conclusions <- read_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/01_oceanian_populations_PA_AN/03_per_pop_locality/01_first_run/conclusions.txt") %>%
  arrange(conclusion) %>%
  mutate(conclusion_number_code=case_when(conclusion == "multiple-dates"~"2",
                                          conclusion == "one-date"~"1",
                                          .default = NA
                                          ))
```


```{r}
for (i in 1:length(conclusions$group3)){
  pop <- conclusions$group3[i]
  conclusion_number <- conclusions$conclusion_number_code[i]
  run_globetrotter_slurm(output_dir = "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/01_oceanian_populations_PA_AN/03_per_pop_locality/02_with_bootstraps",
                       target_popname = pop,
                       bootstrap = "1",
                       admixdates =  conclusion_number )
}
```






```{r}
#| eval: true
boot <- read_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/01_oceanian_populations_PA_AN/03_per_pop_locality/02_with_bootstraps/mc_polynesians_bootstrap_replicates.csv")

newpal_boot <- pal %>%
  filter(group3 %in% boot$target_group) %>%
  select(group3,color) %>%
  distinct()

boot <- boot %>%
  mutate(target_group = factor(target_group, levels=newpal_boot$group3),
         conclusion = factor(conclusion, levels = c("one-date", "multiple-dates"))
         ) %>%
  pivot_longer(cols = starts_with("date"),
             names_to = "date",
             values_to = "estimate")
```



```{r}
#| eval: true
boot %>%
  select(target_group, bootstrap_num) %>%
  distinct() %>%
  group_by(target_group) %>%
  summarise(n=n())
```
### Fit results

```{r}
#| eval: true
boot %>%
  select(-estimate,-date) %>%
  distinct() %>%
  pivot_longer(cols = starts_with("max"),
               names_to = "score",
               values_to = "value") %>%
  ggplot(aes(x=value, y = target_group, color=score, shape =conclusion)) +
  geom_jitter(width = 0) +
  scale_color_manual(values=met.brewer(name="Cassatt2", n=2)) +
  scale_shape_manual(values=c(16,25))
```


### Dates


```{r}
#| eval: true
years_per_generation <- 30
p <- boot %>%
        select(estimate, target_group,date) %>%
        distinct() %>%
        ggplot() +
         geom_density_ridges2(aes(y=target_group, x=estimate*years_per_generation, fill=date),
                              alpha =0.3,
                              quantile_lines = TRUE,
                              quantiles = c(0.025,0.5,0.975)) +
        scale_fill_manual(values = c("#FFFFFF","#FFFFFF")) +
        theme(legend.position = "none") +
        xlab(paste0("Years Ago (",years_per_generation," years per generation)") ) 
p
```

# Native American scheme


```{r}
run_globetrotter_slurm <- function(output_dir,
                                   target_popname,
                                   bootstrap="0",
                                   admixdates = "1",
                                   copymatrix="/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/06_painting_native_american/01_supervised_maf5/painting_by_groups/all_chr.chunklengths.out",
                                   input_file_ids="/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/06_painting_native_american/na_supervised.labels",
                                   painting_files_list="/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/06_painting_native_american/01_supervised_maf5/local_painting_files/sample_files_list.txt",
                                   recom_files_list="/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/01_phase_format/02_full_dataset/recombination_files_list.txt"){
  # Create parameter_files
  output_dir <- glue("{output_dir}/{target_popname}")
  dir.create(output_dir)
  parfile <- paste0(output_dir, "/","globetrotter.params")
  save_file <-paste0(output_dir, "/","globetrotter_output")
  # Write "homogeneous" populations
  surrogate_populations <- c("Yoruba","Mbuti", # African
                            "French","Orcadian", # European
                            "Han", # Chinese 
                            "Chopccas", # Native American
                            "Atiu" # Polynesian
                           )
  surrogate_popnames <- paste(surrogate_popnames,collapse = " ")
  # Create donor populations list
  donor_populations <- read_delim(copymatrix) %>%
  select(-Recipient) %>%
  colnames()
donor_populations <- paste(donor_populations,
                           collapse =  " ")
  #### param file
  params = "prop.ind: 1
  bootstrap.date.ind: {bootstrap}
  null.ind: 1
  input.file.ids: {input_file_ids}
  input.file.copyvectors: {copymatrix}
  save.file.main: {save_file}
  save.file.bootstraps: {save_file}_bootstrap
  copyvector.popnames: {donor_populations}
  surrogate.popnames: {surrogate_popnames}
  target.popname: {target_popname}
  num.mixing.iterations: 5
  props.cutoff: 0.001
  bootstrap.num: 100
  num.admixdates.bootstrap: {admixdates}
  num.surrogatepops.perplot: 3
  curve.range: 1 30
  bin.width: 0.1
  xlim.plot: 0 30
  prop.continue.ind: 0
  haploid.ind: 0"
  parameters_full<- glue(params)
  # Write slurm scripts
  ## --
  fileConn<-file(parfile)
  writeLines(parameters_full, fileConn)
  close(fileConn)
  gt <- "fastGlobeTrotter {parfile} {painting_files_list} {recom_files_list} 1 --no-save"
  gt <- (glue(gt))
  ## slurm
  slurm_command <- "#!/bin/bash
#SBATCH -J globetrotter
#SBATCH -o {output_dir}/gt.out -e  {output_dir}/gt.err
#SBATCH --mem=30000
#SBATCH --qos=geh
#SBATCH --partition=geh
module load  R/4.1.0
module load  fastGlobeTrotter/a1dc395
{gt}"
  slurm_script <- glue(slurm_command)
  ## Write to file
  slurm_script_path <- paste0(output_dir, "/","globetrotter.slurm")
  fileConn<-file(slurm_script_path)
  writeLines(slurm_script, fileConn)
  close(fileConn)
  ## Run using slurm
  run_slurm <- "sbatch {slurm_script_path}"
  run_slurm <- glue(run_slurm)
  system(run_slurm)
}
```



Generate labels file for globetrotter. Use SourceFind files, but regroup them in bigger groups (per archipelago).

```{r}
labels_file <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/06_painting_native_american/na_supervised.labels" 
input_file_ids <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/02_polynesians_NA/globetrotter_locality_filtered.labels"

target_group3 <- c( "Ontong_Java",
                    "RenBell",
                    "Tikopia",
                    "Tonga_Islander",
                    "Samoa_Islander",
                    "Cook_Islander",
                    "Tuamotu_Islander",
                    "Society_Islander",
                    "Austral_Islander",
                    'Marquesas_Islander',
                    "Gambier_Islander")
surrogate_popnames <- c("Yoruba","Mbuti", # African
                            "French","Orcadian", # European
                            "Han", # Chinese 
                            "Chopccas", # Native American
                            "Atiu" # Polynesian
                           )
regrouped_labels <- read_delim(labels_file,
                col_names = c("individual","identifier","include")) %>%
                select(individual,include) %>%
                left_join(ind_labels_cp) %>% 
                left_join(pal, by="identifier") %>%
                select(individual,identifier,group0,group3,include) %>%
                mutate( pop_label = case_when(group3 %!in% target_group3 ~ identifier,
                                              group3 %in% target_group3 ~ group3
                                              ) ) %>%
                mutate( pop_label = case_when(identifier == "Atiu" ~ "Atiu",
                                              .default = pop_label) ) %>%
                select(individual, pop_label, include)
```

Some individuals come from recent migrations. Filter those out by only keeping individuals where all of their grandparents and parents come from Oceania.

```{r}
targets_to_exclude <- read_delim("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/00_inclusion_tables/mc_exclude_ecrf_family_filter_oceanians.txt")

regrouped_labels <- regrouped_labels %>%
                    mutate(include = case_when(individual %in% targets_to_exclude$individual ~ 0,
                                               pop_label %in% surrogate_popnames ~ 1,
                                               pop_label %in% target_group3 ~ 1,
                                               .default=0))
```


```{r}
regrouped_labels %>%
  write_delim(file = input_file_ids,col_names = FALSE,delim = " ")
```


### Donor populations


```{r}
copymatrix <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/06_painting_native_american/01_supervised_maf5/painting_by_groups/all_chr.chunklengths.out"
donor_populations <- read_delim(copymatrix) %>%
  select(-Recipient) %>%
  colnames()
donor_populations <- paste(donor_populations,
                           collapse =  " ")
```



```{r}
#| eval: true
boot <- read_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/02_polynesians_NA/02_with_bootstraps/mc_na_bootstrap_replicates.csv")

newpal_boot <- pal %>%
  filter(group3 %in% boot$target_group) %>%
  select(group3,color) %>%
  distinct()

boot <- boot %>%
  mutate(target_group = factor(target_group, levels=newpal_boot$group3),
         conclusion = factor(conclusion, levels = c("one-date", "multiple-dates"))
         ) %>%
  pivot_longer(cols = starts_with("date"),
             names_to = "date",
             values_to = "estimate")
```



```{r}
#| eval: true
boot %>%
  select(target_group, bootstrap_num) %>%
  distinct() %>%
  group_by(target_group) %>%
  summarise(n=n())
```





## Run on Polynesians


```{r}
polynesian_pops <- c("Ontong_Java",
                    "RenBell",
                    "Tikopia",
                    "Tonga_Islander",
                    "Samoa_Islander",
                    "Cook_Islander",
                     "Tuamotu_Islander",
                    "Society_Islander",
                    "Austral_Islander",
                    'Marquesas_Islander',
                    "Gambier_Islander")
```


Run once to get best conclusion

```{r}
for (pop in polynesian_pops){
  run_globetrotter_slurm(output_dir = "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/02_polynesians_NA/01_first_run",
                       target_popname = pop,input_file_ids = input_file_ids )
}
```





Run with bootstraps.

```{r}
#| eval: true
conclusions <- read_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/02_polynesians_NA/conclusions.txt") %>%
  arrange(conclusion) %>%
  mutate(conclusion_number_code=case_when(conclusion == "multiple-dates"~"2",
                                          conclusion == "one-date"~"1",
                                          .default = NA
                                          ))
```


```{r}
for (i in 1:length(conclusions$group3)){
  pop <- conclusions$group3[i]
  conclusion_number <- conclusions$conclusion_number_code[i]
  run_globetrotter_slurm(output_dir = "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/02_polynesians_NA/02_with_bootstraps",
                       target_popname = pop,
                       bootstrap = "1",
                       admixdates =  conclusion_number,
                       input_file_ids = input_file_ids )
}
```




### Fit results

```{r}
#| eval: true
boot %>%
  select(-estimate,-date) %>%
  distinct() %>%
  pivot_longer(cols = starts_with("max"),
               names_to = "score",
               values_to = "value") %>%
  ggplot(aes(x=value, y = target_group, color=score, shape =conclusion)) +
  geom_jitter(width = 0) +
  scale_color_manual(values=met.brewer(name="Cassatt2", n=2)) +
  scale_shape_manual(values=c(16,25))
```


### Dates


```{r}
#| eval: true
years_per_generation <- 30
p <- boot %>%
        select(estimate, target_group,date) %>%
        distinct() %>%
        ggplot() +
         geom_density_ridges2(aes(y=target_group, x=estimate*years_per_generation, fill=date),
                              alpha =0.3,
                              quantile_lines = TRUE,
                              quantiles = c(0.025,0.5,0.975)) +
        scale_fill_manual(values = c("#FFFFFF","#FFFFFF")) +
        theme(legend.position = "none") +
        xlab(paste0("Years Ago (",years_per_generation," years per generation)") ) 
p
```



# Native American scheme with only NA surogates


```{r}
run_globetrotter_slurm <- function(output_dir,
                                   target_popname,
                                   bootstrap="0",
                                   admixdates = "1",
                                   copymatrix="/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/06_painting_native_american/01_supervised_maf5/painting_by_groups/all_chr.chunklengths.out",
                                   input_file_ids="/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/06_painting_native_american/na_supervised.labels",
                                   painting_files_list="/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/06_painting_native_american/01_supervised_maf5/local_painting_files/sample_files_list.txt",
                                   recom_files_list="/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/01_phase_format/02_full_dataset/recombination_files_list.txt"){
  # Create parameter_files
  output_dir <- glue("{output_dir}/{target_popname}")
  dir.create(output_dir)
  parfile <- paste0(output_dir, "/","globetrotter.params")
  save_file <-paste0(output_dir, "/","globetrotter_output")
  # Write "homogeneous" populations
  surrogate_populations <- c("Yoruba","Mbuti", # African
                            "French","Orcadian", # European
                            "Chopccas", # Native American
                            "Atiu" # Polynesian
                           )
  surrogate_popnames <- paste(surrogate_popnames,collapse = " ")
  # Create donor populations list
  donor_populations <- read_delim(copymatrix) %>%
  select(-Recipient) %>%
  colnames()
donor_populations <- paste(donor_populations,
                           collapse =  " ")
  #### param file
  params = "prop.ind: 1
  bootstrap.date.ind: {bootstrap}
  null.ind: 1
  input.file.ids: {input_file_ids}
  input.file.copyvectors: {copymatrix}
  save.file.main: {save_file}
  save.file.bootstraps: {save_file}_bootstrap
  copyvector.popnames: {donor_populations}
  surrogate.popnames: {surrogate_popnames}
  target.popname: {target_popname}
  num.mixing.iterations: 5
  props.cutoff: 0.001
  bootstrap.num: 100
  num.admixdates.bootstrap: {admixdates}
  num.surrogatepops.perplot: 3
  curve.range: 1 30
  bin.width: 0.1
  xlim.plot: 0 30
  prop.continue.ind: 0
  haploid.ind: 0"
  parameters_full<- glue(params)
  # Write slurm scripts
  ## --
  fileConn<-file(parfile)
  writeLines(parameters_full, fileConn)
  close(fileConn)
  gt <- "fastGlobeTrotter {parfile} {painting_files_list} {recom_files_list} 1 --no-save"
  gt <- (glue(gt))
  ## slurm
  slurm_command <- "#!/bin/bash
#SBATCH -J globetrotter
#SBATCH -o {output_dir}/gt.out -e  {output_dir}/gt.err
#SBATCH --mem=30000
#SBATCH --qos=geh
#SBATCH --partition=geh
module load  R/4.1.0
module load  fastGlobeTrotter/a1dc395
{gt}"
  slurm_script <- glue(slurm_command)
  ## Write to file
  slurm_script_path <- paste0(output_dir, "/","globetrotter.slurm")
  fileConn<-file(slurm_script_path)
  writeLines(slurm_script, fileConn)
  close(fileConn)
  ## Run using slurm
  run_slurm <- "sbatch {slurm_script_path}"
  run_slurm <- glue(run_slurm)
  system(run_slurm)
}
```



Generate labels file for globetrotter. Use SourceFind files, but regroup them in bigger groups (per archipelago).

```{r}
labels_file <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/06_painting_native_american/na_supervised.labels" 
input_file_ids <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/03_polynesians_NA_no_Han/globetrotter_locality_filtered.labels"

target_group3 <- c( "Ontong_Java",
                    "RenBell",
                    "Tikopia",
                    "Tonga_Islander",
                    "Samoa_Islander",
                    "Cook_Islander",
                    "Tuamotu_Islander",
                    "Society_Islander",
                    "Austral_Islander",
                    'Marquesas_Islander',
                    "Gambier_Islander")
surrogate_popnames <- c("Yoruba","Mbuti", # African
                            "French","Orcadian", # European
                            "Chopccas", # Native American
                            "Atiu" # Polynesian
                           )
regrouped_labels <- read_delim(labels_file,
                col_names = c("individual","identifier","include")) %>%
                select(individual,include) %>%
                left_join(ind_labels_cp) %>% 
                left_join(pal, by="identifier") %>%
                select(individual,identifier,group0,group3,include) %>%
                mutate( pop_label = case_when(group3 %!in% target_group3 ~ identifier,
                                              group3 %in% target_group3 ~ group3
                                              ) ) %>%
                mutate( pop_label = case_when(identifier == "Atiu" ~ "Atiu",
                                              .default = pop_label) ) %>%
                select(individual, pop_label, include)
```

Some individuals come from recent migrations. Filter those out by only keeping individuals where all of their grandparents and parents come from Oceania.

```{r}
targets_to_exclude <- read_delim("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/00_inclusion_tables/mc_exclude_ecrf_family_filter_oceanians.txt")

regrouped_labels <- regrouped_labels %>%
                    mutate(include = case_when(individual %in% targets_to_exclude$individual ~ 0,
                                               pop_label %in% surrogate_popnames ~ 1,
                                               pop_label %in% target_group3 ~ 1,
                                               .default=0))
```


```{r}
regrouped_labels %>%
  write_delim(file = input_file_ids,col_names = FALSE,delim = " ")
```


### Donor populations


```{r}
copymatrix <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/06_painting_native_american/01_supervised_maf5/painting_by_groups/all_chr.chunklengths.out"
donor_populations <- read_delim(copymatrix) %>%
  select(-Recipient) %>%
  colnames()
donor_populations <- paste(donor_populations,
                           collapse =  " ")
```



## Run on Polynesians


```{r}
polynesian_pops <- c("Ontong_Java",
                    "RenBell",
                    "Tikopia",
                    "Tonga_Islander",
                    "Samoa_Islander",
                    "Cook_Islander",
                     "Tuamotu_Islander",
                    "Society_Islander",
                    "Austral_Islander",
                    'Marquesas_Islander',
                    "Gambier_Islander")
```


Run once to get best conclusion

```{r}
for (pop in polynesian_pops){
  run_globetrotter_slurm(output_dir = "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/03_polynesians_NA_no_Han//01_first_run",
                       target_popname = pop,input_file_ids = input_file_ids )
}
```





Run with bootstraps.

```{r}
#| eval: true
conclusions <- read_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/02_polynesians_NA/conclusions.txt") %>%
  arrange(conclusion) %>%
  mutate(conclusion_number_code=case_when(conclusion == "multiple-dates"~"2",
                                          conclusion == "one-date"~"1",
                                          .default = NA
                                          ))
```


```{r}
for (i in 1:length(conclusions$group3)){
  pop <- conclusions$group3[i]
  conclusion_number <- conclusions$conclusion_number_code[i]
  run_globetrotter_slurm(output_dir = "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/05_globetrotter/02_polynesians_NA/02_with_bootstraps",
                       target_popname = pop,
                       bootstrap = "1",
                       admixdates =  conclusion_number,
                       input_file_ids = input_file_ids )
}
```













