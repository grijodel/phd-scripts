nextflow.enable.dsl=2                                                           
workflow {
    // Set initial values
    /// Data paths
    skeleton_paramfile = Channel.fromPath(params.skeleton_paramfile)
    sample_file_list = Channel.fromPath(params.sample_file_list)
    recom_file_list = Channel.fromPath(params.recom_file_list)
    globetrotter = Channel.fromPath(params.globetrotter)
    /// Create channels from individuals file
    individuals = Channel
                        .fromPath(params.individuals_list)
                        .splitCsv()
    //////////// WORKFLOW START ////////////
    // Paint chromosomes
    globetrotter_input = individuals.combine(skeleton_paramfile)
                                    .combine(sample_file_list)
                                    .combine(recom_file_list)
                                    .combine(globetrotter)
    RUN_GLOBETROTTER(globetrotter_input)
}

process RUN_GLOBETROTTER {
    scratch true
    publishDir "${params.output_directory}", mode: 'copy'
    module "R/4.1.0:fastGlobeTrotter/a1dc395"
    input:
        tuple val(individual), file(skeleton_paramfile), file(sample_file_list), file(recom_file_list), file(globetrotter)
    output:
        file("*.out")
        file("*.param")

    script:
    """
    generate_paramfile.py ${individual} ${skeleton_paramfile} ${individual}.param
    fastGlobeTrotter ${individual}.param ${sample_file_list} ${recom_file_list}
    """
    stub:
    """
    touch test.out test.param
    """
}




