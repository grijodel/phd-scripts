#!/usr/bin/env python3
import argparse
import os
import glob
from pathlib import Path
import subprocess
import multiprocessing
from functools import partial

# argparse
parser = argparse.ArgumentParser( description = 'Perform within population Hard-Weinberg tests using RUTH. SNPs that are in HW disequilibrium below a p-value threshold are returned.')
parser.add_argument("prefix", type=str, help="Prefix for plink files")
parser.add_argument("outfile", type=str, help="output list of SNPs to exclude")
parser.add_argument("pvalue", type=str, help="P-value threshold, values less or equal than this are considered out of HW equilibrium.")
parser.add_argument("ncpus", type=str, help="Number of cpus to parallelize")
parser.add_argument("field", type=str, help="Field to use for frequency estimation (GL,GT,PL)")

args = parser.parse_args()
#--
prefix=args.prefix
pvalue=float(args.pvalue)
outfile=args.outfile
ncpus=int(args.ncpus)
field=args.field
# Get lists of population ids and create a set
fam=prefix+'.fam'
populations=set()
print( "Reading .fam file..." )
with open(fam,'r') as f:
    for line in f:
        fid, iid, father,mother,sex,phenotype=line.rstrip().split()
        populations.add(fid)

# Create population ids files for filtering
print("Creating population files...")
for population in populations:
    f=open(population+".txt", 'w')
    f.write(population)
    f.close()
# Create function for checking if file is empty
def is_non_zero_file(fpath):  
    return os.path.isfile(fpath) and os.path.getsize(fpath) > 0
# Create function for parallelizing
def hardy(population,thold):
    # create new bed with families
    family=population+".txt"
    convert_plink = [ "plink", "--bfile",
            prefix, "--keep-fam",
            family, "--make-bed",
            "--out", population ]
    convert_plink_p = subprocess.Popen(convert_plink, shell=False)
    convert_plink_p.wait()
    # convert to VCF
    convert_vcf = [ "plink", "--bfile",
            population,"--recode", "vcf-iid","bgz", "--out", population ] 
    convert_vcf_p = subprocess.Popen(convert_vcf, shell=False)
    convert_vcf_p.wait()
    # index
    index = ["tabix" , "-p" , "vcf" , population + ".vcf.gz"]
    index_p = subprocess.Popen(index, shell=False)
    index_p.wait()
    # HW with RUTH
    ruth = ["ruth", "--field", field,   "--site-only" ,"--vcf", population+".vcf.gz",
            "--evec", prefix+".eigenvec.tmp", "--out", population + "_HW.vcf" ]
    ruth_p = subprocess.Popen(ruth, shell=False)
    ruth_p.wait()
    ## compress vcf
    compress = ["bgzip",population + "_HW.vcf"]
    compress_p = subprocess.Popen(compress, shell=False)
    compress_p.wait()
    ## index vcf
    index = ["tabix" , "-p" , "vcf" , population + "_HW.vcf.gz"]
    index_p = subprocess.Popen(index, shell=False)
    index_p.wait()
    # Get snps
    get_hw = ['bcftools' ,'query', 
            '-f', '"%CHROM %POS %REF %ALT %HWE_SLP_I\n"', 
            population + "_HW.vcf.gz"  ]
    get_hw_p = subprocess.Popen(get_hw, shell=False,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    # write snps
    with open(population+".disequilibrium", 'w'):
        for snp in get_hw_p.stdout:
            chrom, pos, ref, alt, pval = snp
            pval=abs(float(pval))
            if pval <= thold:
                f.write(chrom+" "+pos+"\n")
            else:
                pass
print("Compute PCs of dataset...")

# compute PCs
compute_pcs = ["plink","--bfile",prefix,
                "--pca","100","--out", prefix]
compute_pcs_p = subprocess.Popen(compute_pcs, shell=False)
compute_pcs_p.wait()
## remove first column with family names
with open(prefix+".eigenvec", 'r' ) as infile:
    with open(prefix+".eigenvec.tmp", 'w' ) as handle:
        for line in infile:
            l = line.rstrip().split()
            l.pop(0)
            handle.write(" ".join(l)+"\n")



print("Begin to run RUTH...")

pool=multiprocessing.Pool(ncpus)
r=pool.map_async( partial(hardy, thold=pvalue), populations)
r.wait()

# Get unique SNP ids for filtering (in HW disequilibrium in at least one pop)
print("---> Finished")
print("Getting union of SNPs out of HW with p <= {0}...".format(pvalue))
snps_to_exclude = set()
for population in populations:
    if is_non_zero_file( population+".disequilibrium" ):
        with open(population+".disequilibrium") as f:
            chromosome, pos = f.read().splitlines()
        snps_to_exclude.update((chromosome, pos))
# Write SNP ids to exclude 
print("Writing output {0}...".format(outfile))
with open(outfile, 'w') as f:
    for snp in snps_to_exclude:
        chromosome, pos = snp
        f.write(chromosome+" "+pos+"\n")
print("Cleaning temporary files...")
for population in populations:
    fileList = glob.glob(population+'*')
    for filePath in fileList:
        try:
            os.remove(filePath)
        except:
            print("Error while deleting file : ", filePath)
print("Done!")


























