---
title: "Fcs workflow -- Merged Coverage"
author: "Gaston Rijo De León"
format:
  html:
    embed-resources: true
    toc: true
    toc-depth: 2
execute: 
  echo: true
  warning: false
  message: false
  eval: false
date:  today
date-format: 'full'
engine: knitr
---

# Setup

```{r}
#| eval: true
library(tidyverse)
library(janitor)
library(plotly)
library(MetBrewer)
library(qqplotr)
library(here)
library(glue)
library(ggrepel)
library(latex2exp)
library(svglite)
library(qqplotr)
library(gtable)
library(grid)
library(gridExtra)

theme_set(theme_bw())
pal <- readxl::read_excel("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/population_colors.xlsx", sheet = 5) 
ind_labels <- read_delim("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/labels.tsv",col_names = c("individual", "identifier", "cohort"))
'%!in%' <- function(x,y)!('%in%'(x,y))


```

```{r}
#| eval: true
merge_ohana_and_relate <- function(ohana_path,relate_path){
  ohana <- read_table(ohana_path) %>%
    clean_names() %>%
    select(chromosome,position,lle_ratio) %>%
    # Percent rank considering high values of ohana are the ones of interest
    mutate(rank=rank(-lle_ratio,ties.method="random"),
           percent_rank_ohana=rank/(max(rank))) %>%
    rename(ohana="lle_ratio") 
  relate <- read_table(relate_path) %>%
    # Percent rank considering low values of relate are the ones of interest
    mutate(rank=rank(p_val,ties.method="random"),
         percent_rank_relate=rank/(max(rank))) %>%
    rename(relate="p_val")
  s <- full_join(ohana,relate,
                 by=c("chromosome","position")) %>%
        drop_na() %>%
        select(chromosome,
               position,
               ohana,
               relate,
               percent_rank_ohana,
               percent_rank_relate) %>%
        mutate(fcs=-2*(log10(percent_rank_ohana)+log10(percent_rank_relate)))
  return(s)
}
```

```{r}
#| eval: true
fcs <- function(s, outlier_percentile_threshold="99%"){
  # columns: chromosome position s1 s2 ... sN
  fcs <- s %>%
    ungroup() %>%
    group_by(chromosome,position) %>%
    summarise(fcs=-sum(log10(percent_rank+0.00001)))
  fcs_quantiles <- quantile(fcs$fcs, probs =seq(.01, .99, by = .01) )
  thold <- fcs_quantiles[outlier_percentile_threshold]  
  outlier_snps <- fcs %>%
    mutate(outlier=ifelse(fcs>thold, TRUE, FALSE)) 
  return(outlier_snps)
}
```

```{r}
#| eval: true
fcs_window <- function(fcs, chrom, window_size=1000){
  new_fcs <- fcs %>% filter(chromosome==chrom)
  p_holder <- 1:dim(new_fcs)[1]
  for (i in 1:dim(new_fcs)[1]){
    lower_bound = max((i-window_size)/2,0)
    upper_bound = min((i+window_size)/2,dim(new_fcs)[1])
    window = fcs$outlier[lower_bound:upper_bound]
    p = sum(window)/length(window)
    p_holder[i] <- p
  }
  new_fcs$p <- p_holder
  return(new_fcs)
}
```

```{r}
#| eval: true
fcs_window_cm <- function(fcs, chrom, window_size_cm=0.1){
  new_fcs <- fcs %>%
    filter(chromosome==chrom)
  max_coordinate <- dim(new_fcs)[1]
  p_holder <- rep(0,max_coordinate)
  n_snps_holder <- rep(0,max_coordinate)

  for (i in 1:max_coordinate){
          # Get recombination rate for the ith snp
          ith_cm <- new_fcs$cm[i]
          # Get all positions to the left
          left_vector <- new_fcs$outlier[1:i]
          left_window <- c()
          # Only store those where the difference in cM is below the threshold
          for (j in seq(length(left_vector),1,-1)){
            delta <- abs(ith_cm - new_fcs$cm[j])
            if ( delta < (window_size_cm/2) ){
              left_window <- c(left_window,new_fcs$outlier[j])
              # Keep for window
            }else{break}
          }
          # Get all positions to the right
          ## Check if end of table
          if (i != length(new_fcs$outlier)){
            right_vector <- new_fcs$outlier[i+1:length(new_fcs$outlier)]
            right_window <- c()
            # Only store those where the difference in cM is below the threshold
            for (j in seq(i+1,length(new_fcs$outlier),1)){
              delta <- abs(new_fcs$cm[j] - ith_cm)  
              if (delta <= window_size_cm / 2 ){
                right_window <- c(right_window,new_fcs$outlier[j])
                # Keep for window
              }else{break}
            }
            window <- c(left_window, right_window)
          }else{
            # End of table
            window <- left_window
        }
          n_snps <- length(window)
          p <- sum(window)/n_snps
          p_holder[i] <- p
          n_snps_holder[i] <- n_snps 
  }
  new_fcs$p <- p_holder
  new_fcs$n_snps <- n_snps_holder
  return(new_fcs)
}
```

```{r}
#| eval: true
fcs_window_sequential <- function(fcs,window_size=100){
    window_tibble = tibble(
      chromosome = numeric(),
      position = numeric(),
      fcs = numeric(),
      outlier = logical(),
      p = numeric()
    )
    for (i in 1:22){
      tmp <- fcs_window(fcs,i,window_size = window_size)
      suppressWarnings(window_tibble <- full_join(window_tibble,tmp))
    }
    return(window_tibble)
}
```

```{r}
#| eval: true
fcs_window_cm_sequential <- function(fcs,window_size=0.1){
    window_tibble = tibble(
      chromosome = numeric(),
      position = numeric(),
      ohana = numeric(),
      percent_rank_ohana = numeric(),
      relate = numeric(),
      percent_rank_relate = numeric(),
      cm=numeric(),
      fcs = numeric(),
      outlier = logical(),
      p = numeric(),
      n_snps = numeric()
    )
    for (i in 1:22){
      print(paste0("Processing chromosome ", i, "..."))
      tmp <- fcs_window_cm(fcs,i,window_size = window_size)
      window_tibble <- full_join(window_tibble,tmp)
    }
    return(window_tibble)
}
```

```{r}
manhattan_plot <- function(dataframe,column_name="p_value",
                           colors = c("#2E78EF", "#07295F")){
        data_cum <- dataframe %>% 
          group_by(chromosome) %>% 
          summarise(max_position = max(position)) %>% 
          mutate(position_add = lag(cumsum(max_position), default = 0)) %>% 
          select(chromosome, position_add)
        selscan_manhattan <- dataframe %>%
          inner_join(data_cum, by = "chromosome") %>% 
          mutate(position_cum = position + position_add)
        axis_set <- selscan_manhattan %>% 
          group_by(chromosome) %>% 
          summarize(center = mean(position_cum))
        manhplot <- selscan_manhattan %>%
          ggplot(aes(x = position_cum, y = !! sym(column_name), 
                        color = as_factor(chromosome),
                     size = p_val)) +
          geom_point(alpha = 0.5,size=0.8) +
          scale_x_continuous(label = axis_set$chromosome, breaks = axis_set$center) +
          scale_color_manual(values = rep(colors,
                                          unique(length(axis_set$chromosome)))) +
          scale_size_continuous(range = c(0.5,3)) +
          theme( 
            legend.position = "none",
            panel.grid.major.x = element_blank(),
            panel.grid.minor.x = element_blank(),
            axis.text.x = element_text(angle = 60, size = 8, vjust = 0.5)
          ) 
        return(manhplot)
}
```

```{r}
#| eval: true
source("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/00_SCRIPTS/00_script_repository/00_utilities/interpolate_recombination_map_function.R")
```

# Introduction

# Polynesian scan

```{r}
ohana_file <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/06_selection_scans/04_rerun_scans_selected_samples/01_ohana/02_ohana/selscan/K5/MC_for_ohana_component_1.tidy"
relate_file <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/06_selection_scans/04_rerun_scans_selected_samples/02_relate_topology/02_selscan/EXTRACT_Relate.pvals"
```

```{r}
s <- merge_ohana_and_relate(ohana_path = ohana_file,relate_path = relate_file)
```

```{r}
s %>%
  ggplot() + 
  geom_histogram(aes(x=fcs))

s %>%
  ggplot() + 
  geom_density(aes(x=fcs))
```

```{r}
mean(s$fcs)
```

```{r}
di <- "chisq"
dp <- list("df" = 2)
de <- TRUE # enabling the detrend option

s %>% 
    ggplot(., mapping = aes(sample = fcs)) +
      stat_qq_band(distribution=di,dparams=dp,detrend = de) +
      stat_qq_line(distribution=di,dparams=dp,detrend = de) +
      stat_qq_point(distribution=di,dparams=dp,detrend = de)+
      xlab("Theoretical Quantiles") +
      ylab("Sample Quantiles") +
      ggtitle(bquote("QQ plot "~chi^2~ ~(.(2)))) 
```

```{r}
outlier_percentile_threshold <- "99%"
fcs_quantiles <- quantile(s$fcs, probs =seq(.01, .99, by = .01) )
thold <- fcs_quantiles[outlier_percentile_threshold]  
outlier_snps <- s %>%
  mutate(outlier= case_when(
    fcs > thold ~ TRUE,
    fcs <= thold ~ FALSE
  )) %>%
  pull(outlier)
s$outlier <-  outlier_snps       
recombination_map <- s %>%
  ungroup() %>%
  select(chromosome, position) %>%
  distinct() %>%
  interpolate_recombination_map()
f <- full_join(s, recombination_map, by =c("chromosome","position")) %>%
  drop_na()
```

```{r}
f %>%
  ggplot(aes(x=position, y=fcs)) +
  geom_point(aes(color=outlier)) +
  geom_hline(yintercept = thold) +
  facet_wrap(~chromosome, scales="free_x")
```

```{r}
f %>%
  select(-outlier) %>%
  write_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/06_selection_scans/04_rerun_scans_selected_samples/03_fcs/fcs_all_polynesians.tsv")
```

## Compute proportion of outliers per window

```{bash}
cd /pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/06_selection_scans/04_rerun_scans_selected_samples/03_fcs
outlier_proportion_genomewide.py fcs_all_polynesians.tsv fcs_all_polynesians_smooth_99.0_p.tsv fcs cm 0.1 99
outlier_proportion_genomewide.py fcs_all_polynesians.tsv fcs_all_polynesians_smooth_99.9_p.tsv fcs cm 0.1 99.9
```

```{r}
selscan <- read_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/06_selection_scans/04_rerun_scans_selected_samples/03_fcs/fcs_all_polynesians_smooth_99.9_p.tsv") 
```

```{r}
selscan %>%
  filter(is.na(p)) %>%
  summarise(n=n())
```

```{r}
selscan %>%
  ggplot() + geom_histogram(aes(x=n_snps))
```

```{r}
selscan %>%
  ggplot(aes(x=p)) +
  geom_histogram()

selscan %>%
  ggplot(aes(x=p)) +
  geom_density()

selscan %>%
  filter(p != 0) %>%
  ggplot(aes(x=p)) +
  geom_density()
```

## Bin SNPs according to number of SNPs used to compute $p$

```{r}
bin_using_nsnps <- function(n_snps){
  breaks=c(250,500,750,1000)
  bin <- case_when(
    n_snps <= breaks[1] ~ 1,
    n_snps <= breaks[2] ~ 2,
    n_snps <= breaks[3] ~ 3,
    n_snps <= breaks[4] ~ 4,
    n_snps > breaks[4] ~ 5
  )
  return(bin)
}
```

```{r}
n_snps_threshold <- 50
breaks <- c(250,500,750,1000)
```

```{r}
selscan_binned <- selscan %>%
  filter(n_snps > n_snps_threshold) %>%
  mutate(n_snp_bin=bin_using_nsnps(n_snps)) %>%
  mutate(n_snp_bin=factor(n_snp_bin)) 
selscan_binned %>%
  ggplot(aes(x=p, color=n_snp_bin)) +
    geom_density() 

selscan_binned %>%
  ggplot(aes(x=log10(p), color=n_snp_bin)) +
    geom_density() 
```


## Bin SNPs according to Minor Allele Frequency



## Locate candidate regions

```{r}
selscan <- read_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/06_selection_scans/04_rerun_scans_selected_samples/03_fcs/fcs_all_polynesians_smooth_99.9_p_regions.tsv") 
```


First define p outliers based on number of SNPs or MAF, then compute candidate regions


```{r}
selscan %>%
  filter(is_outlier_p == TRUE) %>%
  group_by(chromosome,outlier_region, max_fcs_in_region) %>% 
  summarise(n_snps = n()) %>%
  ggplot() + 
  geom_histogram(aes(x=n_snps))

selscan %>%
  filter(is_outlier_p == TRUE) %>%
  group_by(chromosome,outlier_region, max_fcs_in_region) %>% 
  summarise(n_snps = n()) %>%
  filter(n_snps > 50) %>%
  ggplot() + 
  geom_histogram(aes(x=n_snps))
  
selscan %>%
  filter(is_outlier_p == TRUE) %>%
  group_by(chromosome,outlier_region, max_fcs_in_region) %>% 
  summarise(n_snps = n()) %>%
  filter(n_snps > 100)  
```

```{r}
selscan %>%
  filter(chromsome == query_chromosome) %>%
ggplot(aes(x=cm,y=fcs)) +
  geom_point(color="black",alpha=0.3) +
  geom_vline(aes(x))
```


















## Look at genome-wide outliers

```{r}
#| eval: true
thresholds <- selscan_binned %>%
  group_by(n_snp_bin) %>%
  summarise(threshold=quantile(p, probs =seq(.01, .99, by = .01) )["99%"]) 
thresholds %>%
  knitr::kable()
```

```{r}
#| eval: true
selscan_thold <- selscan_binned %>%
                  left_join(thresholds, by=c("n_snp_bin")) %>%
                  mutate(p_outlier = case_when( p > threshold ~ TRUE,
                                                p <= threshold ~ FALSE) )
```

```{r}
#| eval: true
chr <- 6
outliers <- selscan_thold %>%
  filter(chromosome==chr) %>%
  filter(p_outlier==TRUE)
selscan_thold %>%
  filter(p_outlier==FALSE | fcs_outlier == FALSE ) %>%
  filter(chromosome==chr) %>%
  ggplot(aes(x=position,y=fcs)) +
    geom_point(size=0.4,alpha=0.3) +
    geom_point(data=outliers,color="#DA4167",size=0.5,alpha=0.3) +
    geom_line(aes(x=position,y=p*100,color="blue")) +
    theme(legend.position="none")
```

```{r}
#| eval: true
chr <- 6
outliers <- selscan_thold %>%
  filter(chromosome==chr) %>%
  filter(p_outlier==TRUE)
selscan_thold %>%
  filter(p_outlier==FALSE | fcs_outlier == FALSE ) %>%
  filter(chromosome==chr) %>%
  ggplot(aes(x=position,y=-log10(relate))) +
    geom_point(size=0.4,alpha=0.3) +
    geom_point(data=outliers,color="#DA4167",size=0.5,alpha=0.3) +
    geom_line(aes(x=position,y=p*100,color="blue")) +
    theme(legend.position="none")
```

```{r}
#| eval: true
chr <- 6
outliers <- selscan_thold %>%
  filter(chromosome==chr) %>%
  filter(p_outlier==TRUE)
selscan_thold %>%
  filter(p_outlier==FALSE | fcs_outlier == FALSE ) %>%
  filter(chromosome==chr) %>%
  ggplot(aes(x=position,y=ohana)) +
    geom_point(size=0.4,alpha=0.3) +
    geom_point(data=outliers,color="#DA4167",size=0.5,alpha=0.3) +
    geom_line(aes(x=position,y=p*10,color="blue")) +
    theme(legend.position="none")
```

## Specific SNP

```{r}
snp_name="rs374808845"
query_chromosome=19
query_position=11421898
closest_gene="TSPAN16"
window_size=1e6

thold <- quantile(selscan$fcs, probs = 0.9999)
selscan %>%
  filter(chromosome == query_chromosome) %>%
  ggplot(aes(x=position)) +
    geom_vline(xintercept = query_position, linetype="dashed", color="maroon") +
    geom_hline(yintercept = thold, linetype="dashed", "red") +
    geom_point(aes(y=fcs), alpha =0.7) +
    xlim(query_position - window_size /2, query_position + window_size/2 )
ggsave("~/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/06_selection_scans/04_rerun_scans_selected_samples/03_fcs/rs374808845.png", dpi=1000, width = 10, height = 6)
library(locuszoomr)

chrom <- selscan %>%
      dplyr::filter(chromosome==query_chromosome)
loc <- locus(chrom, 
             gene=closest_gene,
             yvar = "fcs" ,
             flank = window_size,
             ens_db = EnsDb.Hsapiens.v75::EnsDb.Hsapiens.v75,)
locus_zoom <- locus_ggplot(loc,
                          filter_gene_biotype = "protein_coding")
ggsave("~/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/06_selection_scans/04_rerun_scans_selected_samples/03_fcs/rs374808845_locuszoom.png", dpi=1000, width = 10, height = 6)
```
