library(tidyverse)
fam <- read_delim("/Volumes/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/HC_MAF_PRUNED.fam", col_names=FALSE, delim=" ")
fam %>% rename(FID="X1", IID="X2") -> fam
# First small test - East, West, and Outlier (Polynesian)
target_pops <- c("Atiu", "Tonga", "Tikopia", "Mbuti")
fam %>% filter(FID%in%target_pops) %>% select(FID, IID) %>% write_delim("/Volumes/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/06_admixture_graph/admixturebayes/POL_Mbuti/POL_Mbuti.keep", delim="\t", col_names=FALSE, quote="none")
# POL with Ref pops
target_pops <- c("Atiu", "Western_Samoa", "Tikopia", "Nakanai_Bileki", "Paiwan", "French", "Papuan_Highlands", "Mbuti")
fam %>% filter(FID%in%target_pops) %>% select(FID, IID) %>% write_delim("/Volumes/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/06_admixture_graph/admixturebayes/POL_Ref/POL_Ref.keep", delim="\t", col_names=FALSE, quote="none")
# POL with Ref pops (select individuals with no European admixture based on ADMIXTURE K6)
noFRE <- read_delim("/Volumes/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/06_admixture_graph/admixturebayes/POL_PNGvsAN/K6_noFRE_POL.txt", delim="\t")
#target_pops <- c("Atiu", "Western_Samoa", "Tikopia", "Bellona", "Nakanai_Bileki", "Paiwan", "Cebuano", "Bougainville", "Papuan_Highlands", "Mbuti")
target_pops <- c("Atiu", "Western_Samoa", "Tikopia", "Nakanai_Bileki", "Paiwan", "Papuan_Highlands", "Mbuti")
POL_to_remove <- fam %>% filter(FID%in%c("Atiu", "Western_Samoa", "Tikopia", "Bellona")) %>% filter(!IID%in%noFRE$ind) 
fam %>% filter(FID%in%target_pops) %>% filter(!IID%in%POL_to_remove$IID) %>% select(FID, IID) %>% write_delim("/Volumes/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/06_admixture_graph/admixturebayes/POL_PNGvsAN0/POL_PNGvsAN.keep", delim="\t", col_names=FALSE, quote="none")
# EP with Ref pops
target_pops <- c("Atiu", "Rangiroa", "Tahiti", "Raivavae", "Nuku_Hiva", "Mangareva", "Pima", "Chopccas", "Han", "French", "Mbuti")
fam %>% filter(FID%in%target_pops) %>% select(FID, IID) %>% write_delim("/Volumes/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/06_admixture_graph/admixturebayes/EP_Ref/EP_Ref.keep", delim="\t", col_names=FALSE, quote="none")
# WP with Ref pops
target_pops <- c("Western_Samoa", "Tonga", "Fiji", "Nakanai_Bileki", "Bougainville", "Papuan_Highlands", "Paiwan", "Han", "French", "Mbuti")
fam %>% filter(FID%in%target_pops) %>% select(FID, IID) %>% write_delim("/Volumes/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/06_admixture_graph/admixturebayes/WP_Ref/WP_Ref.keep", delim="\t", col_names=FALSE, quote="none")
# PO with Ref pops
target_pops <- c("Ontong_Java", "Bellona", "Tikopia", "Nakanai_Bileki", "Bougainville", "Papuan_Highlands", "Paiwan", "Han", "French", "Mbuti")
fam %>% filter(FID%in%target_pops) %>% select(FID, IID) %>% write_delim("/Volumes/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/06_admixture_graph/admixturebayes/PO_Ref/PO_Ref.keep", delim="\t", col_names=FALSE, quote="none")



