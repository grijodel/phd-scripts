#!/usr/bin/env python3
import argparse
import vcf
import pickle

parser = argparse.ArgumentParser( description = "Create a python dictionary for storing SNPdb info in the way dict[rs]=[chromosome,position,ref,[alternatives]].")
parser.add_argument("VCF", type=str, help="VCF containing rsids")
parser.add_argument("output_basename",  type=str, help="basename for pickle output" )

args=parser.parse_args()
ref_vcf=args.VCF
# Read VCF reference file into dictionary
ref_dict = {} # ref_dict[rsID]=[chromosome, position]
vcf_reader=vcf.Reader(open(ref_vcf, 'r'))

print("READING VCF")
for record in vcf_reader:

    if record.ID != '.':

        chromosome=record.CHROM
        position=record.POS
        ref_dict[record.ID]=[chromosome,position]
    else:
        pass

print("WRITING TO PICKLE DICT")
# Write it to a pickle file
with open(args.output_basename+'.pickle', 'wb') as handle:
    pickle.dump(ref_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

print("DONE!")
