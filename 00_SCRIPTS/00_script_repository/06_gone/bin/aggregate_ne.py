#!/usr/bin/env python3
import glob
import sys
prefix=sys.argv[1]
jackknife_files=glob.glob("Output_Ne_{0}_block_*".format(prefix))
n_snps_files_jackknife=glob.glob("N_snps_{0}_block_*.txt".format(prefix))

full_dataset_file="Output_Ne_{0}".format(prefix)
n_snps_full_dataset_file="N_snps_{0}.txt".format(prefix)
ne_outfile= prefix + "_Ne_estimates.tsv"
n_snps_outfile= prefix + "_N_snps.tsv"
##Aggregate Ne estimates##

with open(ne_outfile, 'w' ) as f:
    f.write("generation\tNe\tblock\n")
    ## jackknife estimates
    for block in jackknife_files:
        block_number = block.split(".")[0].split("_")[-1]
        with open(block,'r') as blockfile:
            next(blockfile)
            next(blockfile)
            for line in blockfile:
                generation, ne=line.rstrip().split()
                newline = "{0}\t{1}\t{2}\n".format(generation,ne,block_number)
                f.write(newline)
    ## full dataset
    with open(full_dataset_file,'r') as full_data:
            next(full_data)
            next(full_data)
            for line in full_data:
                generation,ne=line.rstrip().split()
                newline = "{0}\t{1}\t{2}\n".format(generation,ne,"FULL_DATASET")
                f.write(newline)


## Aggregate number of SNPs ##
with open(n_snps_outfile, 'w') as f:
    f.write("block\tnumber_of_snps\n")
    ## jackknife estimates
    for block in n_snps_files_jackknife:
        block_number = block.split(".")[0].split("_")[-1]
        with open(block,'r') as b:
            nsnps=b.readline().split()[0]
            f.write("{0}\t{1}\n".format(block_number,nsnps))

    ## full dataset
    with open(n_snps_full_dataset_file,'r') as b:
        nsnps=b.readline().split()[0]
        block_number="FULL_DATASET"
        f.write("{0}\t{1}\n".format(block_number,nsnps))

            


