### ____________________________________________________________
.libPaths("/pasteur/homes/lrubioar/R/x86_64-pc-linux-gnu-library/3.6")
source("../../bin/FinestructurePlots/FinestructureLibrary.R") # read in the R functions, which also calls the needed packages
library(dendextend)
library(ggplot2)
library(reshape2)
library(viridis)
library(plotrix)
# library(maps)
# library(mapplots)
library(RColorBrewer)
library(dendextend)
library(factoextra)
library(data.table)

seed=135
K.dendo=136

seed=186
K.dendo=131

seed=437
K.dendo=131

seed=859
K.dendo=131

seed=994
K.dendo=132

args <- commandArgs(TRUE)
seed <- args[1]
K.dendo <- args[2]
K.dendo <- as.numeric(K.dendo)

print(seed)
print(K.dendo)

treefile<- paste0("Fs.seed", seed, ".chunkcounts.tree.out") ## finestructure tree file
###### READ IN THE TREE FILES
treexml<-xmlTreeParse(treefile) ## read the tree as xml format
ttree<-extractTree(treexml) ## extract the tree into ape's phylo format
## If you dont want to plot internal node labels (i.e. MCMC posterior assignment probabilities)
## now is a good time to remove them via:
#     ttree$node.label<-NULL
## Will will instead remove "perfect" node labels
ttree$node.label[ttree$node.label=="1"] <-""
## And reduce the amount of significant digits printed:
ttree$node.label[ttree$node.label!=""] <-format(as.numeric(ttree$node.label[ttree$node.label!=""]),digits=2)
tdend<-myapetodend(ttree,factor=1) # convert to dendrogram format
max.K.inferredState <- as.data.frame.myres(treexml)$K
print("Tree read")

Get.coincidence <- function(K.dendo){
  sum.Yi.Xi.AllInd.df <- read.table(paste0("Robustness_seed", seed, "_K", K.dendo, ".txt"), header=T)
  which(as.numeric(unlist(apply(sum.Yi.Xi.AllInd.df, 1, sum))) != 100)
  inferredState <- get_subdendrograms(tdend,K.dendo)
  inferredState.clusters <- lapply(inferredState, function(x) x %>% labels)
  coincidence <- sapply(1:K.dendo, function(x)
    length(which(!inferredState.clusters[[x]] %in% names(which(apply(sum.Yi.Xi.AllInd.df,1,which.max)==x)))))
  return(coincidence)
  }

threshold=90
Get.coincidence.threshold <- function(K.dendo){
  sum.Yi.Xi.AllInd.df <- read.table(paste0("Robustness_seed", seed, "_K", K.dendo, ".txt"), header=T)
  which(as.numeric(unlist(apply(sum.Yi.Xi.AllInd.df, 1, sum))) != 100)
  inferredState <- get_subdendrograms(tdend,K.dendo)
  inferredState.clusters <- lapply(inferredState, function(x) x %>% labels)
  coincidence <- sapply(1:K.dendo, function(K)
    length(which(
      !inferredState.clusters[[K]] %in% 
       names(which(apply(sum.Yi.Xi.AllInd.df,1,function(x)which(x>threshold))==K))
      )))
  return(coincidence)
}

Get.coincidence.threshold.names <- function(K.dendo){
  sum.Yi.Xi.AllInd.df <- read.table(paste0("Robustness_seed", seed, "_K", K.dendo, ".txt"), header=T)
  which(as.numeric(unlist(apply(sum.Yi.Xi.AllInd.df, 1, sum))) != 100)
  inferredState <- get_subdendrograms(tdend,K.dendo)
  inferredState.clusters <- lapply(inferredState, function(x) x %>% labels)
  coincidence <- sapply(1:K.dendo, function(K)
    inferredState.clusters[[K]][
      !inferredState.clusters[[K]] %in% 
        names(which(apply(sum.Yi.Xi.AllInd.df,1,function(x)which(x>threshold))==K))
      ])
  return(coincidence)
}

Get.means <- function(K.dendo){
  sum.Yi.Xi.AllInd.df <- read.table(paste0("Robustness_seed", seed, "_K", K.dendo, ".txt"), header=T)
  which(as.numeric(unlist(apply(sum.Yi.Xi.AllInd.df, 1, sum))) != 100)
  inferredState <- get_subdendrograms(tdend,K.dendo)
  inferredState.clusters <- lapply(inferredState, function(x) x %>% labels)
  coincidence <- sapply(1:K.dendo, function(K)
      mean(sum.Yi.Xi.AllInd.df[rownames(sum.Yi.Xi.AllInd.df) %in% inferredState.clusters[[K]],K])
  )
  return(coincidence)
}


coincidence.all <- lapply(2:K.dendo, function(K.dendo) Get.coincidence(K.dendo))
coincidence.all.threshold <- lapply(2:K.dendo, function(K.dendo) Get.coincidence.threshold(K.dendo))
coincidence.all.threshold.names <- lapply(2:K.dendo, function(K.dendo) Get.coincidence.threshold.names(K.dendo))
coincidence.means <- cbind(2:K.dendo,unlist(lapply(2:K.dendo, function(K.dendo) mean(Get.means(K.dendo)))),
                           unlist(lapply(2:K.dendo, function(K.dendo) sd(Get.means(K.dendo)))))
colnames(coincidence.means) <- c("K", "Mean_index", "SD")


coincidence.all.threshold.df <- as.data.frame(cbind(2:K.dendo, unlist(lapply(coincidence.all.threshold.names, function(x) length(unlist(x))))))

remove <- lapply(coincidence.all.threshold.names, function(x) unlist(x))
remove.df <- matrix(NA,nrow=max(unlist(lapply(remove, length))), ncol=length(remove))
colnames(remove.df) <- paste0("cluster_", 2:K.dendo)
for(x in 1:ncol(remove.df)){
  if(length(remove[[x]])!= 0){
    remove.df[1:length(remove[[x]]),x] <- unlist(remove[x])
  }  
}

write.table(remove.df, paste0("As_Leslie_indRemove_seed", seed, ".txt"), quote = F, row.names = F)
write.table(coincidence.means, paste0("As_Leslie_MEAN_seed", seed, ".txt"), quote = F, row.names = F)

pdf(paste0("As_Leslie_indRemove_seed", seed, ".pdf"))
  plot(coincidence.all.threshold.df[,2], coincidence.all.threshold.df[,1], pch=19, axes = F, ylab="",
       xlab=paste("number of individuals below threshold", threshold), type="o", col="gray60")
  axis(side = 1, at = c(seq(0,max(coincidence.all.threshold.df[,2]+20), by=20)),cex.axis=0.7)
  axis(side = 2, at = c(seq(0,max(coincidence.all.threshold.df[,1]+20), by=10)),las=2, cex.axis=0.7)
  # abline(v=100, lty=2)
  # abline(h=unique(sum.M.ALLK.df.ggplot$K)[max(which(num.ind.threshold<=100))],
  #        lty=2)
  abline(h=25, lty=2)
  abline(v=coincidence.all.threshold.df[,2][which(coincidence.all.threshold.df[,1]==25)],
         lty=2)
  mtext(side=2, line=2.5,"K",cex=1, las=2)
dev.off()


###
## PLOTS FOR Anlize_As_Leslie outputs

library(ggsci)
library(cowplot)
library(data.table)

seeds <- c(135, 186, 437, 859, 994)

REMOVE <- lapply(seeds, function(seed) read.table(paste0("As_Leslie_indRemove_seed", seed, ".txt"), header=T))

num.remove <- lapply(REMOVE, function(x) apply(x,2,function(y)length(as.vector(na.omit(y)))))

individuals <- tdend %>% labels

num.remove.gg <- NULL
for (seed in 1:5){
  num.remove.gg.one <- cbind(seeds[seed],names(num.remove[[seed]]), as.numeric(num.remove[[seed]]/length(individuals)*100))
  num.remove.gg <- rbind(num.remove.gg, num.remove.gg.one)
}
num.remove.gg  <- as.data.frame(num.remove.gg)
colnames(num.remove.gg) <- c("Seed", "cluster", "filter")
num.remove.gg$cluster <- as.numeric(unlist(lapply(strsplit(num.remove.gg$cluster, "_"), function(x)x[[2]])))

P.filter <- ggplot(num.remove.gg, aes(x=cluster, y=as.numeric(filter), color=as.character(Seed)))+
  geom_point(size=0.5)+
  geom_line()+
  scale_color_simpsons()+
  theme_minimal()+
  labs(x="K", y="Individuals below 0.9 threshold", color="Seed")

P.filter.zoom <- ggplot(num.remove.gg[num.remove.gg$cluster<50,], aes(x=cluster, y=as.numeric(filter), color=as.character(Seed)))+
  geom_point(size=0.5)+
  geom_line()+
  scale_color_simpsons()+
  theme_minimal()+
  labs(x="K", y="Individuals below 0.9 threshold", color="Seed")


MEANS <- lapply(seeds, function(seed) cbind(Seed=seed, read.table(paste0("As_Leslie_MEAN_seed", seed, ".txt"), header=T)))
MEANS <- rbindlist(MEANS)

P.means <- ggplot(MEANS, aes(x=K, y=as.numeric(Mean_index), color=as.character(Seed)))+
  geom_point(size=0.8)+
  geom_line()+
  scale_color_simpsons()+
  # geom_ribbon(aes(ymin = MEANS[MEANS$K<30,]$Mean_index - MEANS[MEANS$K<30,]$SD,
  #                 ymax = 100, fill=as.character(Seed)),alpha=0.1, color=NA)+
  # scale_fill_viridis_d(option = "viridis")+
  theme_minimal()+
  labs(x="K", y="Mean Sum(Xi/Yi)", colour="Seed")
  
P.means.zoom <- ggplot(MEANS[MEANS$K < 50,], aes(x=K, y=as.numeric(Mean_index), color=as.character(Seed)))+
  geom_point(size=0.8)+
  geom_line()+
  scale_color_simpsons()+
  # geom_ribbon(aes(ymin = MEANS[MEANS$K<30,]$Mean_index - MEANS[MEANS$K<30,]$SD,
  #                 ymax = 100, fill=as.character(Seed)),alpha=0.1, color=NA)+
  # scale_fill_viridis_d(option = "viridis")+
  theme_minimal()+
  labs(x="K", y="Mean Sum(Xi/Yi)", colour="Seed")

P.sd <- ggplot(MEANS, aes(x=K, y=as.numeric(Mean_index), color=as.character(Seed)))+
  geom_point(size=0.8)+
  geom_line()+
  scale_color_simpsons()+
  geom_ribbon(aes(ymin = Mean_index - SD,
                 ymax = 100, fill=as.character(Seed)),alpha=0.1, color=NA)+
  scale_fill_simpsons()+
  theme_minimal()+
  labs(x="K", y="Mean Sum(Xi/Yi)", color="Seed", fill="Seed")

P.sd.zoom <- ggplot(MEANS[MEANS$K < 50,], aes(x=K, y=as.numeric(Mean_index), color=as.character(Seed)))+
  geom_point(size=0.8)+
  geom_line()+
  scale_color_simpsons()+
  geom_ribbon(aes(ymin = Mean_index - SD,
                  ymax = 100, fill=as.character(Seed)),alpha=0.1, color=NA)+
  scale_fill_simpsons()+
  theme_minimal()+
  labs(x="K", y="Mean Sum(Xi/Yi)", color="Seed", fill="Seed")

pdf("Plots_analize_as_leslie.pdf")
plot_grid(P.filter, P.filter.zoom, P.means, P.means.zoom, P.sd, P.sd.zoom, ncol = 2)
dev.off()

# slop <- function(SEED) {
#   (as.numeric(num.remove.gg[num.remove.gg$Seed==SEED,]$filter[-1])
#    - as.numeric(num.remove.gg[num.remove.gg$Seed==SEED,]$filter[1:(length(num.remove.gg[num.remove.gg$Seed==SEED,]$filter)-1)])) /
#     (as.numeric(num.remove.gg[num.remove.gg$Seed==SEED,]$cluster[-1])-
#        as.numeric(num.remove.gg[num.remove.gg$Seed==SEED,]$cluster[1:(length(num.remove.gg[num.remove.gg$Seed==SEED,]$cluster)-1)]))
# }
# 
# num.remove.gg$filter <- num.remove.gg$filter+1
# incremento <- function(SEED){
#   (as.numeric(num.remove.gg[num.remove.gg$Seed==SEED,]$filter[-1])
#    - as.numeric(num.remove.gg[num.remove.gg$Seed==SEED,]$filter[1:(length(num.remove.gg[num.remove.gg$Seed==SEED,]$filter)-1)])) /
#     as.numeric(num.remove.gg[num.remove.gg$Seed==SEED,]$filter[1:(length(num.remove.gg[num.remove.gg$Seed==SEED,]$filter)-1)])
# }
# 
# num.increase.gg <- NULL
# for (seed in 1:5){
#   num.increase.gg.one <- cbind(seeds[seed],names(num.remove[[seed]])[-length(num.remove[[seed]])], incremento(seeds[seed]))
#   num.increase.gg <- rbind(num.increase.gg, num.increase.gg.one)
# }
# num.increase.gg  <- as.data.frame(num.increase.gg)
# colnames(num.increase.gg) <- c("Seed", "cluster", "filter")
# num.increase.gg$cluster <- as.numeric(unlist(lapply(strsplit(num.increase.gg$cluster, "_"), function(x)x[[2]])))
# 
# ggplot(num.increase.gg, aes(x=cluster, y=as.numeric(filter), color=as.character(Seed)))+
#   geom_point(size=0.5)+
#   geom_line()+
#   scale_color_simpsons()+
#   theme_minimal()
# 
# ggplot(num.increase.gg[num.increase.gg$cluster<30,], aes(x=cluster, y=as.numeric(filter), color=as.character(Seed)))+
#   geom_point(size=0.5)+
#   geom_line()+
#   scale_color_simpsons()+
#   theme_minimal()
