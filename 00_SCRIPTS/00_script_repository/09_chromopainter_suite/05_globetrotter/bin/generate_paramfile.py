#!/usr/bin/env python3
import sys
'''
modify an existing globetrotter skeleton file to run per individual
'''
target_individual=sys.argv[1]
skeleton_paramfile=sys.argv[2]
output_paramfile=sys.argv[3]
with open(output_paramfile,'w') as outfile:
    with open(skeleton_paramfile,'r') as handle:
        for line in handle:
            if line.startswith("save.file.main"):
                newline="save.file.main: {0}_main.out\n".format(target_individual)
                outfile.write(newline)
            elif line.startswith("save.file.bootstraps"):
                newline="save.file.bootstraps: {0}_boot.out\n".format(target_individual)
                outfile.write(newline)
            elif line.startswith("target.popname"):
                newline="target.popname: {0}\n".format(target_individual)
                outfile.write(newline)
            else:
                outfile.write(line)
