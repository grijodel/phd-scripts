---
title: "FLARE LA quality control" 
author: "Gaston Rijo De León"
format:
  html:
    embed-resources: true
    toc: true
    toc-depth: 5
    fig-dpi: 400
execute: 
  echo: true
  warning: false
  message: false
  eval: false
date:  today
date-format: 'full'
engine: knitr
---

# Setup


```{r, warning=FALSE, message=FALSE, eval=TRUE}
#| eval: true
library(tidyverse)
library(janitor)
library(plotly)
library(MetBrewer)
library(glue)
theme_set(theme_bw())
pal <- readxl::read_excel("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/population_colors.xlsx", sheet = 4) 
'%!in%' <- function(x,y)!('%in%'(x,y))
ind_labels <- read_delim("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/labels.tsv",col_names = c("individual", "identifier", "cohort"))
```



# Introduction

Reading FLARE's output and checking if the results make sense.

# High coverage dataset

## Local ancestry

Read flare files.

```{r}
#| warning: false
la_groups <- c("NATIVE_AMERICAN",
               "PAPUAN",
               "AN_PROXY",
               "AFRICAN",
               "EUROPEAN",
               "EAST_ASIAN")
la <- tibble(individual=character(),
             la_group = character(),
             proportion = numeric(),
             chromosome = numeric()
             )
for (i in 1:22){
  tmp <- read_delim(paste0("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/05_local_ancestry/01_FLARE/02_6_ANCESTRIES/flare_chr",
                         i,
                         ".global.anc.gz"),
                         col_names = c("individual", la_groups)) %>%
    mutate(chromosome=i) %>%
    pivot_longer(names_to = "la_group",
                 values_to = "proportion",
                 cols = all_of(la_groups))

  la <- full_join(la, tmp)  
}
```

```{r}
#| eval: true
la_path <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/05_local_ancestry/01_FLARE/02_6_ANCESTRIES/ancestry_per_chromosome.tsv"
```


```{r}
write_delim(x = la,
            file=la_path)
```


```{r}
#| eval: true
la_groups <- c("AFRICAN",
               "EUROPEAN",
               "EAST_ASIAN",
               "AN_PROXY",
               "PAPUAN",
               "NATIVE_AMERICAN")
la <- read_delim(la_path) %>%
  mutate(la_group=factor(la_group, levels=la_groups)) %>%
  filter(individual != "UV1003")
```



Read recombination files to get number of SNPs per chromosome.


```{r}
recom <- tibble(X1=numeric(),
                X3=numeric(),
                X4=numeric())
for (i in 1:22){
  tmp <- read_delim(
    paste0("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/05_local_ancestry/01_FLARE/01_metadata/01_recombination_maps/snps_chr",i,".map"), col_names = FALSE) %>%
    select(-X2)
  recom <- full_join(recom,tmp)
}
recom <- recom %>%
  rename(chromosome="X1",
         position="X4",
         cM="X3")
```


```{r}
n_snps <- recom %>%
  group_by(chromosome) %>%
  summarise(n=n())
```

```{r}
#| eval: true
n_snps_path <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/05_local_ancestry/01_FLARE/02_6_ANCESTRIES/n_snps.tsv"
```


```{r}
write_delim(x = n_snps,
            file=n_snps_path)
```



```{r}
#| eval: true
n_snps <- read_delim(n_snps_path)
```



Weighted average of proportions

```{r}
#| eval: true


la_avg <- la %>%
            left_join(n_snps) %>%
            group_by(individual,la_group) %>%
            summarise(total_length_sum = sum(proportion*n)) %>%
  mutate(average_proportion = total_length_sum / sum(total_length_sum) )
```


```{r}
#| eval: true

la_avg <- la_avg %>%
  left_join(ind_labels) %>% 
  select(-cohort) %>% 
  left_join(pal) %>%
  mutate(identifier=factor(identifier,pal$identifier),
         group3=factor(group3,unique(pal$group3))) %>%
  mutate(la_group=factor(la_group, levels=la_groups))
```

```{r}
#| eval: true
la_groups
colors <- c("#a00000",
            "#111344",
            "#b1b100",
            "#912f40",
            "#760b9f",
            "#FFF36D"
            )
```


```{r}
#| eval: true
la_avg %>%
  ggplot(aes(x=individual,y=average_proportion,fill=la_group)) +
   geom_bar(stat="identity") +
   facet_wrap(~group3, scales = "free") +
   theme(axis.title.x=element_blank(),
            axis.text.x=element_blank(),
            axis.ticks.x=element_blank()) +
  scale_fill_manual(values=colors)
```



```{r}
#| eval: true
new_pal <- pal %>%
  filter(identifier %in% la_avg$identifier) %>%
  select(group3, color) %>%
  distinct()
la_avg %>%
  ggplot(aes(x=group3,y=average_proportion, color=group3))  +
    geom_point() +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
    facet_wrap(~la_group) +
    theme(legend.position="none" ) +
  scale_color_manual(values=new_pal$color)
```




```{r}
#| eval: true
la_avg %>%
  filter(la_group=="PAPUAN") %>%
  ggplot(aes(x=group3,y=average_proportion, color=group3))  +
    geom_point() +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
    facet_wrap(~la_group) +
    theme(legend.position="none" ) +
  scale_color_manual(values=new_pal$color)


la_avg %>%
  select(individual,la_group,average_proportion,identifier,group3) %>%
  pivot_wider(names_from = la_group, values_from = average_proportion) %>%
  mutate(perc_papuan = PAPUAN /(PAPUAN + AN_PROXY)) %>%
    ggplot(aes(x=group3,y=perc_papuan, color=group3))  +
    geom_point() +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
    theme(legend.position="none" ) +
  ylab("PAPUAN / (PAPUAN + AN_PROXY)") +
  scale_color_manual(values=new_pal$color) +
  ylim(0,1)
```




```{r}
#| eval: true
p <- la_avg %>%
  select(individual,la_group,average_proportion,identifier,group3) %>%
  pivot_wider(names_from = la_group, values_from = average_proportion) %>%
  mutate(perc_papuan = PAPUAN /(PAPUAN + AN_PROXY)) %>%
    ggplot(aes(x=PAPUAN,y=perc_papuan, color=group3))  +
    geom_point() +
    theme(legend.position="none" ) +
    geom_abline(slope = 1,
              intercept = 0,
              linetype="dashed",
              color="maroon") +
  ylab("PAPUAN / (PAPUAN + AN_PROXY)") +
  xlab("PAPUAN") +
  scale_color_manual(values=new_pal$color)
p
ggplotly(p)
```

Correlate with inferred patterns of admixture proportions using the ADMIXTURE software.


Read admixture results


```{r}
#| eval: true
fam <- read_table("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/HC_MAF_PRUNED_THINNED.fam",
                  col_names = c("identifier","individual","a","b","c","d")) %>%
  select(individual) %>% 
  left_join(ind_labels) %>% 
  select(-cohort)
 
cluster <- c("c0","c1","c2","c3","c4","c5","c6","c7","c8","c9","c10","c11")
Q <- read_table("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/admixture/all_samples/K_12_R_6375200/HC_MAF_PRUNED_THINNED.12.Q",
                col_names = cluster)


Q <- cbind(fam,Q) %>%
  as_tibble() %>%
  pivot_longer(cols = starts_with("c"),names_to = "cluster", values_to = "proportion") %>%
  arrange(identifier,proportion)

ind_levels <- Q %>%
  select(individual,identifier) %>%
  distinct() %>%
  mutate(identifier=factor(identifier, levels=pal$identifier)) %>%
  arrange(identifier) 
Q$individual <- factor(Q$individual,levels=ind_levels$individual)
```




```{r}
#| eval: true
ancestry <- c("ni-Vanuatu",
                  "European",
                  "Solomon",
                  "African",
                  "Agta",
                  "Eastern_Polynesian",
                  "South_American",
                  "Karitiana",
                  "Mainland_Papuan",
                  "East_Asian",
                  "North_American",
                  "Polynesian_Outlier")
ancestries <- tibble(cluster,ancestry)
Q <- Q %>% left_join(ancestries)
```

```{r}
#| eval: true
p <- la_avg %>%
  left_join(Q) %>%
  filter(ancestry == "East_Asian", la_group == "EAST_ASIAN") %>%
  ggplot(aes(x=average_proportion,y=proportion,color=group3)) +
  geom_point(alpha=0.8) +
  xlim(0,1) +
  ylim(0,1) +
  xlab("FLARE") +
  ylab("ADMIXTURE K=12") +
  geom_abline(slope = 1,
              intercept = 0,
              linetype="dashed",
              color="maroon") +
  ggtitle("East Asian component")
p
ggplotly(p)
```

```{r}
#| eval: true
p <- la_avg %>%
  left_join(Q) %>%
  filter(ancestry == "European", la_group == "EUROPEAN") %>%
  ggplot(aes(x=average_proportion,y=proportion,color=group3)) +
  geom_point(alpha=0.8) +
  xlim(0,1) +
  ylim(0,1) +
  xlab("FLARE") +
  ylab("ADMIXTURE K=12") +
  geom_abline(slope = 1,
              intercept = 0,
              linetype="dashed",
              color="maroon") +
  ggtitle("European component")
p
ggplotly(p)
```







## Comparing LA with self-identified ancestry



```{r}
#| eval: false
self_identified <- read_delim("/pasteur/helix/projects/MATAEA/dang/Pheno/mataea_full_database_2022_11_28.tsv") %>%
  select(SUBJID, SOCIO_CULTURAL, SOCIO_CULTURAL_PREC) %>%
  rename(individual="SUBJID") %>%
  clean_names()
```




```{r}
#| eval: false
self_identified %>%
  select(socio_cultural) %>%
  distinct() %>%
  knitr::kable()

self_identified %>%
  select(socio_cultural_prec) %>%
  distinct() %>%
  knitr::kable()
```

```{r}
#| eval: false
la_avg %>%
  left_join(.,self_identified, by="individual") %>%
  select(individual,identifier, la_group,average_proportion,socio_cultural,socio_cultural_prec) %>%
  filter(!is.na(socio_cultural)) %>%
  View()
```






## Ckeck source populations

Mamusi



```{r}
#| eval: true
la_avg %>%
  filter(identifier == "Mamusi") %>%
  ggplot(aes(x=individual,y=average_proportion,fill=la_group)) +
   geom_bar(stat="identity") +
   facet_wrap(~group3, scales = "free") +
   theme(axis.title.x=element_blank(),
            axis.text.x=element_blank(),
            axis.ticks.x=element_blank())
```
## Compute average proportions for $F_{adm}$

```{r}
la_avg %>%
  select(individual, la_group, total_length_sum, identifier, group3,group0) %>%
  filter(group0 %in% c("Polynesian_Outlier","Eastern_Polynesian","Western_Polynesian") ) %>%
  ungroup() %>%
  group_by(group3,la_group) %>%
  reframe(alpha=total_length_sum/sum(total_length_sum))



total_length <- la_avg %>%
  select(individual, la_group, total_length_sum, identifier, group3,group0) %>%
  filter(group0 %in% c("Polynesian_Outlier","Eastern_Polynesian","Western_Polynesian") ) %>%
  ungroup() %>%
  group_by(group3) %>%
  reframe(total_length=sum(total_length_sum))

anc_length <- la_avg %>%
  select(individual, la_group, total_length_sum, identifier, group3,group0) %>%
  filter(group0 %in% c("Polynesian_Outlier","Eastern_Polynesian","Western_Polynesian") ) %>%
  ungroup() %>%
  group_by(group3,la_group) %>%
  reframe(anc_length=sum(total_length_sum))
alphas <- full_join(total_length, anc_length) %>%
  mutate(alpha =anc_length / total_length) %>%
  select(group3,la_group, alpha)

  
```



# Merged Coverage dataset


```{r}
#| warning: false

la_groups <- c("NATIVE_AMERICAN",
               "PAPUAN",
               "AN_PROXY",
               "AFRICAN",
               "EUROPEAN",
               "EAST_ASIAN")
la <- tibble(individual=character(),
             la_group = character(),
             proportion = numeric(),
             chromosome = numeric()
             )
for (i in 1:22){
  tmp <- read_delim(paste0("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/flare_chr",
                         i,
                         ".global.anc.gz"),
                         col_names = c("individual", la_groups)) %>%
    mutate(chromosome=i) %>%
    pivot_longer(names_to = "la_group",
                 values_to = "proportion",
                 cols = all_of(la_groups))
  la <- full_join(la, tmp)  
}
```

```{r}
#| eval: true
pal <- readxl::read_excel("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/population_colors.xlsx", sheet = 5) 
la_path <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/ancestry_per_chromosome.tsv"
```


```{r}
write_delim(x = la,
            file=la_path)
```




```{r}
#| eval: true

la_groups <- c("AFRICAN",
               "EUROPEAN",
               "EAST_ASIAN",
               "AN_PROXY",
               "PAPUAN",
               "NATIVE_AMERICAN")
la <- read_delim(la_path) %>%
  mutate(la_group = factor(la_group, levels = la_groups))
```



Read recombination files to get number of SNPs per chromosome.


```{r}
recom <- tibble(X1=numeric(),
                X3=numeric(),
                X4=numeric())
for (i in 1:22){
  tmp <- read_delim(
    paste0("//pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/00_metadata/00_recombination_maps/snps_chr",i,".map"), col_names = FALSE) %>%
    select(-X2)
  recom <- full_join(recom,tmp)
}
recom <- recom %>%
  rename(chromosome="X1",
         position="X4",
         cM="X3")
```


```{r}
n_snps <- recom %>%
  group_by(chromosome) %>%
  summarise(n=n())
```

```{r}
#| eval: true
n_snps_path <- "/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/n_snps.tsv"
```


```{r}
write_delim(x = n_snps,
            file=n_snps_path)
```



```{r}
#| eval: true
n_snps <- read_delim(n_snps_path)
```



Weighted average of proportions

```{r}
#| eval: true
la_avg <- la %>%
            left_join(n_snps) %>%
            group_by(individual,la_group) %>%
            summarise(total_length_sum = sum(proportion*n)) %>%
  mutate(average_proportion = total_length_sum / sum(total_length_sum) )
```


```{r}
#| eval: true
la_avg <- la_avg %>%
  left_join(ind_labels) %>% 
  select(-cohort) %>% 
  left_join(pal) %>%
  mutate(identifier=factor(identifier,pal$identifier),
         group3=factor(group3,unique(pal$group3)))
```

```{r}
#| eval: true

la_groups
colors <- c("#a00000",
            "#111344",
            "#b1b100",
            "#912f40",
            "#760b9f",
            "#FFF36D"
            )
la_avg %>%
  mutate(la_group = factor(la_group, levels = la_groups)) %>%
  ggplot(aes(x=individual,y=average_proportion,fill=la_group)) +
   geom_bar(stat="identity") +
   facet_wrap(~group3, scales = "free_x") +
   theme(axis.title.x=element_blank(),
            axis.text.x=element_blank(),
            axis.ticks.x=element_blank())  + 
  scale_fill_manual(values=colors)


ggsave("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/images/mc_flare_barplots.png", width = 10, height = 5)
ggsave("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/images/mc_flare_barplots.svg", width = 10, height = 5)

```



```{r}
#| eval: true
new_pal <- pal %>%
  filter(identifier %in% la_avg$identifier) %>%
  select(group3, color) %>%
  distinct()


la_avg %>%
  ggplot(aes(x=group3,y=average_proportion, color=group3))  +
    geom_point(alpha = 0.3) +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
    facet_wrap(~la_group) +
    theme(legend.position="none" ) +
    scale_color_manual(values=new_pal$color)

ggsave("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/images/mc_flare_proportions.png", width = 10, height = 5)
ggsave("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/images/mc_flare_proportions.svg", width = 10, height = 5)
```


```{r}
la_avg %>%
  group_by(group3,la_group) %>%
  summarise(mean_ancestry=mean(average_proportion)*100) %>%
  filter(la_group == "NATIVE_AMERICAN")
```


```{r}
#| eval: true
new_pal <- pal %>%
  filter(identifier %in% la_avg$identifier) %>%
  select(group3, color) %>%
  distinct()


la_avg %>%
  filter(la_group=="NATIVE_AMERICAN") %>%
  ggplot(aes(x=group3,y=average_proportion, color=group3))  +
    geom_boxplot(alpha = 0.3) +
  geom_jitter(color="black", alpha=0.3,height = 0) +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
    facet_wrap(~la_group) +
    theme(legend.position="none" ) +
    scale_color_manual(values=new_pal$color) +
    ylim(0,0.15)

ggsave("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/images/mc_flare_proportions_na.png", width = 10, height = 5)
ggsave("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/images/mc_flare_proportions_na.svg", width = 10, height = 5)



la_avg %>%
  filter(group0=="Eastern_Polynesian") %>%
  filter(la_group=="NATIVE_AMERICAN") %>%
  ggplot(aes(x=group3,y=average_proportion, color=group3))  +
    geom_boxplot(alpha = 0.3) +
    geom_jitter(color="black", alpha=0.3,height = 0) +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
    facet_wrap(~la_group) +
    theme(legend.position="none" ) +
    scale_color_manual(values=new_pal$color) +
    ylim(0,0.15)
```
















```{r}
#| eval: true
la_avg %>%
  select(individual,la_group,average_proportion,identifier,group3) %>%
  pivot_wider(names_from = la_group, values_from = average_proportion) %>%
  mutate(perc_papuan = PAPUAN /(PAPUAN + AN_PROXY)) %>%
    ggplot(aes(x=group3,y=perc_papuan, color=EUROPEAN))  +
    geom_point() +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
    scale_color_viridis_c() +
  ylab("PAPUAN / (PAPUAN + FRO)")

la_avg %>%
  select(individual,la_group,average_proportion,identifier,group3) %>%
  pivot_wider(names_from = la_group, values_from = average_proportion) %>%
  mutate(perc_papuan = PAPUAN /(PAPUAN + AN_PROXY)) %>%
    ggplot(aes(x=group3,y=perc_papuan, color=group3))  +
    geom_point() +
    scale_x_discrete(guide = guide_axis(angle = 90)) +
    scale_color_manual(values=new_pal$color) +
  ylab("PAPUAN / (PAPUAN + FRO)") +
  theme(legend.position="none" )


ggsave("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/images/mc_flare_prel.png", width = 10, height = 5)
ggsave("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/images/mc_flare_prel.svg", width = 10, height = 5)


```




```{r}
#| eval: true
new_pal <- pal %>%
  filter(identifier %in% la_avg$identifier) %>%
  select(identifier, color, shape) %>%
  distinct()
p <- la_avg %>%
  select(individual,la_group,average_proportion,identifier,group3) %>%
  pivot_wider(names_from = la_group, values_from = average_proportion) %>%
  mutate(perc_papuan = PAPUAN /(PAPUAN + AN_PROXY)) %>%
    ggplot(aes(x=PAPUAN,y=perc_papuan,fill=identifier, shape=identifier))  +
    geom_point() +
    theme(legend.position="none" ) +
    geom_abline(slope = 1,
              intercept = 0,
              linetype="dashed",
              color="maroon") +
  ylab("PAPUAN / (PAPUAN + AN_PROXY)") +
  xlab("PAPUAN") +
  scale_fill_manual(values = alpha(new_pal$color,0.7)) +
  scale_color_manual(values = alpha("black",0.1)) +
  scale_shape_manual(values = new_pal$shape)


p
ggplotly(p)
```



```{r}
#| eval: true
p <- la_avg %>%
  select(individual,la_group,average_proportion,identifier,group3) %>%
  pivot_wider(names_from = la_group, values_from = average_proportion) %>%
  mutate(perc_papuan = PAPUAN /(PAPUAN + AN_PROXY)) %>%
    ggplot(aes(x=EUROPEAN,y=perc_papuan, fill=identifier, shape=identifier))  +
    geom_point() +
    theme(legend.position="none" ) +
    geom_abline(slope = 1,
              intercept = 0,
              linetype="dashed",
              color="maroon") +
  ylab("PAPUAN / (PAPUAN + AN_PROXY)") +
  xlab("EUROPEAN") +
  scale_fill_manual(values = alpha(new_pal$color,0.7)) +
  scale_color_manual(values = alpha("white",0.1)) +
  scale_shape_manual(values = new_pal$shape)


p
ggsave("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/images/mc_flare_prel_vs_ea.png", width = 10, height = 5)
ggsave("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/images/mc_flare_prel_vs_ea.svg", width = 10, height = 5)


ggplotly(p)
```


```{r}
#| eval: true
p <- la_avg %>%
  select(individual,la_group,average_proportion,identifier,group3) %>%
  pivot_wider(names_from = la_group, values_from = average_proportion) %>%
  mutate(perc_papuan = PAPUAN /(PAPUAN + AN_PROXY)) %>%
    ggplot(aes(x=EAST_ASIAN,y=perc_papuan, fill=identifier, shape=identifier))  +
    geom_point() +
    theme(legend.position="none" ) +
    geom_abline(slope = 1,
              intercept = 0,
              linetype="dashed",
              color="maroon") +
  ylab("PAPUAN / (PAPUAN + FRO)") +
  xlab("EAST_ASIAN") +
  scale_fill_manual(values = alpha(new_pal$color,0.7)) +
  scale_color_manual(values = alpha("white",0.1)) +
  scale_shape_manual(values = new_pal$shape)


p
ggsave("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/images/mc_flare_prel_vs_ea.png", width = 10, height = 5)
ggsave("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/images/mc_flare_prel_vs_ea.svg", width = 10, height = 5)




ggplotly(p)
```


```{r}
#| eval: true
p <- la_avg %>%
  select(individual,la_group,average_proportion,identifier,group3) %>%
  pivot_wider(names_from = la_group, values_from = average_proportion) %>%
    ggplot(aes(x=AN_PROXY,y=AN_PROXY/(PAPUAN + AN_PROXY),  fill=identifier, shape=identifier))  +
    geom_point() +
    theme(legend.position="none" ) +
    geom_abline(slope = 1,
              intercept = 0,
              linetype="dashed",
              color="maroon") +
  ylab("AN_PROXY / (PAPUAN + AN_PROXY)") +
  xlab("AN_PROXY") +
  scale_fill_manual(values = alpha(new_pal$color,0.7)) +
  scale_color_manual(values = alpha("black",0.1)) +
  scale_shape_manual(values = new_pal$shape)

p

ggplotly(p)
```



```{r}
#| eval: true
p <- la_avg %>%
  select(individual,la_group,average_proportion,identifier,group3) %>%
  pivot_wider(names_from = la_group, values_from = average_proportion) %>%
    ggplot(aes(x=PAPUAN,y=AN_PROXY,  fill=identifier, shape=identifier))  +
    geom_point() +
    theme(legend.position="none" ) +
    geom_abline(slope = 1,
              intercept = 0,
              linetype="dashed",
              color="maroon") +
  ylab("AN_PROXY") +
  xlab("PAPUAN") +
  scale_fill_manual(values = alpha(new_pal$color,0.7)) +
  scale_color_manual(values = alpha("black",0.1)) +
  scale_shape_manual(values = new_pal$shape)

p

ggplotly(p)
```





```{r}
#| eval: true
p <- la_avg %>%
  select(individual,la_group,average_proportion,identifier,group3) %>%
  pivot_wider(names_from = la_group, values_from = average_proportion) %>%
    ggplot(aes(x=EAST_ASIAN,y=AN_PROXY/(PAPUAN + AN_PROXY),  fill=identifier, shape=identifier))  +
    geom_point() +
    theme(legend.position="none" ) +
    geom_abline(slope = 1,
              intercept = 0,
              linetype="dashed",
              color="maroon") +
  ylab("FRO / (PAPUAN + FRO)") +
  xlab("EAST_ASIAN") +
    scale_fill_manual(values = alpha(new_pal$color,0.7)) +
  scale_color_manual(values = alpha("black",0.1)) +
  scale_shape_manual(values = new_pal$shape)

p

ggsave("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/images/mc_flare_frorel_vs_ea.png", width = 10, height = 5)
ggsave("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/05_local_ancestry/01_flare/01_6_ANCESTRIES/images/mc_flare_frorel_vs_ea.svg", width = 10, height = 5)


```




```{r}
#| eval: true
p <- la_avg %>%
  select(individual,la_group,average_proportion,identifier,group3) %>%
  pivot_wider(names_from = la_group, values_from = average_proportion) %>%
    ggplot(aes(x=NATIVE_AMERICAN,y=AN_PROXY/(PAPUAN + AN_PROXY),  fill=identifier, shape=identifier))  +
    geom_point() +
    theme(legend.position="none" ) +
    geom_abline(slope = 1,
              intercept = 0,
              linetype="dashed",
              color="maroon") +
  ylab("AN_PROXY / (PAPUAN + AN_PROXY)") +
  xlab("NATIVE_AMERICAN") +
    scale_fill_manual(values = alpha(new_pal$color,0.7)) +
  scale_color_manual(values = alpha("black",0.1)) +
  scale_shape_manual(values = new_pal$shape)

p
ggplotly(p)
```

```{r}
#| eval: true
p <- la_avg %>%
  select(individual,la_group,average_proportion,identifier,group3) %>%
  pivot_wider(names_from = la_group, values_from = average_proportion) %>%
    ggplot(aes(x=AFRICAN,y=PAPUAN/(PAPUAN + AN_PROXY),  fill=identifier, shape=identifier))  +
    geom_point() +
    theme(legend.position="none" ) +
    geom_abline(slope = 1,
              intercept = 0,
              linetype="dashed",
              color="maroon") +
  ylab("PAPUAN / (PAPUAN + AN_PROXY)") +
  xlab("AFRICAN") +
    scale_fill_manual(values = alpha(new_pal$color,0.7)) +
  scale_color_manual(values = alpha("black",0.1)) +
  scale_shape_manual(values = new_pal$shape)

p
ggplotly(p)
```


```{r}
#| eval: true
p <- la_avg %>%
  select(individual,la_group,average_proportion,identifier,group3) %>%
  pivot_wider(names_from = la_group, values_from = average_proportion) %>%
    ggplot(aes(x=NATIVE_AMERICAN,y=EAST_ASIAN,  fill=identifier, shape=identifier))  +
    geom_point() +
    theme(legend.position="none" ) +
    geom_abline(slope = 1,
              intercept = 0,
              linetype="dashed",
              color="maroon") +
  ylab("EAST_ASIAN") +
  xlab("NATIVE_AMERICAN") +
    scale_fill_manual(values = alpha(new_pal$color,0.7)) +
  scale_color_manual(values = alpha("black",0.1)) +
  scale_shape_manual(values = new_pal$shape)

p
ggplotly(p)
```


















```{r}
#| eval: true
new_pal <- pal %>%
  filter(identifier %in% la_avg$identifier) %>%
  select(group3, color) %>%
  distinct()
la_avg_wide<-la_avg %>%
  select(individual,la_group,average_proportion,identifier,group3) %>%
  pivot_wider(names_from = la_group, values_from = average_proportion) %>%
  mutate(group3=factor(group3, levels=new_pal$group3))

```



```{r}
library(GGally)
ggpairs(la_avg_wide,
        columns=4:9,
         ggplot2::aes(colour=group3),)
```

```{r}
#| eval: true
p <- la_avg_wide %>%
    ggplot(aes(y=PAPUAN/(AN_PROXY+PAPUAN),x=group3,
               fill=group3))  +
    geom_boxplot(alpha = 0.3,outlier.shape = NA) +
    geom_jitter(aes(color=group3),alpha = 0.3,height = 0, width = 0.1) +
    theme(legend.position="none" ) +
    scale_fill_manual(values = alpha(new_pal$color,0.7)) +
    scale_color_manual(values =alpha(new_pal$color,0.7)) +
    ylim(0,1) +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))  
  
p
ggplotly(p)
```


```{r}
#| eval: true
p <- la_avg_wide %>%
    ggplot(aes(y=PAPUAN,x=group3,
               fill=group3))  +
    geom_boxplot(alpha = 0.3,outlier.shape = NA) +
    geom_jitter(aes(color=group3),alpha = 0.3,height = 0, width = 0.1) +
    theme(legend.position="none" ) +
    scale_fill_manual(values = alpha(new_pal$color,0.7)) +
    scale_color_manual(values =alpha(new_pal$color,0.7)) +
    ylim(0,1) +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))  
  
p
ggplotly(p)
```





