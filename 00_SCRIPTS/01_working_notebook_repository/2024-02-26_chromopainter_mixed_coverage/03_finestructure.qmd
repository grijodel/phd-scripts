---
title: "fineSTRUCTURE workflow"
author: "Gaston Rijo De León"
format:
  html:
    embed-resources: true
    toc: true
    toc-depth: 2
execute: 
  echo: true
  warning: false
  message: false
  eval: true
date:  today
date-format: 'full'
engine: knitr
---

```{r}
library(tidyverse)
library(janitor)
library(plotly)
library(MetBrewer)
library(here)
library(glue)
library(dendextend)
library(factoextra)
library(ape)
library(ggtree)

theme_set(theme_bw())

pal <- readxl::read_excel("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/population_colors.xlsx",
                          sheet = 5) 
ind_labels_all <- read_delim("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/labels.tsv",
                         col_names = c("individual", "identifier", "cohort"))

ind_labels_cp <- read_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/01_phase_format/02_full_dataset/chr1/CHP_FILTER1_chr1.labels",
                         col_names = c("individual", "identifier", "included")) %>%
  select(-included)

'%!in%' <- function(x,y)!('%in%'(x,y))

```

# Introduction

# Only Polynesians

```{r}
copymatrix_dir <- here("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/02_unsupervised_finestructure/polynesians/paint_all_vs_all/")
output_dir <- here("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/03_finestructure/01_polynesians")
```

## Run fineSTRUCTURE

```{r}
copymatrix <- here(copymatrix_dir,"allchr.chunkcounts.out")
fs_mcmc <- here(output_dir,"fs.mcmc.xml")
burn_in_iterations <- "1000000"
sample_iterations <- "1000000"
thin_interval <-"100"
fs_first_run <- "fs fs -x {burn_in_iterations} -y {sample_iterations} -z {thin_interval} {copymatrix} {fs_mcmc}"
(fs <- glue(fs_first_run))
```

```{r}
#| eval: false
system(fs)
```

```{r}
fs_tree <-  here(output_matrix_dir,"fs.tree.xml")
method <- "T"
tree_building_algorithm <- "2"
tree_initialization <- "1"

fs_second_run <- "fs fs -m {method} -k {tree_building_algorithm} -T {tree_initialization}  {copymatrix} {fs_mcmc} {fs_tree}"
(fs <- glue(fs_second_run))
```

```{r}
#| eval: false
system(fs)
```

## Analyse results

```{r}
source("/pasteur/helix/projects/IGSR/Software/chromopainter_suite/fs_4.1.1/example4/FinestructureLibrary.R") 
```

Read tree and mcmc files.

```{r}
output_matrix_dir <- output_dir
mcmc_file<- here(output_matrix_dir,"fs.mcmc.xml") ## finestructure mcmc file
tree_file<- here(output_matrix_dir,"fs.tree.xml") ## finestructure tree file
###
mcmc <-xmlTreeParse(mcmc_file) ## read into xml format
mcmc <-as.data.frame.myres(mcmc) %>%
  as_tibble() 
###
tree<-xmlTreeParse(tree_file) ## read the tree as xml format
tree<-extractTree(tree) ## extract the tree into ape's phylo format

```


```{r}
replicates <- 100
mcmc <- xmlTreeParse(here(output_matrix_dir,"fs.mcmc.xml")) %>%
  as.data.frame.myres() %>%
    as_tibble() %>%
    head(n=0)
mcmc$replicate <- NA  

trees <- list()
for ( i in 1:replicates ){
  ###
  mcmc_file<- paste0(output_matrix_dir,"REPLICATE_",i,"/fs.mcmc.xml") ## finestructure mcmc file
  tree_file<- paste0(output_matrix_dir,"REPLICATE_",i,"/fs.tree.xml") ## finestructure tree file
  ###
  tmp_mcmc <-xmlTreeParse(mcmc_file) ## read into xml format
  tmp_mcmc <-as.data.frame.myres(tmp_mcmc) %>%
    as_tibble() %>%
    mutate(replicate=i)## convert this into a data frame
  mcmc <- full_join(mcmc,tmp_mcmc)
  ###
  tmp_tree<-xmlTreeParse(tree_file) ## read the tree as xml format
  tmp_tree<-extractTree(tmp_tree) ## extract the tree into ape's phylo format
  trees[[i]] <- tmp_tree
}

```

```{r}
mcmc %>%
  clean_names() %>%
  write_delim(file = here(output_matrix_dir,"aggregated_mcmc.csv"),
              delim = ",",
              col_names = TRUE)

mcmc <- read_delim(here(output_matrix_dir,"aggregated_mcmc.csv"))
```


```{r}
#save(trees,file = here(output_matrix_dir,"trees.RData") )
load(here(output_matrix_dir,"trees.RData"))
```

```{r}
tree <- trees[[1]]
```

Generate metadata table

```{r}
metadata <- ind_labels %>% 
  filter(individual %in% tree$tip.label ) %>%
  left_join(pal) %>%
  mutate(tip.label = individual) 
  
metadata$identifier <- factor(metadata$identifier, levels=unique(metadata$identifier)) 
metadata$group0 <- factor(metadata$group0, levels=unique(metadata$group0) )
metadata$group3 <- factor(metadata$group3, levels=unique(metadata$group3) )
```


```{r}
#tree$tip.label <- metadata$identifier
```


Look at trees

```{r}
color_palette <- c('#19647E','#C7FFED','#68C5DB','#016FB9','#80b1d3','#2708a0',
                            '#6B2466','#CA61C3','#FAB3A9','#034732','#008148','#B5DDA4')
```


```{r}
p <- ggtree(tree,layout="fan", open.angle=10) %<+%
        metadata +
  #geom_tiplab(size=4) +
      geom_tippoint(aes(color = group3)) +
  scale_color_manual(values = color_palette)
p
```



```{r}
p <- ggtree(tree) %<+%
        metadata +
  #geom_tiplab(size=4) +
      geom_tippoint(aes(color = group3)) +
  scale_color_manual(values = color_palette)
p
```


```{r}
ggsave("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/03_finestructure/images/unrooted_notips.svg")
```

Look at MCMC traces.

```{r}
mcmc %>% 
  filter(replicate==1) %>%
  clean_names() %>%
  ggplot(aes(x=number,y=posterior)) +
  geom_line(linewidth=0.1) 
```



## Run with replicates

```{bash}
cd /pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/03_finestructure/01_polynesians

sbatch /pasteur/helix/projects/MATAEA/01_WGS_POPGEN/00_SCRIPTS/00_script_repository/09_chromopainter_suite/03_finestructure/01_fs_parallel.slurm /pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/02_chromopainter/02_unsupervised_finestructure/polynesians/paint_all_vs_all/allchr.chunkcounts.out /pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/03_finestructure/01_polynesians/01_run_with_replicates/
```






































