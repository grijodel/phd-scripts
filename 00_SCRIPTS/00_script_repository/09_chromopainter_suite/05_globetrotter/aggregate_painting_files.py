#!/usr/bin/env python3
import sys

"""
aggregate painting files in the same order as a poplabels file
"""
poplabels_file=sys.argv[1]
sample_paths_file=sys.argv[2]
output_file=sys.argv[3]

def read_samples_file(samples_file,samples_dict): 
    with open(samples_file, 'r' ) as handle:
        next(handle)
        for line in handle:
            if line.startswith("HAP"):
                hap, hap_n, sample = line.split()
                haplotypes = []
            else:
                if line.startswith("10"):
                    haplotypes.append(line)
                    samples_dict[(hap_n,sample)]=haplotypes
                else:
                    haplotypes.append(line)

# Read haplotypes into dictionary
samples_dict={}
with open( sample_paths_file, 'r' ) as handle:
    for line in handle:
        filepath=line.split()[0]
        read_samples_file(filepath, samples_dict)
# Read sample list and print output file

with open(output_file, 'w' ) as outfile:
    with open(poplabels_file, 'r') as handle:
        outfile.write("#aggregated\n")
        for line in handle:
            sample = line.split()[0]
            # first haplotype
            newline_1 = "HAP 1 {0}\n".format(sample)
            outfile.write(newline_1)
            haplotypes_1 = samples_dict[('1',sample)]
            for i in haplotypes_1:
                outfile.write(i)
            # second haplotype
            newline_2 = "HAP 2 {0}\n".format(sample)
            outfile.write(newline_2)
            haplotypes_2 = samples_dict[('2',sample)]
            for i in haplotypes_2:
                outfile.write(i)


            


