#!/usr/bin/env python3
import sys
fam=sys.argv[1]
eigenstrat=sys.argv[2]


# get pops
pop_dict={}
with open(fam,'r') as f:
    for line in f:
        fid,iid,a,b,c,d=line.rstrip().split()
        pop_dict[iid]=fid

with open(eigenstrat,'r') as f:
    for line in f:
        iid, sex, case=line.rstrip().split()
        newline = "{0} {1} {2}".format(iid,sex,pop_dict[iid])
        print(newline)

