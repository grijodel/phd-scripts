#!/usr/bin/env python3
import argparse
import os
import subprocess
import glob
from pathlib import Path
parser = argparse.ArgumentParser( description = 'Create parameter file and run MALDER.')

parser.add_argument("admixed_population", type=str, help="Admixed population name.")
parser.add_argument("reference_1", type=str, help="Reference population 1 name.")
parser.add_argument("reference_2", type=str, help="Reference population 2 name.")
parser.add_argument("prefix", type=str, help="EIGENSTRAT file prefix.")
args = parser.parse_args()

admixed_population=args.admixed_population
reference_1=args.reference_1
reference_2=args.reference_2

geno_file=args.prefix+'.geno'
snp_file=args.prefix+'.snp'
ind_file=args.prefix+'.ind'

prefix = 'T_{0}_R1_{1}_R2_{2}'.format(admixed_population,reference_1,reference_2)
os.mkdir(prefix)
basedir=Path(prefix)


# Write parameter file
parfile_name = prefix + ".par"
with open(parfile_name,'w') as f:
    f.write("genotypename: " + geno_file + "\n")
    f.write("snpname: " + snp_file+ "\n")
    f.write("indivname: " + ind_file+ "\n")
    f.write("admixpop: " + admixed_population + "\n")
    f.write("refpops: " + reference_1 + ";" + reference_2 + "\n")
    f.write("raw_outname: " + prefix + "/" + prefix + ".txt" + "\n")
    f.write("checkmap: NO\n")
    f.write("binsize: 0.000500\n")
    f.write("jackknife: YES\n")


# run alder
logfile = prefix + ".log"
f=open(basedir / logfile, 'w')
alder=subprocess.run(["malder","-V","-p",parfile_name], stdout=f)
f.close()

results = prefix + ".results"
with open(results,'w') as resfile:
    with open(logfile, 'r') as log:
        
        for line in log:
            if line.startswith("DATA:"):
                newline = line.split("\t")[1:-1]
                resfile.write( ','.join( newline ) + "\n" )



'''

#Compute alpha and n
## read 
#exp_fit_files = glob.glob(prefix+'/'+'*.txt-*')
## create function for fitting curve
def fit_ld_curve(alder_output):
        ldcurve=pd.read_table(alder_output,comment='#',
            names=['d','ld','count'])
        ldcurve=ldcurve.astype({'d':float,'ld':float,'count':int})
        affine_term=ldcurve['ld'].values[-1]
        ldcurve=ldcurve.drop(ldcurve[['ld']].iloc[-1:].index)
    
        ld=ldcurve[['ld']].to_numpy()
        d=ldcurve[['d']].to_numpy()




    return (amplitude,decay)


#for f in exp_fit_files:
#    amplitude,decay=fit_ld_curve(f)


'''




















