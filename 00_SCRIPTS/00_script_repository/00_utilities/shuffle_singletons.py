#!/usr/bin/env python3

import random
import argparse

parser = argparse.ArgumentParser( description = "Shuffle singletons in a phased VCF file so that they are equally distributed (p=0.5) on the strands.")
parser.add_argument("input_vcf", type=str, help="Input VCF file. It cannot be bgzipped.")
parser.add_argument("output_vcf", type=str, help="Output VCF file. It will not be bgzipped ")
parser.add_argument("snps_to_exclude", type=str, help="Table containing SNPs to exclude from shuffling (format: chomosome\tposition)")
args=parser.parse_args()

# test ###
input_vcf = "/pasteur/zeus/projets/p02/MATAEA/gaston/sandbox/test.vcf"
output_vcf = "/pasteur/zeus/projets/p02/MATAEA/gaston/sandbox/test_out.vcf" #Read vcf file
snps_to_exclude = "/pasteur/zeus/projets/p02/MATAEA/gaston/sandbox/test_to_exclude.tsv"
######


def create_exclude_dict(snps_to_exclude_file):
    d = {}
    print("/!\ Reading SNPs to exclude from file {0} ".format(snps_to_exclude_file))
    with open(snps_to_exclude_file,'r') as handle:
        for line in handle:
            chromosome, position = line.rstrip().split()
            d[(chromosome,position)] = True
    print("/!\ A total of {0} SNPs will be excluded from shuffling\n".format(len(d)))
    return d

def flip_coin(p=0.5):
    toss = random.random()
    if toss > p:
        return "0|1"
    else:
        return "1|0"

def detect_singleton(record):
    """
    Check whether a record in a VCF file is a singleton (MAC=1)
    """
    n_left = record.count("1|0")
    n_right = record.count("0|1")
    n = n_left + n_right
    if n == 1:
        is_singleton = True
        if n_left == 1:
            side= "1|0"
        elif n_right == 1:
            side="0|1"
    else:
        is_singleton = False
        side = None
    return (is_singleton, side)


def shuffle_singleton(record,side):
    """
    Shuffle the singleton entry in a given VCF record
    """
    new_cell = flip_coin()
    new_record = record.replace(side, new_cell)
    return new_record, new_cell

def exclude(record,loci_dictionary):
    '''
    Check whether the record should be ignored (given a dict)
    '''
    record_split = record.rstrip().split()
    chromosome = record_split[0]
    position = record_split[1]
    try:
        is_excluded = loci_dictionary[(chromosome,position)]
    except KeyError:
        is_excluded = False

    return is_excluded

counters={"left_singletons" : 0,"right_singletons" : 0,"new_left_singletons" : 0,"new_right_singletons" : 0 , "excluded" : 0 }

exclude_dict = create_exclude_dict(args.snps_to_exclude)



with open(args.input_vcf, 'r' ) as infile:
    with open(args.output_vcf, 'w') as outfile:
        for line in infile:
            # Loop records
            ## Skip headers
            if line.startswith("#"):
                pass
                outfile.write(line)
            else:
                # Detect if it is a singleton
                is_singleton, side = detect_singleton(line)
                if is_singleton:
                    # Count singleton
                    if side == "1|0":
                        counters["left_singletons"] += 1
                    elif side == "0|1":
                        counters["right_singletons"] += 1

                    # Check whether site should be excluded
                    if exclude(line,exclude_dict):
                        counters["excluded"] += 1
                        if side == "1|0":
                            counters["new_left_singletons"] += 1
                        elif side == "0|1":
                            counters["new_right_singletons"] += 1
                        outfile.write(line)
                    else:
                        # Shufle it
                        new_record, new_side = shuffle_singleton(line,side)
                        if new_side == "1|0":
                            counters["new_left_singletons"] += 1
                        elif new_side == "0|1":
                            counters["new_right_singletons"] += 1
                        outfile.write(new_record)

                else:
                    # Just write the line as it was
                    outfile.write(line)

print("Of {0} singletons detected:".format(str(counters["left_singletons"] + counters["right_singletons"]+counters["excluded"])))

print(" {0} singletons were shuffled".format(str(counters["left_singletons"] + counters["right_singletons"])))

print( " {0} singletons were not shuffled\n".format(str(counters["excluded"])))

print("Number of singletons on each strand pre-shuffle:\n 1|0: {0}\n 0|1: {1}".format(str(counters["left_singletons"]), str(counters["right_singletons"])))

print("Number of singletons on each strand post-shuffle:\n 1|0: {0}\n 0|1: {1}".format(str(counters["new_left_singletons"]), str(counters["new_right_singletons"])))


