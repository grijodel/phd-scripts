#!/usr/bin/env python3
import argparse
import vcf
import pickle

parser = argparse.ArgumentParser( description = "Convert dbSNP bim file with rs IDs into bim with chr:pos format, given a reference (pickled database).")
parser.add_argument("pickled_database", type=str, help="Reference database")
parser.add_argument("inbim", type=str, help="Input bim file")
parser.add_argument("outbim", type=str, help="Output bim file")
args=parser.parse_args()
pick_db=args.pickled_database
inbim=args.inbim
outbim=args.outbim

# Read dictionaryi



print("READING SNP DB DICTIONARY")
with open(pick_db, 'rb') as handle:
    ref_dict = pickle.load(handle)


# Read bim file and modify it
print("MODIFYING BIM FILE")
with open(inbim,'r') as f:
    with open(outbim,'w') as obim:
        for line in f:
            chromosome,snpid,cm,pos,a1,a2=line.rstrip().split()
            try:
                new_chromosome = str(ref_dict[snpid][0])
                new_position = str(ref_dict[snpid][1])
                new_id = new_chromosome + ':' + new_position
            except KeyError:
                new_chromosome = str(chromosome)
                new_position = str(pos)
                new_id = snpid

            newline = "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n".format(new_chromosome,
                                                                new_id,
                                                                cm,
                                                                new_position,
                                                                a1,
                                                                a2)
            obim.write(newline)
                

