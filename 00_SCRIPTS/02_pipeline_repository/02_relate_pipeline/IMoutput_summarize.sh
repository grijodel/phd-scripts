#!/bin/bash
echo -e "pair\ttgens\tm\tM\tMSMCrCCR" > summary_mMrCCR.txt
for F in *estimates.txt; do
    file=$F
    #file=Atiu_Tikopia.b1_1e-08.b2_1e-06.MSMC_IM.estimates.txt
    file_prefix=$(basename $file .estimates.txt)
    pair=$(basename $file .b1_1e-08.b2_1e-06.MSMC_IM.estimates.txt)
    join -j 2 -o 1.1,1.2,1.3,1.4,2.3 <(tail -n +2 $file | awk -v pair=$pair '{print pair"\t"$1"\t"$4"\t"$5}') <(tail -n +4 ${file_prefix}.fittingdetails.txt | awk -v pair=$pair '{print pair"\t"$1"\t"$9}') | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5}' >> summary_mMrCCR.txt
done