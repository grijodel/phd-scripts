---
title: "SourceFind workflow"
author: "Gaston Rijo De León"
format:
  html:
    embed-resources: true
    toc: true
    toc-depth: 2
execute: 
  echo: true
  warning: false
  message: false
  eval: true
date:  today
date-format: 'full'
engine: knitr
---

```{r}
library(tidyverse)
library(janitor)
library(plotly)
library(MetBrewer)
library(qqplotr)
library(here)
library(glue)
i_am("00_SCRIPTS/01_working_notebook_repository/2023-03-20_chromopainter_pipeline/02_sourcefind.qmd")
theme_set(theme_bw())
pal <- readxl::read_excel("/pasteur/zeus/projets/p02/MATAEA/gaston/mataea/metadata/population_colors_all_sheets.xlsx",
                          sheet = 3) 
ind_labels <- read_delim("/pasteur/zeus/projets/p02/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/04_Quality_Control/labels.tsv",
                         col_names = c("individual", "identifier", "cohort"))
'%!in%' <- function(x,y)!('%in%'(x,y))
```

```{r}
read_sf_file <- function(sf_file){
  column_names <- read_delim(sf_file,
                 col_names = TRUE,
                  n_max = 0,
                  comment = "#") %>%
                      colnames()
  
  sf <- read_delim(sf_file,
                   col_names = column_names,
                   skip = 1,
                   comment = "target") %>% 
    group_by(target) %>%
    slice_max(with_ties = FALSE,order_by = posterior.prob) %>%
    rename(individual="target", posterior_prob="posterior.prob") %>%
    pivot_longer(cols = -c(individual,posterior_prob),names_to = "surrogate",values_to = "proportion") %>%
    left_join(ind_labels) %>%
    left_join(pal) %>%
    ungroup() %>%
    select(individual,identifier,posterior_prob,surrogate,proportion)
  return(sf)
}
```

# Introduction

# Run SourceFind

## Polynesians and all groups

Get poplabel info for parameter file

```{r, warning=FALSE, message=FALSE, eval=FALSE}
keep_groups <- c("Polynesian_Outlier","Fiji","Eastern_Polynesian","Western_Polynesian")

target_individuals <- read_delim(here("01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/03_supervised_sourcefind/polynesians_supervised/polynesians_supervised.labels"),
                                 col_names = c("individual","group","included")) %>%
  left_join(ind_labels) %>%
  left_join(pal) %>%
  filter(group0 %in% keep_groups) %>%
  select(individual) %>% 
  distinct() %>%
             pull(individual) 
```

#### Generate config file

```{r}
# Files and directories
copymatrix <- here("01_HIGH_COVERAGE","01_chromopainter_pipeline",
                        "02_chromopainter","03_supervised_sourcefind",
                        "01_polynesians_supervised_maf5","output.chunklengths.out")

output_dir <- here("01_HIGH_COVERAGE","01_chromopainter_pipeline",
                  "04_sourcefind","01_supervised_sourcefind",
                  "01_polynesians_vs_all")

parfile <- here(output_dir,"sourcefind.params")

outfile <- here(output_dir, "polynesians.tsv" )

idfile <-  here("01_HIGH_COVERAGE","01_chromopainter_pipeline",
                        "02_chromopainter","03_supervised_sourcefind",
                        "01_polynesians_supervised_maf5","polynesians_supervised.labels")
# Targets and surrogates, recipients and donors
donor_populations <- read_delim(copymatrix,skip = 0,n_max = 1) %>%
  select(-Recipient) %>%
  colnames() %>%
  as_tibble() %>%
  rename(identifier="value") %>%
  left_join(pal) 
donor_names <- donor_populations %>%
  pull(identifier)
surrogate_names <- donor_populations %>%
  pull(identifier)
```

```{r}
targets <- paste(target_individuals, collapse = " ")
donors <- paste(donor_names,collapse = " ")
surrogates <- paste(surrogate_names,collapse = " ")
# Write parameter file
params = "self.copy.ind: 0
num.surrogates: 4
exp.num.surrogates: 4
input.file.ids: {idfile}
input.file.copyvectors: {copymatrix} 
save.file: {outfile}
copyvector.popnames: {donors}
surrogate.popnames: {surrogates}
target.popnames: {targets}
num.slots: 100 
num.iterations: 200000
num.burnin: 50000
num.thin: 5000
"
(parameters_full <- glue(params) )
write(parameters_full,file = parfile)
```

#### Run SourceFind

```{r}
sf <- "/pasteur/zeus/projets/p02/IGSR/Software/chromopainter_suite/sourcefindV2/sourcefindv2.R --parameters {parfile} > sourcefind.log"
(sf_full <- glue(sf))
```

```{r}
#| eval: false
system(sf_full)
```

#### Read SourceFind

```{r}
column_names <- read_delim(outfile,
                 col_names = TRUE,
                   skip = 1,n_max = 1) %>%
  colnames()
  
sf <- read_delim(outfile,
                 col_names = column_names,
                 skip = 1,
                 comment = "target") %>% 
  group_by(target) %>%
  slice_max(with_ties = FALSE,order_by = posterior.prob) %>%
  rename(individual="target") %>%
  select(-posterior.prob) %>%
  pivot_longer(cols = -individual,names_to = "surrogate",values_to = "proportion") %>%
  left_join(ind_labels) %>%
  left_join(pal) %>%
  ungroup()
```

Re-level

```{r}
new_pal <- sf %>%
  select(identifier,color,shape) %>%
  distinct() %>%
  mutate(identifier=factor(identifier, levels=pal$identifier)) %>%
  arrange(identifier)
ind_order <- sf %>%
  select(individual,identifier) %>%
  distinct() %>%
  mutate(identifier=factor(identifier, pal$identifier)) %>%
  arrange(identifier,individual)
sf <- sf %>%
  mutate(individual=factor(individual,levels=ind_order$individual)) %>%
  mutate(surrogate=factor(surrogate, levels=pal$identifier)) %>%
  mutate(identifier=factor(identifier, levels=pal$identifier)) %>%
  arrange(surrogate,individual)
```

```{r}
p <- sf %>%
  group_by(surrogate, identifier) %>%
  summarise(mean_proportion=mean(proportion)) %>%
  ggplot(aes(x=identifier,y=mean_proportion,fill=surrogate)) +
  geom_bar(stat="identity") 

ggplotly(p)
```

### Run with replicates

Now that the results make a bit more sense, let's run several replicates and average the results.

```{bash}
cd /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/04_sourcefind/01_supervised_sourcefind/02_polynesians_vs_all

sbatch /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/00_SCRIPTS/00_script_repository/09_chromopainter_suite/04_sourcefind/parallel_sourcefind.slurm /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/04_sourcefind/01_supervised_sourcefind/01_polynesians_vs_all/sourcefind.params /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/04_sourcefind/01_supervised_sourcefind/01_polynesians_vs_all/01_run_replicates

```



```{r}
base_dir <- here("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/04_sourcefind/01_supervised_sourcefind/01_polynesians_vs_all/01_run_replicates/REPLICATE")
n_replicates <- 100
for (i in 1:n_replicates){
  if (i == 1) {
    sf_file <- paste0(base_dir,"_",i,"/","output.tsv")
    sf <- read_sf_file(sf_file) %>%
      mutate(replicate=i)
  }
  else {
    sf_file <- paste0(base_dir,"_",i,"/","output.tsv")o
    tmp <- read_sf_file(sf_file) %>%
      mutate(replicate=i)
    sf <- full_join(sf,tmp) 
    }
}
```

Re-level

```{r}
new_pal <- sf %>% 
  left_join(pal) %>%
  select(identifier,color,shape) %>%
  distinct() %>%
  mutate(identifier=factor(identifier, levels=pal$identifier)) %>%
  arrange(identifier)
ind_order <- sf %>%
  select(individual,identifier) %>%
  distinct() %>%
  mutate(identifier=factor(identifier, pal$identifier)) %>%
  arrange(identifier,individual)
sf <- sf %>%
  mutate(individual=factor(individual,levels=ind_order$individual)) %>%
  mutate(surrogate=factor(surrogate, levels=pal$identifier)) %>%
  mutate(identifier=factor(identifier, levels=pal$identifier)) %>%
  arrange(surrogate,individual)
```

Weighted average of replicates using posterior probabilities.

```{r}
sf_avg <- sf %>%
  ungroup() %>%
  group_by(surrogate,individual) %>%
  reframe(avg_proportion=sum(posterior_prob*proportion)/sum(posterior_prob)) %>%
  left_join(ind_labels) %>%
    left_join(pal)
```

Check that proportions for a given individual sum to one

```{r}
sf_avg %>% 
  group_by(individual) %>%
  reframe(sum(avg_proportion)) 
```

Which are the surrogates for which there is no ancestry inferred?

```{r}
no_ancestry_inferred <- sf_avg %>%
  group_by(surrogate) %>%
  summarise(sum_props=sum(avg_proportion)) %>%
  filter(sum_props == 0)
knitr::kable(no_ancestry_inferred)
```

```{r}
ancestry_inferred <- sf_avg %>%
  group_by(surrogate) %>%
  summarise(sum_props=sum(avg_proportion)) %>%
  filter(sum_props > 0)
knitr::kable(ancestry_inferred)
```

```{r}
p <- sf_avg %>%
  ggplot(aes(x=individual,y=avg_proportion,fill=surrogate)) +
  geom_bar(stat="identity") 
ggplotly(p)
```

Plot proportions averaged per population

```{r}
sf_summarised <- sf_avg %>%
  group_by(surrogate, identifier) %>%
  summarise(mean_proportion=mean(avg_proportion),sd=sd(avg_proportion)) 
p <- sf_summarised %>%
  ggplot(aes(x=identifier,y=mean_proportion,fill=surrogate)) +
  geom_bar(stat="identity") 
ggplotly(p)
```

Exclude populations with no ancestry to simpify plotting (less colors)

```{r}
sf_summarised <- sf_avg %>%
  group_by(surrogate, identifier) %>%
  summarise(mean_proportion=mean(avg_proportion),sd=sd(avg_proportion)) %>%
  filter(surrogate %in% ancestry_inferred$surrogate) %>%
  mutate(surrogate=factor(surrogate))
p <- sf_summarised %>%
  ggplot(aes(x=identifier,y=mean_proportion,fill=surrogate)) +
  geom_bar(stat="identity")  +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
#ggplotly(p)
p 
ggsave("/pasteur/zeus/projets/p02/MATAEA/gaston/slides/images/TAC2/sf.svg")
```

Plot per individual proportions

```{r}
sf_avg %>%
ggplot(aes(x=individual,y=avg_proportion,fill=surrogate,label=group3)) +
  geom_bar(stat="identity") 
ggplotly()
```

```{r}
sf_avg %>%
  filter(surrogate %in% ancestry_inferred$surrogate) %>%
ggplot(aes(x=individual,y=avg_proportion,fill=surrogate,label=group3)) +
  geom_bar(stat="identity")  +
  facet_wrap(group3~.,ncol=3,scales="free_x",)
  
ggplotly()
```

## Polynesians vs homogeneous groups

If we use admixed group as surrogates, recent admixture, or polynesian ancestry in some groups can confound the results. We'll use groups that are relatively homogeneous according to admixture results:

::: {#fig-admxiture layout-ncol="1"}
![admixture](/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/admixture/images/all_samples_K12.png){#fig-admixture} Admixture plot
:::

Get poplabel info for parameter file

```{r, warning=FALSE, message=FALSE, eval=FALSE}
keep_groups <- c("Polynesian_Outlier","Fiji","Eastern_Polynesian","Western_Polynesian")

target_individuals <- read_delim(here("01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/03_supervised_sourcefind/polynesians_supervised/polynesians_supervised.labels"),
                                 col_names = c("individual","group","included")) %>%
  left_join(ind_labels) %>%
  left_join(pal) %>%
  filter(group0 %in% keep_groups) %>%
  select(individual) %>% 
  distinct() %>%
             pull(individual) 
```

#### Generate config file

```{r}
# Files and directories
copymatrix <- here("01_HIGH_COVERAGE","01_chromopainter_pipeline",
                        "02_chromopainter","03_supervised_sourcefind",
                        "01_polynesians_supervised_maf5","output.chunklengths.out")

output_dir <- here("01_HIGH_COVERAGE","01_chromopainter_pipeline",
                  "04_sourcefind","01_supervised_sourcefind",
                  "02_polynesian_vs_homogeneous")

parfile <- here(output_dir,"sourcefind.params")

outfile <- here(output_dir, "polynesians.tsv" )

idfile <-  here("01_HIGH_COVERAGE","01_chromopainter_pipeline",
                        "02_chromopainter","03_supervised_sourcefind",
                        "01_polynesians_supervised_maf5","polynesians_supervised.labels")

# Targets

keep_groups <- c("Polynesian_Outlier","Fiji","Eastern_Polynesian","Western_Polynesian")

target_individuals <- read_delim(idfile,
                                 col_names = c("individual","group","included")) %>%
  left_join(ind_labels) %>%
  left_join(pal) %>%
  filter(group0 %in% keep_groups) %>%
  select(individual) %>% 
  distinct() %>%
             pull(individual) 


# Targets and surrogates, recipients and donors

donor_populations <- read_delim(copymatrix,skip = 0,n_max = 1) %>%
  select(-Recipient) %>%
  colnames() %>%
  as_tibble() %>%
  rename(identifier="value") %>%
  left_join(pal) 
donor_names <- donor_populations %>%
  pull(identifier)
surrogate_names <- donor_populations %>%
  pull(identifier)

```

We will define the surrogates by hand based on our assumed knowledge and the "unsupervised" sourcefind results.

```{r}
targets <- paste(target_individuals, collapse = " ")
donors <- paste(donor_names,collapse = " ")


# Write "homogeneous" populations
surrogate_names <- c("French","Orcadian", # European
                     "Atayal","Paiwan", # Taiwanese
                     "Han","Dai", # East Asian
                     "Agta","Cebuano", # Island SEA
                     "Papuan_Highlands","Papuan_Sepik","Mendi","Marawaka","Bundi","Kundiawa","Tari", # Papuan mainland
                     "Mamusi", # Bismarck
                     "Bougainville", # Solomon I.
                     "Pentecost", # ni-Vanuatu
                     "Karitiana","Surui","Chopccas", # South American
                     "Pima" # North American
                     )

surrogates <- paste(surrogate_names,collapse = " ")
# Write parameter file
params = "self.copy.ind: 0
num.surrogates: 4
exp.num.surrogates: 4
input.file.ids: {idfile}
input.file.copyvectors: {copymatrix} 
save.file: {outfile}
copyvector.popnames: {donors}
surrogate.popnames: {surrogates}
target.popnames: {targets}
num.slots: 100 
num.iterations: 200000
num.burnin: 50000
num.thin: 5000
"
(parameters_full <- glue(params) )

write(parameters_full,file = parfile)
```

```{r}
sf <- "/pasteur/zeus/projets/p02/IGSR/Software/chromopainter_suite/sourcefindV2/sourcefindv2.R --parameters {parfile} > sourcefind.log"
(sf_full <- glue(sf))
```

```{r}
#| eval: false
system(sf_full)
```

#### Read results

```{r}
column_names <- read_delim(outfile,
                 col_names = TRUE,
                   skip = 1,n_max = 1) %>%
  colnames()
  
sf <- read_delim(outfile,
                 col_names = column_names,
                 skip = 1,
                 comment = "target") %>% 
  group_by(target) %>%
  slice_max(with_ties = FALSE,order_by = posterior.prob) %>%
  rename(individual="target") %>%
  select(-posterior.prob) %>%
  pivot_longer(cols = -individual,names_to = "surrogate",values_to = "proportion") %>%
  left_join(ind_labels) %>%
  left_join(pal) %>%
  ungroup()
```

Re-level

```{r}
new_pal <- sf %>%
  select(identifier,color,shape) %>%
  distinct() %>%
  mutate(identifier=factor(identifier, levels=pal$identifier)) %>%
  arrange(identifier)
ind_order <- sf %>%
  select(individual,identifier) %>%
  distinct() %>%
  mutate(identifier=factor(identifier, pal$identifier)) %>%
  arrange(identifier,individual)
sf <- sf %>%
  mutate(individual=factor(individual,levels=ind_order$individual)) %>%
  mutate(surrogate=factor(surrogate, levels=pal$identifier)) %>%
  mutate(identifier=factor(identifier, levels=pal$identifier)) %>%
  arrange(surrogate,individual)
```

```{r}
p <- sf %>%
  ggplot(aes(x=individual,y=proportion,fill=surrogate)) +
  geom_bar(stat="identity") 

ggplotly(p)
```

```{r}
p <- sf %>%
  group_by(surrogate, identifier) %>%
  summarise(mean_proportion=mean(proportion)) %>%
  ggplot(aes(x=identifier,y=mean_proportion,fill=surrogate)) +
  geom_bar(stat="identity") 

ggplotly(p)
p + theme(legend.position = "none",axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) 
```




### Run with replicates



```{bash}
cd /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/04_sourcefind/01_supervised_sourcefind/02_polynesian_vs_homogeneous

sbatch /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/00_SCRIPTS/00_script_repository/09_chromopainter_suite/04_sourcefind/parallel_sourcefind.slurm /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/04_sourcefind/01_supervised_sourcefind/02_polynesian_vs_homogeneous/sourcefind.params /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/04_sourcefind/01_supervised_sourcefind/02_polynesian_vs_homogeneous/01_run_replicates
```

```{r}
base_dir <- here("01_HIGH_COVERAGE/01_chromopainter_pipeline/04_sourcefind/01_supervised_sourcefind/02_polynesian_vs_homogeneous/01_run_replicates/REPLICATE")
n_replicates <- 100
for (i in 1:n_replicates){
  if (i == 1) {
    sf_file <- paste0(base_dir,"_",i,"/","output.tsv")
    sf <- read_sf_file(sf_file) %>%
      mutate(replicate=i)
  }
  else {
    sf_file <- paste0(base_dir,"_",i,"/","output.tsv")
    tmp <- read_sf_file(sf_file) %>%
      mutate(replicate=i)
    sf <- full_join(sf,tmp)
    }
}
```

Re-level

```{r}
sf <- sf %>%
  left_join(pal) 
new_pal <- sf %>%
  select(identifier,color,shape) %>%
  distinct() %>%
  mutate(identifier=factor(identifier, levels=pal$identifier)) %>%
  arrange(identifier)
ind_order <- sf %>%
  select(individual,identifier) %>%
  distinct() %>%
  mutate(identifier=factor(identifier, pal$identifier)) %>%
  arrange(identifier,individual)
sf <- sf %>%
  mutate(individual=factor(individual,levels=ind_order$individual)) %>%
  mutate(surrogate=factor(surrogate, levels=pal$identifier)) %>%
  mutate(identifier=factor(identifier, levels=pal$identifier)) %>%
  arrange(surrogate,individual)
```

Weighted average of replicates using posterior probabilities.

```{r}
sf_avg <- sf %>%
  group_by(individual,surrogate) %>%
  reframe(avg_proportion=sum(posterior_prob*proportion)/sum(posterior_prob)) %>%
  left_join(ind_labels) %>%
    left_join(pal)
```

Re-level

```{r}
new_pal <- sf_avg %>%
  select(identifier,color,shape) %>%
  distinct() %>%
  mutate(identifier=factor(identifier, levels=pal$identifier)) %>%
  arrange(identifier)
ind_order <- sf_avg %>%
  select(individual,identifier) %>%
  distinct() %>%
  mutate(identifier=factor(identifier, pal$identifier)) %>%
  arrange(identifier,individual)
sf_avg <- sf_avg %>%
  mutate(individual=factor(individual,levels=ind_order$individual)) %>%
  mutate(surrogate=factor(surrogate, levels=pal$identifier)) %>%
  mutate(identifier=factor(identifier, levels=pal$identifier)) %>%
  arrange(surrogate,individual)
```

Check that proportions for a given individual sum to one

```{r}
sf_avg %>% 
  group_by(individual) %>%
  reframe(sum(avg_proportion)) 
```



```{r}
sf_table_name <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/04_sourcefind/01_supervised_sourcefind/02_polynesian_vs_homogeneous/sf_summarised.tsv"
sf_summarised <- sf_avg %>%
  group_by(surrogate, identifier) %>%
  summarise(mean_proportion=mean(avg_proportion),sd=sd(avg_proportion)) 
sf_summarised %>%
  write_delim(file = sf_table_name)
```



```{r}
sf_summarised <- read_delim(sf_table_name) %>%
  mutate(identifier = factor(identifier, levels=pal$identifier)) %>%
  mutate(surrogate = factor(surrogate, levels=pal$identifier))
```


```{r}
p <- sf_summarised %>%
  ggplot(aes(x=identifier,y=mean_proportion,fill=surrogate)) +
  geom_bar(stat="identity") 
ggplotly(p)
```



```{r}
filter_surrogates <- c("French","Han","Dai","Orcadian")

p <- sf_summarised %>%
  filter(surrogate %!in% filter_surrogates ) %>%
  group_by(identifier) %>%
  mutate(mean_proportion = mean_proportion / sum(mean_proportion)) %>%
  ggplot(aes(x=identifier,y=mean_proportion,fill=surrogate)) +
  geom_bar(stat="identity") 
ggplotly(p)
```
























