nextflow.enable.dsl=2                                                           
workflow {
    // Set initial values
    /// Data paths
    //def chromosomes = ([1,2,3,4,5,6,8,9,10,11,12,14,15,16,17,18,20,21,22])
    def chromosomes = ([1,7,13,19])
    def data_paths = chromosomes.collect{ 
        params.input_directory+'/chr'+it+'/*.{haplotypes,recomrates,labels}'
        }
    /// Create channels 
    data=Channel.fromFilePairs(data_paths, checkIfExists: true, size: 3)
                .map { [ it, it[0].split("_chr")[1].toInteger() ] }

    individuals=Channel.from( 1..params.number_of_individuals ) 
    chromosomes=Channel.from(chromosomes) 
    //////////// WORKFLOW START ////////////
    // Paint chromosomes
    chromopainter_input = data.combine(individuals)
                              .map { it.flatten() }
    ESTIMATE_PARAMETERS( chromopainter_input )
}

process ESTIMATE_PARAMETERS {
    scratch false
    publishDir "${params.output_directory}/parameter_estimation/chr${chromosome}", mode: 'move'

    module "chromopainter/v2"
    input:
        tuple val(prefix), file(haplotypes), file(labels), file(recomrates), val(chromosome), val(ind_number)
    output:
        file("*.out")

    script:
    """
    individual_id=\$(cat ${prefix}.labels| head -n ${ind_number} | tail -n 1 | awk '{print \$1}' )
    ChromoPainter \
        -g ${prefix}.haplotypes \
        -r ${prefix}.recomrates \
        -t ${prefix}.labels \
        -a ${ind_number} ${ind_number} \
        -i ${params.number_of_iterations} \
        -in \
        -iM \
        -o \${individual_id}_${params.output_prefix}_estimation_chr${chromosome}
    rm *.samples.out 
    """
    stub:
    """
    individual_id=\$(cat ${prefix}.labels| head -n ${ind_number} | tail | awk '{print \$1}' )
    output_prefix=\${individual_id}_${params.output_prefix}_estimation_chr${chromosome}
    touch \${output_prefix}.out
    """
}




