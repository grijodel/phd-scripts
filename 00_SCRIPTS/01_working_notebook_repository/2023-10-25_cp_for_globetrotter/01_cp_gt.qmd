---
title: "Chromopainter for Globetrotter"
author: "Gaston Rijo De León"
format:
  html:
    embed-resources: true
    toc: true
    toc-depth: 2
date:  today
execute: 
  echo: true
  warning: false
  message: false
  eval: true
date-format: 'full'
engine: knitr
---

# Setup

```{r}
library(tidyverse)
library(janitor)
library(plotly)
library(MetBrewer)
library(ggtree)
library(treeio)
library(qqplotr)
library(here)
library(glue)

i_am("00_SCRIPTS/01_working_notebook_repository/2023-09-25_ohana_refined_analysis/01_ohana_workflow_updated.qmd")
theme_set(theme_bw())
pal <- readxl::read_excel("/pasteur/zeus/projets/p02/MATAEA/gaston/mataea/metadata/population_colors_all_sheets.xlsx", sheet = 3) 
ind_labels <- read_delim("/pasteur/zeus/projets/p02/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/04_Quality_Control/labels.tsv",col_names = c("individual", "identifier", "cohort"))


'%!in%' <- function(x,y)!('%in%'(x,y))

admixture_plot <- function(Q_matrix, K=5) {
  p <- Q_matrix %>%
  ggplot() +
    geom_bar(aes(x=individual,y=proportion,fill=cluster),stat="identity") +
    scale_fill_manual(values = met.brewer("Isfahan2", n=K)) +
    theme(axis.text.x=element_blank())
  return(p)
}
```


```{r}
#| eval: false
run_globetrotter_slurm <- function(input_file_ids,
                                   copymatrix,
                                   painting_files_list,
                                   recom_files_list="/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/01_phase_format/02_full_dataset/01_all_populations/recombination_files_list.txt",
                                   output_dir,
                                   target_popname,
                                   bootstrap="0",
                                   admixdates = "1",
                                   surrogate_popnames=c()){
  # Create parameter_files
  parfile <- paste0(output_dir, "/","globetrotter.params")
  save_file <-paste0(output_dir, "/","globetrotter_output")
  # Write "homogeneous" populations
  surrogate_popnames <- paste(surrogate_popnames,collapse = " ")
  params = "prop.ind: 1
  bootstrap.date.ind: {bootstrap}
  null.ind: 1
  input.file.ids: {input_file_ids}
  input.file.copyvectors: {copymatrix}
  save.file.main: {save_file}
  save.file.bootstraps: {save_file}_bootstrap
  copyvector.popnames: {surrogate_popnames}
  surrogate.popnames: {surrogate_popnames}
  target.popname: {target_popname}
  num.mixing.iterations: 5
  props.cutoff: 0.001
  bootstrap.num: 100
  num.admixdates.bootstrap: {admixdates}
  num.surrogatepops.perplot: 3
  curve.range: 1 30
  bin.width: 0.1
  xlim.plot: 0 30
  prop.continue.ind: 0
  haploid.ind: 0"
  parameters_full<- glue(params)
  # Write slurm scripts
  ## --
  fileConn<-file(parfile)
  writeLines(parameters_full, fileConn)
  close(fileConn)
  gt <- "fastGlobeTrotter {parfile} {painting_files_list} {recom_files_list} 1 --no-save"
  gt <- (glue(gt))
  ## slurm
  slurm_command <- "#!/bin/bash
#SBATCH -J globetrotter
#SBATCH -o {output_dir}/gt.out -e  {output_dir}/gt.err
#SBATCH --mem=30000
#SBATCH --qos=geh
#SBATCH --partition=geh
module load  R/4.1.0
module load  fastGlobeTrotter/a1dc395
{gt}"
  slurm_script <- glue(slurm_command)
  ## Write to file
  slurm_script_path <- paste0(output_dir, "/","globetrotter.slurm")
  fileConn<-file(slurm_script_path)
  writeLines(slurm_script, fileConn)
  close(fileConn)
  ## Run using slurm
  run_slurm <- "sbatch {slurm_script_path}"
  run_slurm <- glue(run_slurm)
  system(run_slurm)
  return(run_slurm)
}
```















# Introduction

I already ran a chromopainter run that paints chromosomes in a supervised way with all polynesians + Fiji as recipients, and "homogeneous" populations as donors. This is useful because it allows me to model admixture in Polynesians, specifically Papuan-Austronesian admixture. But if I want to go into more detailed analyses, I need to define more specific Donors and Recipients.


As a reference, I will use the "homogeneous" groups as seen in the admixture analysis:

![Admixture plot High Coverage samples](/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/01_pca_admixture_fstats/admixture/images/all_samples_K12.png)


Depending on the specific questions, I will try to use Polynesian "homogeneous" populations to investigate timing of admixture.

### Label files

For chromopainter, we need files that indicate the usage of different individuals, and the direction of the copying (specifying donors and recipients). We will start with the file that we've been using for the more global approaches, and modify it accordingly. 

Keep in mind that **order of individuals** must be maintained.

```{r}
poplist_file <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/03_supervised_sourcefind/01_polynesians_supervised_maf5/polynesians_supervised.poplist"
labels_file <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/03_supervised_sourcefind/01_polynesians_supervised_maf5/polynesians_supervised.labels"
```

Define function that easily changes poplist and labels file.

```{r}
update_files <- function(poplist_file,
                         labels_file,
                         donor_populations,
                         recipient_populations){
  update_labels_file <- function()
  poplist <- read_delim(poplist_file,col_names = c("identifier","role"))
  labels <- read_delim(labels_file, col_names = c("individual","identifier","include"))
  # load recipient individuals on tibble
  recipient_individuals <- ind_labels %>%
    select(-cohort) %>%
    left_join(pal) %>%
    filter(identifier %in% recipient_populations)
  # load donor individuals on tibble
  donor_individuals <- ind_labels %>%
    select(-cohort) %>%
    left_join(pal) %>%
    filter(identifier %in% donor_populations)
  # update labels according to recipients and donors
  updated_labels <- labels %>%
                      select(-identifier) %>%
                      left_join(ind_labels) %>%
                      select(individual, identifier,include) %>%
                      mutate(
                        identifier = case_when(
                                    individual %in% recipient_individuals$individual ~ individual,
                                    individual %in% donor_individuals$individual ~ identifier,
                                    individual %!in% c(recipient_individuals$individual,donor_individuals$individual) ~ identifier ),
                        include = case_when(
                                    individual %in% c(recipient_individuals$individual,donor_individuals$individual) ~ 1,
                                    individual %!in% c(recipient_individuals$individual,donor_individuals$individual) ~ 0
                        ))
  # update population lists
  donor_poplist_D <- updated_labels %>%
    filter(individual %in% donor_individuals$individual) %>%
    select(identifier) %>%
    distinct() %>%
    mutate(role = "D")
  donor_poplist_R <- updated_labels %>%
    filter(individual %in% donor_individuals$individual) %>%
    select(identifier) %>%
    distinct() %>%
    mutate(role = "R")
  donor_poplist <- full_join(donor_poplist_D,
                             donor_poplist_R)
  recipient_poplist <-  updated_labels %>%
    filter(individual %in% recipient_individuals$individual) %>%
    select(individual) %>%
    distinct() %>%
    mutate(role = "R") %>%
    rename(identifier="individual")
  
  updated_poplist <- full_join(donor_poplist,
                               recipient_poplist)
  
  
  
  return(
    list("poplist"=updated_poplist, 
          "labels"=updated_labels) )
}

```



# Run ChromoPainter

## Western Polynesians as recipients

We already have a notion of the Papuan-Austronesian admixture process in WP, but he haven't characterized admixture between polynesian groups. We will attempt to do that by setting Western Polynesians as the recipient population, and the following as Donor populations:


- East Asians (all)
- Papuan
- Bismarck
- Solomon
- Vanuatu
- Polynesian (Atiu)
- Polynesian Outlier (Renell and Belonna)

Generate label files:

```{r}
new_poplist <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/01_target_wp/populations.poplist"
new_labels <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/01_target_wp/populations.labels"
recipient_populations <- c("Western_Samoa","Tonga")
donor_populations <- c(
                     "Atayal","Paiwan", # Taiwanese
                     "Han","Dai", # East Asian
                     "Agta","Cebuano", # Island SEA
                     "Papuan_Highlands","Papuan_Sepik","Mendi","Marawaka","Bundi","Kundiawa","Tari", # Papuan mainland
                     "Mamusi", # Bismarck
                     "Bougainville", # Solomon I.
                     "Pentecost", # ni-Vanuatu
                     "Atiu", # Polynesian
                     "Bellona",
                     "Rennell"
                     )
```

```{r}
updated_files <- update_files(poplist_file = poplist_file,
                               labels_file = labels_file,
                               donor_populations = donor_populations,
                               recipient_populations = recipient_populations)
updated_poplist <- updated_files$poplist
updated_labels <- updated_files$labels
include <- updated_files$include

write_delim(updated_poplist,
            new_poplist,
            col_names = FALSE)
write_delim(updated_labels,
            new_labels,
            col_names = FALSE)

```








## Fijians as recipients


Using the same reasoning, we use Fijians as recipients, and as Donors:

- East Asians (all)
- Papuan
- Bismarck
- Solomon
- Vanuatu
- Polynesian (Atiu)
- Polynesian Outlier (Renell and Belonna)



Generate label files:

```{r}
new_poplist <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/02_target_fiji/populations.poplist"
new_labels <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/02_target_fiji/populations.labels"




recipient_populations <- c("Fiji")
donor_populations <- c(
                     "Atayal","Paiwan", # Taiwanese
                     "Han","Dai", # East Asian
                     "Agta","Cebuano", # Island SEA
                     "Papuan_Highlands","Papuan_Sepik","Mendi","Marawaka","Bundi","Kundiawa","Tari", # Papuan mainland
                     "Mamusi", # Bismarck
                     "Bougainville", # Solomon I.
                     "Pentecost", # ni-Vanuatu
                     "Atiu", # Polynesian
                     "Bellona",
                     "Rennell"
                     )
```

```{r}
updated_files <- update_files(poplist_file = poplist_file,
                               labels_file = labels_file,
                               donor_populations = donor_populations,
                               recipient_populations = recipient_populations)
updated_poplist <- updated_files$poplist
updated_labels <- updated_files$labels
write_delim(updated_poplist,
            new_poplist,
            col_names = FALSE)
write_delim(updated_labels,
            new_labels,
            col_names = FALSE)
```



## Polynesian Outliers as recipients


Polynesian Outliers as recipients, Donors:

- East Asians (all)
- Papuan
- Bismarck
- Solomon
- Vanuatu
- Polynesian (Atiu)


Generate label files:



```{r}
new_poplist <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/03_target_po//populations.poplist"
new_labels <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/03_target_po//populations.labels"


recipient_populations <- c("Ontong_Java","Tikopia","Rennell", "Bellona")
donor_populations <- c(
                     "Atayal","Paiwan", # Taiwanese
                     "Han","Dai", # East Asian
                     "Agta","Cebuano", # Island SEA
                     "Papuan_Highlands","Papuan_Sepik","Mendi","Marawaka","Bundi","Kundiawa","Tari", # Papuan mainland
                     "Mamusi", # Bismarck
                     "Bougainville", # Solomon I.
                     "Pentecost", # ni-Vanuatu
                     "Atiu" # Polynesian
                     )
```

```{r}
updated_files <- update_files(poplist_file = poplist_file,
                               labels_file = labels_file,
                               donor_populations = donor_populations,
                               recipient_populations = recipient_populations)
updated_poplist <- updated_files$poplist
updated_labels <- updated_files$labels
write_delim(updated_poplist,
            new_poplist,
            col_names = FALSE)
write_delim(updated_labels,
            new_labels,
            col_names = FALSE)
```



## Eastern Polynesians as recipients

Donors:

- East Asians (all)
- Papuan
- Bismarck
- Solomon
- Vanuatu
- Polynesian Outlier (Renell and Belonna)
- Atiu (this assumes a phylogenetic structure within EP)


```{r}
new_poplist <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/04_target_ep/populations.poplist"
new_labels <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/04_target_ep/populations.labels"
individuals_to_include <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/04_target_ep/populations.include"
recipient_populations <- c("Rangiroa","Bora_Bora","Tahiti","Rurutu","Raivavae","Hiva_Oa","Nuku_Hiva","Mangareva")
donor_populations <- c(
                     "Atayal","Paiwan", # Taiwanese
                     "Han","Dai", # East Asian
                     "Agta","Cebuano", # Island SEA
                     "Papuan_Highlands","Papuan_Sepik","Mendi","Marawaka","Bundi","Kundiawa","Tari", # Papuan mainland
                     "Mamusi", # Bismarck
                     "Bougainville", # Solomon I.
                     "Pentecost", # ni-Vanuatu
                     "Rennell", "Bellona", # Polynesian outliers
                     "Atiu"
                     )
```

```{r}
updated_files <- update_files(poplist_file = poplist_file,
                               labels_file = labels_file,
                               donor_populations = donor_populations,
                               recipient_populations = recipient_populations)
updated_poplist <- updated_files$poplist
updated_labels <- updated_files$labels
write_delim(updated_poplist,
            new_poplist,
            col_names = FALSE)
write_delim(updated_labels,
            new_labels,
            col_names = FALSE)
```
































# Run GlobeTrotter




## Western Polynesians as recipients


Set donor populations.

```{r}
donor_populations <- c(
                     "Atayal","Paiwan", # Taiwanese
                     "Han","Dai", # East Asian
                     "Agta","Cebuano", # Island SEA
                     "Papuan_Highlands","Papuan_Sepik","Mendi","Marawaka","Bundi","Kundiawa","Tari", # Papuan mainland
                     "Mamusi", # Bismarck
                     "Bougainville", # Solomon I.
                     "Pentecost", # ni-Vanuatu
                     "Atiu", # Polynesian
                     "Bellona",
                     "Rennell"
                     )
```


Generate labels file.


```{r}
input_labels_globetrotter <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/02_selected_surrogates/01_target_wp/population.labels"

read_delim("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/01_target_wp/populations.labels",
           col_names = c("individual","identifier","include")) %>%
  mutate(identifier = case_when(individual==identifier ~ "Western_Polynesian",
                                individual!=identifier ~ identifier)) %>%
  write_delim(input_labels_globetrotter,
              col_names = FALSE)
```


Generate local painting files


```{bash}
sbatch /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/00_SCRIPTS/02_pipeline_repository/01_chromopainter_pipeline/03a_aggregate_local_painting_files.bash /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/01_target_wp/painting_by_groups test_supervised_sourcefind_ind /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/01_target_wp/painting_by_groups/local_painting_files 



readlink -f /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/01_target_wp/painting_by_groups/local_painting_files/all_samples_chr* | sort -V > /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/01_target_wp/painting_by_groups/local_painting_files/local_painting_files.txt

```




Run globetrotter.

```{r}
run_globetrotter_slurm( input_file_ids=input_labels_globetrotter,
                        painting_files_list = "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/01_target_wp/painting_by_groups/local_painting_files/local_painting_files.txt",
                        copymatrix="/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/01_target_wp/painting_by_groups/output.chunklengths.out",
                        output_dir="/pasteur/appa/homes/grijodel/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/02_selected_surrogates/01_target_wp",
                        target_popname = "Western_Polynesian",
                        bootstrap = "0",
                        admixdates = "1",
                        surrogate_popnames = donor_populations)
```


Run globetrotter with bootstrap replicates


```{r}
run_globetrotter_slurm( input_file_ids=input_labels_globetrotter,
                        painting_files_list = "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/01_target_wp/painting_by_groups/local_painting_files/local_painting_files.txt",
                        copymatrix="/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/01_target_wp/painting_by_groups/output.chunklengths.out",
                        output_dir="/pasteur/appa/homes/grijodel/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/02_selected_surrogates/01_target_wp/bootstrap_run",
                        target_popname = "Western_Polynesian",
                        bootstrap = "1",
                        admixdates = "2",
                        surrogate_popnames = donor_populations)
```


## Fijians as recipients




Set donor populations.

```{r}
donor_populations <- c(
                     "Atayal","Paiwan", # Taiwanese
                     "Han","Dai", # East Asian
                     "Agta","Cebuano", # Island SEA
                     "Papuan_Highlands","Papuan_Sepik","Mendi","Marawaka","Bundi","Kundiawa","Tari", # Papuan mainland
                     "Mamusi", # Bismarck
                     "Bougainville", # Solomon I.
                     "Pentecost", # ni-Vanuatu
                     "Atiu", # Polynesian
                     "Bellona",
                     "Rennell"
                     )
```


Generate labels file.


```{r}
input_labels_globetrotter <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/02_selected_surrogates/02_target_fiji/population.labels"

read_delim("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/02_target_fiji/populations.labels",
           col_names = c("individual","identifier","include")) %>%
  mutate(identifier = case_when(individual==identifier ~ "Fiji_Islander",
                                individual!=identifier ~ identifier)) %>%
  write_delim(input_labels_globetrotter,
              col_names = FALSE)
```


Generate local painting files


```{bash}
sbatch /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/00_SCRIPTS/02_pipeline_repository/01_chromopainter_pipeline/03a_aggregate_local_painting_files.bash /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/02_target_fiji/painting_by_groups fiji_supervised_sourcefind_ind /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/02_target_fiji/painting_by_groups/local_painting_files 

readlink -f /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/02_target_fiji/painting_by_groups/local_painting_files/all_samples_chr* | sort -V > /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/02_target_fiji/painting_by_groups/local_painting_files/local_painting_files.txt
```




Run globetrotter.

```{r}
run_globetrotter_slurm( input_file_ids=input_labels_globetrotter,
                        painting_files_list = "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/02_target_fiji/painting_by_groups/local_painting_files/local_painting_files.txt",
                        copymatrix="/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/04_directed_for_globetrotter/02_target_fiji/painting_by_groups/output.chunklengths.out",
                        output_dir="/pasteur/appa/homes/grijodel/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/02_selected_surrogates/02_target_fiji/bootstrap_run/",
                        target_popname = "Fiji_Islander",
                        bootstrap = "1",
                        admixdates = "2",
                        surrogate_popnames = donor_populations)
```







































## Polynesian Outliers as recipients

## Eastern Polynesians as recipients






