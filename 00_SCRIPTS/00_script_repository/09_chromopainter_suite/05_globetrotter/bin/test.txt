prop.ind: 1
bootstrap.date.ind: 1
null.ind: 1
input.file.ids: /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/03_supervised_sourcefind/polynesians_supervised/polynesians_supervised.labels
input.file.copyvectors: /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/03_supervised_sourcefind/polynesians_supervised/output.chunklengths.out 
save.file.main: WS51_main.out
save.file.bootstraps: WS51_boot.out
copyvector.popnames: Yoruba Mbuti French Orcadian Han Northern_Han Dai Atayal Paiwan Cebuano Ag Agta Papuan_Sepik Papuan_Highlands Mendi Marawaka Bundi Kundiawa Tari Nakanai_Bileki Nailik Mussau Melamala Lavongai Mamusi Pasismanua Baining Ata Bougainville Vella_Lavella Malaita Santa_Cruz Ureparapara Maewo Ambae Espiritu_Santo Pentecost Ambrym Malakula Emae Efate Tanna Karitiana Colombian Surui Chopccas Cusco Iquitos Matzes Moches Trujillo Uros Pima Maya_2 Huichol Maya Nahua Tarahumara Totonac Tzotzil Zapotec
surrogate.popnames: Malaita Atayal French Paiwan Malakula
target.popname: WS51
num.mixing.iterations: 2
props.cutoff: 0.001
bootstrap.num: 3
num.admixdates.bootstrap: 1
num.surrogatepops.perplot: 3
curve.range: 1 30
bin.width: 0.1
xlim.plot: 0 30
prop.continue.ind: 0
haploid.ind: 0
