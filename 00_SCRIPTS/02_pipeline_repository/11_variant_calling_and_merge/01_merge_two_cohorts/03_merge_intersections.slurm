#!/bin/bash
#SBATCH --job-name=MERGE   # Job name
#SBATCH --mail-type=END,FAIL          # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=gaston.rijo-de-leon@pasteur.fr     # Where to send mail     
#SBATCH --ntasks=1                    # Run on a single CPU
#SBATCH --mem=30gb                     # Job memory request
#SBATCH --output=MERGE_INTERSECTIONS_%A_%a.log -e MERGE_INTERSECTIONS_%A_%a.err  # Standard output and error log
#SBATCH --qos=geh
#SBATCH --partition=geh
#SBATCH --cpus-per-task=1
#SBATCH -a 21
module load tabix
module load samtools
chromosome=${SLURM_ARRAY_TASK_ID}

### Merge VCFs of intersection of polymorphic sites
POLY_INTERSECTION_DIR=/pasteur/zeus/projets/p02/MATAEA/00_DATA/01_HC_MC_merge/01_intersection/chr${chromosome}/
A_INTERSECTION_POLY_POLY=${POLY_INTERSECTION_DIR}/0000_clean.vcf.gz
B_INTERSECTION_POLY_POLY=${POLY_INTERSECTION_DIR}/0001_clean.vcf.gz
bcftools merge --force-samples \
	--merge all \
	--output ${POLY_INTERSECTION_DIR}/merge.vcf.gz \
	--output-type z \
	--write-index \
	$A_INTERSECTION_POLY_POLY $B_INTERSECTION_POLY_POLY
bcftools stats ${POLY_INTERSECTION_DIR}/merge.vcf.gz > ${POLY_INTERSECTION_DIR}/merge.stats
SAMPLE_LIST=${POLY_INTERSECTION_DIR}/merge_samples.txt

## Merge VCFs of intersection of polymorphic and monomorphic sites (A & B)
A_PRIVATE_B_MONO_DIR=/pasteur/zeus/projets/p02/MATAEA/00_DATA/01_HC_MC_merge/02a_private_A_monomorphic_B/chr${chromosome}/
A_PRIVATE_B_MONO_POLY=${A_PRIVATE_B_MONO_DIR}/0000_clean.vcf.gz
A_PRIVATE_B_MONO_MONO=${A_PRIVATE_B_MONO_DIR}/0001_clean.vcf.gz
bcftools merge --force-samples \
        --merge all \
        --output ${A_PRIVATE_B_MONO_DIR}/merge.vcf.gz \
        --output-type z \
        --write-index \
        $A_PRIVATE_B_MONO_POLY $A_PRIVATE_B_MONO_MONO
bcftools stats ${A_PRIVATE_B_MONO_DIR}/merge.vcf.gz > ${A_PRIVATE_B_MONO_DIR}/merge.stats

# Merge VCFs of intersection of polymorphic and monomorphic sites (B & A)
B_PRIVATE_A_MONO_DIR=/pasteur/zeus/projets/p02/MATAEA/00_DATA/01_HC_MC_merge/02b_private_B_monomorphic_A/chr${chromosome}/
B_PRIVATE_A_MONO_POLY=${B_PRIVATE_A_MONO_DIR}/0000_clean.vcf.gz
B_PRIVATE_A_MONO_MONO=${B_PRIVATE_A_MONO_DIR}/0001_clean.vcf.gz
bcftools merge --force-samples \
        --merge all \
        --output ${B_PRIVATE_A_MONO_DIR}/merge.vcf.gz \
        --output-type z \
        --write-index \
        $B_PRIVATE_A_MONO_MONO $B_PRIVATE_A_MONO_POLY
#
bcftools stats ${B_PRIVATE_A_MONO_DIR}/merge.vcf.gz > ${B_PRIVATE_A_MONO_DIR}/merge.stats
#
