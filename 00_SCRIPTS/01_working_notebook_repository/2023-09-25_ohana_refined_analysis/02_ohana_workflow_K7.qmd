---
title: "Ohana refined"
author: "Gaston Rijo De León"
format:
  html:
    embed-resources: true
    toc: true
    toc-depth: 2
date:  today
execute: 
  echo: true
  warning: false
  message: false
  eval: true
date-format: 'full'
engine: knitr
---

# Setup

```{r}
library(tidyverse)
library(janitor)
library(plotly)
library(MetBrewer)
library(ggtree)
library(treeio)
library(qqplotr)
library(glue)
library(here)
i_am("00_SCRIPTS/01_working_notebook_repository/2023-09-25_ohana_refined_analysis/02_ohana_workflow_K7.qmd")
theme_set(theme_bw())
pal <- readxl::read_excel("/pasteur/zeus/projets/p02/MATAEA/gaston/mataea/metadata/population_colors_all_sheets.xlsx", sheet = 3) 
ind_labels <- read_delim("/pasteur/zeus/projets/p02/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/04_Quality_Control/labels.tsv",col_names = c("individual", "identifier", "cohort"))


'%!in%' <- function(x,y)!('%in%'(x,y))

admixture_plot <- function(Q_matrix, K=5) {
  p <- Q_matrix %>%
  ggplot() +
    geom_bar(aes(x=individual,y=proportion,fill=cluster),stat="identity") +
    scale_fill_manual(values = met.brewer("Isfahan2", n=K)) +
    theme(axis.text.x=element_blank())
  return(p)
}
```

# Introduction

The previous approach did not seem to work, as results were noisy and p-values were not calibrated.

We'll change things a little and do the following: take focal populations (Polynesians and Fijians) and "homogeneous" populations (non-admixed according to admixture analysis), and compute the best K. Use those K ancestral components to compute the covariance matrix and test the hypothesis for longer branches in all leaves. If I want to test for selection on an ancestral branch, I just use a lower value of K, effectively merging the tips.





# Selection scans for K 7


## Smoothing curves 


When smoothing curves, we have to take into account that the majority of the likelihood ratio values are zero. This pushes the lle_ratio smoothed distribution towards zero. It also makes it very slow to compute the averages. What I will proceed to do is exclude those values where `lle_ratio == 0`, to get a clearer signal.


### Polynesian ancestry 


## Compute outliers


Read file with smoothed values.

```{r}
selscan_K7 <- read_delim("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/01_ohana/02_refined_analysis/K7/selscan/HC_for_ohana_component_0_filtered_smooth.selscan",col_types = cols(id=col_character())) %>%
  select(-index)
```

Check the distribution of the smoothed `lle_ratio` and the number of SNPs used to compute it.


```{r}
selscan_K7 %>%
  ggplot(aes(x=n_snps)) +
  geom_histogram()
selscan_K7 %>%
  ggplot(aes(x=lle_ratio)) +
  geom_histogram()
selscan_K7 %>%
  ggplot(aes(x=smooth)) +
  geom_histogram()
```


```{r}
selscan_K7 %>%
  ggplot(aes(x=n_snps,y=smooth)) +
  geom_hex() +
  scale_fill_viridis_c()
```

The highest smoothed values have a low number of SNPs. That is not good, as these will have a higher variance. Does this mean that it might be that there will be a higher false positive rate?



Let's define the outlier threshold at $99.95\%$ and functionally annotate those variants with `annovar`

```{r}
quantile_threshold <- 0.9995
smooth_threshold <- quantile(selscan_K7 %>%
                              filter(n_snps > 5) %>%
                              drop_na() %>%
                              pull(smooth),
                                quantile_threshold)
smooth_outliers <- selscan_K7 %>%
  filter(smooth > smooth_threshold) %>%
  arrange(smooth)
dim(smooth_outliers)
```


```{r}
quantile_threshold <- 0.9995
lle_ratio_threshold <- quantile(selscan_K7 %>%
                              filter(lle_ratio != 0 ) %>%
                              pull(lle_ratio),
                                quantile_threshold)
lle_ratio_outliers <- selscan_K7 %>%
  filter(lle_ratio > lle_ratio_threshold) %>%
  arrange(lle_ratio)
dim(lle_ratio_outliers)
```

```{r}
intersect(lle_ratio_outliers$id, smooth_outliers$id)
```


```{r}
chr <- 13 
selscan_K7 %>%
  filter(chromosome == chr) %>%
  ggplot(aes(x=position)) +
  geom_point(aes(y=lle_ratio),alpha=0.3) + 
  geom_point(data=smooth_outliers %>% filter(chromosome==chr),
             aes(y=lle_ratio),color="blue") +
  geom_line(aes(y=smooth), color="red")
```




```{r}
selscan_K7 %>%
  ggplot(aes(x=position)) +
  geom_point(aes(y=lle_ratio),alpha=0.3) + 
  geom_point(data=smooth_outliers,
             aes(y=lle_ratio),color="blue") +
  geom_line(aes(y=smooth), color="red") +
  facet_wrap(~chromosome, ncol = 4, scales = "free_x")
```




