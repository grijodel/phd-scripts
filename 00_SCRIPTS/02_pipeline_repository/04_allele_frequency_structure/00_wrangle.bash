#!/bin/bash
#SBATCH --job-name=STRUCTURE_HC   # Job name
#SBATCH --mail-type=END,FAIL          # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=gaston.rijo-de-leon@pasteur.fr     # Where to send mail     
#SBATCH --ntasks=1                    # Run on a single CPU
#SBATCH --mem=25gb                     # Job memory request
#SBATCH --output=STRUCTURE_HC_%j.log   # Standard output and error log
#SBATCH --qos=geh
#SBATCH --partition=geh
#SBATCH --cpus-per-task=1

source /pasteur/appa/homes/grijodel/.bashrc
module load plink/1.90b6.16 tabix

INDIR=/pasteur/zeus/projets/p02/MATAEA/00_DATA/01_HC_MC_merge/07_remove_inds
PREFIX=merged
VCF=${PREFIX}_af_filters.vcf.gz
IND2POP=/pasteur/zeus/projets/p02/MATAEA/00_DATA/00_metadata/ALL_IND2POP.txt
# Convert to plink
#plink --vcf ${INDIR}/${VCF} \
#        --recode \
#	--double-id \
#        --make-bed \
#        --out ${PREFIX}
#rm ${PREFIX}.ped ${PREFIX}.map ${PREFIX}.nosex ${PREFIX}.log
## Convert to chr:pos
edit_snpids.py ${PREFIX}.bim tmp
mv tmp ${PREFIX}.bim
## Assign population names
edit_fids.py ${PREFIX}.fam $IND2POP tmp
mv tmp ${PREFIX}.fam
# MAF filter of 0.005
plink --bfile ${PREFIX} \
       --freq \
       --out ${PREFIX}
cat ${PREFIX}.frq | \
	grep -v CHR | \
	awk '{if ($5 < 0.005) print $2}' > ${PREFIX}_maf_filter.txt
plink --bfile  ${PREFIX} \
        --exclude ${PREFIX}_maf_filter.txt \
        --make-bed \
        --out ${PREFIX}_MAF
# LD prune
plink --bfile ${PREFIX}_MAF \
       --indep-pairwise 50 5 0.5 \
       --out ${PREFIX}_MAF
plink --bfile  ${PREFIX}_MAF \
        --exclude  ${PREFIX}_MAF.prune.out \
        --make-bed \
        --out ${PREFIX}_MAF_PRUNED
# Thin out number of loci to 0.5M
cat ${PREFIX}_MAF_PRUNED.bim | \
	awk '{print $2}' |\
	shuf -n 500000 > \
	${PREFIX}_MAF_PRUNED_snp_subset_exclude.txt
plink --bfile  ${PREFIX}_MAF_PRUNED \
        --exclude  ${PREFIX}_MAF_PRUNED_snp_subset_exclude.txt \
        --make-bed \
        --out ${PREFIX}_MAF_PRUNED_THINNED
