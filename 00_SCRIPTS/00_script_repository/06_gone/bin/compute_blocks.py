#!/usr/bin/env python3
import argparse
parser = argparse.ArgumentParser( description = 'Compute genomic blocks to exclude from a PLINK file and write to a table with block coordinates.' )
parser.add_argument("prefix", type=str, help="PLINK prefix")
parser.add_argument('--block_size', type=float, help="Block size (cM)", default=10)
parser.add_argument("output_prefix" , type=str, help='Output files prefix')


args = parser.parse_args()
block_size = args.block_size
output_prefix = args.output_prefix
bim = args.prefix + ".bim"
blocks = {}

current_chromosome = 0
new_chromosome = False
new_block = False
block_counter = 0

with open(bim, 'r' ) as f:
    
    for line in f:
        # Extract information from line
        chromosome, snpid, cM, position, a1,a2 = line.rstrip().split()
        cM=float(cM)
        position=int(position)
        # Check if it is a new chromosome
        # If it is a new chromosome, set the centimorgan counter to starting snp and create new block
        if current_chromosome != chromosome: 
            last_cM = cM
            new_block = True
            block_counter = 0
            current_chromosome = chromosome
        else:
            new_chromosome = True
            
        # Record start coordinate for new block
        if new_block:
            start_position = position
            start_cM = cM
            block_counter += 1
        # Compute distance in cM
        distance = float(cM) - last_cM
        # If distance is equal or greater than block size, record end and start a new block
        if distance >= block_size:
            end_position = position
            end_cM = cM
            new_block = True
            last_cM = cM
            # record start and end coordinates in dictionary        
            blocks[(chromosome,block_counter)] = [start_position,end_position,start_cM,end_cM]
            

        else:
            new_block = False




# Write a table with the computed blocks
with open(output_prefix,'w') as f:
    f.write( "chromosome\tblock\tstart_position\tend_position\tstart_cM\tend_cM\n")
    for i in blocks:
        chromosome=i[0]
        block=i[1]
        start_position=blocks[i][0]
        end_position=blocks[i][1]
        start_cM=blocks[i][2]
        end_cM=blocks[i][3]
        newline="{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n".format(
                   chromosome,block,start_position,end_position,start_cM,end_cM
                        )
        f.write(newline)





    







