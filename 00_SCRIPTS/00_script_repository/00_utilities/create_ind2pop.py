#!/usr/bin/env python3
import argparse
parser = argparse.ArgumentParser( description = 'Generate a IND2POP file from a sample list and two -reference- IND2POP files applying python dictionaries')
parser.add_argument("samples", type=str, help=".fam input file")
parser.add_argument("ref1", type=str, help="First reference .fam file")
parser.add_argument("ref2", type=str, help="Second reference .fam file")

args = parser.parse_args()

ref1=args.ref1
ref2=args.ref2
samples=args.samples


ref1_dict={}
ref2_dict={}
with open(ref1, 'r') as f:
    for line in f:
        identifier = line.split()[0]
        ref1_dict[identifier]=line.rstrip()
with open(ref2, 'r') as f:
    for line in f:
        identifier = line.split()[0]
        ref2_dict[identifier]=line.rstrip()


with open(samples, 'r') as samples:    
    for line in samples:
        identifier = line.rstrip() 
        try: 
            print(ref1_dict[identifier])
        except KeyError:
            try:
                print(ref2_dict[identifier])
            except KeyError:
                pass




