## Here I do something similar to Leslie et al. 2015
### ____________________________________________________________
source("../../bin/FinestructurePlots/FinestructureLibrary.R") # read in the R functions, which also calls the needed packages
library(dendextend)
library(ggplot2)
library(reshape2)
library(viridis)
library(plotrix)
library(maps)
library(mapplots)
library(RColorBrewer)
library(dendextend)
library(factoextra)
## Add an alpha value to a colour
add.alpha <- function(col, alpha=1){
  if(missing(col))
    stop("Please provide a vector of colours.")
  apply(sapply(col, col2rgb)/255, 2,
        function(x)
          rgb(x[1], x[2], x[3], alpha=alpha))
}

## Name files
seed="365"

mcmcfile<- paste0("Fs.seed", seed, ".chunkcounts.mcmc.out") ## finestructure mcmc file
treefile<- paste0("Fs.seed", seed, ".chunkcounts.tree.out") ## finestructure tree file

###### READ IN THE MCMC FILES
mcmcxml<-xmlTreeParse(mcmcfile) ## read into xml format
mcmcdata<-as.data.frame.myres(mcmcxml) ## convert this into a data frame

###### READ IN THE TREE FILES

treexml<-xmlTreeParse(treefile) ## read the tree as xml format
ttree<-extractTree(treexml) ## extract the tree into ape's phylo format
## If you dont want to plot internal node labels (i.e. MCMC posterior assignment probabilities)
## now is a good time to remove them via:
#     ttree$node.label<-NULL
## Will will instead remove "perfect" node labels
ttree$node.label[ttree$node.label=="1"] <-""
## And reduce the amount of significant digits printed:
ttree$node.label[ttree$node.label!=""] <-format(as.numeric(ttree$node.label[ttree$node.label!=""]),digits=2)
tdend<-myapetodend(ttree,factor=1) # convert to dendrogram format

# This is just a check 
# tree.pops <- as.data.frame.myres(treexml)$Pop
# tree.pops2 <- lapply(strsplit(tree.pops, ')\\('), function(x) gsub(")|\\(", "", x))
# tree.pops3 <- lapply(tree.pops2, function(x)strsplit(x,","))
# test.pops <- tree.pops3 
# test.tree <- lapply(get_subdendrograms(tdend,150), function(x)x %>% labels)
# a <- NULL 
# for (f in 1:150){
#   x=test.pops[[1]][f]
#   a <- c(a, (all(test.tree[unlist(lapply(test.tree, function(y) x %in% y))] %in% x)))
# }
# all(a)

### Work witht the MCMC samples ------------------------------------------------

mcmc.pops <- mcmcdata$Pop[-1]
mcmc.pops2 <- lapply(strsplit(mcmc.pops, ')\\('), function(x) gsub(")|\\(", "", x))
mcmc.pops3 <- lapply(mcmc.pops2, function(x)strsplit(x,","))

# Get vector with individual names
individuals <- unlist(mcmc.pops3[[1]])

## Estimate Xi ------------------------------------------------------------------

# Xi function (for one mcmc sample)
get.Xi <- function(ind,mcmc.sample){
  clust <- unlist(mcmc.sample[which(unlist(lapply(mcmc.sample, function(y) any(y==ind))))])
  Xi <- length(clust)
  return(Xi)
}

# Xi function (for one mcmc sample)
get.Xi.m <- function(ind,mcmc){
  Xi.m = unlist(lapply(mcmc,function(mcmc.sample)get.Xi(ind=ind,mcmc.sample)))
  return(Xi.m)
}

Xi.m.all <- sapply(individuals, function(ind) get.Xi.m(ind=ind, mcmc=mcmc.pops3))

## Estimate Yi --------------------------------------------------------------------

# Xi function, names of ind instead of length of the cluster (for one mcmc sample)
get.Xi.names <- function(ind,x){
  clust <- unlist(x[which(unlist(lapply(x, function(y) any(y==ind))))])
  return(clust)
}

# Xi function, names of ind instead of length of the cluster (for one mcmc sample)
get.Xi.m.names <- function(ind, mcmc){
  Xi.m = lapply(mcmc,function(x)get.Xi.names(ind=ind,x))
  return(Xi.m)
}

# Yik function
Get.YiK <- function(ind, mcmc, clusters){
  Xi.m.names <- get.Xi.m.names(ind, mcmc)
  dendo.clust <- unlist(clusters[which(unlist(lapply(clusters, function(y) any(y==ind))))])
  Yik.m <- matrix(as.numeric(lapply(Xi.m.names, function(x) length(which(dendo.clust %in% x)))))
  colnames(Yik.m) <- ind
  return(Yik.m)
}

# get clusters for K=2...k from the dendogram
# make list with all dendograms at diferent K
# Note: not all K values are possible, as we go down in the dendo there are branches with the same height
Dendo.allK=NULL
H=get_nodes_attr(tdend, "height")[1]
dendo.cut <- cut.dendrogram(tdend,H)
K=length(dendo.cut$lower)
print(K)
new.dendo <- dendo.cut$lower
Dendo.allK[[K]] <- new.dendo
while(H > 1){
  H=mean(unique(sort(unlist(lapply(new.dendo, function(x)get_nodes_attr(x, "height"))), decreasing = T))[1:2])
  dendo.cut <- cut.dendrogram(tdend,H)
  K=length(dendo.cut$lower)
  print(K)
  new.dendo <- dendo.cut$lower
  Dendo.allK[[K]] <- new.dendo
}

# Function to Get.sumM
Get.sumM <- function(Dendo.allK, mcmc, individuals, K){
  dendo.clust <- Dendo.allK[[as.numeric(K)]]
  clusters <- lapply(dendo.clust, function(x)return(x %>% labels))
  Yik.m.all <- sapply(individuals, function(x)Get.YiK(x, mcmc, clusters))
  if(any(!Yik.m.all <= Xi.m.all)){
    stop("Yik bigger than Xi")
  }
  sum.M=as.data.frame(apply(Yik.m.all/Xi.m.all, 2,sum))
  colnames(sum.M) <- K
  return(sum.M)
}

#iterarte only for not emty K from the list
K.vector <- labels(Dendo.allK)[lapply(Dendo.allK, length)>0]
sum.M.ALLK <- lapply(K.vector, function(K)Get.sumM(Dendo.allK, mcmc=mcmc.pops3, individuals, K))

sum.M.ALLK.df <- data.frame(matrix(unlist(sum.M.ALLK), ncol=length(sum.M.ALLK), byrow=F))
colnames(sum.M.ALLK.df) <- K.vector
rownames(sum.M.ALLK.df) <- rownames(sum.M.ALLK[[1]])

# Plot matrix with ggplot
sum.M.ALLK.df.ggplot <- melt(t(sum.M.ALLK.df)) # transform to ggplot format
colnames(sum.M.ALLK.df.ggplot) <- c("K", "Ind", "sum.M")
png("matrix.png")
ggplot(sum.M.ALLK.df.ggplot, aes(x = as.character(K), y = Ind, fill = sum.M)) +
  geom_tile() +
  scale_fill_viridis(option="viridis", direction=-1)+
  theme(axis.text=element_text(size=0.6),
        axis.ticks.length=unit(.1, "cm"))
dev.off()

sum.M.ALLK.df.ggplot[sum.M.ALLK.df.ggplot$K %in% 2:50,]
ggplot(sum.M.ALLK.df.ggplot[sum.M.ALLK.df.ggplot$K %in% 2:10,], aes(x = sum.M, fill = as.character(K)), col=as.character(K)) +
  geom_density(alpha=0.3)


MEAN <- tapply(sum.M.ALLK.df.ggplot$sum.M, sum.M.ALLK.df.ggplot$K, mean)
MIN <- tapply(sum.M.ALLK.df.ggplot$sum.M, sum.M.ALLK.df.ggplot$K, min)
SD <- tapply(sum.M.ALLK.df.ggplot$sum.M, sum.M.ALLK.df.ggplot$K, sd)

plot(as.numeric(names(MEAN)), as.numeric(MEAN), pch=19, axes = F, type="o", col="gray60",
     ylab="mean Sum.M", xlab="K")
axis(side = 1, at = c(seq(0,150, by=5)),cex.axis=0.7)
axis(side = 2, las=2, cex.axis=0.8)
# abline(h=99, lty=2)
# abline(v=as.numeric(names(MEAN))[max(which(as.numeric(MEAN)>=99))],
#          lty=2)
abline(v=20, lty=2)
abline(h=as.numeric(MEAN)[which(as.numeric(names(MEAN))==20)],
       lty=2)

# plotCI(as.numeric(names(MEAN)), as.numeric(MEAN), pch=19, ui=as.numeric(MEAN+SD), li=as.numeric(MEAN-SD))

threshold=90
num.ind.threshold <- tapply(sum.M.ALLK.df.ggplot$sum.M, sum.M.ALLK.df.ggplot$K, function(x) length(which(x < threshold)))

plot(num.ind.threshold, unique(sum.M.ALLK.df.ggplot$K), pch=19, axes = F, ylab="",
     xlab=paste("number of individuals below threshold", threshold), type="o", col="gray60")
axis(side = 1, at = c(seq(0,max(num.ind.threshold), by=20)),cex.axis=0.7)
axis(side = 2, at = c(seq(0,max(unique(sum.M.ALLK.df.ggplot$K)), by=5)),las=2, cex.axis=0.7)
# abline(v=100, lty=2)
# abline(h=unique(sum.M.ALLK.df.ggplot$K)[max(which(num.ind.threshold<=100))],
#        lty=2)
abline(h=20, lty=2)
abline(v=num.ind.threshold[which(unique(sum.M.ALLK.df.ggplot$K)==20)],
       lty=2)
mtext(side=2, line=2.5,"K",cex=1, las=2)


###===========================================================================================
## Plot clusters in a map
###___________________________________________________________________________________________

# Function to add multiple pies at the same time in the map though a lapply
add.multiplePies <- function(pop, table, colors, radio.factor){
  pop.coor <- Coor[Coor[,1] %in% pop,]
  radio <- sum(unlist(table))*radio.factor
  table  <- as.data.frame(cbind(table=unlist(table) , colors=colors))
  table  <- table [!table$table==0,]
  plot.pie <- add.pie(z=as.numeric(as.vector(table$table)), x=pop.coor[,3], y=pop.coor[,2], 
                      col = add.alpha(unlist(table$colors),1), 
                      radius=radio, 
                      labels = "", border = NA, edge=3000)
  return(plot.pie)
}

# get clusters for K=2...k from the dendogram
# make list with all dendograms at diferent K
# Note: not all K values are possible, as we go down in the dendo there are branches with the same heightDendo.allK=NULL
H=get_nodes_attr(tdend, "height")[1]
dendo.cut <- cut.dendrogram(tdend,H)
K=length(dendo.cut$lower)
print(K)
new.dendo <- dendo.cut$lower
Dendo.allK[[K]] <- new.dendo
while(H > 1){
  H=mean(unique(sort(unlist(lapply(new.dendo, function(x)get_nodes_attr(x, "height"))), decreasing = T))[1:2])
  dendo.cut <- cut.dendrogram(tdend,H)
  K=length(dendo.cut$lower)
  print(K)
  new.dendo <- dendo.cut$lower
  Dendo.allK[[K]] <- new.dendo
}

# Work with the coordinates files
Coor <- read.table("Coordinates_Samples.txt") # Normal coordinates for the samples
Coor.names <- read.table("Coordinates_Samples_2.txt") # Coordiantes only to plot the island name
K.vector <- as.numeric(labels(Dendo.allK)[lapply(Dendo.allK, length)>0]) #vector of the K values

# PLOT MAP with pie charts ------------------------------------------
pdf("maps.pdf")
  for (K in K.vector){
    dendo.clust <- Dendo.allK[[as.numeric(K)]]
    clusters <- lapply(dendo.clust, function(x)return(x %>% labels))
    populations <- unique(sapply(strsplit(individuals, "\\."), head, 1))
    clusters.pops <- lapply(clusters, function(clust)sapply(strsplit(clust, "\\."), head, 1))
    
    table <- lapply(populations, function(pop) lapply(clusters.pops, function(y) length(which(y==pop))))
    labels(table) <- populations
    
    par(mar=c(0,0,0,0))
    vanutatu <- map(database="world", xlim=range(166.2,170),
                    ylim=range(-12.8,-20.3), fill=T, col="gray50", bg="white",
                    border="gray50", mar=c(0,0,0,0))
    
    pal <- c("#F6DC1B", "#E31770", "#258FCF", "#B6CA02")
    # colors <- colorRampPalette(brewer.pal(9,"Set1"))(K)
    # colors <- colorRampPalette(viridis(12))(K)
    colors <- colorRampPalette(pal)(K)    
    sapply(names(table), function(pop) add.multiplePies(pop, table[[pop]], colors, radio.factor=0.0017))
  }
dev.off()


# PLOT MAP with points using jitter ------------------------------------------
# The coordinates in this case correspond to Villages (if I don't have the coordinates for a village I use the Island)

# work with ind names and coordinates to get the coordinates for the villages (it's messy but should work)
correspondance <- read.table("../../labels_correspondence.txt")
coor <- read.table("../../../../../VANUATU_1442/Map/240320/Village_info_070420.txt", header = T, stringsAsFactors = F)
fam <- read.table("../../../../../VANUATU_1442/Map/240320/Vanuatu_1433x2358955.fam", stringsAsFactors = F)
info <- read.table("../../../../../VANUATU_1442/Map/240320/genotype_info_24032020.txt", header = T, stringsAsFactors = F)
info.fam <- info[match(fam[,2], info$IID),]
## Some villages were annotated as duplicated, because they small variations in the name of the village, 
# but acutally was consider the same village. Here I am arranging those villages from the fam
DUP.vil <- unlist(strsplit(as.vector(coor[!is.na(coor$Othere_names_in_the_dataset),]$Othere_names_in_the_dataset), "/"))
DUP <-  info.fam[as.vector(info.fam$VILLAGE) %in% unlist(strsplit(as.vector(coor[!is.na(coor$Othere_names_in_the_dataset),]$Othere_names_in_the_dataset), "/")),]
DUP.vil.found <- DUP.vil[DUP.vil %in% DUP$VILLAGE]
for (dup in DUP.vil.found){
  new.vil <- as.vector(coor[which(
    unlist(lapply(strsplit(as.vector(coor$Othere_names_in_the_dataset), "/"), function(x) dup %in% x))
  ),]$VIL.correct)
  info.fam[info.fam$VILLAGE==dup,]$VILLAGE <- new.vil
}
info.fam.coor <- info.fam[info.fam$VILLAGE %in% coor$VIL.correct,]
table(info.fam[! info.fam$VILLAGE %in% coor$VIL.correct,]$VILLAGE) # list villages that I don't have coor.
info.fam.coor.ALL <- coor[match(info.fam.coor$VILLAGE, coor$VIL.correct),1:4]
vill.coor <- unique(info.fam.coor.ALL)
vill.coorespondance <- merge(correspondance, info.fam.coor, by.x = "V2", by.y = "IID")[,c(1,2,3,5)]
vill.coor.all <- merge(vill.coorespondance, vill.coor, by.x = "VILLAGE", by.y = "VIL.correct")

pdf("map_jitter.pdf")
  for (K in K.vector){
    dendo.clust <- Dendo.allK[[as.numeric(K)]]
    clusters <- lapply(dendo.clust, function(x)return(x %>% labels))
    populations <- unique(sapply(strsplit(individuals, "\\."), head, 1))
    clusters.pops <- lapply(clusters, function(clust)sapply(strsplit(clust, "\\."), head, 1))
  
    vanutatu <- map(database="world", xlim=range(166.2,170.5),
                    ylim=range(-12.8,-20.3), fill=T, col="gray50", bg="white",
                    border="gray50", mar=c(0,0,0,0))
    
    pal <- c("#F6DC1B", "#E31770", "#258FCF", "#B6CA02")
    # colors <- colorRampPalette(brewer.pal(9,"Set1"))(K)
    # colors <- colorRampPalette(viridis(12))(K)
    colors <- colorRampPalette(pal)(K)
    
    cluster.index <- rep(1:K,unlist(lapply(clusters.pops, length)))
    coor.list <- Coor[match(unlist(clusters.pops),Coor[,1]),]
    col <-  rep(colors,unlist(lapply(clusters.pops, length)))
    pch <- 21
    info <- cbind(unlist(clusters.pops), cluster.index, coor.list, col, pch, unlist(clusters))
    info[info[,8] %in% vill.coor.all[,4],4:5] <- vill.coor.all[na.omit(match(info[,8], vill.coor.all[,4])),6:7]
    
    text(Coor.names[,3], Coor.names[,2], Coor.names[,1], col = add.alpha("black", 0.6), cex=0.7, adj = 0)
    apply(info,1,function(f)points(
      x=jitter(as.numeric(f[5]),1, amount=0.07),
      y=jitter(as.numeric(f[4]),1,amount=0.07),
      col=as.character(f[6]),
      bg=add.alpha(as.character(f[6]),0.5),
      pch=as.numeric(f[7]),
      cex=0.2))
  }
dev.off()      


###===========================================================================================
## DENDOGRAM
###___________________________________________________________________________________________

# get clusters for K=2...k from the dendogram
# make list with all dendograms at diferent K
# Note: not all K values are possible, as we go down in the dendo there are branches with the same height
Dendo.allK=NULL
H=get_nodes_attr(tdend, "height")[1]
dendo.cut <- cut.dendrogram(tdend,H)
K=length(dendo.cut$lower)
print(K)
new.dendo <- dendo.cut$lower
Dendo.allK[[K]] <- dendo.cut$upper
while(H > 1){
  H=mean(unique(sort(unlist(lapply(new.dendo, function(x)get_nodes_attr(x, "height"))), decreasing = T))[1:2])
  dendo.cut <- cut.dendrogram(tdend,H)
  K=length(dendo.cut$lower)
  print(K)
  new.dendo <- dendo.cut$lower
  Dendo.allK[[K]] <- dendo.cut$upper
}
K.vector <- as.numeric(labels(Dendo.allK)[lapply(Dendo.allK, length)>0]) #vector of the K values

# Color palette
pal <- c("#F6DC1B", "#E31770", "#258FCF", "#B6CA02")

## Use this to plot the dendogram complete, with label names reduced, but not cuting at any hight
# mapstate<-extractValue(treexml,"Pop") # map state as a finestructure clustering
# mapstatelist<-popAsList(mapstate) # .. and as a list of individuals in populations
# popnames<-lapply(mapstatelist,NameSummary) # population names IN A REVERSIBLE FORMAT (I.E LOSSLESS)
# ## NOTE: if your population labels don't correspond to the format we used (NAME<nufviz_dendmber>) YOU MAY HAVE TROUBLE HERE. YOU MAY NEED TO RENAME THEM INTO THIS FORM AND DEFINE YOUR POPULATION NAMES IN popnamesplot BELOW
# popnamesplot<-lapply(mapstatelist,NameMoreSummary) # a nicer summary of the populations
# names(popnames)<-popnamesplot # for nicety only
# names(popnamesplot)<-popnamesplot # for nicety only
# popdend<-makemydend(tdend,mapstatelist) # use NameSummary to make popdend
# popdend<-fixMidpointsComplete(popdend) # needed for obscure dendrogram reasons
# popdendclear<-makemydend(tdend,mapstatelist,"NameMoreSummary")# use NameMoreSummary to make popdend
# popdendclear<-fixMidpointsComplete(popdendclear) # needed for obscure dendrogram reasons
# K=8 # loop here or plot the K that you want
# colors <- colorRampPalette(pal)(K)
# fviz_dend(popdendclear , cex = 0.5, k = K,lwd=0.5, lower_rect=T, main=paste("  K", K),
#           k_colors = colors, type = "rectangle", horiz=T,labels_track_height = 30)+
#   theme(plot.margin=unit(c(1,1,1,1.2),"cm"))+
#   theme_void()


## Use this to plot the dendogram cuting for K clusters and reduce label names
# K=20 # loop here or plot the K that you want
# colors <- colorRampPalette(pal)(K)
# dendo.clust <- Dendo.allK[[K]]
# mapstatelist2 <- lapply(get_subdendrograms(tdend, K), function(x)return(x %>% labels))
# popnames<-lapply(mapstatelist2,NameSummary) # population names IN A REVERSIBLE FORMAT (I.E LOSSLESS)
# ## NOTE: if your population labels don't correspond to the format we used (NAME<nufviz_dendmber>) YOU MAY HAVE TROUBLE HERE. YOU MAY NEED TO RENAME THEM INTO THIS FORM AND DEFINE YOUR POPULATION NAMES IN popnamesplot BELOW
# popnamesplot<-lapply(mapstatelist2,NameMoreSummary) # a nicer summary of the populations
# names(popnames)<-popnamesplot # for nicety only
# labels(dendo.clust) <- names(popnames)
# dendo.clust.pop <- fixMidpointsComplete(dendo.clust)
# labels(dendo.clust) <- unlist(popnames)
# dendo.clust.pop <- fixMidpointsComplete(dendo.clust)
# fviz_dend(dendo.clust.pop , cex = 0.5, k = K,lwd=0.5, lower_rect=T, main=paste("  K", K),
#           k_colors = colors, type = "rectangle", horiz=T,labels_track_height = 30)+ 
#   theme(plot.margin=unit(c(1,1,1,1.2),"cm"))+
#   theme_void()


## Use this to plot the dendogram cuting for K clusters and reduce label names AND simplify some populations
# K=20 # loop here or plot the K that you want

Plot.Dendo.LARA <- function(K){

  colors <- colorRampPalette(pal)(K)
  dendo.clust <- Dendo.allK[[K]]
  
  New_Britain <- c("Baining", "Mussau","Nakanai_Bileki", "Nakanai", "Lavongai", "Nailik",  "Mamusi",
                   "Pasismanua", "Melamala"  )  
  Solomon_Is <- c("Vella_Lavella",  "Malaita"  )
  PNG <- c("Mendi", "Tari", "Marawaka","Bundi","Kundiawa"  )
  EastAsia <- c( "Paiwan",  "Agta", "Atayal", "Cebuano")
  
  mapstatelist <- lapply(get_subdendrograms(tdend, K), function(x)return(x %>% labels))
  popnames <- lapply(mapstatelist, function(x)sapply(strsplit(x, "\\."), head,1))
  popnames <- lapply(popnames, function(x){
                        x[x %in% Solomon_Is] <- "Solomon_Is"
                        x[x %in% PNG] <- "PNG"
                        x[x %in% EastAsia] <- "EastAsia"
                        x[x %in% New_Britain] <- "New_Britain"
                        return(x)})
  popnames <- lapply(popnames, function(x)
              paste(paste0(as.numeric(table(x)),names(table(x))), collapse = ";"))
  labels(dendo.clust) <- unlist(popnames)
  dendo.clust.pop <- fixMidpointsComplete(dendo.clust)
  
  ## Plot phylogenic dendogram
  # fviz_dend(dendo.clust.pop , cex = 0.5, k = K,lwd=0.5, lower_rect=T, main=paste("  K", K),
  #           k_colors = colors, type = "phylogenic", horiz=T,labels_track_height = 30)+ 
  #   theme(plot.margin=unit(c(1,1,1,1.2),"cm"))+
  #   theme_void()
  #
  ## Plot circular dendogram
  # fviz_dend(dendo.clust.pop , cex = 0.5, k = K,lwd=0.5, lower_rect=T, main=paste("  K", K),
  #           k_colors = colors, type = "circular", horiz=T)+ 
  #   theme(plot.margin=unit(c(1,1,1,1.2),"cm"))+
  #   theme_void()
  
  ## Plot normal dendogram
  dendo.plot <- fviz_dend(dendo.clust.pop , cex = 0.5, k = K,lwd=0.5, lower_rect=T, main=paste("  K", K),
            k_colors = colors, type = "rectangle", horiz=T,labels_track_height = 30)+ 
    theme(plot.margin=unit(c(1,1,1,1.2),"cm"))+
    theme_void()
  print(dendo.plot)
}

pdf("Dendogram_allK_reduced.pdf")
  sapply(K.vector, Plot.Dendo.LARA)
dev.off()

##-------------
# Make label list and table with the K choice
K=25
mapstatelist <- lapply(get_subdendrograms(tdend, K), function(x)return(x %>% labels))
popnames <- lapply(mapstatelist, function(x)sapply(strsplit(x, "\\."), head,1))


clusters.names <- paste("cluster", 1:20, sep = "_")
pop.names <- unique(unlist(popnames))
table.pops <- matrix(nrow = length(pop.names), ncol = length(clusters.names), 0)
colnames(table.pops) <- clusters.names
rownames(table.pops) <- pop.names

list.table <- lapply(popnames, table)

for (f in 1:ncol(table.pops)){
  table.pops[names(table.pops[,f]) %in% names(list.table[[f]]),f] <- as.numeric(list.table[[f]])
}


labels <- read.table("../Inputs/Vanuatu_1412x2269868_SNPs_Sequences_filtered_WithREL__chr1_ChP.labels")
labels.clusters <- as.data.frame(cbind(unlist(mapstatelist), 
rep(paste("cluster", 1:K, sep = "_"), unlist(lapply(mapstatelist, length)))))

labels.clusters.table <- merge(labels, labels.clusters, by="V1", all = T)
labels.clusters.table[is.na(labels.clusters.table$V2.y),]$V2.y <- labels.clusters.table[is.na(labels.clusters.table$V2.y),]$V2.x
labels.clusters.final <- labels.clusters.table[,c(1,4,3)]
labels.clusters.final -> labels.clusters.final.notRem

# Ind to exclude: 
seeds=c(365, 552, 659, 798, 856)
remove <- c()
for (seed in seeds){
  consist <- data.frame(read.table(paste0("MCM_consistency_indRemove_seed", seed, ".txt"), header=F))
  colnames(consist) <- consist[1,]
  consist <- consist[-1,]
  remove <- rbind(remove, cbind(seed, na.omit(consist$`25`)))
}
remove <- rownames(sum.M.ALLK.df)[which(sum.M.ALLK.df$`20` < threshold)]
labels.clusters.final[labels.clusters.final$V1 %in% remove,]$V3 <- 0

write.table(labels.clusters.final, paste0("clusters_K", K, ".labels"), quote = F, col.names = F, row.names = F)



### ======================================================
## ___________________________________
## TVD

library(data.table)


permutation <- function(x) {
  random.X <- coancestry.matrix[unlist(list.of.samples.X[x]),]
  random.Y <- coancestry.matrix[unlist(list.of.samples.Y[x]),]
  copyvector.random.X <- apply(random.X,2,function(x) mean(as.numeric(x)))
  copyvector.random.Y <- apply(random.Y,2,function(x) mean(as.numeric(x)))
  TVD.random=0.5*sum(abs(copyvector.random.X-copyvector.random.Y))
  return(TVD.random)
}

coancestry.matrix <- fread("../Donors_140520/Donors_140520_allchr.chunklengths.out", header=T, data.table=TRUE)
coancestry.matrix <- fread("../Donors_260520/Donors_260520_allchr.chunklengths.out", header=T, data.table=TRUE)

colnames(coancestry.matrix)[1] <- "Recipients"

labels <- read.table("../Donors_260520/clusters_K20.labels")
labels <- labels[labels$V3==1,]
cluster.vector <- labels[match(coancestry.matrix$Recipients, labels$V1),2]
coancestry.matrix <- coancestry.matrix[,-1]
coancestry.matrix <- coancestry.matrix/apply(coancestry.matrix,1,sum)

 # coancestry.matrix <- coancestry.matrix[,c(3)]

TVD.matrix <- matrix(nrow = length(unique(cluster.vector)), ncol= length(unique(cluster.vector)))
row.names(TVD.matrix) <- unique(cluster.vector)
colnames(TVD.matrix) <- unique(cluster.vector)

pvalue.matrix <- matrix(nrow = length(unique(cluster.vector)), ncol= length(unique(cluster.vector)))
row.names(pvalue.matrix) <- unique(cluster.vector)
colnames(pvalue.matrix) <- unique(cluster.vector)

nPerm=1000

for (X.name in rownames(TVD.matrix)){
  for (Y.name in colnames(TVD.matrix)){
  
  copyvector.X <- apply(coancestry.matrix[which(cluster.vector==X.name),],2,function(x) mean(as.numeric(x)))
  copyvector.Y <- apply(coancestry.matrix[which(cluster.vector==Y.name),],2,function(x) mean(as.numeric(x)))
  
  TVD=0.5*sum(abs(copyvector.X-copyvector.Y))
  TVD.matrix[which(rownames(TVD.matrix)==X.name), which(colnames(TVD.matrix)==Y.name)] <- TVD
  
  #Random permutations
  list.of.samples.X = lapply(1:nPerm, function(x) sample(1:nrow(coancestry.matrix), length(which(cluster.vector==X.name))))
  list.of.samples.Y = lapply(1:nPerm, function(x) sample(1:nrow(coancestry.matrix), length(which(cluster.vector==Y.name))))
  # lapply(1:1000, function(x) any(list.of.samples.X[x] %in% list.of.samples.Y[x]))
  
  random.values <- sapply(1:nPerm, permutation)
  
  # pvalue=ecdf(random.values)(TVD)
  # if(pvalue > 0.5){pvalue=1-pvalue}
  # pvalue.matrix[X,Y] <- pvalue
  
  pvalue <- length(random.values[random.values >= TVD])/nPerm
  pvalue.matrix[which(rownames(pvalue.matrix)==X.name), which(colnames(pvalue.matrix)==Y.name)] <- pvalue
  
  }
}


pdf(paste0("TVD_ALLvsALLdonors.pdf"))

library(ggforce)
circles_v <- data.frame(
  x0 = rep(0, nrow(pvalue.matrix)),
  y0 = seq(1,nrow(pvalue.matrix))
)
circles_h <- data.frame(
  y0 = rep(0, nrow(pvalue.matrix)),
  x0 = seq(1,nrow(pvalue.matrix))
)

# Color palette
pal <- c("#F6DC1B", "#E31770", "#258FCF", "#B6CA02")
colors <- colorRampPalette(pal)(20)[-c(4,5,20)]


sorted <- order(as.numeric(unlist(lapply(strsplit(colnames(TVD.matrix), "_"),function(x)x[2]))))
TVD.matrix <- TVD.matrix[sorted,sorted]
TVD.matrix.ggplot <- melt(TVD.matrix)

ggplot(TVD.matrix.ggplot, aes(x = Var1, y = Var2, fill = value)) +
  geom_tile() +
  scale_fill_viridis(option = "inferno", name="TVD")+
  theme(axis.text.x = element_text(angle = 45, hjust = 1))+
  geom_circle(data=circles_v, aes(y=y0, x=x0, x0=0, y0=0, r = 0,
                                  colour=factor(y0)), 
              size=4, inherit.aes = F)+
  scale_colour_manual(values = colors)+
  geom_circle(data=circles_h, aes(y = y0, x=x0, x0=0, y0=0, r = 0,
                                  colour=factor(x0)), 
              size=4, inherit.aes = F)+
  geom_circle(aes(y0=0, x0=-1, r = 0), 
              size=0, inherit.aes = F)+
  geom_circle(aes(y0=-1, x0=0, r = 0), 
              size=0, inherit.aes = F)+
  theme(panel.background = element_blank(),axis.title=element_blank())+
  guides(color = FALSE, size = FALSE)

sorted <- order(as.numeric(unlist(lapply(strsplit(colnames(pvalue.matrix), "_"),function(x)x[2]))))
pvalue.matrix <- pvalue.matrix[sorted,sorted]
pvalue.matrix.bi <- pvalue.matrix
pvalue.matrix.bi[pvalue.matrix.bi < 0.05] <- "<0.05"
pvalue.matrix.bi[pvalue.matrix.bi != "<0.05"] <- ">=0.05"
pvalue.matrix.ggplot.bi <- melt(pvalue.matrix.bi)
pvalue.matrix.ggplot <- melt(pvalue.matrix)

ggplot(pvalue.matrix.ggplot.bi, aes(x = Var1, y = Var2, fill = value)) +
  geom_tile() +
  scale_fill_manual(values=c("grey70", "grey20"), name="TVD p-value")+
  theme(axis.text.x = element_text(angle = 45, hjust = 1))+
  geom_circle(data=circles_v, aes(y=y0, x=x0, x0=0, y0=0, r = 0,
                                colour=factor(y0)), 
            size=4, inherit.aes = F)+
  scale_colour_manual(values = colors)+
  geom_circle(data=circles_h, aes(y = y0, x=x0, x0=0, y0=0, r = 0,
                                  colour=factor(x0)), 
              size=4, inherit.aes = F)+
  geom_circle(aes(y0=0, x0=-1, r = 0), 
              size=0, inherit.aes = F)+
  geom_circle(aes(y0=-1, x0=0, r = 0), 
              size=0, inherit.aes = F)+
  theme(panel.background = element_blank(),axis.title=element_blank())+
  guides(color = FALSE, size = FALSE)

ggplot(pvalue.matrix.ggplot, aes(x = Var1, y = Var2, fill = value)) +
  geom_tile(color="white") +
  scale_fill_viridis(option = "inferno",name="TVD p-value", direction = -1)+
  theme(axis.text.x = element_text(angle = 45, hjust = 1))+
  geom_circle(data=circles_v, aes(y=y0, x=x0, x0=0, y0=0, r = 0,
                                  colour=factor(y0)), 
              size=4, inherit.aes = F)+
  scale_colour_manual(values = colors)+
  geom_circle(data=circles_h, aes(y = y0, x=x0, x0=0, y0=0, r = 0,
                                  colour=factor(x0)), 
              size=4, inherit.aes = F)+
  geom_circle(aes(y0=0, x0=-1, r = 0), 
              size=0, inherit.aes = F)+
  geom_circle(aes(y0=-1, x0=0, r = 0), 
              size=0, inherit.aes = F)+
  theme(panel.background = element_blank(),axis.title=element_blank())+
  guides(color = FALSE, size = FALSE)

dev.off()




