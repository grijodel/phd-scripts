#!/bin/sh
#SBATCH -J CP_Convert
#SBATCH -o 01_CP_convert_%A_%a.out -e 01_CP_convert_%A_%a.err
#SBATCH --mem=50000
#SBATCH -c 1
#SBATCH --array=1-22
#SBATCH --qos=geh
#SBATCH --partition=geh

module load R/4.1.0
module load plink/2.00a3
source /pasteur/appa/homes/grijodel/.bashrc

chromosome=$SLURM_ARRAY_TASK_ID
VCF=/pasteur/zeus/projets/p02/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/05_Phasing/03_Phase_Scaffold/shapeit4Phased_scaffold_chr${chromosome}.vcf.gz
OUTDIR=/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/00_preprocess_datasets/01_all_populations/chr${chromosome}
PREFIX=shapeit4Phased_scaffold_chr${chromosome}
IND2POP=/pasteur/zeus/projets/p02/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/04_Quality_Control/ind2pop.tsv
KEEP_INDS=/pasteur/zeus/projets/p02/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/04_Quality_Control/all_passing_individuals.tsv
## MERGED HC AND MC
VCF=/pasteur/zeus/projets/p02/MATAEA/00_DATA/01_HC_MC_merge/07_remove_inds/vcf_by_chr/merged_chr${chromosome}.vcf.gz
OUTDIR=/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/04_chromopainter/00_preprocess_datasets/02_full_dataset/chr${chromosome}
PREFIX=merged_chr${chromosome}
IND2POP=/pasteur/zeus/projets/p02/MATAEA/00_DATA/00_metadata/ALL_IND2POP.txt
#KEEP_INDS=



mkdir -p $OUTDIR
# Convert to plink format
plink2 --vcf $VCF \
        --chr ${chromosome} \
        --make-bed \
        --out ${OUTDIR}/${PREFIX}
# Chr:pos format
edit_snpids.py ${OUTDIR}/${PREFIX}.bim ${OUTDIR}/tmp
mv ${OUTDIR}/tmp ${OUTDIR}/${PREFIX}.bim
# Add population info
edit_fids.py ${OUTDIR}/${PREFIX}.fam ${IND2POP} ${OUTDIR}/tmp
mv ${OUTDIR}/tmp ${OUTDIR}/${PREFIX}.fam
# Interpolate cM
interpolate_recombination_map.R ${OUTDIR}/${PREFIX}.bim ${chromosome}
# Remove:
## SNPs excluded in interpolation
## related individuals
## variants with MAC less than 2 (no singletons and invariant sites)
plink2 --bfile ${OUTDIR}/${PREFIX} \
        --exclude  ${OUTDIR}/${PREFIX}_snps_to_remove.txt \
        --out ${OUTDIR}/CHP_FILTER1_chr${chromosome} \
	--maf 0.05 \
	--make-bed


rm ${OUTDIR}/${PREFIX}*  
