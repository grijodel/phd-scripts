#!/bin/bash

# modules/softwares
module load plink/1.90b6.16
admixturebayes_path=~/bin/AdmixtureBayes/admixturebayes

# input settings
pruned_plink_file_prefix=/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/HC_MAF_PRUNED
infile_prefix=$(basename $1 .keep)
outgroup=Mbuti

# prepare the allele count input
plink --bfile $pruned_plink_file_prefix --keep ${infile_prefix}.keep --missing --family --make-bed --out $infile_prefix
less ${infile_prefix}.lmiss | awk '$7==1' | awk '{print $2}' | sort -u | grep -v SNP > ${infile_prefix}.lmiss.F1
plink --bfile ${infile_prefix} --freq --family --exclude ${infile_prefix}.lmiss.F1 --out ${infile_prefix}_noF1
python3 /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/00_SCRIPTS/02_pipeline_repository/06_admixture_graph/plink2treemix.py ${infile_prefix}_noF1.frq.strat ${infile_prefix}_noF1.count.txt
cat ${infile_prefix}_noF1.count.txt | sed 's/_//g' > ${infile_prefix}_adb_in.txt

# run MCMC
MCMC_n=$2
python -u ${admixturebayes_path}/runMCMC.py --input_file ${infile_prefix}_adb_in.txt --outgroup $outgroup --n $MCMC_n --MCMC_chains 32 --result_file final_chain.csv
#python -u ${admixturebayes_path}/runMCMC.py --input_file ${infile_prefix}_adb_in.txt --outgroup $outgroup --n 100000 --MCMC_chains 32 --result_file chain1.csv
#python -u ${admixturebayes_path}/runMCMC.py --input_file ${infile_prefix}_adb_in.txt --outgroup $outgroup --n 100000 --MCMC_chains 32 --continue_samples chain1.csv --result_file chain2.csv
#python -u ${admixturebayes_path}/runMCMC.py --input_file ${infile_prefix}_adb_in.txt --outgroup $outgroup --n 100000 --MCMC_chains 32 --continue_samples chain2.csv --result_file chain3.csv

# analyse all samples
python ${admixturebayes_path}/analyzeSamples.py --mcmc_results final_chain.csv --burn_in_fraction 0.35 --thinning_rate 40 --result_file thinned_samples.csv
python ${admixturebayes_path}/makePlots.py --plot top_minimal_topologies --posterior thinned_samples.csv --top_minimal_topologies_to_plot 3 --write_rankings ALL_rankings.txt
rename minimal_topology ALL_minimal_topology minimal_topology*pdf

# analyse subsamples - POLs
#python ${admixturebayes_path}/analyzeSamples.py  --mcmc_results final_chain.csv --burn_in_fraction 0.35 --thinning_rate 40 --subnodes Atiu WesternSamoa Tikopia --result_file thinned_POL_samples.csv
#python ${admixturebayes_path}/makePlots.py --plot top_minimal_topologies --posterior thinned_POL_samples.csv --top_minimal_topologies_to_plot 3 --write_rankings POL_rankings.txt
#rename minimal_topology POL_minimal_topology minimal_topology*pdf