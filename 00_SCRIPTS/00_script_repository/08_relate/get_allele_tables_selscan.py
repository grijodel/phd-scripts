#!/usr/bin/env python3
import tskit
import numpy as np
import pandas as pd
import copy
import argparse
def generate_ids(poplabels_file):
    chromosome_counter = 0
    individual_counter = 0 
    population_counter = -1
    individuals = dict()
    chromosomes = dict()
    populations = dict()
    with open(poplabels_file, 'r') as handle:
        next(handle)
        for line in handle:
            sample_name,population,group,sex = line.rstrip().split()
            # Update population dict and counter
            if population not in populations.values():
                population_counter += 1
                populations[population_counter] = population
            # Update 
            individuals[individual_counter] = sample_name
            chromosomes[chromosome_counter] = (sample_name,individual_counter,population_counter)
            chromosome_counter += 1
            chromosomes[chromosome_counter] = (sample_name,individual_counter,population_counter)
            chromosome_counter += 1
            individual_counter += 1
    return((chromosomes,individuals,populations))
def reset_schemas(tables):
    basic_schema = tskit.MetadataSchema({'codec': 'json'})
    tables.individuals.metadata_schema=basic_schema
    tables.populations.metadata_schema=basic_schema
def modify_tables(tables,
                 population_dict,
                 chromosome_dict):
    # Modify population tables
    for pop in population_dict.values():
        tables.populations.add_row(metadata=pop)
    # Modify individual tables
    for chromosome in chromosome_dict.keys():
        sample_name,individual_id,population_id=chromosome_dict[chromosome]
        new_row=tables.individuals[chromosome].replace(metadata=sample_name)
        tables.individuals[chromosome]=new_row
    # Modify leaf nodes in node table
    is_leaf = tables.nodes.flags == 1 
    leaf_node_ids = [i for i, x in enumerate(is_leaf) if x]
    for i in leaf_node_ids:
        sample_name, individual_id, population_id=chromosome_dict[i]
        new_row = tables.nodes[i].replace(individual=i, population=population_id)
        tables.nodes[i]=new_row
def ts_relate_modify(tree_sequence_file,
                        poplabels_file):
    # Load tree sequence
    ts = tskit.load(tree_sequence_file)
    # Load population info
    chromosomes,individuals,populations = generate_ids(poplabels_file)
    # Dump tree tables
    tables = ts.dump_tables()
    # Reset metadata schemas
    reset_schemas(tables)
    # Modify relevant tables
    modify_tables(tables,
              populations,
              chromosomes)
    # Turn modified tables into tree sequence
    tables.sort()
    modified_ts = tables.tree_sequence()
    return(modified_ts)
def samples_under_origin_node(position,locus_id,treesequence):
    origin_node=treesequence.site( locus_id ).mutations[0].node
    snp_tree = treesequence.at(position)
    samples = []
    for i in snp_tree.samples(origin_node):
        samples.append(i)
    return samples
def get_derived_alleles(snp_list,treesequence):
    # store sites in a dictionary
    site_dict = {}
    for site in treesequence.sites():
        site_dict[site.position] = site.id
    # store individuals in a dictionary
    individual_dict = {}
    for i in treesequence.individuals():
        individual_dict[i.id]=i.metadata
    # allele count dictionary template
    allele_count_dict = {}
    for i in individual_dict:
        allele_count_dict[individual_dict[i]] = 0
    # initialize locus dict
    locus_dict = {}
    # loop through every snp and get derived alleles
    for position in snp_list:
        samples = samples_under_origin_node( position, site_dict[position], treesequence )
        locus_row = copy.deepcopy(allele_count_dict)
        for sample_id in samples:
            locus_row[ individual_dict[sample_id] ] += 1
        locus_dict[position] = locus_row
    
    locus_table = pd.DataFrame.from_dict(locus_dict, orient="index")
    return locus_table
# argparse 
parser = argparse.ArgumentParser( description = 'Output a polarised genotype table given by a relate selection scan. Make sure files are for the same chromosome.')
parser.add_argument("treesequence", type=str, help="treesequence file")
parser.add_argument("poplabels", type=str, help="poplabels file")
parser.add_argument("selscan", type=str, help="selscan file from relate RTS")
parser.add_argument("significance_threshold", type=str, help="significance thold to filter sites")
parser.add_argument("output_file", type=str, help="output dataframe containing polarised genotypes")
args=parser.parse_args()
# Read and process files
ts = ts_relate_modify( args.treesequence, args.poplabels )
rts = pd.read_table(args.selscan,delimiter=" ")[["pos","rs_id","when_mutation_has_freq2"]]
rts["pval"] = 10 ** rts["when_mutation_has_freq2"]
significant = rts[rts["pval"]<=float(args.significance_threshold)]
snp_list = list(significant["pos"])
locus_table = get_derived_alleles( snp_list, ts )
# Write dataframe
locus_table.reset_index().to_csv(args.output_file, sep="\t", index = False)

