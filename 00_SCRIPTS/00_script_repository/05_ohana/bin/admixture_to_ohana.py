#!/usr/bin/env python3
import argparse
import numpy as np 
from sklearn.preprocessing import normalize

parser = argparse.ArgumentParser( description = 'This script converts the output matrices of ADMIXTURE (P,Q) into input matrices of nemeco and qpas in Ohana. This is necessary because the P matrix needs to be transposed, and the Q matrix rows need to be normalized to sum to 1.' )
parser.add_argument("P", type=str, help="ADMIXTURE P matrix (inferred ancestral allele frequencies)")
parser.add_argument('Q', type=str, help="ADMIXTURE Q matrix (inferred cluster membership)")
parser.add_argument('f_ohana', type=str, help="Ohana-compatible f matrix output file name")
parser.add_argument('q_ohana', type=str, help="Ohana-compatible q matrix output file name")


args = parser.parse_args()
P_file=args.P
Q_file=args.Q
f_ohana_file=args.f_ohana
q_ohana_file=args.q_ohana

with open( f_ohana_file , 'w' ) as handle:
    P=np.loadtxt(P_file).transpose()
    rows, columns = P.shape
    handle.write("{0} {1}\n".format(rows, columns))
    for row in P:
        newline=" ".join([str(i) for i in row]) + "\n"
        handle.write(newline)

with open( q_ohana_file, 'w' ) as handle:
    Q=np.loadtxt(Q_file)
    Q_norm = normalize(Q,axis=1,norm='l1')
    rows, columns = Q_norm.shape
    handle.write("{0} {1}\n".format(rows, columns))
    for row in Q_norm: 
        newline=" ".join([str(i) for i in row]) + "\n"
        handle.write(newline)





