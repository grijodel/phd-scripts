#!/usr/bin/env python3
import sys
input_bim = sys.argv[1]



def is_ambiguous(a1,a2):
    if a1 == "A" and a2 == "T":
        ambi=True
    elif a1 == "T" and a2 == "A":
        ambi=True
    elif a1=="G" and a2 == "C":
        ambi=True
    elif a1=="C" and a2 == "G":
        ambi=True
    else:
        ambi=False
    return(ambi)



with open(input_bim) as f:
    for line in f:
        chromosome,snpid,cm,position,a1,a2=line.rstrip().split()
        if is_ambiguous(a1,a2):
            print(snpid)


