#!/usr/bin/env python3
import tskit
import numpy as np
import pandas as pd
import copy
import argparse
from tqdm import tqdm
def count_mutations(treesequence):
    # Define the number of iterations for your task
    total_iterations = treesequence.num_trees
    # Create a tqdm progress bar
    progress_bar = tqdm(total=total_iterations, desc="Reading trees")
    mutations = []
    for tree in treesequence.trees():
        mutations.append( tree.num_mutations )
        # Update the progress bar
        progress_bar.update(1)
    # Close the progress bar
    progress_bar.close()
    return mutations
def snps_at_trees_with_low_n_mutations( treesequence, threshold ):
    # Define the number of iterations for your task
    total_iterations = treesequence.num_trees
    # Create a tqdm progress bar
    progress_bar = tqdm(total=total_iterations, desc="Filtering SNPs")
    snps = []
    for tree in treesequence.trees():
        if tree.num_mutations <= threshold:
            for site in tree.sites():
                snps.append( site.position)
        else:
            pass
        # Update the progress bar
        progress_bar.update(1)
    # Close the progress bar
    progress_bar.close()
    return snps
def filter_1(treesequence, percentile_thold ):
    mutations = count_mutations(ts)
    threshold = np.percentile(mutations, percentile_thold)
    snps_to_exclude = snps_at_trees_with_low_n_mutations( ts, threshold )
    return(snps_to_exclude)
#------------------------------------
# argparse 
parser = argparse.ArgumentParser( description = 'Output a SNPs whose trees contain less mutations that a given specified threshold based on the treesequence distribution')
parser.add_argument("treesequence", type=str, help="treesequence file")
parser.add_argument("output_file", type=str, help="output file containing SNPs that don't pass the filter")
parser.add_argument("threshold", type=int, help="percentile threshold (percentage)")
args=parser.parse_args()
#----------------
ts = tskit.load(args.treesequence)
snps_filter1 = filter_1(ts,args.threshold)
with open(args.output_file,'w') as handle:
    handle.write("position\n")
    for i in snps_filter1:
        newline = str(int(i)) + '\n'
        handle.write(newline)
