#!/usr/bin/env Rscript
library(tidyverse)
args = commandArgs(trailingOnly=TRUE)
# test if there is at least one argument: if not, return an error
if (length(args)!=2) {
  stop("Usage: <this script> sele_file_prefix output_file", call.=FALSE)
} 
 
input_file_prefix = args[1]
output_file = args[2]

chromosomes <- 1:22
selscan <- tibble(chromosome=numeric(),
                  position=numeric(),
                  p_val=numeric()
                  )

for (chromosome in chromosomes){
  input_file <-  paste0(input_file_prefix,"_chr", chromosome, ".sele")
  tmp <- read_delim(input_file) %>%
    mutate(chromosome=chromosome) %>%
    rename(position="pos") %>%
    mutate(p_val=10^when_mutation_has_freq2) %>%
    filter(p_val != 10) %>%
    select(chromosome, position, p_val)
    
  selscan <- full_join(selscan,tmp)
}  
  
write_delim(x=selscan, file=output_file, delim="\t")
