#!/bin/bash

module load java

VCF_dir=/pasteur/zeus/projets/p02/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/05_Phasing/03_Phase_Scaffold
MAP_dir=/pasteur/appa/homes/danliu/bin/plink_maps_h37


for chromosome in {1..22}; do
java -Xss5m  -Xmx100g -jar ~/bin/hap-ibd.jar \
	gt=$VCF_dir/shapeit4Phased_scaffold_chr${chromosome}.vcf.gz \
	map=$MAP_dir/plink.chr${chromosome}.GRCh37.map \
	min-mac=404 \
	out=hapIBD_chr${chromosome} \
	nthreads=$SLURM_CPUS_PER_TASK
done
