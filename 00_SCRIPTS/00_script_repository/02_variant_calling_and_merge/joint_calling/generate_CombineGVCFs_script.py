#!/usr/bin/env python3
import argparse

# ARGPARSE SETTINGS  -------------
parser = argparse.ArgumentParser( description = "Create a SLURM script for running CombineGVCFs in a per-chromosome manner. A list of directories leading to GVCFs is necesary.")


parser.add_argument("gvcf_dirs_list",type=str, help='File containing the paths to GVCFs directories, one per line.')

parser.add_argument("--slurm_script", type=str, help= "Slurm script output name (default: %(default)s)", default = 'CombineGVCFs.slurm' )
parser.add_argument("--outdir", type=str, help="Directory to write output GVCFs in (default: %(default)s)", default = 'CombineGVCFs')
parser.add_argument("--tmp_dir", type=str, help="Temporary directory for working (default: %(default)s)", default = '/pasteur/appa/scratch/grijodel/TMP' )
parser.add_argument("--reference_genome", type=str, help = "Reference genome (fasta) (default: %(default)s)", default = '/pasteur/zeus/projets/p02/IGSR/Automated_Pipeline_For_WGS/Resources/Data/1_Human_Reference_Genome/hs37d5/hs37d5.fa')
parser.add_argument('--gatk_path', type=str, help="Path to GATK binary (default: %(default)s)", default ="/pasteur/zeus/projets/p02/IGSR/Automated_Pipeline_For_WGS/Resources/Software/bin/gatk-4.1.2.0/gatk")
parser.add_argument('--dbsnp', type=str, help = "Path to dbSNP VCF (default: %(default)s)", default = "/pasteur/zeus/projets/p02/IGSR/Automated_Pipeline_For_WGS/Resources/Data/2_Databases_for_GATK/b37/dbsnp_138.b37.vcf")

args = parser.parse_args()

outdir = args.outdir
script_name = args.slurm_script
path_to_gvcf_dirs_file = args.gvcf_dirs_list

tmp_dir = args.tmp_dir
gatk_path = args.gatk_path
reference_genome = args.reference_genome
dbsnp = args.dbsnp

# WRITE SCRIPT --------------------

outfile = open(script_name,'w')

slurm_header = '''#!/bin/bash
#SBATCH -J Combine_GVCFs
#SBATCH -o logs/Combine_GVCFs_%A_%a.out -e  logs/Combine_GVCFs_%A_%a.err
#SBATCH --mem=100000
#SBATCH --array=0-23
#SBATCH -c 1
#SBATCH --qos=geh
#SBATCH --partition=geh

module purge
module load java/1.8.0
module load Python/2.7.17

chromosomes=(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 "X")
chromosome=${chromosomes[$SLURM_ARRAY_TASK_ID]}\n
'''

outfile.write(slurm_header)

gatk_head = """
{0} --java-options \"-Xmx100g \\
        -Djava.io.tmpdir={1} \\
        -XX:ParallelGCThreads=1\" \\
        CombineGVCFs \\
        -R {2} \\""".format(gatk_path,tmp_dir,reference_genome)

outfile.write(gatk_head + '\n')


with open(path_to_gvcf_dirs_file, 'r' ) as f:
    for line in f:
       
        path_to_vcf_dir = line.split()[0]
        sample_id = path_to_vcf_dir.split("/")[-1]
        vcf_path = path_to_vcf_dir + "/" + "Haplotypecaller_" + sample_id + '_${chromosome}.g.vcf.gz'
        flag = '\t--variant ' + vcf_path + ' \\' + '\n'

        outfile.write(flag)
       


gatk_tail = """        -L $chromosome \\
        --dbsnp {0} \\
        --output {1}/CombineGVCFs_$chromosome.g.vcf.gz
""".format(dbsnp,outdir)

outfile.write(gatk_tail)

outfile.close()

