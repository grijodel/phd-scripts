#!/usr/bin/env python3
import argparse
from pathlib import Path
import os
import pandas as pd
import glob


parser = argparse.ArgumentParser( description = 'Fix a recombination rates file that is actually a recombination map.')
parser.add_argument("input_map", type=str, help="Input rate file")
parser.add_argument("output_recomrates", type=str, help="Output recomrates file (chromopainter)")

args = parser.parse_args()
snp_list = []

with open( args.input_map , 'r' ) as handle:
    number_of_snps = int(handle.readline().rstrip())
    for line in handle:
        pos, cM = line.rstrip().split()
        snp_list.append( [pos,cM] )

with open( args.output_recomrates, 'w' ) as handle:
    handle.write(str(number_of_snps)+"\n")
    for i in range(0,number_of_snps):
        snp_i = snp_list[i]
        try:
            snp_i1 = snp_list[i+1]
        except IndexError:
            snp_i1 = snp_list[i] # if last snp in list
        rate = float(snp_i1[1]) - float(snp_i[1]) # take the difference 
        rate = rate / 100 # convert to morgan
        pos = snp_i[0]
        newline = str(pos) + " " + f'{rate:.20f}' + "\n"
        handle.write(newline)



