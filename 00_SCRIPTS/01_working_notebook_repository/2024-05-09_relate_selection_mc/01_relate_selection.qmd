---
title: "Prepping and reading relate selection results"
author: "Gaston Rijo De León"
format:
  html:
    embed-resources: true
    toc: true
    toc-depth: 2
date:  today
execute: 
  echo: true
  warning: false
  message: false
  eval: true
date-format: 'full'
engine: knitr
---

# Setup

```{r}
library(tidyverse)
library(janitor)
library(plotly)
library(MetBrewer)
library(glue)
'%!in%' <- function(x,y)!('%in%'(x,y))
theme_set(theme_bw())
pal <- readxl::read_excel("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/population_colors.xlsx", sheet = 5) 
ind_labels <- read_delim("/pasteur/helix/projects/MATAEA/00_DATA/00_metadata/labels.tsv",col_names =c("individual", "identifier", "cohort")) %>%
  select(individual,identifier)
ind_labels_2 <- read_delim("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/02_relate_pipeline/01_input_prep/Chr8_processed.poplabels") %>%
  rename(individual="sample",
         identifier="population") %>%
  select(individual,identifier) %>%
  filter(individual %!in% c("RG0108ILM","UV1003"))
ind_labels <- full_join(ind_labels, ind_labels_2) %>%
  distinct()
```



# Edit poplabels files to remove unwanted individuals


### All Polynesians




```{r}
selscan <- read_table("/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/02_MIXED_COVERAGE/06_selection_scans/04_rerun_scans_selected_samples/02_relate_topology/02_selscan/EXTRACT_Relate.pvals") %>%
  mutate(p_val_adj=p.adjust(p_val, method="fdr"))
```



```{r}
selscan %>%
  ggplot(aes(x=p_val)) +
  geom_histogram()
```











