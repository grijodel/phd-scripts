

merge_ohana_and_relate <- function(ohana_path,relate_path){
  ohana <- read_table(ohana_path) %>%
    clean_names() %>%
    select(chromosome,position,lle_ratio) %>%
    # Percent rank considering high values of ohana are the ones of interest
    mutate(rank=rank(-lle_ratio,ties.method="random"),
           percent_rank_ohana=rank/(max(rank))) %>%
    rename(ohana="lle_ratio") 
    
  
  relate <- read_table(relate_path) %>%
    # Percent rank considering low values of relate are the ones of interest
    mutate(rank=rank(p_val,ties.method="random"),
         percent_rank_relate=rank/(max(rank))) %>%
    rename(relate="p_val")
    
  
  s <- full_join(ohana,relate,
                 by=c("chromosome","position")) %>%
        drop_na() %>%
        select(chromosome,
               position,
               ohana,
               relate,
               percent_rank_ohana,
               percent_rank_relate) %>%
        mutate(fcs=-2*(log10(percent_rank_ohana)+log10(percent_rank_relate)))
  return(s)
}



#| eval: true
fcs <- function(s, outlier_percentile_threshold="99%"){
  # columns: chromosome position s1 s2 ... sN
  fcs <- s %>%
    ungroup() %>%
    group_by(chromosome,position) %>%
    summarise(fcs=-sum(log10(percent_rank+0.00001)))
  fcs_quantiles <- quantile(fcs$fcs, probs =seq(.01, .99, by = .01) )
  thold <- fcs_quantiles[outlier_percentile_threshold]  
  outlier_snps <- fcs %>%
    mutate(outlier=ifelse(fcs>thold, TRUE, FALSE)) 
  return(outlier_snps)
}




fcs_window <- function(fcs, chrom, window_size=1000){
  new_fcs <- fcs %>% filter(chromosome==chrom)
  p_holder <- 1:dim(new_fcs)[1]
  for (i in 1:dim(new_fcs)[1]){
    lower_bound = max((i-window_size)/2,0)
    upper_bound = min((i+window_size)/2,dim(new_fcs)[1])
    window = fcs$outlier[lower_bound:upper_bound]
    p = sum(window)/length(window)
    p_holder[i] <- p
  }
  new_fcs$p <- p_holder
  return(new_fcs)
}




fcs_window_cm <- function(fcs, chrom, window_size_cm=0.1){
  new_fcs <- fcs %>%
    filter(chromosome==chrom)
  max_coordinate <- dim(new_fcs)[1]
  p_holder <- rep(0,max_coordinate)
  n_snps_holder <- rep(0,max_coordinate)

  for (i in 1:max_coordinate){
          # Get recombination rate for the ith snp
          ith_cm <- new_fcs$cm[i]
          # Get all positions to the left
          left_vector <- new_fcs$outlier[1:i]
          left_window <- c()
          # Only store those where the difference in cM is below the threshold
          for (j in seq(length(left_vector),1,-1)){
            delta <- abs(ith_cm - new_fcs$cm[j])
            if ( delta < (window_size_cm/2) ){
              left_window <- c(left_window,new_fcs$outlier[j])
              # Keep for window
            }else{break}
          }
          # Get all positions to the right
          ## Check if end of table
          if (i != length(new_fcs$outlier)){
            right_vector <- new_fcs$outlier[i+1:length(new_fcs$outlier)]
            right_window <- c()
            # Only store those where the difference in cM is below the threshold
            for (j in seq(i+1,length(new_fcs$outlier),1)){
              delta <- abs(new_fcs$cm[j] - ith_cm)  
              if (delta <= window_size_cm / 2 ){
                right_window <- c(right_window,new_fcs$outlier[j])
                # Keep for window
              }else{break}
            }
            window <- c(left_window, right_window)
          }else{
            # End of table
            window <- left_window
        }
          n_snps <- length(window)
          p <- sum(window)/n_snps
          p_holder[i] <- p
          n_snps_holder[i] <- n_snps 
  }
  new_fcs$p <- p_holder
  new_fcs$n_snps <- n_snps_holder
  return(new_fcs)
}




fcs_window_sequential <- function(fcs,window_size=100){
    window_tibble = tibble(
      chromosome = numeric(),
      position = numeric(),
      fcs = numeric(),
      outlier = logical(),
      p = numeric()
    )
    for (i in 1:22){
      tmp <- fcs_window(fcs,i,window_size = window_size)
      suppressWarnings(window_tibble <- full_join(window_tibble,tmp))
    }
    return(window_tibble)
}




fcs_window_cm_sequential <- function(fcs,window_size=0.1){
    window_tibble = tibble(
      chromosome = numeric(),
      position = numeric(),
      ohana = numeric(),
      percent_rank_ohana = numeric(),
      relate = numeric(),
      percent_rank_relate = numeric(),
      cm=numeric(),
      fcs = numeric(),
      outlier = logical(),
      p = numeric(),
      n_snps = numeric()
    )
    for (i in 1:22){
      print(paste0("Processing chromosome ", i, "..."))
      tmp <- fcs_window_cm(fcs,i,window_size = window_size)
      window_tibble <- full_join(window_tibble,tmp)
    }
    return(window_tibble)
}

