# -*- coding: utf-8 -*-


import os
import sys


#**********************************************************************
#**************************** Define Paths ****************************
#**********************************************************************
Dir_Flagstat=sys.argv[1]


#**********************************************************************
#*********************** Create File of Results ***********************
#**********************************************************************
#--- Open file for results ----------------------
res=open(Dir_Flagstat+"/Flagstat_Summary.txt","w")
#--- Write header -------------------------------
res.write("Sample\tTotal\tPrimary_Alignment\tSecondary_Alignment\tTotal_Mapped\tPCT_Mapped\tTotal_Unmapped\tPCT_Unmapped\n")


#**********************************************************************
#********************* Retrieve Flagstat Results **********************
#******************** &
#**********************************************************************
#--- Flagstat Files -------------------------------
Flagstat_Files = [x for x in os.listdir(Dir_Flagstat) if x.endswith("_Flagstat_Final_Bam.txt")]
Flagstat_Files.sort()
#--- Retrieve Flagstat Results --------------------
for a in range(len(Flagstat_Files)):
	file=open(Dir_Flagstat+"/"+Flagstat_Files[a],"r")
	Sample=Flagstat_Files[a].split("_Flagstat_Final_Bam.txt")[0]
	line=file.readline()
	Total=line.split(" +")[0]
	line=file.readline()
	Secondary_Alignment=line.split(" +")[0]
	line=file.readline()
	line=file.readline()
	line=file.readline()
	Total_Alignment=line.split(" +")[0]
	file.close()
	Primary_Alignment=str(int(Total_Alignment)-int(Secondary_Alignment))
	PCT_Mapped=str(round((int(Total_Alignment))*100.0/int(Total),3))
	Total_Unmapped=str(int(Total)-int(Total_Alignment))
	PCT_Unmapped=str(round((int(Total)-int(Total_Alignment))*100.0/int(Total),3))
	res.write(Sample+"\t"+Total+"\t"+Primary_Alignment+"\t"+Secondary_Alignment+"\t"+Total_Alignment+"\t"+PCT_Mapped+"\t"+Total_Unmapped+"\t"+PCT_Unmapped+"\n")


#**********************************************************************
#************************ Close file of Results ***********************
#**********************************************************************
res.close()
