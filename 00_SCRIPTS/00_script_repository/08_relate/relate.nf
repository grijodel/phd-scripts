nextflow.enable.dsl=2                                                           
workflow {                                                                      
    vcf=Channel.fromFilePairs(params.prefix + '{.vcf.gz,.vcf.gz.tbi}', size:-1)  
    chromosomes=Channel.from(1..22)                                             
    // Preprocessing                                                            
    //// remove non biallelic snps, flip haplotypes, remove smaples, filter SNPs, adjust distances, generate annotations
    PREPROCESS(vcf.combine(chromosomes))                                                        
    // Relate algorithm
    MAKE_CHUNKS(PREPROCESS.out)
    PROCESS_CHUNKS( MAKE_CHUNKS.out )
    COMBINE_CHUNKS( PROCESS_CHUNKS.out.collect()
}

process PREPROCESS {


}

process MAKE_CHUNKS {

}

process PROCESS_CHUNKS {


}

process COMBINE_CHUNKS {

}

