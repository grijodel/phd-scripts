#!/usr/bin/env python3
import argparse
import os
import subprocess
import glob
from pathlib import Path
parser = argparse.ArgumentParser( description = 'Process MALDER results.')

parser.add_argument("malder_output", type=str, help="MALDER output file")
parser.add_argument("outfile", type=str, help="Processed output file")
args = parser.parse_args()

with open( args.outfile , 'w' ) as output_handle:
    with open(args.malder_output, 'r') as input_handle:

        header="jackknife_replicate name refpops amp0 time0 amp1 time1\n"
        output_handle.write(header)

        for line in input_handle:
            if "RESULT" in line and "refpops" not in line and not line.startswith("RESULT"): 
                output_handle.write( line )


