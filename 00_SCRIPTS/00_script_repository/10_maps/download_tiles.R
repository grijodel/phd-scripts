library(tidyverse)
library(osmdata)
library(patchwork)
library(sf)
library(ggmap)

x_1 <- -160
x_2 <- -122
y_1 <- -4  
y_2 <- -31 
fp_map <- get_stamenmap(bbox = c(left = x_1 ,bottom = y_2, 
                                  right = x_2,top = y_1), 
                         zoom = 11)
save(fp_map,file="")
