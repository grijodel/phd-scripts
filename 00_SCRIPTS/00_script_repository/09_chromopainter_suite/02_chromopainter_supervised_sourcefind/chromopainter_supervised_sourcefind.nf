nextflow.enable.dsl=2                                                           
workflow {
    // Set initial values
    /// Data paths
    //def chromosomes = [22] //test
    def chromosomes = (1..22)
    def data_paths = chromosomes.collect{ 
        params.input_directory+'/chr'+it+'/*.{haplotypes,recomrates}'
        }
    /// Create channels 
    data=Channel.fromFilePairs(data_paths, checkIfExists: true, size: 2)
                .map { [ it, it[0].split("_chr")[1].toInteger() ] }


    poplist=Channel.fromPath( params.poplist )
    labels=Channel.fromPath( params.labels )
    individuals=Channel.from( 1..params.number_of_individuals )
    //individuals=Channel.from( 1..3 )
    chromosomes=Channel.from(chromosomes)
 
    //////////// WORKFLOW START ////////////
    // Paint chromosomes
    chromopainter_input = data.combine(individuals)
                              .map { it.flatten() }
                              .combine( poplist )
                              .combine( labels )

    CHROMOPAINTER( chromopainter_input )
}

process CHROMOPAINTER {
    memory { 5.GB * task.attempt }
    errorStrategy 'retry'
    scratch true
    publishDir "${params.output_directory}/painting_by_groups/chr${chromosome}", mode: 'copy'
    module "chromopainter/v2"
    input:
        tuple val(prefix), file(haplotypes), file(recomrates), val(chromosome), val(ind_number), file(poplist), file(labels)
    output:
        file("*.out")

    script:
    """
    ChromoPainter \
        -g ${prefix}.haplotypes \
        -r ${prefix}.recomrates \
        -t ${labels} \
        -f ${poplist} ${ind_number} ${ind_number} \
        -i 0 \
        -n ${params.switch_rate}\
        -M ${params.global_mutation_rate} \
        -o ${params.output_prefix}_supervised_sourcefind_ind${ind_number}_chr${chromosome}
    """
    stub:
    """
    individual_id=\$(cat ${prefix}.labels| head -n ${ind_number} | tail | awk '{print \$1}' )
    output_prefix=\${individual_id}_${params.output_prefix}_estimation_chr${chromosome}
    touch \${output_prefix}.out
    """
}




