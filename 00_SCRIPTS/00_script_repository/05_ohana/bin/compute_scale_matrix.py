#!/usr/bin/env python3
import argparse
import sys
import numpy as np
parser = argparse.ArgumentParser( description = 'Compute modified covariance matrix for selection scan' )
parser.add_argument("cov_matrix", type=str, help="H0 covariance matrix")
parser.add_argument("ancestry_component", type=str, help="Number of ancestry component to scale")

args = parser.parse_args()
h=10
ancestry_component=int(args.ancestry_component)
# Read covariance matrix
matrix = np.loadtxt(args.cov_matrix, skiprows=1)
# Read header
infile=open(args.cov_matrix,"r")
header=infile.readline().rstrip()
infile.close()

# Precomute h matrix
rows = matrix.shape[0]
cols = matrix.shape[1]
size=rows*cols


# Modify matrix
if ancestry_component == 0:
    h_matrix = np.array([h]*size).reshape(rows,cols)
    # sum
    scaled_matrix=np.add(matrix,h_matrix)
else:
    h_matrix = np.array([0]*size).reshape(rows,cols)
    h_matrix[ancestry_component-1,ancestry_component-1]=h
    # sum
    scaled_matrix=np.add(matrix,h_matrix)

np.savetxt(sys.stdout, scaled_matrix, fmt='%.4f', header=header, comments='')
    
