#!/usr/bin/env bash
#SBATCH -J REFtree
#SBATCH -o /pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/02_relate_pipeline/logs/REFtree_%A_%a.out -e /pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/02_relate_pipeline/logs/REFtree_%A_%a.err
#SBATCH --mem=20G
#SBATCH -c 1
#SBATCH --array=0-21
#SBATCH --partition=geh,common

module load R/4.1.0


chromosomes=(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22)
chromosome=${chromosomes[$SLURM_ARRAY_TASK_ID]}

# v1.1.8
RELATE_PATH=/pasteur/helix/projects/IGSR/Software/relate_v1.1.8_x86_64_static
# pilot mask
POP_LABEL=/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/02_relate_pipeline/02_input_prep/Chr1_processed.poplabels
ANC=/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/02_relate_pipeline/04_coal_rate/1.1.8/Relate_chr${chromosome}.anc.gz
MUT=/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/02_relate_pipeline/04_coal_rate/1.1.8/Relate_chr${chromosome}.mut.gz
POPS=Atiu,Tahiti,Bora_Bora,Rangiroa,Rurutu,Raivavae,Hiva_Oa,Nuku_Hiva,Mangareva,Fiji,Tonga,Western_Samoa,Ontong_Java,Bellona,Rennell,Tikopia,Paiwan,Cebuano,Han,French,Papuan_Highlands,Karitiana,Mbuti
OUTPUT_PREFIX=/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/02_relate_pipeline/06-1_REFtree/1.1.8/REFtree_chr${chromosome}
cd /pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/02_relate_pipeline/06-1_REFtree/1.1.8/

# # v1.2.1
# RELATE_PATH=/pasteur/helix/projects/IGSR/Software/relate_v1.2.1_x86_64_static
# # pilot mask
# POP_LABEL=/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/02_relate_pipeline/02_input_prep/Chr1_processed.poplabels
# ANC=/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/02_relate_pipeline/04_coal_rate/Relate_chr${chromosome}.anc.gz
# MUT=/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/02_relate_pipeline/04_coal_rate/Relate_chr${chromosome}.mut.gz
# POPS=Atiu,Tahiti,Bora_Bora,Rangiroa,Rurutu,Raivavae,Hiva_Oa,Nuku_Hiva,Mangareva,Fiji,Tonga,Western_Samoa,Ontong_Java,Bellona,Rennell,Tikopia,Paiwan,Cebuano,Han,French,Papuan_Highlands,Karitiana,Mbuti
# OUTPUT_PREFIX=/pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/02_relate_pipeline/06-1_REFtree/REFtree_chr${chromosome}
# cd /pasteur/helix/projects/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/02_relate_pipeline/06-1_REFtree/

${RELATE_PATH}/bin/RelateExtract \
      --mode SubTreesForSubpopulation \
      --anc $ANC \
      --mut $MUT \
      --poplabels $POP_LABEL \
      --pop_of_interest $POPS \
      -o $OUTPUT_PREFIX