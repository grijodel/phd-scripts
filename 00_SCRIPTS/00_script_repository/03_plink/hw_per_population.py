#!/usr/bin/env python3
import argparse
import os
import glob
from pathlib import Path
import subprocess
import multiprocessing
from functools import partial
# argparse
parser = argparse.ArgumentParser( description = 'Perform within population Hard-Weinberg tests using plink. SNPs that are in HW disequilibrium below a p-value threshold are returned.')
parser.add_argument("prefix", type=str, help="Prefix for plink files")
parser.add_argument("pvalue", type=str, help="P-value threshold, values less or equal than this are considered out of HW equilibrium.")
parser.add_argument("outfile", type=str, help="output list of SNPs to exclude")
parser.add_argument("ncpus", type=str, help="Number of cpus to parallelize")
args = parser.parse_args()
#--
prefix=args.prefix
pvalue=float(args.pvalue)
outfile=args.outfile
ncpus=int(args.ncpus)
# Get lists of population ids and create a set
fam=prefix+'.fam'
populations=set()
print( "Reading .fam file..." )
with open(fam,'r') as f:
    for line in f:
        fid, iid, father,mother,sex,phenotype=line.rstrip().split()
        populations.add(fid)
# Create population ids files for filtering
print("Creating population files...")
for population in populations:    
    f=open(population+".txt", 'w')
    f.write(population)
    f.close()
# Create function for parallelizing
def hardy(population,thold):
    # create new bed with families

    family=population+".txt"
    command1 = [ "plink", "--bfile",
            prefix, "--keep-fam",
            family, "--make-bed",
            "--out", population ]
    process1 = subprocess.Popen(command1, shell=False)
    process1.wait()
    # hw 
    command2 = [ "plink", "--bfile",
            population, "--hardy",
            "--out", population ]
    process2 = subprocess.Popen(command2, shell=False)
    process2.wait()
    # write snps
    f = open(population+".disequilibrium", 'w')
    with open(population+".hwe") as hw:
        next(hw)
        for line in hw:
            chromosome,snp,test,a1,a2,geno,o,e,p=line.rstrip().split()
            p=float(p)
            if p <= thold:
                f.write(snp+"\n")
            else:
                pass
    f.close()
# run in parallel
print("Begin to run --hardy...")
snps_to_exclude=set()
pool=multiprocessing.Pool(ncpus)
r=pool.map_async( partial(hardy, thold=pvalue), populations)
r.wait()
# Get unique SNP ids for filtering (in HW diseq in at least one pop)
print("---> Finished")
print("Filtering SNPs out of HW with p <= {0}...".format(pvalue))
snps_to_exclude = set()
for population in populations:
    with open(population+".disequilibrium") as f:
        lines = f.read().splitlines()
    snps_to_exclude.update(lines)
all_snps = set()
bim = prefix + ".bim"
with open(bim, 'r') as f:
    for line in f:
        chromosome, snpid, cM, pos, a1, a2 = line.rstrip().split()
        all_snps.add(snpid)
        

# Write SNP ids to include
print("Writing output...")
with open(outfile, 'w') as f:
    for snp in snps_to_exclude:
        f.write(snp+"\n")
print("Cleaning temporary files...")
for population in populations:
    fileList = glob.glob(population+'.*')
    for filePath in fileList:
        try:
            os.remove(filePath)
        except:
            print("Error while deleting file : ", filePath)
print("Done!")
