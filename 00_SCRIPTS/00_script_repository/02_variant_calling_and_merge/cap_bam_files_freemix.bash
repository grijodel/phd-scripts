#!/bin/bash
#$ -cwd
#$ -j y
#$ -S /bin/bash
#SBATCH -e LOG_CAPMQ_array_%A_%a
#SBATCH --mem=10000
#SBATCH -p geh
#SBATCH -q geh
module load samtools
module load capmq

# Metadata file containing freemix values and bam names
METADATA=$1
BAM_PATH=$2
OUTPUT_DIRECTORY=$3
ITERATOR=$((${SLURM_ARRAY_TASK_ID}))
SAMPLE_LINE=$(cat $METADATA | grep -v Sample | head -n ${ITERATOR} | tail -n 1 )
SAMPLE_NAME=$(echo $SAMPLE_LINE | awk '{print $1}')
FREEMIX=$(echo $SAMPLE_LINE | awk '{print $2}')
BAM_FULLPATH=${BAM_PATH}/${SAMPLE_NAME}/${SAMPLE_NAME}_FINAL.bam

echo "##################################"
echo "Metadata file: $METADATA"
echo "bam path: $BAM_PATH"
echo "Output directory: $OUTPUT_DIRECTORY"
echo "Sample: $SAMPLE_NAME"
echo "Freemix value $FREEMIX"
echo "##################################"

mkdir -p ${OUTPUT_DIRECTORY}/${SAMPLE_NAME}

# Cap mapping quality
capmq -C ${FREEMIX} \
	-f \
	-m 20 \
	-v ${BAM_FULLPATH} \
	${OUTPUT_DIRECTORY}/${SAMPLE_NAME}/${SAMPLE_NAME}_capped.bam

samtools index ${OUTPUT_DIRECTORY}/${SAMPLE_NAME}/${SAMPLE_NAME}_capped.bam
