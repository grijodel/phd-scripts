#!/usr/bin/env python3
# coding: utf-8

# In[ ]:


import argparse
# Options for script
parser = argparse.ArgumentParser( description = "Process FLARE's output VCF with posterior probabilities, classify ancestries with a given threshold, and generate bed files that assigns local ancestry.")
parser.add_argument("input_vcf", type=str, help="VCF file (FLARE OUTPUT)")
parser.add_argument("output_directory", type=str, help="Output directory")
parser.add_argument("chromosome", type=str, help="Chromosome id")
parser.add_argument("confidence_threshold",type=float, help="Posterior probability threshold for assigning a given ancestry. In the range [0,1].")
args = parser.parse_args()

# In[19]:


from cyvcf2 import VCF
import numpy as np
from multiprocessing.pool import Pool
import pandas as pd
import os
import sys


#------
vcf=VCF(args.input_vcf)
output_directory=args.output_directory
chromosome=args.chromosome
confidence_threshold=args.confidence_threshold





# In[20]:


def field_to_dict(field_string):
    """
    Convert a string of the form <VALUE1=KEY1, ... , VALUEN=KEYN> to a dictionary.
    """
    d={}
    string_list=field_string.replace("<","").replace(">","").split(",")
    for i in string_list:
        value,key=i.split("=")
        d[int(key)]=value
    return(d)   


# In[21]:


def ancestry_header_to_dict(vcf):
    """
    Convert the ancestry line to a dictionary.
    """
    for i in vcf.raw_header.split("\n"):
        if i.startswith("##ANCESTRY"):
            field=i.split("##ANCESTRY=")[1]
            ancestry_dict=field_to_dict(field)
            break
    return(ancestry_dict)

def classify_probability_vector(prob_list, threshold=0.9):
    """
    Classify taking the index corresponding to the maximum above a certain threshold. If below, return uncertain, coded as 9.
    """
    if np.max(prob_list) >= threshold:
        return(np.argmax(prob_list))
    else:
        return(9)
# In[22]:

def ancestry_probs_to_array(vcf, threshold):
    """
    Convert the posterior probabilities in a VCF outputted by flare to two numpy two dimensional arrays (one per haplotype).
    Classification is done using a posterior probability threshold, and choosing the maximum over ancestries.
    (m x n)
    m is number of variants
    n is number of individuals
    """
    hap1=[]
    hap2=[]
    positions=[]
    for variant in vcf:
        samples_genotypes_1 = []
        samples_genotypes_2 = []
        for sample in variant.format("ANP1"):
            samples_genotypes_1.append(classify_probability_vector(sample, threshold))
        for sample in variant.format("ANP2"):
            samples_genotypes_2.append(classify_probability_vector(sample, threshold))
        positions.append(variant.POS)
        hap1.append(samples_genotypes_1)
        hap2.append(samples_genotypes_2)
    hap1=np.array(hap1)
    hap2=np.array(hap2)  
    ancestry_dict=ancestry_header_to_dict(vcf)
    samples=vcf.samples
    return( (hap1,hap2,positions,ancestry_dict,samples) )

def write_bed(individual_haplotype,
                           chromosome,
                           positions,
                           ancestry_dict,
                           filename):    
    """
    Write a bed file with the ancestry tracts for a given classified haplotype.
    """
    classifications=[]
    assert len(positions) == individual_haplotype.shape[0]
    classifications = [None] * len(positions)
    with open(filename, 'w' ) as handle:
        # write header mapping ancestry codes
        handle.write("#<")
        for i in ancestry_dict:
            code=i
            ancestry=ancestry_dict[i]
            handle.write(f"{code}={ancestry},")
        handle.write("9=UNCERTAIN")
        handle.write(">\n")
            
        for i in range(individual_haplotype.shape[0]):
            classifications[i]=str(individual_haplotype[i])
            ## Check continuity and write bed
            if i == 0:
                # first line
                handle.write(str(chromosome) + " " + str(positions[i]) + " ")
            elif i == individual_haplotype.shape[0]-1:
                # last line
                handle.write( str(positions[i]) + " " + classifications[i])
            else:
                if classifications[i] == classifications[i-1]:
                    pass
                else:
                    # finish previous line
                    handle.write( str(positions[i-1]) + " " + classifications[i-1] + "\n")
                    # Create new line
                    handle.write( str(chromosome) + " " + str(positions[i]) + " " )
        

def process_single_individual(classication_tuple,
                              chromosome,
                              individual_number,
                              output_directory):
    """
    Generate bed files for a single individual (one file per haplotype).
    """
    hap1,hap2,positions,ancestry_dict,samples=classification_tuple
    
    filename_prefix = samples[individual_number]
    write_bed(hap1[:,individual_number],
                           chromosome,
                           positions,
                           ancestry_dict,
                           output_directory + "/" + filename_prefix + "_hap1.bed")
    write_bed(hap2[:,individual_number],
                           chromosome,
                           positions,
                           ancestry_dict,
                           output_directory + "/" + filename_prefix + "_hap2.bed")

    
    


# protect the entry point
if __name__ == '__main__':
    # create and configure the process pool
    
    classification_tuple=ancestry_probs_to_array(vcf, confidence_threshold)
    positions=classification_tuple[2]
    ancestry_dict=classification_tuple[3]
    samples=classification_tuple[4]
    bed_directory=f"{output_directory}/${chromosome}_bed"
    try:
        os.makedirs(bed_directory)
    except OSError:
        print("Warning: bed dir already created")
        pass
    with Pool() as pool:
        # prepare arguments
        items = [ (classification_tuple,chromosome,individual,bed_directory) for individual in range(len(samples)) ]    # execute tasks and process results in order
        pool.starmap(process_single_individual, items)
    """
    # process pool is closed automatically
    ## save haplotype 1

    save_haplotypes(hap1,
                    samples,
                    ancestry_dict,
                    positions, 
                    chromosome,
                    output_directory + "/" + "all_haps_1")
    ## save haplotype 2
    save_haplotypes(hap2,
                    samples,
                    ancestry_dict,
                    positions, 
                    chromosome,
                    output_directory + "/" + "all_haps_2")

    """


