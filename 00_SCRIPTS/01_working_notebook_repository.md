
Author: `grijodel@pasteur.fr`
Date: `2023-03-15`

# Working notebooks repository


This directory contains working notebooks for specific analyses. These notebooks _have_ to be RMarkdown, Jupyter, or Quarto notebooks.


## Notebook conventions

All notebooks have to comply to the following:

- Notebooks _have_ to be RMarkdown, Jupyter, or Quarto notebooks
- Filenames have to take the form of `YYYY-MM-DD_descriptive_notebook_name_[0-9][0-9]`

For example:

`2023-03-15_ohana_selection_scan_01.Rmd`

`2023-03-15_ohana_selection_scan_02.ipynb`

`2023-03-15_relate_analysis_02.qmd`


## Directory conventions

__Naming convention for directories changes here:__ All subdirectories will have to comply to the following convention:

`YYYY-MM-DD_descriptive_notebook_name`

