#!/usr/bin/env Rscript
library(tidyverse)

args = commandArgs(trailingOnly=TRUE)
# test if there is at least one argument: if not, return an error
if (length(args)!=3) {
  stop("Arguments to be supplied: ancestral file, bim file, output file", call.=FALSE)
}

bim_file <- args[1]
ancestral_file <- args[2]
output_file <- args[3]

ancestral <- read_delim(ancestral_file,
                        col_names = c("CHR","POS","AA"))
bim <- read_delim(bim_file,
                  col_names = c("CHR","ID","CM","POS","REF","ALT") )

bim  %>%
  select(CHR,POS,REF,ALT) %>%
  left_join(ancestral) %>%
  drop_na() %>%
  select(CHR,POS,AA) %>%
  write_delim(output_file, col_names = FALSE,delim = "\t")