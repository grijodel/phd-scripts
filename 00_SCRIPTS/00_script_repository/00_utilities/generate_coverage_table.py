#!/usr/bin/env python3
# Usage : generate_coverage_table.py filelist.txt coverage.csv

import sys
import pandas as pd
filelist=sys.argv[1]
outfile=sys.argv[2]
counter=0
with open(filelist,'r') as h:
    for line in h:
        f=line.rstrip().split()[0]
        if counter==0:
            coverage=pd.read_csv(f,delimiter='\t')
            coverage=coverage[["SampleID","MeanCoverageOnAutosomes"]]
        else:
            coverage_2=pd.read_csv(f,delimiter='\t')
            coverage_2=coverage_2[["SampleID","MeanCoverageOnAutosomes"]]
            coverage=pd.concat([coverage,coverage_2])
        counter+=1



coverage.to_csv(outfile,sep='\t',index=False)
