#!/usr/bin/env python3
import glob
import pandas as pd
import argparse
import os
import sys
### SETTINGS
parser = argparse.ArgumentParser( description = 'Aggregate globetrotter first run outputs. This script assumes a very rigid directory structure and naming convention, so read how it works before using.')
parser.add_argument("input_directory", type=str, help="Input dir with one folder per population")
parser.add_argument("output_prefix", type=str, help="Output prefix for resulting tables")
args = parser.parse_args()
input_directory=args.input_directory
output_prefix =args.output_prefix

### CHANGE WD ###
old_wd = os.getcwd()
new_wd = input_directory
os.chdir(new_wd)

### SET COLUMN NAMES FOR OUTPUT DATAFRAMES ###
fit_evidence_onedate_colnames = ["target_group","conclusion","mode","gen_1_date","proportion_source_1","max_R2_fit_1_date","fit_quality_1_event","fit_quality_2_events","bestmatch_event_1_source_1","bestmatch_event_1_source_2","proportion_event_2_source_1","bestmatch_event_2_source_1","bestmatch_event_2_source_2"]
fit_evidence_twodate_colnames = ["target_group","conclusion","mode","gen_2_dates_date_1","gen_2_dates_date_2","maxScore_2_events","proportion_date_1_source_1","bestmatch_date_1_source_1","bestmatch_date_1_source_2","proportion_date_2_source_1","bestmatch_date_2_source_1","bestmatch_date_2_source_2"]
sources_colnames = ["target_group","conclusion","mode","component","component_proportion","PC","surrogate","subcomponent_proportion"]
### INITIALIZE LISTS FOR ROWS ###
fit_evidence_onedate = []
fit_evidence_twodate = []
sources_pcs = []
### LOOP OVER DIRECTORIES ###
for i in glob.glob("*/globetrotter_output.txt"):
    group_name = i.split("/")[-2]
    with open(i, 'r') as handle:
        for line in handle:
            if line.startswith("### INFERRED SOURCES"):
                conclusion = line.split(" ")[-1].replace(")","").rstrip()
            break
    if (conclusion != "one-date") & (conclusion != "multiple-dates"):
        sys.exit("Conclusion not found")
    
    with open(i, 'r') as handle:
        for line in handle:
            ######## ONE DATE INFORMATION #########
            if line.startswith("### 1-DATE FIT EVIDENCE"):
                mode="one-date"
                # GET FIT EVIDENCE (EZ)
                next(handle)
                row = [group_name,conclusion,mode]
                row.extend( next(handle).split() )
                fit_evidence_onedate.append(row)
            elif line.startswith("### 1-DATE FIT SOURCES, PC1"):
                mode="one-date"
                # GET FIT SOURCES INFORMATION
                columns_1 = next(handle).split()[1:]
                component_1=next(handle).split()
                columns_2 = next(handle).split()[1:]
                component_2=next(handle).split()
                component_1_proportion = component_1.pop(0)
                component_2_proportion = component_2.pop(0)
                # MAKE TABLE

                ## Loop component 1
                for name, value in zip(columns_1,component_1):
                    row = [group_name,conclusion,mode,"component_1",component_1_proportion,"PC1",name,value]
                    sources_pcs.append(row)
                ## Loop component 2
                for name, value in zip(columns_2,component_2):
                    row = [group_name,conclusion,mode,"component_2",component_2_proportion,"PC1",name,value]
                    sources_pcs.append(row)
            elif line.startswith("### 1-DATE FIT SOURCES, PC2"):
                mode="one-date"
                # GET FIT SOURCES INFORMATION
                columns_1 = next(handle).split()[1:]
                component_1=next(handle).split()
                columns_2 = next(handle).split()[1:]
                component_2=next(handle).split()
                component_1_proportion = component_1.pop(0)
                component_2_proportion = component_2.pop(0)
                # MAKE TABLE

                ## Loop component 1
                for name, value in zip(columns_1,component_1):
                    row = [group_name,conclusion,mode,"component_1",component_1_proportion,"PC2",name,value]
                    sources_pcs.append(row)
                ## Loop component 2
                for name, value in zip(columns_2,component_2):
                    row = [group_name,conclusion,mode,"component_2",component_2_proportion,"PC2",name,value]
                    sources_pcs.append(row)
            #### TWO DATES INFORMATION ####

            if line.startswith("### 2-DATE FIT EVIDENCE"):
                mode="two-dates"
                # GET FIT EVIDENCE (EZ)
                next(handle)
                row = [group_name, conclusion,mode]
                row.extend( next(handle).split() )
                fit_evidence_twodate.append(row)
            elif line.startswith("### 2-DATE FIT SOURCES, DATE1-PC1"):
                mode="two-dates"
                # GET FIT SOURCES INFORMATION
                columns_1 = next(handle).split()[1:]
                component_1=next(handle).split()
                columns_2 = next(handle).split()[1:]
                component_2=next(handle).split()
                component_1_proportion = component_1.pop(0)
                component_2_proportion = component_2.pop(0)
                # MAKE TABLE

                ## Loop component 1
                for name, value in zip(columns_1,component_1):
                    row = [group_name,conclusion,mode,"component_1",component_1_proportion,"DATE1_PC1",name,value]
                    sources_pcs.append(row)
                ## Loop component 2
                for name, value in zip(columns_2,component_2):
                    row = [group_name,conclusion,mode,"component_2",component_2_proportion,"DATE1_PC1",name,value]
                    sources_pcs.append(row)
            elif line.startswith("### 2-DATE FIT SOURCES, DATE2-PC1"):
                mode="two-dates"
                # GET FIT SOURCES INFORMATION
                columns_1 = next(handle).split()[1:]
                component_1=next(handle).split()
                columns_2 = next(handle).split()[1:]
                component_2=next(handle).split()
                component_1_proportion = component_1.pop(0)
                component_2_proportion = component_2.pop(0)
                # MAKE TABLE

                ## Loop component 1
                for name, value in zip(columns_1,component_1):
                    row = [group_name,conclusion,mode,"component_1",component_1_proportion,"DATE2_PC1",name,value]
                    sources_pcs.append(row)
                ## Loop component 2
                for name, value in zip(columns_2,component_2):
                    row = [group_name,conclusion,mode,"component_2",component_2_proportion,"DATE2_PC1",name,value]
                    sources_pcs.append(row)
                    
fit_onedate = pd.DataFrame(fit_evidence_onedate,columns=fit_evidence_onedate_colnames)
fit_twodate = pd.DataFrame(fit_evidence_twodate,columns=fit_evidence_twodate_colnames)
sources = pd.DataFrame(sources_pcs,columns=sources_colnames)

fit_onedate.to_csv(f"{output_prefix}_fit_onedate.csv", index=False)
fit_twodate.to_csv(f"{output_prefix}_fit_twodate.csv", index=False)
sources.to_csv(f"{output_prefix}_sources.csv", index=False)


os.chdir(old_wd)
