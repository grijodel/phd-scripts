#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
if (length(args)!=3) {
  stop("INPUT: NE_ESTIMATES_TABLE SNP_NUMBERS_TABLE OUTPUT_FILENAME", call.=FALSE)
} 
ne_estimates_table <- args[1]
snp_numbers_table <- args[2]
outfile <- args[3]
library(dplyr)
library(readr)
library(tidyr)
library(ggplot2)
 
# Columns: generation ne block
ne<-read_table(ne_estimates_table, col_types=cols(
					  generation = col_double(),
					  Ne = col_double(),
					  block = col_character() ) )
# Columns: block number_of_snps
snp_numbers<-read_table(snp_numbers_table)

n <- snp_numbers %>%
	filter(block=="FULL_DATASET") %>%
	pull(number_of_snps)
snp_blocks <- snp_numbers %>%
        filter(block!="FULL_DATASET")
g <- snp_blocks %>%
	summarise(g=n()) %>%
	pull(g)

snp_blocks$block <- as.integer(snp_blocks$block)
snp_blocks <- snp_blocks %>%
	arrange(block)


# Following nick patterson notation on weighted jackknife
######### MATRIX VERSION OF COMPUTATIONS ###########
generation_v <- ne %>%
			select(Ne,block,generation) %>%
			pivot_wider(values_from=Ne,names_from=block) %>%
			drop_na() %>%
			pull(generation) %>%
			matrix(ncol=1)

## Vector of full dataset estimation
theta_v <- ne %>%      
                select(Ne,block,generation) %>%
                pivot_wider(values_from=Ne,names_from=block) %>%
                drop_na() %>%
                select(FULL_DATASET) %>%
		pull(FULL_DATASET) %>%
	        matrix(ncol=1)

## Matrix of leave m out jackknife
ne_blocks <-  ne %>% 
		filter(block!="FULL_DATASET")
ne_blocks$block <- as.integer(ne_blocks$block)
ne_blocks <- ne_blocks %>%
		arrange(block,generation)

theta_matrix <- ne_blocks %>% 	
		pivot_wider(values_from=Ne,names_from=block) %>% 
		drop_na() %>%
		select(-generation) %>% 
		as.matrix()

## Vector of weights
snps_v <- snp_blocks %>% 
	select(number_of_snps) %>% 
	pull(number_of_snps) %>%
	matrix(ncol=1)
weights_v <- ( snps_v ) / n 


## Compute jackknife estimate of mean
theta_J <- g*theta_v - theta_matrix%*%weights_v
## Compute jackknife estimate of variance
h <- n*1/(n-snps_v)
tau<-theta_v%*%t(h) - t( apply(theta_matrix,1,`*`,t(h-1)) )

A <- theta_J %x% matrix(rep(1,times = length(h)),nrow= 1)
B <- tau-A
C <- B*B
D <- apply(C,1,`/`,(h-1)) 
var_sq <- rowSums( t(D)  ) * 1/g 


ne_est <- tibble(theta_J=as.numeric(theta_J),
               var_sq=as.numeric(var_sq),
               generation=as.numeric(generation_v),
               theta=as.numeric(theta_v))
        
ggplot(ne_est,aes(x=generation)) +
  geom_line(aes(y=theta_J),color='black') +
  geom_line(aes(y=theta), color='red') +
  ylab("Ne") 
  

ne  %>%
  #filter(generation<50) %>%
ggplot(aes(x=generation)) +
  # geom_line(aes(y=theta_J),color='black') +
  theme(legend.position='none') +
  geom_line(aes(y=Ne,color=block)) +
  geom_line(data=ne_est,aes(y=log10(theta)), color='black') +
  xlim(0,200) +
  ylab("Ne") 
  


quantile_ne <- ne %>%
            group_by(generation) %>%
            summarise(mean_ne=mean(Ne),
                      quantile=scales::percent(seq(0,1,0.025)),
                      quant=quantile(Ne,seq(0,1,0.025)))

quantile_ne %>%
  filter(quantile=="2.5%"|quantile=="97.5%") %>%
  pivot_wider(names_from = "quantile", values_from = "quant") %>%
  clean_names() %>%
  ggplot(aes(x=generation,y=mean_ne)) +
    geom_line() +
    geom_ribbon(aes(ymin = x2_5_percent, ymax = x97_5_percent), alpha = 0.1)














