#!/usr/bin/env python3
import argparse
import sys
parser = argparse.ArgumentParser( description = 'Compute expected disk space used for a given number of samples.')
parser.add_argument("samples", type=int, help="Number of samples.")
parser.add_argument("sample_type", type=str, help="Sample coverage type, either M (Medium) or H (High).")

args = parser.parse_args()

def compute_disk_space(sample_number, sample_type):
    if sample_type=="M":
        sizes=[12.25,92.93,67.21,22.8]
        scaled_sizes=list(map(lambda x: x * sample_number, sizes))
        fastq,max_during_alignment,max_after_alignment,max_after_vc=scaled_sizes
    elif sample_type=="H":
        sizes=[95,251.45,182.27,40.56]
        scaled_sizes=list(map(lambda x: x * sample_number, sizes))
        fastq,max_during_alignment,max_after_alignment,max_after_vc=scaled_sizes
    else:
        sys.exit(1)
    max_temporary=fastq+max_during_alignment+max_after_vc
    max_fixed=fastq+max_after_alignment+max_after_vc
    print("FASTQ {0}GB".format(round(fastq)))
    print("MAX_DURING_ALIGNMENT {0}GB".format(round(max_during_alignment)))
    print("MAX_AFTER_ALIGNMENT {0}GB".format(round(max_after_alignment)))
    print("MAX_AFTER_VC {0}GB".format(round(max_after_vc)))
    print("MAX_TEMPORARY {0}GB".format(round(max_temporary)))
    print("MAX_FIXED {0}GB".format(round(max_fixed)))
    


compute_disk_space(args.samples,args.sample_type)
