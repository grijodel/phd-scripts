#!/usr/bin/env bash
#SBATCH -J gnomix
#SBATCH -o /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/05_local_ancestry/logs/relate_%A_%a.out -e /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/05_local_ancestry/logs/relate_%A_%a.err
#SBATCH --mem=50G
#SBATCH --array=0-21
#SBATCH -c 20
#SBATCH --partition=gehbigmem

chromosomes=(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22)
chromosome=${chromosomes[$SLURM_ARRAY_TASK_ID]}

PHASED_VCF=/pasteur/zeus/projets/p02/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/05_Phasing/03_Phase_Scaffold/shapeit4Phased_scaffold_chr${chromosome}.vcf.gz
WORKING_DIR=/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/05_local_ancestry/gnomix
GNOMIX_PATH=/pasteur/zeus/projets/p02/IGSR/Software/gnomix

conda activate gnomix

bcftools view -S ${WORKING_DIR}/data/ref_ind.txt -O z -o ${WORKING_DIR}/data/ref_chr${chromosome}.vcf.gz $PHASED_VCF
bcftools view -S ${WORKING_DIR}/data/query_ind.txt -O z -o ${WORKING_DIR}/data/query_chr${chromosome}.vcf.gz $PHASED_VCF

python3 ${GNOMIX_PATH}/gnomix.py ${WORKING_DIR}/data/query_chr${chromosome}.vcf.gz ${WORKING_DIR}/output/chr${chromosome} ${chromosome} False ${WORKING_DIR}/data/allchrs.b37.gmap ${WORKING_DIR}/data/ref_chr${chromosome}.vcf.gz ${WORKING_DIR}/data/ref_ind.smap ${WORKING_DIR}/config_custom.yaml

cd ${WORKING_DIR}/output/chr${chromosome}/query_results_bed
for f in *; do mv -i "$f" "${f//[\'[:space:]]}"; done
for i in *bed ; do name=$(basename -s .bed $i); awk '{OFS="\t";print $0, "'$name'"} ' $i > ${name}_tag.txt; done
cat *txt | grep -v epos | grep PNG | awk '{print $7"\t"$1"\t"$2"\t"$3"\tPNG"}' | sed 's/_0/A/g' | sed 's/_1/B/g' > gnomix_chr${chr}_PNG.bed;
cat *txt | grep -v epos | grep FRE | awk '{print $7"\t"$1"\t"$2"\t"$3"\tFRE"}' | sed 's/_0/A/g' | sed 's/_1/B/g' > gnomix_chr${chr}_FRE.bed;
cat *txt | grep -v epos | grep HAN | awk '{print $7"\t"$1"\t"$2"\t"$3"\tHAN"}' | sed 's/_0/A/g' | sed 's/_1/B/g' > gnomix_chr${chr}_HAN.bed;
cat *txt | grep -v epos | grep POL | awk '{print $7"\t"$1"\t"$2"\t"$3"\tPOL"}' | sed 's/_0/A/g' | sed 's/_1/B/g' > gnomix_chr${chr}_POL.bed;