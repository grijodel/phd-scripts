#!/bin/bash

module load java

VCF_dir=/pasteur/zeus/projets/p02/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/05_Phasing/03_Phase_Scaffold
MAP_dir=/pasteur/appa/homes/danliu/bin/plink_maps_h37

gunzip *.ibd.gz
gunzip *.hbd.gz

cat hapIBD_chr{1..22}.ibd hapIBD_chr{1..22}.hbd > all.lPSC


for chromosome in {1..22}; do

cat hapIBD_chr${chromosome}.ibd hapIBD_chr${chromosome}.hbd | java -jar ~/bin/merge-ibd-segments.jar $VCF_dir/Chr${chromosome}.phased.vcf $MAP_dir/plink.chr${chromosome}.GRCh37.map 0.6 1 > Chr${chromosome}.Merged.lPSC
done

cat Chr{1..22}.Merged.lPSC > all.Merged.lPSC
