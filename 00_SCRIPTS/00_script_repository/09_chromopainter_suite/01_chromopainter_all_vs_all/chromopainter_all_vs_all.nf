nextflow.enable.dsl=2                                                           
workflow {
    // Set initial values
    /// Data paths
    def chromosomes = (1..22)
    def data_paths = chromosomes.collect{ 
        params.input_directory+'/chr'+it+'/*.{haplotypes,recomrates,labels}'
        }
    /// Create channels 
    data=Channel.fromFilePairs(data_paths, checkIfExists: true, size: 3)
                .map { [ it, it[0].split("_chr")[1].toInteger() ] }
    individuals=Channel.from( 1..params.number_of_individuals ) 
    chromosomes=Channel.from(chromosomes) 
    //////////// WORKFLOW START ////////////
    // Paint chromosomes
    chromopainter_input = data.combine(individuals)
                              .map { it.flatten() }
    PAINT_ALL_VS_ALL( chromopainter_input )
}

process PAINT_ALL_VS_ALL {
    memory { 10.GB * task.attempt }
    errorStrategy 'retry'
    scratch false
    publishDir "${params.output_directory}/paint_all_vs_all/chr${chromosome}", mode: 'copy'
    module "chromopainter/v2"
    input:
        tuple val(prefix), file(haplotypes), file(labels), file(recomrates), val(chromosome), val(ind_number)
    output:
        file("*.out")

    script:
    """
    individual_id=\$(cat ${prefix}.labels| head -n ${ind_number} | tail -n 1 | awk '{print \$1}' )
    ChromoPainter \
        -g ${prefix}.haplotypes \
        -r ${prefix}.recomrates \
        -t ${prefix}.labels \
        -a ${ind_number} ${ind_number} \
        -i 0 \
        -n ${params.switch_rate} \
        -M ${params.global_mutation_rate} \
        -o \${individual_id}_${params.output_prefix}_estimation_chr${chromosome}
    """
    stub:
    """
    individual_id=\$(cat ${prefix}.labels| head -n ${ind_number} | tail | awk '{print \$1}' )
    output_prefix=\${individual_id}_${params.output_prefix}_all_vs_all_chr${chromosome}
    touch \${output_prefix}.out
    """
}




