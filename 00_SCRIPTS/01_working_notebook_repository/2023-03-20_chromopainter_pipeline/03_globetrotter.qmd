---
title: "GlobeTrotter workflow"
author: "Gaston Rijo De León"
format:
  html:
    embed-resources: true
    toc: true
    toc-depth: 2
execute: 
  eval: true
  warning: false
  message: false
  echo: true
date:  today
date-format: 'full'
engine: knitr
---

```{r}
library(tidyverse)
library(janitor)
library(plotly)
library(MetBrewer)
library(qqplotr)
library(here)
library(glue)
i_am("00_SCRIPTS/01_working_notebook_repository/2023-03-20_chromopainter_pipeline/02_sourcefind.qmd")
theme_set(theme_bw())
pal <- readxl::read_excel("/pasteur/zeus/projets/p02/MATAEA/gaston/mataea/metadata/population_colors_all_sheets.xlsx",
                          sheet = 3) 
ind_labels <- read_delim("/pasteur/zeus/projets/p02/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/04_Quality_Control/labels.tsv",
                         col_names = c("individual", "identifier", "cohort"))
'%!in%' <- function(x,y)!('%in%'(x,y))
```


# Introduction


# Run globetrotter

We will run globetrotter using the "supervised" chromosome painting, where we paint all the target populations (Polynesians & Fiji) with all the surrogate populations (reference populations).






## Generate universal files

We need to generate the parameter file, and for this we will need to pass it the copymatrix and a population labels file.

The original population labels file has the fine grained population labels, but we should use a coarser grain to have more samples per population. We will use the archipelago level.


### Labels

```{r}

labels_file <- here("01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/03_supervised_sourcefind/01_polynesians_supervised_maf5/polynesians_supervised.labels")
input_file_ids <- here("01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/polynesians_supervised_archipelagos.labels")

target_regions <- c("Fiji",
                    "Western_Polynesian",
                    "Eastern_Polynesian",
                    "Polynesian_Outlier")

regrouped_labels <- read_delim(labels_file,
                col_names = c("individual","identifier","include")) %>%
                select(individual,include) %>%
                left_join(ind_labels) %>% 
                left_join(pal, by="identifier") %>%
                select(individual,identifier,group0,group3,include) %>%
                mutate( pop_label = case_when(group0 %in% target_regions ~ group3,
                                              group3 %!in% target_regions ~ identifier) ) %>%
                select(individual, pop_label, include)


regrouped_labels %>%
  write_delim(file = input_file_ids,col_names = FALSE,delim = " ")
```


### Donor populations



```{r}
#| eval: false
copymatrix <- here("01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/03_supervised_sourcefind/01_polynesians_supervised_maf5/output.chunklengths.out")
donor_populations <- read_delim(copymatrix) %>%
  select(-Recipient) %>%
  colnames()
donor_populations <- paste(donor_populations,
                           collapse =  " ")
```


## Test run (Marquesas)



Parameter file

```{r}
#| eval: false
parfile <- here("01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/test.params")
save_file <- here("01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/test")
save_file_boot <- here("01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/test")

# Write "homogeneous" populations
surrogate_popnames <- c("French","Orcadian", # European
                     "Atayal","Paiwan", # Taiwanese
                     "Han","Dai", # East Asian
                     "Agta", # Island SEA
                     "Papuan_Highlands","Papuan_Sepik","Mendi","Marawaka","Bundi","Kundiawa","Tari", # Papuan mainland
                     "Mamusi", # Bismarck
                     "Bougainville", # Solomon I.
                     "Pentecost", # ni-Vanuatu
                     "Karitiana","Surui","Chopccas", # South American
                     "Pima" # North American
                     )
surrogate_popnames <- paste(surrogate_popnames,collapse = " ")

target_popname <- "Marquesas_Islander"

# Write parameter file
params = "prop.ind: 1
bootstrap.date.ind: 0
null.ind: 1
input.file.ids: {input_file_ids}
input.file.copyvectors: {copymatrix}
save.file.main: {save_file}
save.file.bootstraps: {save_file_boot}
copyvector.popnames: {donor_populations}
surrogate.popnames: {surrogate_popnames}
target.popname: {target_popname}
num.mixing.iterations: 5
props.cutoff: 0.001
bootstrap.num: 100
num.admixdates.bootstrap: 1
num.surrogatepops.perplot: 3
curve.range: 1 30
bin.width: 0.1
xlim.plot: 0 30
prop.continue.ind: 0
haploid.ind: 0"

parameters_full <- glue(params)

write(parameters_full,file = parfile)
```

Additional files

```{r}
#| eval: false
painting_files_list <- here("01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/03_supervised_sourcefind/01_polynesians_supervised_maf5/local_painting_files/sample_files_list.txt")
recom_files_list <- here("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/01_phase_format/02_full_dataset/01_all_populations/recombination_files_list.txt")
```



```{r}
#| eval: false
gt <- "fastGlobeTrotter {parfile} {painting_files_list} {recom_files_list} 1 --no-save"
(glue(gt))
```



This works. Now I will run things with each of the focal populations.

## Function to facilitate running GLOBETROTTER

1. Create parameter file
  a. Parameter file for run with no bootstrap
  b. Parameter file for run with bootstrap
2. Create slurm scripts for this
3. Run slurm scripts using sbatch


```{r}
#| eval: false
run_globetrotter_slurm <- function(output_dir,target_popname,bootstrap="0", admixdates = "1"){
  # Create parameter_files
  parfile <- paste0(output_dir, "/","globetrotter.params")
  save_file <-paste0(output_dir, "/","globetrotter_output")
  # Write "homogeneous" populations
  surrogate_popnames <- c("French","Orcadian", # European
                       "Atayal","Paiwan", # Taiwanese
                       "Han","Dai", # East Asian
                       "Agta", # Island SEA
                       "Papuan_Highlands","Papuan_Sepik","Mendi","Marawaka","Bundi","Kundiawa","Tari", # Papuan mainland
                       "Mamusi", # Bismarck
                       "Bougainville", # Solomon I.
                       "Pentecost", # ni-Vanuatu
                       "Karitiana","Surui","Chopccas", # South American
                       "Pima" # North American
                       )
  surrogate_popnames <- paste(surrogate_popnames,collapse = " ")
  params = "prop.ind: 1
  bootstrap.date.ind: {bootstrap}
  null.ind: 1
  input.file.ids: {input_file_ids}
  input.file.copyvectors: {copymatrix}
  save.file.main: {save_file}
  save.file.bootstraps: {save_file}_bootstrap
  copyvector.popnames: {donor_populations}
  surrogate.popnames: {surrogate_popnames}
  target.popname: {target_popname}
  num.mixing.iterations: 5
  props.cutoff: 0.001
  bootstrap.num: 100
  num.admixdates.bootstrap: {admixdates}
  num.surrogatepops.perplot: 3
  curve.range: 1 30
  bin.width: 0.1
  xlim.plot: 0 30
  prop.continue.ind: 0
  haploid.ind: 0"
  parameters_full<- glue(params)
  # Write slurm scripts
  painting_files_list <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/02_chromopainter/03_supervised_sourcefind/01_polynesians_supervised_maf5/local_painting_files/sample_files_list.txt"
  recom_files_list <-  "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/01_phase_format/02_full_dataset/01_all_populations/recombination_files_list.txt"
  ## --
  fileConn<-file(parfile)
  writeLines(parameters_full, fileConn)
  close(fileConn)
  gt <- "fastGlobeTrotter {parfile} {painting_files_list} {recom_files_list} 1 --no-save"
  gt <- (glue(gt))
  ## slurm
  slurm_command <- "#!/bin/bash
#SBATCH -J globetrotter
#SBATCH -o {output_dir}/gt.out -e  {output_dir}/gt.err
#SBATCH --mem=30000
#SBATCH --qos=geh
#SBATCH --partition=geh
module load  R/4.1.0
module load  fastGlobeTrotter/a1dc395
{gt}"
  slurm_script <- glue(slurm_command)
  ## Write to file
  slurm_script_path <- paste0(output_dir, "/","globetrotter.slurm")
  fileConn<-file(slurm_script_path)
  writeLines(slurm_script, fileConn)
  close(fileConn)
  ## Run using slurm
  run_slurm <- "sbatch {slurm_script_path}"
  run_slurm <- glue(run_slurm)
  system(run_slurm)
}
```


```{r}
# Function to read globetrotter bootstrap files
tidy_globetrotter <- function(bootstrap_file){
  est <- read_delim(bootstrap_file) %>%
    clean_names() %>%
    pivot_longer(names_to = "n_dates",values_to = "estimate",cols = starts_with("date")) %>%
    mutate(n_dates = str_replace(n_dates,pattern="_est_boot",replacement="")) %>%
    mutate(n_dates = str_replace(n_dates,pattern="date",replacement="")) %>%
    select(bootstrap_num,n_dates,estimate)
  scores <- read_delim(bootstrap_file) %>%
              clean_names() %>%
              pivot_longer(names_to = "n_dates",values_to = "r2",cols = starts_with("max")) %>%
              mutate(n_dates = str_replace(n_dates,pattern="max_",replacement="")) %>%
              mutate(n_dates = str_replace(n_dates,pattern="_boot",replacement="")) %>%
              separate(col = n_dates,sep = "_",into = c("accuracy_metric","n_dates")) %>%
              mutate(n_dates= str_replace(n_dates,pattern="events","")) %>%
              mutate(n_dates= str_replace(n_dates,pattern="date","")) %>%
              select(bootstrap_num,n_dates,accuracy_metric,r2) %>%
    distinct()
  tib <- full_join(est,scores, by=c("n_dates","bootstrap_num"))
  return(tib)
}
```





## Eastern Polynesia

**Marquesas**

```{r}
#| eval: false
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/01_Marquesas"
target_popname <- "Marquesas_Islander"
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="0",
                       admixdates = "1")
```

```{r}
#####
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/01_Marquesas/bootstrap_run/"
target_popname <- "Marquesas_Islander"
```

```{r}
#| eval: false
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="1",
                       admixdates = "2")
```

```{r}
marquesas <- tidy_globetrotter(paste0(output_dir,"/globetrotter_output_bootstrap.txt")) %>%
  mutate(group=target_popname)
```


**Australes**

```{r}
#| eval: false
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/02_Australes"
target_popname <- "Austral_Islander"
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="0",
                       admixdates = "1")
```

```{r}
#####
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/02_Australes/bootstrap_run/"
target_popname <- "Austral_Islander"
```

```{r}
#| eval: false
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="1",
                       admixdates = "2")
```


```{r}
australes <- tidy_globetrotter(paste0(output_dir,"/globetrotter_output_bootstrap.txt")) %>%
  mutate(group=target_popname)
```




**Gambier**

```{r}
#| eval: false
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/03_Gambier"
target_popname <- "Gambier_Islander"
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="0",
                       admixdates = "1")
```


```{r}
#####
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/03_Gambier/bootstrap_run/"
target_popname <- "Gambier_Islander"
```

```{r}
#| eval: false
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="1",
                       admixdates = "2")
```

```{r}
gambier <- tidy_globetrotter(paste0(output_dir,"/globetrotter_output_bootstrap.txt")) %>%
  mutate(group=target_popname)
```




**Tuamotu**

```{r}
#| eval: false
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/04_Tuamotu"
target_popname <- "Tuamotu_Islander"
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="0",
                       admixdates = "1")
```

```{r}
#####
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/04_Tuamotu/bootstrap_run/"
target_popname <- "Tuamotu_Islander"
```

```{r}
#| eval: false
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="1",
                       admixdates = "2")
```

```{r}
tuamotu <- tidy_globetrotter(paste0(output_dir,"/globetrotter_output_bootstrap.txt")) %>%
  mutate(group=target_popname)
```


**Society** 

```{r}
#| eval: false
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/05_Society"
target_popname <- "Society_Islander"
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="0",
                       admixdates = "1")
```


```{r}
#####
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/05_Society/bootstrap_run/"
target_popname <- "Society_Islander"
```

```{r}
#| eval: false
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="1",
                       admixdates = "2")
```

```{r}
society <- tidy_globetrotter(paste0(output_dir,"/globetrotter_output_bootstrap.txt")) %>%
  mutate(group=target_popname)
```


**Atiu**

```{r}
#| eval: false
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/06_Atiu"
target_popname <- "Cook_Islander"
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="0",
                       admixdates = "1")
```

```{r}
#####
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/06_Atiu/bootstrap_run/"
target_popname <- "Cook_Islander"
```

```{r}
#| eval: false
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="1",
                       admixdates = "1")
```

```{r}
atiu <- tidy_globetrotter(paste0(output_dir,"/globetrotter_output_bootstrap.txt")) %>%
  mutate(group=target_popname)
```


## Western Polynesia

**Tonga**

```{r}
#| eval: false
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/07_Tonga"
target_popname <- "Tonga_Islander"
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="0",
                       admixdates = "1")
```


```{r}
#####
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/07_Tonga/bootstrap_run/"
target_popname <- "Tonga_Islander"
```

```{r}
#| eval: false
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="1",
                       admixdates = "1")
```

```{r}
tonga <- tidy_globetrotter(paste0(output_dir,"/globetrotter_output_bootstrap.txt")) %>%
  mutate(group=target_popname)
```



**Western Samoa**

```{r}
#| eval: false
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/08_Samoa"
target_popname <- "Samoa_Islander"
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="0",
                       admixdates = "1")
```

```{r}
#####
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/08_Samoa/bootstrap_run/"
target_popname <- "Samoa_Islander"
```

```{r}
#| eval: false
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="1",
                       admixdates = "1")
```


```{r}
western_samoa <- tidy_globetrotter(paste0(output_dir,"/globetrotter_output_bootstrap.txt")) %>%
  mutate(group=target_popname)
```

## Fiji

**Fiji**


```{r}
#| eval: false
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/09_Fiji"
target_popname <- "Fiji_Islander"
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="0",
                       admixdates = "1")
```

```{r}
#####
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/09_Fiji/bootstrap_run/"
target_popname <- "Fiji_Islander"
```

```{r}
#| eval: false
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="1",
                       admixdates = "1")
```


```{r}
fiji <- tidy_globetrotter(paste0(output_dir,"/globetrotter_output_bootstrap.txt")) %>%
  mutate(group=target_popname)
```


## Polynesian Outliers


**Rennell and Bellona**

```{r}
#| eval: false
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/10_RenBell"
target_popname <- "RenBell"
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="0",
                       admixdates = "1")
```

```{r}
#####
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/10_RenBell/bootstrap_run/"
target_popname <- "RenBell"
```

```{r}
#| eval: false
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="1",
                       admixdates = "?")
```

```{r}
#| eval: false

renbell <- tidy_globetrotter(paste0(output_dir,"/globetrotter_output_bootstrap.txt")) %>%
  mutate(group=target_popname)
```

**Tikopia**

```{r}
#| eval: false
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/11_Tikopia"
target_popname <- "Tikopia"
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="0",
                       admixdates = "1")
```


```{r}
#####
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/11_Tikopia/bootstrap_run/"
target_popname <- "Tikopia"
```

```{r}
#| eval: false
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="1",
                       admixdates = "1")
```


```{r}
#| eval: false
tikopia <- tidy_globetrotter(paste0(output_dir,"/globetrotter_output_bootstrap.txt")) %>%
  mutate(group=target_popname)
```

**Ontong Java**

```{r}
#| eval: false
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/12_Ontong_Java"
target_popname <- "Ontong_Java"
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="0",
                       admixdates = "1")
```


```{r}
#####
output_dir <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/12_Ontong_Java/bootstrap_run/"
target_popname <- "Ontong_Java"
```

```{r} 
#| eval: false
run_globetrotter_slurm(output_dir,
                       target_popname,
                       bootstrap="1",
                       admixdates = "1")
```


```{r}
ontong_java <- tidy_globetrotter(paste0(output_dir,"/globetrotter_output_bootstrap.txt")) %>%
  mutate(group=target_popname)
```




## Join data

```{r}
globetrotter <- marquesas %>%
  full_join(australes) %>%
  full_join(gambier) %>%
  full_join(society) %>%
  full_join(tuamotu) %>%
  full_join(atiu) %>%
  full_join(tonga) %>%
  full_join(western_samoa) %>%
  full_join(fiji) %>%
  #full_join(renbell) %>% run failed
  #full_join(tikopia) %>% run failed
  full_join(ontong_java)

new_pal <- pal %>% 
  filter(group3 %in% unique(globetrotter$group))

globetrotter$group <- factor(globetrotter$group, levels = unique(new_pal$group3))
```

```{r}
globetrotter %>%
  write_delim("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/joined_results.tsv")
```



```{r}
library(ggridges)
```



```{r}
globetrotter %>%
  ggplot() +
  geom_jitter(aes(x=r2,color=n_dates,y=group),width = 0, alpha=0.7) +
  scale_color_manual(values = met.brewer(name="Cassatt2", n=2, type="discrete"))
```



```{r}
p <- globetrotter %>%
        ggplot() +
         geom_density_ridges2(aes(y=group, x=estimate*28, fill = n_dates),
                              alpha =0.3,
                              quantile_lines = TRUE,
                              quantiles = c(0.025,0.5,0.975)) +
        scale_fill_manual(values = c("#FFFFFF","#FFFFFF")) +
        theme(legend.position = "none") +
        xlab(paste0("Years Ago (",years_per_generation," years per generation)"))
p
```




```{r}
ggsave(plot = p,
       filename = "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/01_chromopainter_pipeline/05_globetrotter/02_per_population/01_all_surrrogates/images/globetrotter_joint_results.svg",
       width = 12,
       height = 6)
```


