#!/usr/bin/env python3
# coding: utf-8

import argparse
# Options for script
parser = argparse.ArgumentParser( description = "Process FLARE's output VCF with posterior probabilities, classify ancestries with a given threshold, and generate bed files that assigns local ancestry.")
parser.add_argument("input_vcf", type=str, help="VCF file (FLARE OUTPUT)")
parser.add_argument("output_directory", type=str, help="Output directory")
parser.add_argument("chromosome", type=str, help="Chromosome id")
parser.add_argument("confidence_threshold",type=float, help="Posterior probability threshold for assigning a given ancestry. In the range [0,1].")
args = parser.parse_args()

#


from cyvcf2 import VCF
import numpy as np
from multiprocessing.pool import Pool
import pandas as pd
import os
import sys


#------
vcf=VCF(args.input_vcf)
output_directory=args.output_directory
chromosome=args.chromosome
confidence_threshold=args.confidence_threshold

####################

def field_to_dict(field_string):
    """
    Convert a string of the form <VALUE1=KEY1, ... , VALUEN=KEYN> to a dictionary.
    """
    d={}
    string_list=field_string.replace("<","").replace(">","").split(",")
    for i in string_list:
        value,key=i.split("=")
        d[int(key)]=value
    return(d)   

def ancestry_header_to_dict(vcf):
    """
    Convert the ancestry line to a dictionary.
    """
    for i in vcf.raw_header.split("\n"):
        if i.startswith("##ANCESTRY"):
            field=i.split("##ANCESTRY=")[1]
            ancestry_dict=field_to_dict(field)
            break
    return(ancestry_dict)


def classify_probability_vector(prob_list, threshold=0.9):
    if np.max(prob_list) >= threshold:
        return(np.argmax(prob_list))
    else:
        return(9)

def bed_dict_from_vcf(vcf,chromosome,confidence_threshold):
    bedstrings={}
    tmp_lists={}
    counter = 0
    #loop variants
    for variant in vcf:
        counter += 1
        first_variant = (counter == 1)
        position = variant.POS
        # loop samples
        for sample, prob_vector_hap1, prob_vector_hap2 in zip(vcf.samples, variant.format("ANP1"), variant.format("ANP2")):
            ancestry = [0,0]
            ancestry[0]=classify_probability_vector(prob_vector_hap1,confidence_threshold)
            ancestry[1]=classify_probability_vector(prob_vector_hap2,confidence_threshold)
            if first_variant:
                # initialize some variables with the first position
                tmp_lists[sample] = [position, ancestry]
                bedstrings[(sample,1)] = f"{chromosome} {position} "
                bedstrings[(sample,2)] = f"{chromosome} {position} "
            else:
                prev_position = tmp_lists[sample][0]
                prev_ancestry_hap1 = tmp_lists[sample][1][0]
                prev_ancestry_hap2 = tmp_lists[sample][1][1]

                tmp_lists[sample] = [position,ancestry]

                ### Check haplotype 1
                if prev_ancestry_hap1 == ancestry[0]:
                    pass
                else:
                    bedstrings[(sample,1)] = bedstrings[(sample,1)] + f"{prev_position} {prev_ancestry_hap1}\n"
                    bedstrings[(sample,1)] = bedstrings[(sample,1)] + f"{chromosome} {position} "

                ### Check haplotype 2    
                if prev_ancestry_hap2 == ancestry[1]:
                    pass
                else:
                    bedstrings[(sample,2)] = bedstrings[(sample,2)] + f"{prev_position} {prev_ancestry_hap2}\n"
                    bedstrings[(sample,2)] = bedstrings[(sample,2)] + f"{chromosome} {position} "
    # write last entry
    for sample in tmp_lists:
        # Haplotype 1
        last_position = tmp_lists[sample][0]
        last_ancestry_hap1 = tmp_lists[sample][1][0]
        bedstrings[(sample,1)] = bedstrings[(sample,1)] + f"{last_position} {last_ancestry_hap1}\n"
        # Haplotype 2
        last_position = tmp_lists[sample][0]
        last_ancestry_hap2 = tmp_lists[sample][1][1]
        bedstrings[(sample,2)] = bedstrings[(sample,2)] + f"{last_position} {last_ancestry_hap2}\n"   
        
    return(bedstrings)

def write_dict_to_bed_files(bedstrings,ancestry_dict,output_directory):
    header="<"
    for key in ancestry_dict:
        header=header + f"{str(key)}={str(ancestry_dict[key])},"
    header=header+"9=UNCERTAIN>\n"
    for hap in bedstrings:
        # generate filename
        filename = f"{output_directory}/{hap[0]}_hap{str(hap[1])}.bed"
        with open(filename,'w') as handle:
            # write header
            handle.write(header)
            # write string
            handle.write(bedstrings[hap])



# protect the entry point
if __name__ == '__main__':
    try:
        os.makedirs(f"{output_directory}/chr{chromosome}")
    except OSError:
        print("Warning: bed dir already created")
        pass
    bedstrings = bed_dict_from_vcf(vcf,chromosome, confidence_threshold)
    ancestry_dict = ancestry_header_to_dict(vcf)
    write_dict_to_bed_files(bedstrings, ancestry_dict,f"{output_directory}/chr{chromosome}")
