worldwide_allele_frequencies <- function(dataframe, snp_id,scale=1){
        require(tidyverse)
        require(ggmap)
        require(maps)
        require(mapproj)
        require(ggrepel)
        require(scatterpie)
        
        # input dataframe with following columns (respect names):
        # population alternate_allele_frequency reference_allele_frequency lon lat
        # Get the map
        ## Pacific centered map
        world <- map_data('world2',
                          wrap=c(-25, 335)) #rewarpping the worldmap, i.e. shifting the center
        # Shift the lon/lat of our data to match the pacific centered map
        d <- dataframe %>%
          mutate(lon = ifelse(lon <= -25, lon + 360, lon)) %>%
          filter(!is.na(lon)) %>%
          rename(A="alternate_allele_frequency",
                 R="reference_allele_frequency")
        # basemap
        basemap <- ggplot() + 
          geom_polygon(data=world,
                       aes(x=long, y=lat, group=group),
                       fill="#e7c9a7ff",
                       color="#e7c9a7ff",
                       size=0.15) +
          theme() + 
          ylim(-55, 90) # cutting out antarctica (not obligatory)
        # Plot!
        # The map
        p <- basemap +
          coord_map(ylim=c(min(d$lat),max(d$lat)),
                    xlim=c(min(d$lon),max(d$lon)),
                    projection = "vandergrinten") #a non-rectangular world map projection that is a decen compromise between area and distances accuracy
        # Pie chart of allele freq.
        p <- p +
          geom_scatterpie(data=d,
                          aes(x=lon, y=lat, group=population),
                          cols=c("R", "A"),
                          pie_scale=scale, alpha=0.7) +
          coord_equal()
        # Style
        p <- p + theme(legend.text = element_text(size = 12), legend.title = element_text(size = 14)) +
          theme(axis.title.x = element_text(size = 14), axis.title.y = element_text(size = 14)) +
          theme(axis.text.x = element_text(size = 10), axis.text.y = element_text(size = 10)) +
          theme(strip.text.x = element_text(size = 12), strip.text.y = element_text(size=12)) +
          labs(x="Longitude", y="Latitude", fill="Allele freq.")  +
          ggtitle(snp_id) +
          scale_fill_manual(values = c("#F6EFA6","#37A6E6"))
        
        p
}
