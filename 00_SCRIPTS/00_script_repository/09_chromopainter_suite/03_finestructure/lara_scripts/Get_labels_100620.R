##-------------
## Here I do something similar to Leslie et al. 2015
### ____________________________________________________________
source("../../bin/FinestructurePlots/FinestructureLibrary.R") # read in the R functions, which also calls the needed packages
library(dendextend)
library(ggplot2)
library(reshape2)
library(viridis)
library(plotrix)
library(maps)
library(mapplots)
library(RColorBrewer)
library(dendextend)
library(factoextra)
## Add an alpha value to a colour
add.alpha <- function(col, alpha=1){
  if(missing(col))
    stop("Please provide a vector of colours.")
  apply(sapply(col, col2rgb)/255, 2,
        function(x)
          rgb(x[1], x[2], x[3], alpha=alpha))
}

## Name files
seed="365"

mcmcfile<- paste0("Fs.seed", seed, ".chunkcounts.mcmc.out") ## finestructure mcmc file
treefile<- paste0("Fs.seed", seed, ".chunkcounts.tree.out") ## finestructure tree file

###### READ IN THE MCMC FILES
mcmcxml<-xmlTreeParse(mcmcfile) ## read into xml format
mcmcdata<-as.data.frame.myres(mcmcxml) ## convert this into a data frame

###### READ IN THE TREE FILES

treexml<-xmlTreeParse(treefile) ## read the tree as xml format
ttree<-extractTree(treexml) ## extract the tree into ape's phylo format
## If you dont want to plot internal node labels (i.e. MCMC posterior assignment probabilities)
## now is a good time to remove them via:
#     ttree$node.label<-NULL
## Will will instead remove "perfect" node labels
ttree$node.label[ttree$node.label=="1"] <-""
## And reduce the amount of significant digits printed:
ttree$node.label[ttree$node.label!=""] <-format(as.numeric(ttree$node.label[ttree$node.label!=""]),digits=2)
tdend<-myapetodend(ttree,factor=1) # convert to dendrogram format


# Make label list and table with the K choice
K=25
mapstatelist <- lapply(get_subdendrograms(tdend, K), function(x)return(x %>% labels))
popnames <- lapply(mapstatelist, function(x)sapply(strsplit(x, "\\."), head,1))

clusters.names <- paste("cluster", 1:K, sep = "_")
pop.names <- unique(unlist(popnames))
table.pops <- matrix(nrow = length(pop.names), ncol = length(clusters.names), 0)
colnames(table.pops) <- clusters.names
rownames(table.pops) <- pop.names

list.table <- lapply(popnames, table)

for (f in 1:ncol(table.pops)){
  table.pops[names(table.pops[,f]) %in% names(list.table[[f]]),f] <- as.numeric(list.table[[f]])
}


labels <- read.table("../Inputs/Vanuatu_1412x2269868_SNPs_Sequences_filtered_WithREL__chr1_ChP.labels")
labels.clusters <- as.data.frame(cbind(unlist(mapstatelist), 
                                       rep(paste("cluster", 1:K, sep = "_"), unlist(lapply(mapstatelist, length)))))

labels.clusters.table <- merge(labels, labels.clusters, by="V1", all = T)
labels.clusters.table[is.na(labels.clusters.table$V2.y),]$V2.y <- labels.clusters.table[is.na(labels.clusters.table$V2.y),]$V2.x
labels.clusters.final <- labels.clusters.table[,c(1,4,3)]
labels.clusters.final -> labels.clusters.final.notRem

# Ind to exclude: 
Remove <- read.table(paste0("As_Leslie_indRemove_seed", seed, ".txt"), header=T)
Remove <- as.vector(na.omit(Remove$cluster_25))
labels.clusters.final[labels.clusters.final$V1 %in% Remove,]$V3 <- 0

write.table(labels.clusters.final, paste0("100620_clusters_K", K, ".labels"), quote = F, col.names = F, row.names = F)

popnames <- cbind(unlist(lapply(labels.clusters.final$V1, function(x)sapply(strsplit(x, "\\."), head,1))), labels.clusters.final$V2.y)
popnames <- popnames[labels.clusters.final$V3==1,]
tapply(popnames[,1], popnames[,2], table)
lapply(tapply(popnames[,1], popnames[,2], table), function(x)
  
write.table(" ",  paste0("100620_clusters_table_K", K, ".txt"),row.names = F, col.names = F, quote = F)
for(cluster in 1:length(tapply(popnames[,1], popnames[,2], table))){
  cluster.n <- tapply(popnames[,1], popnames[,2], table)[cluster]
  write.table(cluster.n, paste0("100620_clusters_table_K", K, ".txt")  , append= T, sep=',' , row.names = F, col.names = F, quote = F)
  write.table(cluster.n[[1]], paste0("100620_clusters_table_K", K, ".txt")  , append= T, sep=',' , row.names = F, col.names = F, quote = F)
}


