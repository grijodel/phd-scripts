#!/usr/bin/env bash
#SBATCH -J ibdends
#SBATCH --mem=160G
#SBATCH -c 20
#SBATCH --array=0-21
#SBATCH --partition=gehbigmem

module load java

VCF_dir=/pasteur/zeus/projets/p02/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/05_Phasing/03_Phase_Scaffold
MAP_dir=/pasteur/appa/homes/danliu/bin/plink_maps_h37
IBD_dir=/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/08_IBD/hapIBD

chromosomes=(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22)
chromosome=${chromosomes[$SLURM_ARRAY_TASK_ID]}

java -Xss5m  -Xmx150g -jar ~/bin/ibd-ends.jar \
	gt=$VCF_dir/shapeit4Phased_scaffold_chr${chromosome}.vcf.gz \
    ibd=$IBD_dir/hapIBD_chr${chromosome}.ibd.gz \
	map=$MAP_dir/plink.chr${chromosome}.GRCh37.map \
    err=1.5E-4 \
	out=IBDends_chr${chromosome} \
	nthreads=$SLURM_CPUS_PER_TASK