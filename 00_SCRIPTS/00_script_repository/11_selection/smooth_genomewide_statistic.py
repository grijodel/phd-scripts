#!/usr/bin/env python3
import argparse
parser = argparse.ArgumentParser( description = 'Smooth a statistic over genomic positions using a sliding window and average.')
parser.add_argument("input_file", type=str, help="Input dataframe")
parser.add_argument("output_file", type=str, help="Output dataframe")
parser.add_argument("statistic_name", type=str, help="Column name of statistic to smooth")
parser.add_argument("distance_metric", type=str, help="Column name of distance metric to use (e.g. bp, cM)")
parser.add_argument("window_size",type=float,help="Window size to compute average with. Must be in same units as distance metric.")
args = parser.parse_args()
import pandas as pd
import numpy as np
from tqdm import tqdm
import multiprocessing


def smooth_genomestat(statistic_dataframe,statistic_name,distance_metric,window_size,chromosome):
    '''
    Smooth a statistic over a chromosome using a sliding window and average.
    ---
    Input dataframe:
        - should contain columns: ["chromosome","distance_metric","statistic_name"]
        - should either contain all chromosomes or match the specified chromosome
    Output dataframe:
        - adds two columns: ["smooth","n_snps"] which is the per-SNP smoothed statistic and the number of SNPs used to compute the value.
        - Only specified chromosome is outputted (facilitates parallelization)
    '''
    # Filter for chromosome
    df = statistic_dataframe[statistic_dataframe["chromosome"]==chromosome].reset_index()
    # Check if the DataFrame is empty (chromosome misspecification)
    if df.empty:
        raise ValueError("DataFrame is empty, and an error is raised.")
    # Get maximum coordinate
    max_coordinate = df[statistic_name].shape[0]
    # Define the number of iterations
    total_iterations = max_coordinate-1
    # Create a tqdm progress bar
    progress_bar = tqdm(total=total_iterations, desc=f'Chromosome {chromosome}: Smoothing of {statistic_name} using distance metric {distance_metric}')
      
    # Initialize vector containing smoothed values and number of snps used
    smoothed_vector = np.zeros(max_coordinate)
    n_snps_vector = np.zeros(max_coordinate)
    for ith_snp in range(0,max_coordinate):
        # Get position in bp for ith snp
        ith_pos = df[distance_metric][ith_snp]
        # Get all values to the left
        left_vector = df[statistic_name][0:ith_snp]
        left_window = []
        ## Only store those where the difference in bp is below threshold
        for jth_snp in range(len(left_vector)-1,-1,-1):
            jth_pos = df[distance_metric][jth_snp]
            delta = abs(ith_pos - jth_pos)
            ## check if below thold
            if delta < (window_size/2):
                ### keep value of snp for window
                left_window.append(df[statistic_name][jth_snp])
                ### out of window range
            else:
                break
        # Get all values to the right
        ## check if end of table
        if ith_snp != max_coordinate-1:
            right_vector = df[statistic_name][ith_snp:max_coordinate]
            right_window = []
            ## Only store those where the difference in bp is below threshold
            for jth_snp in range(ith_snp,max_coordinate):
                jth_pos = df[distance_metric][jth_snp]
                ## check if below thold
                delta = abs(ith_pos - jth_pos)
                if delta <= (window_size/2):
                    right_window.append(df[statistic_name][jth_snp])
                else:
                    #### out of window range
                    break
            window = left_window
            window.extend(right_window)
        else:
            window = left_window
        # Update lists
        ## number of snps in window
        n_snps = len(window) 
        n_snps_vector[ith_snp] = n_snps
        ## STATISTIC SMOOTHING
        try:
            smooth = sum(window)/n_snps
        except ZeroDivisionError:
            smooth = None
        smoothed_vector[ith_snp] = smooth
        # Update progress bar
        progress_bar.update(1)
    # Update statistic df
    df['smooth'] = smoothed_vector
    df['n_snps'] = n_snps_vector
    # Close the progress bar
    progress_bar.close()
    return df

def parallel_smooth_genomestat(statistic_dataframe,statistic_name,distance_metric="bp",window_size=10000):
    '''
    Smooth a statistic over a chromosome using a sliding window and average.
    ---
    Input dataframe:
        - should contain columns: ["chromosome","distance_metric","statistic_name"]

    Runs per-chromosome smoothing of statistic.

    Output dataframe:
        - adds two columns: ["smooth","n_snps"] which is the per-SNP smoothed statistic and the number of SNPs used to compute the value.
    '''
    # Chromosomes to compute
    chromosomes = list(range(1,23))
    # Initialize a multiprocessing Pool
    pool = multiprocessing.Pool()
    # Use a list comprehension to apply smooth_geonmestat to the list of chromosomes in parallel
    results = pool.starmap(smooth_genomestat, [(statistic_dataframe,statistic_name,distance_metric,window_size,chromosome) for chromosome in chromosomes])
    # Create a dictionary where each key is a list element, and the value is the result
    results_dict = dict(zip(chromosomes, results))
    # Close the pool to free up resources
    pool.close()
    pool.join()
    results_df = pd.concat(results_dict.values(),ignore_index=True).sort_values(by=["chromosome",distance_metric], ascending=True)
    return(results_df)



if __name__ == "__main__":
    selscan = pd.read_csv(args.input_file,sep=" ")
    smooth = parallel_smooth_genomestat(selscan, args.statistic_name,args.distance_metric,args.window_size)
    smooth.to_csv(args.output_file,sep="\t",na_rep="NA",index=False)

