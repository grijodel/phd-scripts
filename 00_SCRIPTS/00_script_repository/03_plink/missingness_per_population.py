#!/usr/bin/env python3
import argparse
import os
import glob
from pathlib import Path
import subprocess
import multiprocessing
from functools import partial
# argparse
parser = argparse.ArgumentParser( description = 'Perform within population missingness computation using plink. lmiss and imiss files are returned.')
parser.add_argument("prefix", type=str, help="Prefix for plink files")
parser.add_argument("fam_prefix", type=str, help=".fam file prefix")
parser.add_argument("output_prefix", type=str, help="output prefix")
parser.add_argument("ncpus", type=str, help="Number of cpus to parallelize")
parser.add_argument("loci_thold", type=str, help ="Missingness threshold to report a locus, in percentage (if missingness is higher than this number, it is reported).")
parser.add_argument("ind_thold", type=str, help ="Missingness threshold to report an individual, in percentage (idem).")


args = parser.parse_args()
#--
prefix=args.prefix
fam_prefix=args.fam_prefix
output_prefix=args.output_prefix
ncpus=int(args.ncpus)
loci_thold=float(args.loci_thold)
ind_thold=float(args.ind_thold)

# Get lists of population ids and create a set
fam=fam_prefix+".fam"
populations=set()
print( "Reading .fam file..." )
with open(fam,'r') as f:
    for line in f:
        fid, iid, father,mother,sex,phenotype=line.rstrip().split()
        populations.add(fid)
# Create population ids files
print("Creating population files...")
for population in populations:    
    f=open(population+".txt", 'w')
    f.write(population)
    f.close()
# Create function for parallelizing
def missingness(population):
    # create new bed with families
    family=population+".txt"
    command1 = [ "plink", "--bfile",
            prefix, "--fam", fam_prefix+".fam",
            "--keep-fam",
            family, "--make-bed",
            "--out", population ]
    process1 = subprocess.Popen(command1, shell=False)
    process1.wait()
    # missingness
    command2 = [ "plink", "--bfile",
            population ,"--missing",
            "--out", population ]
    process2 = subprocess.Popen(command2, shell=False)
    process2.wait()
    # write snps that with more than 10% missingness
    f = open(population+".lmissing", 'w')
    with open(population+".lmiss") as lmiss:
        next(lmiss)
        for line in lmiss:
            chromosome,snp,n_miss,n_geno,f_miss=line.rstrip().split()
            f_miss=float(f_miss)
            if f_miss >= loci_thold/100:
                f.write(snp+"\n")
            else:
                pass
    f.close()

    f = open(population+".imissing", 'w')
    with open(population+".imiss") as imiss:
        next(imiss)
        for line in imiss:
            fid,iid,miss_pheno,n_miss,n_geno,f_miss=line.rstrip().split()
            f_miss=float(f_miss)   
            f.write(fid+" "+iid+" "+str(f_miss)+"\n")
          
         
    f.close()


# run in parallel
print("Begin to run --missing...")
snps_to_exclude=set()
samples_to_exclude=set()
pool=multiprocessing.Pool(ncpus)
r=pool.map_async( partial(missingness), populations)
r.wait()
# Get unique SNP ids for filtering 
print("---> Finished")
print("Summarizing samples and loci to exclude...")

for population in populations:
    with open(population+".lmissing") as f:
        lines = f.read().splitlines()
    snps_to_exclude.update(lines)
    with open(population+".imissing") as f:
        lines = f.read().splitlines()
    samples_to_exclude.update(lines)

        

# Write SNP ids to exclude
print("Writing SNPs with at least " + str(loci_thold) + "% missingness in a single cohort...")
with open(output_prefix+".locimiss", 'w') as f:
    for snp in snps_to_exclude:
        f.write(snp+"\n")

# Write samples to exclude
print("Writing samples with at least "+ str(ind_thold) +"% missingness in a single cohort...")
with open(output_prefix+".indmiss", 'w') as f:
    for individual in samples_to_exclude:
        f.write(individual+"\n")


print("Cleaning temporary files...")
for population in populations:
    fileList = glob.glob(population+'.*')
    for filePath in fileList:
        try:
            os.remove(filePath)
        except:
            print("Error while deleting file : "+str(filePath))
print("Done!")
