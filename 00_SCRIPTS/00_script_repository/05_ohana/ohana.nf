nextflow.enable.dsl=2
workflow {
    genotype=Channel.fromFilePairs(params.prefix + '{.bim,.bed,.fam}', size:-1)
    K=params.K
    ancestral_components=Channel.from(0..K-1) 
    chromosomes=Channel.from(1..22)    
    // Preprocessing
    //// Select populations, Filter for missing data and MAF
    PREPROCESS(genotype)
    // Estimate Structure
    //// LD prune, estimate genome wide covariance
    STRUCTURE(PREPROCESS.out,K)
    matrices = STRUCTURE.out[0]
    // Selection scan
    //// Separate into chromosomes, compute admixture corrected
    //// allele frequencies, perform selection scan.
    selection_files = PREPROCESS.out.combine(matrices).combine(chromosomes).combine(ancestral_components)
    SELECTION(selection_files, K)
    // Aggregate results
    AGGREGATE(SELECTION.out.collect(), PREPROCESS.out )
}
process PREPROCESS {
    scratch true
    publishDir "${params.outdir}/genotypes/", mode: 'copy'
    module "plink/1.90b6.16"
    memory '5 GB'
    input:
        tuple val(prefix), file(genotype)
    output:
        tuple val(prefix), file("${prefix}_POPS.bim"), file("${prefix}_POPS.bed"), file("${prefix}_POPS.fam")
    script:
        """
        # Select populations
        plink --bfile $prefix \
            --geno 0.0 \
            --maf 0.05 \
            --make-bed \
            --out ${prefix}_POPS
        """
    stub:
        """
        touch ${prefix}_POPS.bim ${prefix}_POPS.bed ${prefix}_POPS.fam
        """
}
process STRUCTURE {
    scratch true
    memory '2 GB'
    publishDir "${params.outdir}/structure/matrices", mode: 'copy', pattern: "*.matrix"
    publishDir "${params.outdir}/structure/genotypes", mode: 'copy', pattern: "${prefix}_POPS_LD.{ped,map}"
    publishDir "${params.outdir}/structure/", mode: 'copy', pattern: "q_subset_plot.pdf"
    publishDir "${params.outdir}/structure/", mode: 'copy', pattern: "cov_subset.nwk"
    publishDir "${params.outdir}/structure/", mode: 'copy', pattern: "q_subset_iid.txt"



    module "plink/1.90b6.16:ohana/0.1.7666.41124"    
    input:
        tuple val(prefix), file(bim), file(bed), file(fam)
        val(K)
    output:
        tuple file("q_subset.matrix"), file("f_subset.matrix"),file("cov_subset.matrix")
        tuple file("${prefix}_POPS_LD.ped"), file("${prefix}_POPS_LD.map")
        tuple file("cov_subset.nwk"), file("q_subset_plot.pdf"), file("q_subset_iid.txt")
        
    script:
        """
        # Subset variants in Linkage Equilibrium for matrix construction
        plink --bfile  ${prefix}_POPS \
               --indep-pairwise 50 5 0.5 \
               --out ${prefix}_POPS
        plink --bfile  ${prefix}_POPS \
                --exclude  ${prefix}_POPS.prune.out \
                --recode12 tab \
                --make-bed \
                --out ${prefix}_POPS_LD
        # Convert to dgm
        convert ped2dgm ${prefix}_POPS_LD.ped ${prefix}.dgm
        # Compute admixture matrices using ADMIXTURE
        admixture1.3 ${prefix}_POPS_LD.bed $K \
                    -j${task.cpus}
        # Make admixture matrices compatible
        admixture_to_ohana.py ${prefix}_POPS_LD.5.P ${prefix}_POPS_LD.5.Q f_subset.matrix q_subset.matrix        
        # Compute admixture-aware covariance matrices
        nemeco ${prefix}.dgm \
                f_subset.matrix \
                -mi 5 \
                -e 1e-10 \
                -co cov_subset.matrix
        #### ADDITIONAL FILES ####
        # Compute tree from matrix
        convert cov2nwk cov_subset.matrix cov_subset.nwk 
        # Assign individual ID to clustering
        assign_iid_to_cluster.py ${prefix}_POPS_LD.ped q_subset.matrix q_subset_iid.txt
        # Draw Q matrix
        plot_q.py q_subset.matrix q_subset_plot.pdf
        """
    stub:
        """
        touch q_subset.matrix f_subset.matrix cov_subset.matrix
        """
}

process SELECTION {
    scratch true
    module "plink/1.90b6.16:ohana/0.1.7666.41124"
    memory '2 GB'
    input:
        tuple val(prefix), file(bim), file(bed), file(fam), file(q_matrix), file(f_matrix), file(cov_matrix), val(chromosome), val(ancestral_component)
        val(K)
    output:
        file("${prefix}_${chromosome}_${ancestral_component}.selscan")
        
    script:
        """
        # Get chromosome from plink file
        plink --bfile ${prefix}_POPS \
            --chr $chromosome \
            --recode12 tab\
            --out ${prefix}_${chromosome}
        convert ped2dgm ${prefix}_${chromosome}.ped ${prefix}_${chromosome}.dgm
        qpas ${prefix}_${chromosome}.dgm \
                -qi $q_matrix \
                -fo ${prefix}_${chromosome}_f.matrix \
                -mi 300 \
                -e 1e-10 \
                -fq
        # Compute scale matrix 
        compute_scale_matrix.py $cov_matrix ${ancestral_component} > scale_matrix_${ancestral_component}.matrix
        # Selection scan
        selscan  ${prefix}_${chromosome}.dgm \
                    ${prefix}_${chromosome}_f.matrix \
                    $cov_matrix \
                    --c-scale scale_matrix_${ancestral_component}.matrix > ${prefix}_${chromosome}_${ancestral_component}.selscan
        """

    stub:
        """
        touch ${prefix}_${chromosome}_${ancestral_component}.selscan
        touch scale_matrix_component_${ancestral_component}.matrix
        """
}
process AGGREGATE {
    publishDir "${params.outdir}", mode: 'copy'
    memory '1 GB'
    input:
        file("*")
        tuple val(prefix), file(bim), file(bed), file(fam)                      

    output:
        file("${prefix}*.selscan")

    script:
        """
        aggregate.py . $bim --output_prefix ${prefix}
        """
    stub:
        """
        touch ${prefix}*.selscan
        """
}

















