nextflow.enable.dsl=2    
workflow { 
    genotype = Channel.fromFilePairs(params.prefix + '{.bim,.bed,.fam}', size:-1)   
    parameter_file=Channel.fromPath(params.parameter_file)                      
    // Compute blocks
    COMPUTE_BLOCKS(genotype)
    block_table = COMPUTE_BLOCKS.out[0]
    number_of_blocks = COMPUTE_BLOCKS.out[1]
    // Compute gone
    parameters_gone = genotype.combine(parameter_file)
    GONE(parameters_gone)
    // Compute Jackknife for GONE
    /// Create channel for blocks
    blocks = number_of_blocks
                            .first()
                            .flatMap {[1..it.toInteger()]}
                            .collect()
                            .flatten()
    /// Create channel for replicates and run GONE
    parameters_jackknife = genotype.combine(parameter_file).combine(block_table).combine(blocks)
    GONE_JACKKNIFE(parameters_jackknife)
    /// Aggregate results 
    AGGREGATE( GONE.out, GONE_JACKKNIFE.out.collect() )
}
process COMPUTE_BLOCKS {
    publishDir "${params.outdir}", mode: 'copy'
    input:
        tuple val(prefix), file(genotype)
    output:
        file("${prefix}_jackknife_blocks.tsv")
        stdout
    script:
        """
        compute_blocks.py ${prefix} ${prefix}_jackknife_blocks.tsv
        cat ${prefix}_jackknife_blocks.tsv | grep -v chromosome | wc | awk '{print \$1}'  
        """
    stub:
        """
        touch ${prefix}_jackknife_blocks.tsv
        echo 700
        """ 
}
process GONE {
    cpus 50
    scratch true
    module "plink/1.90b6.16"                                                    
    input:                                                                      
        tuple val(prefix), file(genotype), file(parameter_file)
    output:                                                                     
        tuple val(prefix), file("Output_Ne_${prefix}"), file("N_snps_${prefix}.txt")
    script:
        """
        # Convert to ped
        plink --bfile ${prefix} \
                --recode \
                --out ${prefix}
        # Run GONE
        script_GONE.sh ${prefix}
        # Get number of SNPs in full dataset
        wc ${prefix}.map | awk '{print \$1}' > N_snps_${prefix}.txt
        """
    stub:
        """
        touch "Output_Ne_${prefix}"
        touch "N_snps_${prefix}.txt"
        """
}
process GONE_JACKKNIFE {
    cpus 50
    scratch true
    module "plink/1.90b6.16"
    input:
        tuple val(prefix),file(genotype),file(parameter_file),file(block_table),val(block)        
    output:
        tuple val(block), file("Output_Ne_${prefix}_block_${block}"), file("N_snps_${prefix}_block_${block}.txt") 
    script:
        """
        # Get region to exclude 
        cat ${block_table} | head -n ${block} | tail -n 1 | awk '{print \$1,\$3,\$4,"set_exclude"}' > set_to_exclude.txt
        # exclude with plink
        plink --bfile ${prefix} \
                --recode \
                --exclude range set_to_exclude.txt \
                --out ${prefix}_block_${block}
        # Run GONE
        script_GONE.sh ${prefix}_block_${block}
        # Count number of SNPs for weighted jackknife
        wc ${prefix}_block_${block}.map | awk '{print \$1}' > N_snps_${prefix}_block_${block}.txt
        """
    stub:
        """
        touch Output_Ne_${prefix}_block_${block}
        touch N_snps_${prefix}_block_${block}.txt
        """
}
process AGGREGATE {
    publishDir "${params.outdir}", mode: 'copy' 
    input:
        tuple val(prefix),file(Ne_estimates),file(Nsnps)
        file("*")
    output:
        tuple file("${prefix}_Ne_estimates.tsv"), file("${prefix}_N_snps.tsv")
    script:
        """
        aggregate_ne.py ${prefix}
        """
    stub:
        """
        touch ${prefix}_Ne_estimates.tsv
        """
}


