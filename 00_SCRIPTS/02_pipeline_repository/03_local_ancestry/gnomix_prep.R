# R script to prepare ref and query files for gnomix
# lastv_20230707

# Libs
library(tidyverse)

# Read input files
poplabels <- read_delim("/Volumes/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/02_relate_pipeline/02_input_prep/Chr1_processed.poplabels", delim=" ", col_names=TRUE)
ad_fam <- read_delim("/Volumes/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/HC_MAF_PRUNED_THINNED.fam", delim=" ", col_names=FALSE)
K6 <- read_delim("/Volumes/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/admixture/all_samples/K_6_R_1047214/HC_MAF_PRUNED_THINNED.6.Q", delim=" ", col_names=FALSE)

ad_K6 <- ad_fam %>% select(X1,X2) %>% rename(pop="X1", ind="X2") %>% bind_cols(K6)

# Selected pops
pops <- c("Atiu","Bora_Bora","Fiji","Mangareva","Hiva_Oa","Nuku_Hiva",
          "Rurutu","Raivavae","Tahiti","Tonga","Western_Samoa","Bundi","Kundiawa","Mendi","Marawaka",
          "Rangiroa","Ontong_Java","Kundiawa","Tikopia","Atayal","Rennell",
          "Paiwan","Bellona","French","Papuan_Highlands","Han")
sub_pop <- poplabels %>% filter(population %in% pops)
sub_pop %>% write_delim("/Volumes/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/02_relate_pipeline/06_subtree/subtree.poplabels", delim=" ", col_names=TRUE, quote="none")
table(sub_pop$population)

sub_pop_POL <- sub_pop %>% rename(ind="sample") %>% left_join(ad_POL)

# Prepare ref inds
pure_POL_ind <- sub_pop %>% rename(ind="sample") %>% left_join(ad_K6) %>% filter(X4>=0.99)
pure_PNG_ind <- sub_pop %>% filter(population%in%c("Bundi","Kundiawa","Mendi","Marawaka","Kundiawa","Papuan_Highlands")) %>%
  rename(ind="sample") %>% left_join(ad_K6)
pure_FRE_ind <- sub_pop %>% filter(population%in%c("French")) %>%
  rename(ind="sample") %>% left_join(ad_K6) %>% filter(!is.na(X1))
pure_HAN_ind <- sub_pop %>% filter(population%in%c("Han")) %>%
  rename(ind="sample") %>% left_join(ad_K6) %>% filter(!is.na(X1))


ref_ind <- pure_POL_ind %>% bind_rows(pure_PNG_ind, pure_FRE_ind, pure_HAN_ind) %>% select(ind, pop) %>%
  mutate(pop=case_when(
    pop%in%c("Atiu","Bora_Bora","Mangareva","Hiva_Oa","Nuku_Hiva","Rurutu","Raivavae","Rangiroa","Tahiti") ~ "POL",
    pop%in%c("Bundi","Kundiawa","Mendi","Marawaka","Kundiawa","Papuan_Highlands") ~ "PNG",
    pop%in%c("French") ~ "FRE",
    pop%in%c("Han") ~ "HAN"
  ))
write_delim(ref_ind, "/Volumes/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/05_local_ancestry/gnomix/data/ref_ind.smap", delim="\t", col_names=FALSE, quote="none")
ref_ind %>% select(ind) %>% write_delim("/Volumes/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/05_local_ancestry/gnomix/data/ref_ind.txt", delim="\t", col_names=FALSE, quote="none")

# Prepare query inds
query_ind <- sub_pop %>% filter(!population%in%c("Bundi","Kundiawa","Mendi","Marawaka","Kundiawa","Papuan_Highlands","French","Han","Atayal","Paiwan"))
query_ind %>% select(sample) %>% write_delim("/Volumes/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/05_local_ancestry/gnomix/data/query_ind.txt", delim="\t", col_names=FALSE, quote="none")
