#!/usr/bin/env python3
import argparse
parser = argparse.ArgumentParser( description = 'Check if map file (plink) is sorted according to position.')
parser.add_argument("mapfile", type=str, help=".map input file")

args = parser.parse_args()

with open(args.mapfile, 'w') as f:
    l=f.readlines()

for i in range(0,len(l)):
    chromosome,snpid,cm,pos=l[i].split()
    try:
        chromosome_j,snpid_j,cm_j,pos_j=l[i+1].split()
    except IndexError:
        print('No discordance, end of list (snp {0})'.format(snpid))
        break
    else:
        diff = int(pos_j) - int(pos)

        if diff < 0 and(chromosome==chromosome_j) :
            print('Order discordance found at SNPs-({0},{1})'.format(snpid,snpid_j))
            break
