---
title: "pcadapt workflow"
author: "Gaston Rijo De León"
format:
  html:
    embed-resources: true
    toc: true
    toc-depth: 2
date:  today
date-format: 'full'
editor: visual
engine: knitr
---

# Setup

```{r global-options}
knitr::opts_chunk$set(engine.opts = list(bash = "-l"), eval = FALSE)
```

```{r, warning=FALSE, message=FALSE, eval=TRUE}
library(tidyverse)
library(janitor)
library(plotly)
library(MetBrewer)

theme_set(theme_bw())
pal <- readxl::read_excel("/pasteur/zeus/projets/p02/MATAEA/gaston/mataea/metadata/population_colors_all_sheets.xlsx", sheet = 3) 
'%!in%' <- function(x,y)!('%in%'(x,y))
ind_labels <- read_delim("/pasteur/zeus/projets/p02/MATAEA/WGS_Joint_Calling/HIGH_COVERAGE_ALL/04_Quality_Control/labels.tsv",col_names = c("individual", "identifier", "cohort"))
```

# Introduction

In addition to Ohana, we will run [*pcadapt*](https://doi.org/10.1111/1755-0998.12592) to detect signals of selection using an allele frequency method accounting for admixture.

# Prepare the data

In contrast with Ohana, when using pcadapt, we should only use the populations of interest: if we also used the reference panels, we would not know which populations are driving the signal of the selective sweep.

We will then select only regional groups of Polynesian populations.

## Subset Eastern Polynesian populations

Make a file containing the IDs of Eastern Polynesian populations

```{r, warning=FALSE, message=FALSE, eval=TRUE}
ep_inds <- read_delim("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/HC_MAF.fam",
                  col_names = c("identifier","individual"))  %>%
  select(identifier,individual) %>%
  left_join(pal) %>%
  filter(region=="Eastern_Polynesia") %>%
  select(identifier,individual)


write_delim(ep_inds,"/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/02_pcadapt/eastern_polynesian_individuals.tsv",col_names = FALSE, delim = " ")
```

Filter from dataset using `plink`.\

```{bash}
plink --bim /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/HC_MAF.bim \
        --bed /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/HC_MAF.bed \
        --fam /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/HC_MAF.fam \
        --keep  /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/02_pcadapt/eastern_polynesian_individuals.tsv \
        --make-bed \
        --out /pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/02_pcadapt/eastern_polynesians
```

Read plink file and make scree plot with 50 principal components to decide on the number of principal components to be used.

```{r, warning=FALSE, message=FALSE, eval=FALSE}
library(pcadapt)
path_to_file <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/02_pcadapt/eastern_polynesians.bed"
bed <-  read.pcadapt(path_to_file, type = "bed")
```

```{r, warning=FALSE, message=FALSE, eval=FALSE}
res_50_pcs <- pcadapt(input = bed,
                      K = 50,
                      LD.clumping = list(size = 50, thr = 0.5),
                      pca.only = TRUE)
```

```{r, warning=FALSE, message=FALSE, eval=FALSE}
res_plot <- plot(res_50_pcs, option = "screeplot")
```

Well, this is annoying. The proportion of variance explained decreases constantly, but does not stop decreasing. That means that random noise is not responsible for the variability at least up until the 50th pricipal component.

We can see though that there is a shoulder at principal component 12. We will use that as a cutoff.

Use the first 12 principal components, compute pcs again.

```{r, warning=FALSE, message=FALSE, eval=FALSE, cache=FALSE}
library(pcadapt)
path_to_file <- "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/02_pcadapt/eastern_polynesians.bed"
bed <-  read.pcadapt(path_to_file, type = "bed")
```

```{r, warning=FALSE, message=FALSE, eval=FALSE, cache=FALSE}
res <- pcadapt(bed, 
               K = 12,
               LD.clumping = list(size = 50, thr = 0.5) )
save(res,file = "/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/02_pcadapt/eastern_polynesians_pcadapt_12_pcs.RData")
```

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
load("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/02_pcadapt/eastern_polynesians_pcadapt_12_pcs.RData")
```

```{r, warning=FALSE, message=FALSE, eval=FALSE, cache=FALSE}
pdf("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/02_pcadapt/eastern_polynesians_diagnostics.pdf")
  plot(res,option="screeplot")
  plot(res, option = "qqplot")
  hist(res$pvalues, xlab = "p-values", main = NULL, breaks = 50, col = "orange")
  plot(res, option = "stat.distribution")
dev.off()

png("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/02_pcadapt/eastern_polynesians_diagnostics_loadings_0104.png")
par(mfrow = c(2, 2))
for (i in 1:4)
  plot(res$loadings[, i], pch = 19, cex = .3, ylab = paste0("Loadings PC", i))
dev.off()


png("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/02_pcadapt/eastern_polynesians_diagnostics_loadings_0508.png")
par(mfrow = c(2, 2))
for (i in 5:8)
  plot(res$loadings[, i], pch = 19, cex = .3, ylab = paste0("Loadings PC", i))
dev.off()


png("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/02_pcadapt/eastern_polynesians_diagnostics_loadings_0912.png")
par(mfrow = c(2, 2))
for (i in 9:12)
  plot(res$loadings[, i], pch = 19, cex = .3, ylab = paste0("Loadings PC", i))
dev.off()
```

We need to load the information on position and chromosome from the bim file.

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
bim <- read_delim("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/02_pcadapt/eastern_polynesians.bim", 
                  col_names = c("chromosome","snpid","cM",'position','a1','a2')) %>%
  select(chromosome,position,a1,a2)
```

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
selscan <- bim %>%
  mutate(p_val=res$pvalues) %>%
  mutate(chisq=res$chi2.stat) %>%
  drop_na() %>%
  mutate(p_val_adj=p.adjust(p_val,"fdr")) %>%
  mutate(chromosome=factor(chromosome,levels=1:22)) %>%
  arrange(chromosome,position) %>%
  ungroup() %>%
  rownames_to_column(var = "relpos") %>%
        mutate(relpos=as.numeric(relpos))
```

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
p <- selscan %>%
  ggplot(aes(x=p_val)) +
  geom_histogram(fill="white", color="black",bins = 500) +
  xlab("p-value")

png("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/00_SCRIPTS/01_working_notebook_repository/2023-03-23_pcadapt_selection_scan/p_value_histogram.png")
p
dev.off()
```

```{r}

p <- selscan %>%
      ggplot(., mapping = aes(sample = chisq)) +
        stat_qq_band(distribution="chisq",dparams=list("df" = 12)) +
        stat_qq_line(distribution="chisq",dparams=list("df" = 12)) +
        stat_qq_point(distribution="chisq",dparams=list("df" = 12))+
        xlab("Theoretical Quantiles") +
        ylab("Sample Quantiles") +
        ggtitle(bquote("QQ plot "~chi^2~ ~(.(12)))) 

png("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/00_SCRIPTS/01_working_notebook_repository/2023-03-23_pcadapt_selection_scan/qqplot.png")
p
dev.off()
```

### Genetic structure

Plot the PCA computed as the neutral expectation:

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
fam <- read_delim("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/03_selection_scans/02_pcadapt/eastern_polynesians.fam", col_names = c("identifier","individual")) %>%
  select(identifier,individual)
eigenvalues <- tibble(eigenvalue=res$singular.values) %>%
  mutate(pVar=eigenvalue/sum(eigenvalue))


pcs <- res$scores
colnames(pcs) <- paste0("PC",1:12)
pcs <- as_tibble(pcs) %>%
  mutate(identifier=fam$identifier, individual=fam$individual) %>%
  left_join(pal, by="identifier") %>%
  mutate(group3=factor(group3,levels=unique(pal$group3))) %>%
  mutate(identifier=factor(identifier,levels=pal$identifier)) 
```

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
shapes <- pcs %>%
  select(group3,shape) %>%
  distinct()
```

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
p12 <-  ggplot(pcs)+
  geom_point(aes(x=PC1,y=PC2,fill=identifier,shape=group3,label=individual),
             stroke = 0.1,
             size=2) +
  xlab(paste0('PC1 (', signif(eigenvalues$pVar[1],3),'%)')) +
  ylab(paste0('PC2 (', signif(eigenvalues$pVar[2],3),'%)')) + 
  coord_fixed() +
  theme(legend.position ="NONE") +
  #scale_fill_manual(values = alpha(pal$color,0.7)) +
  scale_color_manual(values = alpha("black",0.5)) +
  scale_shape_manual(values = shapes$shape)
ggplotly(p12)
```

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
p34 <-  ggplot(pcs)+
  geom_point(aes(x=PC3,y=PC4,fill=identifier,shape=group3,label=individual),
             stroke = 0.1,
             size=2) +
  xlab(paste0('PC3 (', signif(eigenvalues$pVar[3],3),'%)')) +
  ylab(paste0('PC4 (', signif(eigenvalues$pVar[4],3),'%)')) + 
  coord_fixed() +
  theme(legend.position ="NONE") +
  #scale_fill_manual(values = alpha(pal$color,0.7)) +
  scale_color_manual(values = alpha("black",0.5)) +
  scale_shape_manual(values = shapes$shape)
ggplotly(p34)
```

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
p56 <-  ggplot(pcs)+
  geom_point(aes(x=PC5,y=PC6,fill=identifier,shape=group3,label=individual),
             stroke = 0.1,
             size=2) +
  xlab(paste0('PC5 (', signif(eigenvalues$pVar[5],3),'%)')) +
  ylab(paste0('PC6 (', signif(eigenvalues$pVar[6],3),'%)')) + 
  coord_fixed() +
  theme(legend.position ="NONE") +
  #scale_fill_manual(values = alpha(pal$color,0.7)) +
  scale_color_manual(values = alpha("black",0.5)) +
  scale_shape_manual(values = shapes$shape)
ggplotly(p56)
```

Load admixture components to see if any of the PCs is driven by recent European or East Asian admixture (use the best K=12).

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
fam_admixture <- read_delim("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/HC_MAF_PRUNED_THINNED.fam", col_names = c("identifier","individual"))
Q <- read_table("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/01_HIGH_COVERAGE/04_allele_frequency_structure/admixture/all_samples/K_12_R_1682489/HC_MAF_PRUNED_THINNED.12.Q",
                col_names =  c(paste0("c",1:12))) %>%
  mutate(identifier=fam_admixture$identifier, individual=fam_admixture$individual) %>%
  mutate(identifier=factor(identifier, levels=pal$identifier)) %>%
  arrange(identifier)

Q$individual <- factor(Q$individual, levels=Q$individual)
  
Q <- Q %>%
  pivot_longer(cols = starts_with("c"),names_to = "cluster", values_to = "proportion")
```

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
p <- Q %>%
  ggplot() +
    geom_bar(aes(x=individual,y=proportion, fill=cluster,label=identifier),stat="identity") +
    theme(axis.text.x = element_text(angle = 45, vjust = 0.5, hjust=1, size=6)) +
    scale_fill_manual(values = c('#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a','#ffff99','#b15928'))
ggplotly(p)
```

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
clusters <- paste0("c",1:12)
clusters_with_assignments <- c("Chopccas",
                              "Surui",
                              "Polynesian",
                              "Polynesian_Outlier",
                              "East_Asian",
                              "European",
                              "ni-Vanuatu",
                              "Papuan",
                              "Pima",
                              "Solomon_Islander",
                              "South_East_Asian",
                              "African")
cluster_tibble <- tibble(cluster=clusters,
                         assignment=clusters_with_assignments)
```

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}

Q %>% 
  left_join(cluster_tibble) %>%
  select(individual,proportion, assignment) %>%
  pivot_wider(names_from = assignment, values_from = proportion  ) %>%
  right_join(pcs,by = "individual") %>%
  
ggplot()+
  geom_point(aes(x=PC1,y=PC2,fill=European,label2=identifier,shape=group3,label=individual),
             stroke = 0.1,
             size=2) +
  xlab(paste0('PC1 (', signif(eigenvalues$pVar[1],3),'%)')) +
  ylab(paste0('PC2 (', signif(eigenvalues$pVar[2],3),'%)')) + 
  coord_fixed() +
  #theme(legend.position ="NONE") +
  #scale_fill_manual(values = alpha(pal$color,0.7)) +
  scale_fill_viridis_c() +
  scale_color_manual(values = alpha("black",0.5)) +
  scale_shape_manual(values = shapes$shape)
```

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
Q %>% 
  left_join(cluster_tibble) %>%
  select(individual,proportion, assignment) %>%
  pivot_wider(names_from = assignment, values_from = proportion  ) %>%
  right_join(pcs,by = "individual") %>%
  
ggplot()+
  geom_point(aes(x=PC1,y=PC2,fill=East_Asian,label2=identifier,shape=group3,label=individual),
             stroke = 0.1,
             size=2) +
  xlab(paste0('PC1 (', signif(eigenvalues$pVar[1],3),'%)')) +
  ylab(paste0('PC2 (', signif(eigenvalues$pVar[2],3),'%)')) + 
  coord_fixed() +
  #theme(legend.position ="NONE") +
  #scale_fill_manual(values = alpha(pal$color,0.7)) +
  scale_fill_viridis_c() +
  scale_color_manual(values = alpha("black",0.5)) +
  scale_shape_manual(values = shapes$shape)
```

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
Q %>% 
  left_join(cluster_tibble) %>%
  select(individual,proportion, assignment) %>%
  pivot_wider(names_from = assignment, values_from = proportion  ) %>%
  right_join(pcs,by = "individual") %>%
  
ggplot()+
  geom_point(aes(x=PC1,y=PC2,fill=Polynesian,label2=identifier,shape=group3,label=individual),
             stroke = 0.1,
             size=2) +
  xlab(paste0('PC1 (', signif(eigenvalues$pVar[1],3),'%)')) +
  ylab(paste0('PC2 (', signif(eigenvalues$pVar[2],3),'%)')) + 
  coord_fixed() +
  #theme(legend.position ="NONE") +
  #scale_fill_manual(values = alpha(pal$color,0.7)) +
  scale_fill_viridis_c() +
  scale_color_manual(values = alpha("black",0.5)) +
  scale_shape_manual(values = shapes$shape)

```

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
Q %>% 
  left_join(cluster_tibble) %>%
  select(individual,proportion, assignment) %>%
  pivot_wider(names_from = assignment, values_from = proportion  ) %>%
  right_join(pcs,by = "individual") %>%
  
ggplot()+
  geom_point(aes(x=PC3,y=PC4,fill=Polynesian,label2=identifier,shape=group3,label=individual),
             stroke = 0.1,
             size=2) +
  xlab(paste0('PC3 (', signif(eigenvalues$pVar[3],3),'%)')) +
  ylab(paste0('PC4 (', signif(eigenvalues$pVar[4],3),'%)')) + 
  coord_fixed() +
  #theme(legend.position ="NONE") +
  #scale_fill_manual(values = alpha(pal$color,0.7)) +
  scale_fill_viridis_c() +
  scale_color_manual(values = alpha("black",0.5)) +
  scale_shape_manual(values = shapes$shape)
```

### Analyze loadings

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
loadings <- res$loadings %>% 
              as_tibble() %>%
              mutate(chromosome=bim$chromosome, position=bim$position) %>%
              drop_na() %>%
              pivot_longer(cols = 1:12,names_to="PC", values_to="loading" ) %>%
              mutate(PC=as.integer(str_replace(PC,pattern = "V",replacement = "")))
```

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
loadings %>%
  filter(PC==1) %>%
  ggplot(aes(x=position,y=loading)) +
    geom_point() +
    facet_wrap(~chromosome, ncol = 4, scales = "free")
```

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
loadings %>%
  filter(PC==2) %>%
  ggplot(aes(x=position,y=loading)) +
    geom_point() +
    facet_wrap(~chromosome, ncol = 4, scales = "free")
```

### Analyze hits

```{r, warning=FALSE, message=FALSE, eval=TRUE, cache=FALSE}
manhattan <- selscan %>%
    ggplot(aes(x=relpos,y=-log10(p_val_adj), color=chromosome)) +
      geom_point(alpha=0.3) +
      scale_color_manual(values = rep(c("#035E7B","#A2A77F"),11)) +
        theme(axis.text.x=element_blank(),
                axis.ticks.x=element_blank(),
               legend.position ="NONE"  ) +
        xlab("Position") + ylab("-log10(p-value)")

png("/pasteur/zeus/projets/p02/MATAEA/01_WGS_POPGEN/00_SCRIPTS/01_working_notebook_repository/2023-03-23_pcadapt_selection_scan/manhattan.png", width= 2000, height=1000)
manhattan
dev.off()

```
