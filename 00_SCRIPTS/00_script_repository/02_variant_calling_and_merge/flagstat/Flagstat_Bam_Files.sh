#!/bin/bash
#SBATCH -J Flagstat
#SBATCH --mem=5000
#SBATCH -c 1


#*******************************************************************
#*************************** Load Module ***************************
#*******************************************************************
module load samtools/1.10


#*******************************************************************
#****************************** Paths ******************************
#*******************************************************************
#--- Input Directory -------------
indir=$1
indir=/pasteur/appa/homes/grijodel/MATAEA/WGS/2_Processing/2206KNO-0076_HighCoverage/2_Results/3_Alignment_of_Fastq_Files/2_Alignment
#--- Metadata --------------------
metadata=${indir}/Metadata_Post_Alignment.txt
#--- Output Directory ------------
outdir=$2
#outdir=/pasteur/appa/homes/grijodel/MATAEA/WGS/2_Processing/2206KNO-0076_HighCoverage/2_Results/5_Supplementary_Data___Human_DNA_Capture/Additional_Metrics_on_Bam_Files/Results


#*******************************************************************
#*************************** Array Task ****************************
#*******************************************************************
#=================================
#=========== Iterator ============
#=================================
iterator=$((${SLURM_ARRAY_TASK_ID}+1))
#=================================
#============ Dataset ============
#=================================
#--- SampleID --------------------
sample=$(sed -n ${iterator}p ${metadata} | cut -f2)
echo "Sample:" $sample
#--- Bam File ---------------
bam=$(sed -n ${iterator}p ${metadata} | cut -f3)
echo "Bam:" $bam
#=================================
#============ Flagstat ===========
#=================================
samtools flagstat ${indir}/${bam} > ${outdir}/${sample}_Flagstat_Final_Bam.txt
