#!/usr/bin/env python
# coding: utf-8
import argparse
# Options for script
parser = argparse.ArgumentParser( description = "Process FLARE VCF with posterior probabilities, mask all loci not assigned to a given ancestry.")
parser.add_argument("input_vcf", type=str, help="VCF file (FLARE OUTPUT)")
parser.add_argument("output_vcf", type=str, help="Output VCF with masked calls")
parser.add_argument("target_ancestry", type=str, help="Ancestry to keep in output")
parser.add_argument("mode", type=str, help="Mode of masking: naive or threshold")
parser.add_argument("confidence_threshold",type=float, help="Posterior probability threshold for assigning a given ancestry. In the range [0,1].")
args = parser.parse_args()
#------
vcf=VCF(args.input_vcf)
output_vcf=args.output_vcf
confidence_threshold=args.confidence_threshold
target_ancestry=args.target_ancestry


import cyvcf2
import numpy as np
from multiprocessing.pool import Pool
import pandas as pd
import os
import sys

# In[2]:


def field_to_dict(field_string):
    """
    Convert a string of the form <VALUE1=KEY1, ... , VALUEN=KEYN> to a dictionary.
    """
    d={}
    string_list=field_string.replace("<","").replace(">","").split(",")
    for i in string_list:
        value,key=i.split("=")
        d[int(key)]=value
    return(d)   


# In[3]:


def ancestry_header_to_dict(vcf):
    """
    Convert the ancestry line to a dictionary.
    """
    for i in vcf.raw_header.split("\n"):
        if i.startswith("##ANCESTRY"):
            field=i.split("##ANCESTRY=")[1]
            ancestry_dict=field_to_dict(field)
            break
    return(ancestry_dict)


# In[66]:


def get_ancestry_code(vcf, target_ancestry):
    ancestries = ancestry_header_to_dict(vcf)
    target_ancestry_number_code=""
    for i in ancestries:
        if target_ancestry == ancestries[i]:
            target_ancestry_number_code=i
            break
        else:
            pass
    assert target_ancestry_number_code, f"ERROR: Ancestry '{target_ancestry}' not found"
    return(target_ancestry_number_code)


# In[99]:


def naive_mask(input_vcf,output_vcf,target_ancestry):
    """
    Mask a VCF's variants that are not assigned to a specific ancestry group. This function does not use posterior probabilities, but the 'AN' field.
    """
    # Read vcf
    vcf=cyvcf2.VCF(input_vcf)
    # Get ancestry code
    target_ancestry_number_code=get_ancestry_code(vcf,"TAIWANESE")
    # create a new vcf Writer using the input vcf as a template.
    w = cyvcf2.Writer(output_vcf, vcf)
    # Start looping over variants
    for variant in vcf:
        # loop over samples
        for sample in vcf.samples:
            index=vcf.samples.index(sample)
            #break
            # Left haplotype
            if variant.format("AN1")[index] == target_ancestry_number_code:
                pass
            else:
                variant.genotypes[index][0] = -1
            # Right haplotype
            if variant.format("AN2")[index] == target_ancestry_number_code:
                pass
            else:
                variant.genotypes[index][1] = -1
        variant.genotypes=variant.genotypes
        w.write_record(variant)    
    # Close handles
    w.close()
    vcf.close()


# In[97]:


def max_posterior_probability_mask(input_vcf,output_vcf,target_ancestry,confidence_threshold):
    """
    Hard-mask VCF variants that are not assigned to a specific ancestry with a posterior probability greater than <confidence_threshold>
    """
    # Read vcf
    vcf=cyvcf2.VCF(input_vcf)
    # Get ancestry code
    target_ancestry_number_code=get_ancestry_code(vcf,"TAIWANESE")
    # create a new vcf Writer using the input vcf as a template.
    w = cyvcf2.Writer(output_vcf, vcf)
    # Start looping over variants
    for variant in vcf:
        # loop over samples
        for sample in vcf.samples:
            index=vcf.samples.index(sample)
            #break
            # Left haplotype
            probability_array=variant.format("ANP1")[index]
            if np.argmax(probability_array) == target_ancestry_number_code and max(probability_array) > confidence_threshold:
                pass
            else:
                variant.genotypes[index][0] = -1
            # Right haplotype
            probability_array=variant.format("ANP2")[index]
            if np.argmax(probability_array) == target_ancestry_number_code and max(probability_array) > confidence_threshold:
                pass
            else:
                variant.genotypes[index][1] = -1    
        # Record modifications
        variant.genotypes=variant.genotypes
        w.write_record(variant)    
    # Close handles
    w.close()
    vcf.close()


confidence_threshold=0.90
target_ancestry="TAIWANESE"


# In[109]:
if __name__ == '__main__':
    # create and configure the process pool
    if args.mode == "naive":
        naive_mask(input_vcf,output_vcf, "TAIWANESE")
    elif args.mode == "threshold":
        max_posterior_probability_mask(input_vcf,output_vcf,"TAIWANESE",confidence_threshold)
    else:
        break



