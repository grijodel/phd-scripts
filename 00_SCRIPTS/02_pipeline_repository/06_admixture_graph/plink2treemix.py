#!/usr/bin/env python3
import argparse
parser = argparse.ArgumentParser( description = 'Convert plink frq.strat file to tremix format')
parser.add_argument("input", type=str, help="frq.strat input file")
parser.add_argument("output", type=str, help="treemix output file")

args = parser.parse_args()

input_file=args.input
output_file=args.output
snp_dict={}
populations=[]

with open(input_file, 'r') as h:
    # Read populations first
    next(h)
    for line in h:
        chromosome,snp,pop,A1,A2,MAF,MAC,NCHROBS=line.rstrip().split() 
        if pop not in populations:
            populations.append(pop)
        else:
            break
    last_pop=populations[-1]

with open(input_file, 'r') as h:
    next(h)
    # Read snps now and write
    with open(output_file,'w' ) as handle:
        handle.write( " ".join(populations) + "\n" )
        for line in h:

            chromosome,snp,pop,A1,A2,MAF,MAC,NCHROBS=line.rstrip().split()
            MAC=int(MAC)
            MAYOR_AC=int(NCHROBS)-MAC
            MAC=str(MAC)
            MAYOR_AC=str(MAYOR_AC)
            
            if pop == last_pop:
                handle.write(MAC+","+MAYOR_AC+"\n")
            else:
                handle.write(MAC+","+MAYOR_AC+" ")



