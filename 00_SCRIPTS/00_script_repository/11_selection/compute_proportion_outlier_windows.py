#!/usr/bin/env python3
import argparse
import pandas as pd
import numpy as np
import time
import sys

parser = argparse.ArgumentParser( description = 'Compute proportion of outlier windows from Fcs dataframe.')
parser.add_argument("input_table", type=str, help="Input dataframe")
parser.add_argument("window_size", type=str, help="Window size")
parser.add_argument("output_table", type=str, help="Output dataframe")

args = parser.parse_args()


def fcs_window(fcs, window_size=100):
    f = fcs.reset_index(drop=True)
    p_holder = np.empty(len(f))
    for index, row in f.iterrows():
        lower_bound = int(max( index - window_size/2, 0 ))
        upper_bound = int(min( index + window_size/2, len(f) ))
        window = f["outlier"][lower_bound:upper_bound]
        p = sum(window)/len(window)
        p_holder[index]=p
    f["p"] = p_holder
    return(f)
  
def fcs_window_filter_chromosome(fcs, chromosome, window_size=100):
  return( fcs_window( fcs[fcs["chromosome"]==chromosome], window_size ) )

def fcs_sequential(fcs,window_size=100):
    chromosomes = list(range(1,23))
    f = pd.DataFrame()
    for chromosome in chromosomes:
        tmp=fcs_window_filter_chromosome(fcs,chromosome, window_size)
        f = pd.concat([f,tmp])
    return(f)
  
  
f = pd.read_table(args.input_table)
start_time = time.time()
windows = fcs_sequential(f, window_size=int(args.window_size))
print("--- %s seconds ---" % (time.time() - start_time))


windows.to_csv(path_or_buf=args.output_table, sep='\t',index=False)
