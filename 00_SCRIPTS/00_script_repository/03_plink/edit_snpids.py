#!/usr/bin/env python3
import argparse
parser = argparse.ArgumentParser( description = 'Update .bim file SNP ids in the form chr:pos.')
parser.add_argument("bim", type=str, help=".bim file to update")
parser.add_argument("outfile", type=str, help="output .bim")
args = parser.parse_args()
bim = args.bim
outfile = args.outfile
with open(outfile, 'w') as outf:
    with open(bim, 'r') as f:
        for line in f:
            chromosome, snpid, cM, pos, a1, a2=line.rstrip().split()
            snpid = chromosome + ":" + pos
            newline = "{0} {1} {2} {3} {4} {5}\n".format( chromosome, snpid, cM, pos, a1, a2 )
            outf.write(newline)
