#!/usr/bin/env python3
import argparse
parser = argparse.ArgumentParser( description = 'Identify and label contiguous outlier regions based on the "p" statistics and find the highest scoring "fcs" position within each outlier region in the given dataframe.')
parser.add_argument("input_df_path", type=str, help="Input dataframe")
parser.add_argument("output_df_path", type=str, help="Output dataframe")
args = parser.parse_args()
import pandas as pd
import numpy as np

def identify_outlier_regions_with_max_fcs(dataframe):
    """
    Identify and label contiguous outlier regions based on the 'p' statistics and
    find the highest scoring 'fcs' position within each outlier region in the given dataframe.

    Args:
    dataframe (pd.DataFrame): A pandas dataframe containing at least the columns:
                               'chromosome', 'position', 'fcs', 'p'.

    Returns:
    pd.DataFrame: A modified dataframe with two additional columns:
                  - 'is_outlier_p' indicates whether the 'p' value is an outlier.
                  - 'outlier_region' labels contiguous outlier regions with unique identifiers.
                  - 'max_fcs_in_region' marks the position with the highest 'fcs' within each region.
    """
    # Calculate the 99.5th percentile of the 'p' column
    #threshold = dataframe['p'].quantile(0.995)

    # Create a new column for outlier detection
    #dataframe['is_outlier_p'] = dataframe['p'] > threshold

    # Identifying changes for new grouping
    dataframe['group'] = ((dataframe['is_outlier_p'] != dataframe['is_outlier_p'].shift()) | 
                          (dataframe['chromosome'] != dataframe['chromosome'].shift(1))).cumsum()

    # Label groups where 'is_outlier_p' is True
    dataframe['outlier_region'] = None  # Initialize the column
    outlier_groups = dataframe['group'][dataframe['is_outlier_p']].unique()
    dataframe.loc[dataframe['group'].isin(outlier_groups), 'outlier_region'] = dataframe['group']

    # Group by identified regions and find the highest 'fcs'
    def get_max_fcs_position(group):
        if len(group) == 1:
            return group['position'].iloc[0]
        else:
            max_fcs = group['fcs'].max()
            best_positions = group[group['fcs'] == max_fcs]
            return best_positions['position'].sample(n=1).iloc[0]  # Tie breaking randomly

    dataframe['max_fcs_in_region'] = None
    for name, group in dataframe.groupby('outlier_region'):
        if name is not None:  # Non-outlier groups have None as name
            max_position = get_max_fcs_position(group)
            dataframe.loc[dataframe['outlier_region'] == name, 'max_fcs_in_region'] = max_position

    # Cleanup temp columns
    dataframe.drop(columns=['group'], inplace=True)

    return dataframe


# Example usage of the new function name and column names
if __name__ == "__main__":
    input_df = pd.read_csv(args.input_df_path, delimiter = '\t')
    # Process the dataframe through the function
    result_df = identify_outlier_regions_with_max_fcs(input_df)
    result_df.to_csv(args.output_df_path,header=True,index=False, sep = "\t")


