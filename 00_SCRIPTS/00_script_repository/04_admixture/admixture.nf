nextflow.enable.dsl=2                                                           
process ADMIXTURE {

	publishDir "${params.outdir}/K_${K}_R_${seed}", mode: 'move'
	input:
		tuple val(K), val(seed), val(prefix), file(genotype)
	output:
		tuple val(K), val(seed), file('*.Q'), file('*.P'), file('*.log')
	"""
	admixture1.3 --cv ${prefix}.bed $K \
			-j${task.cpus} \
			--seed $seed |
			tee K${K}_S${seed}.log 
	"""
}

workflow {
    K = Channel.from(params.minK..params.maxK)
    seed = Channel.from( 1..10000000 ).randomSample( params.Nruns, 234 )
    genotype = Channel.fromFilePairs(params.prefix + '{.bed,.bim,.fam}', size:-1)
    parameters = K.combine(seed).combine(genotype)

    ADMIXTURE(parameters)
}
