#!/usr/bin/env python3
import argparse
parser = argparse.ArgumentParser( description = 'Update .fam file using an IND2POP reference file.')
parser.add_argument("fam", type=str, help=".fam file to update")
parser.add_argument("ind2pop", type=str, help="file containing mapping of individuals to columns")
parser.add_argument("outfile", type=str, help="output .fam")

args = parser.parse_args()


fam = args.fam
ind2pop = args.ind2pop
outfile = args.outfile

reference = {}
with open(ind2pop, 'r') as f:
    for line in f:
        individual, population = line.rstrip().split()
        reference[individual]=population

with open(outfile, 'w' ) as outf:
    with open(fam, "r" ) as f:
        for line in f:
            fid, iid, father, mother, sex, phenotype = line.rstrip().split()
            try:
                fid = reference[iid]
            except KeyError:
                pass

            newline = "{0} {1} {2} {3} {4} {5}\n".format(fid,iid,father,mother,sex,phenotype)
            outf.write(newline)





