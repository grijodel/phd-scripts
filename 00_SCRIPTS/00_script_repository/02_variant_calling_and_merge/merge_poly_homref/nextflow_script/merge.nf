nextflow.enable.dsl=2       
////////////////////// WORKFLOW ///////////////////////
                                                    
workflow {           
    /// SETTINGS /// 
    chromosomes = Channel.from(1..22)                    
    // Set the GQ cutoff                                       
    gq_cutoff = Channel.from(params.genotype_quality_cutoff)
    // Get path to polymorphic vcfs with index file
    polymorphic_vcf =  Channel.fromFilePairs( params.prefix_poly +'{.vcf.gz,.vcf.gz.tbi}',
                                     size: 2 ) 
    // Get prefix to homref vcf files directory 
    homref_vcf_dir = Channel.fromPath( params.prefix_homref )
    // Get path to table relating individuals to populations
    ind2pop = Channel.fromPath(params.ind2pop, type:'file', checkIfExists:true)
    // Get path to reference genome to normalize loci
    reference_genome = Channel.fromPath(params.reference_genome, type:'file', checkIfExists: true )
    reference_genome_index = Channel.fromPath(params.reference_genome_index, type:'file', checkIfExists: true)
    /// RUN WORKFLOW ///                 
    HARD_FILTER(polymorphic_vcf, gq_cutoff)
    // Run HW equilibrium test
    HARDY_WEINBERG(HARD_FILTER.out,ind2pop)
    // Combine them into a single channel
    homref_combined = reference_genome
                        .combine(reference_genome_index)
                        .combine(homref_vcf_dir)
                        .combine( HARDY_WEINBERG.out ).combine(chromosomes).combine(gq_cutoff)
    
    MERGE(homref_combined )
}    
////////////// MODULES ///////////////////
process HARD_FILTER {
    module "tabix/0.2.6:vcftools/0.1.16"
    scratch true
    input:    
        tuple val(prefix), file(genotype)
        val(gq_cutoff)
    output:
        tuple val(prefix), file("${prefix}_GQ.vcf.gz"), file("${prefix}_GQ.vcf.gz.tbi")

    script:
    """
    vcftools --gzvcf ${prefix}.vcf.gz \
        --stdout \
        --minGQ ${gq_cutoff} \
        --recode \
        --max-missing 0.99 \
        --recode-INFO-all |\
        bgzip -c > ${prefix}_GQ.vcf.gz
    tabix -p vcf ${prefix}_GQ.vcf.gz    
    """    
    stub:
    """
    touch ${prefix}_GQ.vcf.gz
    touch ${prefix}_GQ.vcf.gz.tbi
    """

}

process HARDY_WEINBERG {
    module "plink/1.90b6.16:tabix/0.2.6:vcftools/0.1.16"
    cpus 20
    scratch true
    input: 
        tuple val(prefix), file(vcf), file(index)
        file(poptable)
    output:
        tuple val(prefix), file("${prefix}_HWE.vcf.gz"), file("${prefix}_HWE.vcf.gz.tbi")

    script:
    """
    plink --vcf ${prefix}_GQ.vcf.gz \
            --make-bed \
            --out ${prefix}_GQ

    edit_fids.py ${prefix}_GQ.fam $poptable tmp
    mv tmp ${prefix}_GQ.fam
    edit_snpids.py ${prefix}_GQ.bim tmp
    mv tmp ${prefix}_GQ.bim

    hw_per_population.py ${prefix}_GQ 1e-6 snps_hw_dis.txt 20
    cat ${prefix}_GQ.bim | grep -f snps_hw_dis.txt | awk '{print \$1,\$4}' > pos_hw_dis.txt

    vcftools --gzvcf ${prefix}_GQ.vcf.gz \
                --stdout \
                --recode \
                --exclude-positions pos_hw_dis.txt | \
            bgzip -c > ${prefix}_HWE.vcf.gz
    tabix -p vcf ${prefix}_HWE.vcf.gz 
    """
    stub:
    """
    touch ${prefix}_HWE.vcf.gz
    touch ${prefix}_HWE.vcf.gz.tbi
    """
}


process MERGE {
    publishDir "${params.outdir}", mode: 'move'
    module "tabix/0.2.6:samtools/1.13:vcftools/0.1.16"                                       
    cpus 11 
    scratch true

    input:
        tuple file(reference_genome),file(reference_genome_index),val(homref_prefix), val(prefix_poly),file(vcf_poly),file(index_poly), val(chromosome), val(gq_cutoff)

    output:
        tuple val(chromosome), file("MERGED_CLEAN_chr${chromosome}.vcf.gz"), file("MERGED_CLEAN_chr${chromosome}.vcf.gz.tbi")

    script:
    """
    #1. Filter with GQ
    #2. Normalize homref 
    #3. Concatenate
    #4. Remove extra fields
   

    vcftools --gzvcf ${homref_prefix}${chromosome}.vcf.gz \
        --stdout \
        --minGQ ${gq_cutoff} \
        --recode \
        --max-missing 0.99 \
        --recode-INFO-all | \
        bgzip -c > HOMREF_GQ_${chromosome}.vcf.gz
    tabix -p vcf HOMREF_GQ_${chromosome}.vcf.gz
 
    bcftools view -f .,PASS HOMREF_GQ_${chromosome}.vcf.gz -O u | \
    bcftools norm \
             -f $reference_genome \
             --threads ${task.cpus} \
             -O u | \
    bcftools concat             -r ${chromosome} \
                                -a \
                                --threads ${task.cpus} \
                                ${vcf_poly} \
                                HOMREF_GQ_${chromosome}.vcf.gz \
                                        -O u |   \
    bcftools annotate -x ^FORMAT/GT,FORMAT/DP,FORMAT/GQ,FORMAT/PL,^INFO/AC,INFO/AF,INFO/AN \
            --threads ${task.cpus} \
            -O z \
            -o MERGED_CLEAN_chr${chromosome}.vcf.gz 

    tabix -p vcf MERGED_CLEAN_chr${chromosome}.vcf.gz
    """

    stub:
    """
    touch MERGED_CLEAN_chr${chromosome}.vcf.gz
    touch MERGED_CLEAN_chr${chromosome}.vcf.gz.tbi
    """

}

