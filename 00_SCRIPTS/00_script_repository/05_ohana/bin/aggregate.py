#!/usr/bin/env python3
import argparse
import glob
import os
from datetime import datetime
parser = argparse.ArgumentParser( description = 'Aggegate Ohana selscan results' )
parser.add_argument("directory", type=str, help="Directory containing {prefix}_{block}_{K}.selscan files")
parser.add_argument('bim_file', type=str, help="bim file used to run the selection scan")
parser.add_argument('--output_prefix', type=str, help="Output prefix", default="selection_scan")

args = parser.parse_args()
directory=args.directory
bim_file=args.bim_file
output_prefix=args.output_prefix



def pdate(message):
    current_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print(f"[{current_date}] {message}")
# Read file paths
files=glob.glob( directory + "/*.selscan")
# Make file list for ordering
files_dict = {}


blocks = []
ks = []
for i in files:
    basename=i.split('/')[-1].split('.')[0]
    block_number = int( basename.split("_")[-2] )
    blocks.append(block_number)
    k = int(basename.split("_")[-1])
    ks.append( k )
    files_dict[(block_number,k)] = i
# Get ordered keys
blocks.sort()
ks.sort()
counter=0


# Join all files 

for k in ks:
    pdate("Processing {0}/{1} Ks".format(str(k),str(len(ks))))
    with open( str(k) +  '.tmp' , 'w' ) as tmpfile:
        for block in blocks:
            pdate("Processing block {0}/{1} of k {2}".format(str(block),str(len(blocks)),str(k) ))
            sel_file=files_dict[(block,k)]
            counter+=1
            with open(sel_file,'r') as selfile:
                if counter == 1:
                    pass
                else:
                    next(selfile)
                for line in selfile:
                    linelist = line.split()
                    newline = '\t'.join(linelist) + '\n'
                    tmpfile.write(newline)
            counter=0



# Write final file
pdate("Writing final files...")
for k in ks:
    with open(str(k)+".tmp",'r') as selscan, open(bim_file,'r') as bim:
        header = selscan.readline()
        new_header = "chromosome\tposition\t" + header
        with open(output_prefix + "_" + str(k) + ".selscan", 'w') as outfile:
            outfile.write(new_header)
            for x,y in zip(bim, selscan):
                chromosome=x.split()[0]
                position=x.split()[3]
                newline = chromosome + "\t" + position + "\t" + y.strip() + '\n' 
                outfile.write(newline)

# Remove temporary files
for f in glob.glob("*.tmp"):
    os.remove(f)


