#!/usr/bin/env python3
import argparse
import pandas as pd
parser = argparse.ArgumentParser( description = 'Modify population identifiers (FID) according to a table.')
parser.add_argument("input_fam", type=str, help=".fam input file")
parser.add_argument("table", type=str, help="Excel file containing population mappings")
parser.add_argument("output_fam", type=str, help=".fam output file")
parser.add_argument("column_name", type=str, help="Column name to use for new grouping.")
parser.add_argument("sheet_number",type=int,help="Sheet number in excel file")

parser.add_argument("--population_file", type=str,default='NONE',help="File specifying populations to keep individual tags instead of assigning a population FID. For individual-based f values. Population names must coincide with colum_name option." )



args=parser.parse_args()
input_fam=args.input_fam
table=args.table
column_name=args.column_name
sheet_number=args.sheet_number-1
output_fam=args.output_fam

population_file=args.population_file
individual_based_populations=[]
if population_file != "NONE":
    with open(population_file,'r') as h:
        for line in h:
            p=line.rstrip().split()[0]
            individual_based_populations.append(p)


# Get populations from table

groupings = pd.read_excel(table,sheet_number)
select_columns = ['identifier', column_name]
groupings = groupings[select_columns]
groupings=groupings.set_index('identifier').to_dict()
groupings=groupings[column_name]

# Read fam file and modify
with open(input_fam, 'r' ) as ifam:
    with open(output_fam, 'w' ) as ofam:
        for line in ifam:
            fid,iid,a,b,c,d = line.rstrip().split()
            try:
                if groupings[fid] in individual_based_populations:
                    new_fid=iid
                else:
                    new_fid = groupings[fid]
            except KeyError:
                new_fid=fid
            newline = '{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n'.format(new_fid,iid,a,b,c,d)
            ofam.write(newline)









